trigger ExperianDataQuality_Lead_BIBU on Lead (before insert, before update) {
    EDQ.DataQualityService.SetValidationStatus(Trigger.new, Trigger.old, Trigger.IsInsert, 2);
}