/**=====================================================================
 * Appirio, Inc
 * Name: LeadTrigger
 * Description: T-194935, On Lead conversion if billing address is not blank 
                then create address record and associate that address with 
                the converted account by creating the AccountAddress record
                and associate converted contact by creating the ContactAddress record.
 * Created Date: Nov 01st, 2013
 * Created By: Manish Soni ( Appirio )
 * 
 * Date Modified       Modified By                  Description of the update
 * Nov 19th, 2013      Pankaj Mehra(Appirio)        T-213204: Populate Maketing Activity contact lookup on conversion of lead
 * Jan 30th, 2014      Naresh Kr Ojha(Appirio)      T-232755: Homogenize triggers 
 * Jan 30th, 2014      Jinesh Goyal(Appirio)        T-232760: Homogenize the comments
 * Mar 04th, 2014      Arpita Bose (Appirio)        T-243282: Added Constants in place of String
 * July 21th,2015      Noopur                       Added before delete event 
 * Sept 1st, 2015      Venkat Akula (Appirio)       T-431221: Added method to call before insert and update
 * Aug 12th, 2016      Paul Kissick                 CRM2:W-005663: Added support for IsDataAdmin==true on conversions - Also optimised class code
 =====================================================================*/
trigger LeadTrigger on Lead (after update, before delete, before insert, before update) {
  //Ensure a data admin is not loading the data, so the triggers will not fire
  if (TriggerState.isActive(Constants.LEAD_TRIGGER)) {
    
    LeadTriggerHandler.isDataAdmin = IsDataAdmin__c.getInstance().IsDataAdmin__c;
    
    if (Trigger.isBefore && Trigger.isInsert) {
      LeadTriggerHandler.beforeInsert(Trigger.new);
    }
    if (Trigger.isBefore && Trigger.isUpdate) {
      LeadTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
    }
    if (Trigger.isAfter && Trigger.isUpdate) {
      LeadTriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
    }
  }
}