trigger ExperianDataQuality_Contract_BIBU on Contract (before insert, before update) {
    EDQ.DataQualityService.SetValidationStatus(Trigger.new, Trigger.old, Trigger.IsInsert, 2);
}