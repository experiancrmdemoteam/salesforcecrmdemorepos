trigger ExperianDataQuality_Contract_AIAU on Contract (after insert, after update) {
    EDQ.DataQualityService.ExecuteWebToObject(Trigger.New, 2, Trigger.IsUpdate);
}