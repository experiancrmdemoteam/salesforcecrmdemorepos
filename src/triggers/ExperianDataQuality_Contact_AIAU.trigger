trigger ExperianDataQuality_Contact_AIAU on Contact (after insert, after update) {
    EDQ.DataQualityService.ExecuteWebToObject(Trigger.New, 2, Trigger.IsUpdate);
}