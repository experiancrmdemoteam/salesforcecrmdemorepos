/**=====================================================================
 * Experian
 * Name: NominationTrigger
 * Description: Trigger for the Nomination__c object
 * Created Date: March 29th 2016
 * Created By: Richard Joseph
 
 * Date Modified        Modified By         Description of the update
 * Sep 13th, 2016       Paul Kissick        Added support for before update, and after delete
 * Oct 4th, 2016        Paul Kissick        Moved IsDataAdmin check into Handler
 * Nov 7th, 2016        Paul Kissick        W-006354: Added before delete
 =====================================================================*/
trigger NominationTrigger on Nomination__c  (before insert, after insert, before update, after update, before delete, after delete) {
  
  // Moved check into the handler, since some operations will always run
  NominationTriggerHandler.isDataAdmin = IsDataAdmin__c.getInstance().IsDataAdmin__c;
   
  if (Trigger.isBefore && Trigger.isInsert) {
    NominationTriggerHandler.beforeInsert(Trigger.new);        
  }
   
  if (Trigger.isAfter && Trigger.isInsert) {
    NominationTriggerHandler.afterInsert(Trigger.new);        
  }
   
  if (Trigger.isBefore && Trigger.isUpdate) {
    NominationTriggerHandler.beforeUpdate(Trigger.oldMap, Trigger.new);        
  }
   
  if (Trigger.isAfter && Trigger.isUpdate) {
    NominationTriggerHandler.afterUpdate(Trigger.oldMap, Trigger.new);        
  }
  
  if (Trigger.isBefore && Trigger.isDelete) {
    NominationTriggerHandler.beforeDelete(Trigger.oldMap);
  }
    
  if (Trigger.isAfter && Trigger.isDelete) {
    NominationTriggerHandler.afterDelete(Trigger.oldMap);
  }
  
  
}