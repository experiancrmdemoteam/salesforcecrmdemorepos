/*=============================================================================
 * Experian
 * Name: NominationPayrollReportCtrlr_Test
 * Description: Test class for NominationPayrollReportCtrlr
 * Created Date: 20 Sep 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 * 
 =============================================================================*/

@isTest
private class NominationPayrollReportCtrlr_Test {

  static String holdGuid = BatchHelper.newGuid();

  static testMethod void basicTests() {
    
    PageReference repPage = Page.NominationPayrollReportXLS;
    Test.setCurrentPage(repPage);
    repPage.getParameters().put('nogross', 'yes');
    repPage.getParameters().put('guid', '129084721047241');
    
    NominationPayrollReportCtrlr c = new NominationPayrollReportCtrlr();
    system.assert(c.xlsHeader != null);
    system.assert(c.csvLineBreak != null); 
    system.assertEquals(c.showNoGross, true);
    system.assert(c.reportFields.size() > 0);
    system.assert(c.grossAmountColumn != null);
    system.assert(String.isNotBlank(c.getUpdateUrl()));
    system.assert(c.getNomRecords().size() == 0);
    
  }
  
  static testMethod void dataTests() {
    
    List<Nomination__c> noms = [SELECT Payroll_Batch_GUID__c FROM Nomination__c WHERE Notify_Payroll__c = true];
    
    for (Nomination__c n : noms) {
      n.Payroll_Batch_GUID__c = holdGuid;
    }
    update noms;
    
    Test.startTest();
    
    PageReference repPage = Page.NominationPayrollReportXLS;
    Test.setCurrentPage(repPage);
    repPage.getParameters().put('nogross', 'no');
    repPage.getParameters().put('guid', holdGuid);
    repPage.getParameters().put('layout', NominationPayrollReportCtrlr.defaultLayout);
    
    NominationPayrollReportCtrlr c = new NominationPayrollReportCtrlr();
    system.assertEquals(c.showNoGross, false);
    system.assert(c.reportFields.size() > 0);
    system.assert(c.grossAmountColumn != null);
    system.assert(String.isNotBlank(c.getUpdateUrl()));
    system.assert(c.getNomRecords().size() == noms.size());
    
  }
  
  static testMethod void dataTests2() {
    
    List<Nomination__c> noms = [SELECT Payroll_Notified__c, Payroll_Notified_Date__c, Oracle_Payroll_Name__c FROM Nomination__c WHERE Notify_Payroll__c = true AND Payroll_Notified__c = false];
    
    String payrollName;
    
    for (Nomination__c n : noms) {
      n.Payroll_Notified__c = true;
      n.Payroll_Notified_Date__c = Datetime.now().addHours(-1);
      payrollName = n.Oracle_Payroll_Name__c;
    }
    update noms;
    
    Test.startTest();
    
    PageReference repPage = Page.NominationPayrollReportXLS;
    Test.setCurrentPage(repPage);
    repPage.getParameters().put('nogross', 'no');
    repPage.getParameters().put('layout', NominationPayrollReportCtrlr.defaultLayout);
    repPage.getParameters().put('payrollname', payrollName);
    repPage.getParameters().put('beforedate', JSON.serialize(Datetime.now()));
    
    NominationPayrollReportCtrlr c = new NominationPayrollReportCtrlr();
    system.assertEquals(c.showNoGross, false);
    system.assert(c.reportFields.size() > 0);
    system.assert(c.grossAmountColumn != null);
    system.assert(String.isBlank(c.getUpdateUrl()));
    system.assert(c.getNomRecords().size() == noms.size());
    
  }
  
  @testSetup
  private static void setupData() {
    
    NominationTestHelper_Test.createTestUsers();
    
    NominationTestHelper_Test.createRecognitionBadges();
    
    NominationTestHelper_Test.createNominations(NominationHelper.NomConstants.get('Approved'), NominationHelper.NomConstants.get('Level2SpotAward'), 2, false);
    
    NominationTestHelper_Test.createNominations(NominationHelper.NomConstants.get('Won'), NominationHelper.NomConstants.get('Level3Individual'), 2, false);
    
  }
  
}