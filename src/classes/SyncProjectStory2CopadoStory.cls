/*=============================================================================
 * Experian
 * Name: SyncProjectStory2CopadoStory
 * Description: Creates a Copado Project and set it on edit mode for extra Copado data to be completed
 * Created Date: 07/24/2017
 * Created By: Diego Olarte
 *
 * Date Modified      Modified By           Description of the update
 * 
 
 * 
 =============================================================================*/
global with sharing class SyncProjectStory2CopadoStory {
   
   webservice static String syncWorkItem(String proID) {

      String returnResult = '';
      
      Project__c originProject = [Select id,Name,Start_Date__c,Project_End_Date__c,Description__c,Owner.Id FROM Project__c WHERE id = :proID];
      
      copado__Project__c originCopadoProject = [Select id, SMC_Project__c FROM copado__Project__c WHERE SMC_Project__c = :proID ORDER BY CreatedDate DESC LIMIT 1];
      
      List<ID> cleanWorkList = new List<ID>();
      
      List<ID> listToRemove = new List<ID>();
      
      List<ID> workToSetUserStory = new List<ID>();
      
      for(copado__User_Story__c listCleaning : [SELECT id, Related_Work_Item__c, Related_Work_Item__r.id,copado__Project__c FROM copado__User_Story__c WHERE copado__Project__c = : originCopadoProject.id]){

          listToRemove.add(listCleaning.Related_Work_Item__r.id);
      }
      
      for(agf__ADM_Work__c originalWorkList:[Select id FROM agf__ADM_Work__c WHERE (Experian_Project__r.Id = :proID) AND (NOT(Id = :listToRemove))]){

          workToSetUserStory.add(originalWorkList.id);
      }
      
      //Save point
      Savepoint sp = Database.setSavepoint();
    
      List<copado__User_Story__c> userStoryList = new List<copado__User_Story__c>();
            
      try {
          for(agf__ADM_Work__c workList:[Select id,Name,agf__Subject__c,agf__Details__c,Owner.Id FROM agf__ADM_Work__c WHERE Id in :workToSetUserStory]){
              
              copado__User_Story__c newAutoCaseCUS = new copado__User_Story__c (
              
              Related_Work_Item__c = workList.id,
              copado__Developer__c = workList.OwnerId,
              copado__Project__c = originCopadoProject.Id,
              copado__User_Story_Title__c = workList.Name + ': ' + workList.agf__Subject__c,
              Description__c = workList.agf__Details__c);
              userStoryList.add(newAutoCaseCUS);
                        
          }
          
          insert userStoryList;
                    
              returnResult = 'Click OK to sync the Stories with the most resent Copado Project.';
      }
      
      catch (Exception e) {
      Database.rollback(sp);
      returnResult = 'Cannot sync user stories because of error: ' + e.getMessage();
      system.debug('[SyncProjectStory2CopadoStory: SyncProjectStory2CopadoStory] Exception: ' + e.getMessage());
      ApexLogHandler.createLogAndSave('SyncProjectStory2CopadoStory','SyncProjectStory2CopadoStory', e.getStackTraceString(), e);
    }
    return returnResult;
      
   }
}