/**=====================================================================
 * Experian
 * Name: BatchAssignmentTeamDeleted
 * Description: Batch to remove assignment team members from accounts if Account_Assignment__c deleted.
 *            Case 01662532
 *      This is called after the BatchAssignmentTeamV2 batch automatically.
 * Created Date: 8 Jan 2016
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Apr 15th, 2016               Paul Kissick                 Case 01912624: Improving to remove null references
 * Apr 15th, 2016               Paul Kissick                 Case 01912624: Adding support for deleted assignment teams
 * Apr 26th, 2016               Paul Kissick                 Case 01962593: Fixing aatOther if empty
 =====================================================================*/
public class BatchAssignmentTeamDeleted implements Database.Batchable<sObject>, Database.Stateful  {
  
  private DateTime lastRunTime;
  private Datetime startTime;
  
  private List<String> deleteErrors;
  private List<String> exceptionErrors;
  
  // Adding to allow custom runs to detect errors.
  private Datetime deletedFrom;
  private Datetime deletedTo;
  
  private Boolean customRun = false;
  
  public BatchAssignmentTeamDeleted() {
  }
  
  public BatchAssignmentTeamDeleted(Datetime dtFrom, Datetime dtTo) {
    deletedFrom = dtFrom;
    deletedTo = dtTo;
  }
  
  //=====================================================================================================
  // Start - Will query all Assignment_Team_Member__c that have been updated (check on _Updated__c field)
  //=====================================================================================================
  public Database.Querylocator start ( Database.Batchablecontext bc ) {
    lastRunTime = BatchHelper.getBatchClassTimestamp('AccountAssignmentTeamDeletedJobLastRun');
    if (lastRunTime == null) {
      lastRunTime = Datetime.now().addYears(-5);
    }
    
    startTime = system.now();
    
    deleteErrors = new List<String>();
    exceptionErrors = new List<String>();
    
    if (deletedFrom != null && deletedTo != null) {
      customRun = true;
      return Database.getQueryLocator([
        SELECT Id, Account__c, Assignment_Team__c, Assignment_Triggered__c, IsDeleted, SystemModstamp
        FROM Account_Assignment_Team__c 
        WHERE SystemModStamp >= :deletedFrom AND SystemModStamp <= :deletedTo
        AND IsDeleted = true
        AND Account__c != null 
        AND Assignment_Team__c != null
        ALL ROWS
      ]);
    }
    
    return Database.getQueryLocator([
      SELECT Id, Account__c, Assignment_Team__c, Assignment_Triggered__c, IsDeleted, SystemModstamp
      FROM Account_Assignment_Team__c 
      WHERE SystemModStamp >= :lastRunTime 
      AND IsDeleted = true
      AND Account__c != null 
      AND Assignment_Team__c != null
      ALL ROWS
    ]);
  }
  
  public void execute (Database.BatchableContext bc, List<Account_Assignment_Team__c> scope) {
    
    Savepoint sp = Database.setSavepoint();
    
    try {
    
      Map<Id,AccountTeamMember> accTeamMembersToDelete = new Map<Id,AccountTeamMember>();
      
      // Now we have a list of accounts to process, and the related assignment team.
      // Convert this assignment team into a list of members that have changed, so we can process the required changes on the account.
      Set<Id> assignmentTeamIdSet = new Set<Id>();
      Set<Id> accountIdSet = new Set<Id>();
      
      for (Account_Assignment_Team__c aat : scope) {
        if (aat.Assignment_Team__c != null) assignmentTeamIdSet.add(aat.Assignment_Team__c);
        if (aat.Account__c != null) accountIdSet.add(aat.Account__c);
      }
      
      Map<Id,Account> accountMap = new Map<Id,Account>([
        SELECT Id, Name,
          (SELECT Id, AccountId, UserId, TeamMemberRole, User.IsActive
           FROM AccountTeamMembers),
          (SELECT Assignment_Team__c
           FROM Account_Assignment_Teams__r)
        FROM Account
        WHERE Id IN :accountIdSet
      ]);
      
      Map<Id,Assignment_Team__c> assignTeamMap = new Map<Id,Assignment_Team__c>([
        SELECT Id, Name, IsDeleted, (
          SELECT Id, User__c, IsDeleted
          FROM Assignment_Team_Members__r 
        ) 
        FROM Assignment_Team__c 
        WHERE Id IN :assignmentTeamIdSet
        ALL ROWS
      ]);
      
      Map<Id,Assignment_Team__c> assignTeamToCheckMap = new Map<Id,Assignment_Team__c>([
        SELECT Id, Name,
         (SELECT User__c 
          FROM Assignment_Team_Members__r 
          WHERE IsActive__c = true) 
        FROM Assignment_Team__c 
        WHERE Id IN (
          SELECT Assignment_Team__c 
          FROM Account_Assignment_Team__c 
          WHERE Account__c IN :accountIdSet) 
      ]);
        
      
    
      for (Account_Assignment_Team__c aat : scope) {
        
        // here we have 2 ids, 1 for the account, 1 for the assignment team. Now we need to see what we have to update for each entry here...
        Id accountId = aat.Account__c;
        Id assignmentTeamId = aat.Assignment_Team__c;
        
        if (!accountMap.containsKey(accountId) && !assignTeamMap.containsKey(assignmentTeamId)) {
          // Break out of this loop since the account and assignment team are both missing
          continue;
        }
        
        Account acc = accountMap.get(accountId);
        Assignment_Team__c assTeam = assignTeamMap.get(assignmentTeamId);
        
        Set<Id> assTeamMembers = new Set<Id>();  // All Assignment Members in this team
        Set<Id> activeAssTeamMembers = new Set<Id>(); // For this account, holds other active team members (excluding the current ass team)
        
        if (assTeam.Assignment_Team_Members__r != null && !assTeam.Assignment_Team_Members__r.isEmpty()) {
          for (Assignment_Team_Member__c assTeamMember : assTeam.Assignment_Team_Members__r) {
            assTeamMembers.add(assTeamMember.User__c);
            system.debug('*** Found '+assTeamMember.User__c+' on '+assTeam.Id);
          }
        }
        
        if (acc.Account_Assignment_Teams__r != null && !acc.Account_Assignment_Teams__r.isEmpty()) {
          for (Account_Assignment_Team__c aatOther : acc.Account_Assignment_Teams__r) {
            // For all the current assignment teams on the account...
            Assignment_Team__c tmpAt = assignTeamToCheckMap.get(aatOther.Assignment_Team__c);
            // Current members...
            for (Assignment_Team_Member__c astm1 : tmpAt.Assignment_Team_Members__r) {
              activeAssTeamMembers.add(astm1.User__c);
              system.debug('*** Found Other User '+astm1.User__c+' on '+tmpAt.Id);
            }
          }
        }
        
        if (acc.AccountTeamMembers != null && !acc.AccountTeamMembers.isEmpty()) {
          for (AccountTeamMember atm : acc.AccountTeamMembers) {
            system.debug('User : '+atm.UserId+ ' is on '+acc.Name);
            if (assTeamMembers.contains(atm.UserId) && !activeAssTeamMembers.contains(atm.UserId)) {
              system.debug('User : '+atm.UserId+ ' is still on '+acc.Name+ ' for team '+assTeam.Name+' for the Acc AT: '+aat.Id + ' and AccID '+aat.Account__c+ ' - last mod: '+aat.SystemModStamp);
              accTeamMembersToDelete.put(atm.Id,atm);
            }
          }
        }
        
      }
      
      if (!accTeamMembersToDelete.isEmpty()) {
        // This part checks the user isn't a member of the account from another assignment team
        List<Database.DeleteResult> delRes = Database.delete(accTeamMembersToDelete.values(),false);
        for (Database.DeleteResult dr : delRes) {
          if (!dr.isSuccess()) deleteErrors.addAll(parseErrors(dr.getErrors()));
        }
      }
    }
    catch (Exception e) {
      system.debug(e);
      Database.rollback(sp);
      exceptionErrors.add(e.getMessage() +'\n' +e.getStackTraceString());
    }
  }
  
  public void finish (Database.BatchableContext bc) {
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchAssignmentTeamDeleted', false);
    
    String emailBody = '';
    
    if (!deleteErrors.isEmpty() || !exceptionErrors.isEmpty() || Test.isRunningTest()) {
      
      bh.batchHasErrors = true;
      
      if (!deleteErrors.isEmpty() || Test.isRunningTest()) {
        emailBody += '\nThe following errors were observed when deleting records:\n';
        emailBody += String.join(deleteErrors,'\n');
      }
      
      if (!exceptionErrors.isEmpty() || Test.isRunningTest()) {
        emailBody += '\nThe following errors were observed when processing records:\n';
        emailBody += String.join(exceptionErrors,'\n\n');
      }
      
      bh.emailBody += emailBody;
    }
    
    bh.sendEmail();
    if (!customRun) {
      BatchHelper.setBatchClassTimestamp('AccountAssignmentTeamDeletedJobLastRun', startTime);
    }
  }
  
  private static List<String> parseErrors(List<Database.Error> dbErrors) {
    List<String> errorsFound = new List<String>();
    for (Database.Error err : dbErrors) {
      errorsFound.add('Code: '+err.getStatusCode()+' - Message:'+err.getMessage()+' - Fields:'+String.join(err.getFields(),','));
    }
    return errorsFound;
  }
  
}