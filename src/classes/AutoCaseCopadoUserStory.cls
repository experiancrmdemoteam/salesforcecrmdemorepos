/*=============================================================================
 * Experian
 * Name: AutoCaseCopadoUserStory
 * Description: Creates a copado user story and set it on edit mode for extra Copado data to be completed
 * Created Date: 07/21/2017
 * Created By: Diego Olarte
 *
 * Date Modified      Modified By           Description of the update
 * 
 
 * 
 =============================================================================*/
global with sharing class AutoCaseCopadoUserStory {
   
   global static Id defaultCP = Global_Settings__c.getValues(Constants.GLOBAL_SETTING).Default_Copado_Project__c;
   
   webservice static String autoCaseCUS(String casID) {

      String returnResult = '';
      
      Case originCase = [Select id,Subject,Implementation_Status__c,Description,Owner.Id, CaseNumber FROM Case WHERE id = :casID];
                                 
      //Save point
      Savepoint sp = Database.setSavepoint();
    
      List<copado__User_Story__c> userStoryList = new List<copado__User_Story__c>();
            
      try {
      
          copado__User_Story__c newAutoCaseCUS = new copado__User_Story__c (
          
          Case__c = originCase.id,
          copado__Developer__c = originCase.OwnerId,
          copado__Project__c = defaultCP,
          copado__User_Story_Title__c = originCase.CaseNumber + ': ' + originCase.Subject,
          Description__c = originCase.Description,          
          Implementation_Status__c = originCase.Implementation_Status__c);
          userStoryList.add(newAutoCaseCUS);
          
          insert userStoryList;
                
          returnResult = 'Click OK to continue with a new Copado User Story.';          
      }
      
      catch (Exception e) {
      Database.rollback(sp);
      returnResult = 'Cannot add User Story because of error: ' + e.getMessage();
      system.debug('[AutoCaseCopadoUserStory: AutoCaseCopadoUserStory] Exception: ' + e.getMessage());
      ApexLogHandler.createLogAndSave('AutoCaseCopadoUserStory','AutoCaseCopadoUserStory', e.getStackTraceString(), e);
    }
    return returnResult;
      
   }
}