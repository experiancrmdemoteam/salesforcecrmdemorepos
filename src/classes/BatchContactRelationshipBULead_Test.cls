/*======================================================================================
 * Experian Plc.
 * Name: BatchContactRelationshipBULead_Test
 * Description: Test Class for BatchChatterGroupArchiveMaintenance
 * Created Date: Jul 27th, 2016
 * Created By: Diego Olarte
 *
 * Date Modified      Modified By        Description of the update
 * 27/07/2016(QA)     Diego Olarte       CRM2:W-005406: Contact relationship BU Lead
 =======================================================================================*/

@isTest
private class BatchContactRelationshipBULead_Test {
    
  static testMethod void testBatch() {
    
    List<Contact_team__c> testRelationship1 = [Select Id, Primary_User__c, Business_Unit__c, Contact__c, Custom_Record_Creation_ID__c,CreatedDate,Contact__r.LastName FROM Contact_team__c WHERE Contact__r.LastName = 'testrelationship'];
        
    Test.startTest();
    
    BatchContactRelationshipBULead batchToProcess = new BatchContactRelationshipBULead();
    database.executebatch(batchToProcess,10);
    
    Test.stopTest();
    
  }
  
  
  static testMethod void testScheduler() {
    
    Test.startTest();
    
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = System.schedule('ScheduleBatchContactRelationshipBULead '+String.valueOf(Datetime.now().getTime()), CRON_EXP, new ScheduleBatchContactRelationshipBULead());
    
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];     
    
    Test.stopTest();

    // Verify the Schedule has been scheduled with the same time specified  
    system.assertEquals(CRON_EXP, ct.CronExpression);

    // Verify the job has not run, but scheduled  
    system.assertEquals(0, ct.TimesTriggered);
  }
  
  @testSetup
  static void setupData() {
  
    User tstUser1 = Test_utils.createUser(Constants.PROFILE_EXP_SALES_ADMIN);
    tstUser1.Business_Unit__c = 'Global Corporate Finance';
    User tstUser2 = Test_utils.createUser(Constants.PROFILE_EXP_SALES_ADMIN);
    tstUser2.Business_Unit__c = 'Global Corporate Finance';
    User tstUser3 = Test_utils.createUser(Constants.PROFILE_EXP_SALES_ADMIN);
    tstUser3.Business_Unit__c = 'Global Sales Operations';
    User tstUser4 = Test_utils.createUser(Constants.PROFILE_EXP_SALES_ADMIN);
    tstUser4.Business_Unit__c = 'Global Sales Operations';
    insert new List<User>{tstUser1,tstUser2,tstUser3,tstUser4};
    
    Account tstAccount = Test_utils.createAccount();
    insert new List<Account>{tstAccount};
    
    Contact tstContact = Test_utils.createContact(tstAccount.id);
    tstContact.LastName = 'testrelationship';
    insert new List<Contact>{tstContact};
    
    Contact_team__c tstContactTeam1 = Test_Utils.createContactTeam(tstContact.Id, tstUser1.Id);
    tstContactTeam1.Primary_User__c = true;
    Contact_team__c tstContactTeam2 = Test_Utils.createContactTeam(tstContact.Id, tstUser2.Id);
    Contact_team__c tstContactTeam3 = Test_Utils.createContactTeam(tstContact.Id, tstUser3.Id);
    Contact_team__c tstContactTeam4 = Test_Utils.createContactTeam(tstContact.Id, tstUser4.Id);
    insert new List<Contact_team__c>{tstContactTeam1,tstContactTeam2,tstContactTeam3,tstContactTeam4};
  }
  
  
}