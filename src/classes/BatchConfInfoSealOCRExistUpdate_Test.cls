/*=============================================================================
 * Experian
 * Name: BatchConfInfoSealOCRExistUpdate_Test
 * Description: Case 02224510 : Test class to verify the behaviour of BatchConfInfoSealOCRExistUpdate.cls
  * Created Date: 06th Dec 2016
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 * June21st 2017      Manoj Gopu            Fixed test class Failure for June 22nd release Making isdataadmin to true to avoid future runs with in the batch class
 ============================================================================*/
@isTest
private class BatchConfInfoSealOCRExistUpdate_Test{
    static testMethod void myUnitTest(){
        Test.startTest();
            Account acc = Test_Utils.insertAccount();
            
            Confidential_information__c confInfo=new Confidential_information__c();
            confInfo.Account__c=acc.Id;
            confInfo.Name='Test Confidential';
            insert confInfo;                    
            
            FeedItem post = new FeedItem();
            post.body = '[Welcome to test Knowledge]';
            Post.parentId = confInfo.Id;           
            insert post;
            
            IsDataAdmin__c isDateAdmin = Test_Utils.insertIsDataAdmin(true);
            Database.executeBatch(new BatchConfInfoSealOCRExistUpdate());
            ScheduleConfInfoSealOCRExistUpdate bc = new  ScheduleConfInfoSealOCRExistUpdate();                
            String sch = '0 0 18 * * ?'; 
            system.schedule('Test ConfInfoSealOCRExistUpdate', sch, bc);            
        
        Test.stoptest();
    }
}