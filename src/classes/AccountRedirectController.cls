/**=====================================================================
 * Appirio, Inc
 * Name: AccountRedirectController
 * Description: T-421509
 * Created Date: Aug 06th, 2015
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified      Modified By                  Description of the update
 * Aug 17th, 2015     Arpita Bose                  I-176317 
 * Aug 25th, 2015     Arpita Bose                  Updated code to remove reference of serasa Profiles 
 * Sep 16th, 2015     Paul Kissick                 I-180579: Adding changes for allowing 'No Address' for some users
 * Oct 7th, 2015      Paul Kissick                 I-184075: Adding fix for recordtype not passed to next page
*  =====================================================================*/
public with sharing class AccountRedirectController {
  
  private ApexPages.StandardController controller;
  public String retURL {get; set;}
  public Boolean showNewAccountButtons {get;set;}
  private String recordTypeId;
  
  private Set<String> permissionSetsAllowedForNewAccount = new Set<String>{
      Constants.PERMISSIONSET_EDQ_SAAS_DEPLOYMENT_MANAGER
  };
  private Set<String> profilesAllowedForNewAccount = new set<String>{Constants.PROFILE_SYS_ADMIN};
  
  public String getNewAccountPermSets() {
    List<String> tmpList = new List<String>(permissionSetsAllowedForNewAccount);
    return String.join(tmpList,',').replace('_',' ');
  }
  
  public String getNewAccountProfSets() {
    List<String> tmpList = new List<String>(profilesAllowedForNewAccount);
    return String.join(tmpList,',');
  }
  
  
  //constructor  
  public AccountRedirectController(ApexPages.StandardController controller) {
    this.controller =  controller;
    retURL = ApexPages.currentPage().getParameters().get(Constants.PARAM_NAME_RETURL);
    recordTypeId = (ApexPages.currentPage().getParameters().containsKey('RecordType')) ? ApexPages.currentPage().getParameters().get('RecordType') : null;
    showNewAccountButtons = false;
  }
  
  // method to redirect Page based on Profile of current user
  public PageReference redirect(){
    PageReference returnURL;
    List<PermissionSet> permissionSetsAllowed = [
      SELECT Id, Name
      FROM PermissionSet
      WHERE Name in: permissionSetsAllowedForNewAccount
    ];
    
    User usrRec = [
      SELECT Profile.Name, Country__c,Region__c, 
      (SELECT Id FROM PermissionSetAssignments WHERE PermissionSetId in: permissionSetsAllowed) 
      FROM User 
      WHERE id=:UserInfo.getUserId()
    ];

    Set<String> validSeresaProfiles = new Set<String>();
    Serasa_User_Profiles__c seresaUsrProfiles = Serasa_User_Profiles__c.getall().get(Constants.SERASA);
    // Serasa_User_Profiles__c seresaUsrProfiles1 = Serasa_User_Profiles__c.getall().get('Serasa Customer Care Profiles');
    // Serasa_User_Profiles__c seresaUsrProfiles2 = Serasa_User_Profiles__c.getall().get('Serasa Customer Care Profiles2');
    Serasa_User_Profiles__c serasaCustomerCareProfiles = Serasa_User_Profiles__c.getall().get(Constants.SERASA_CUSTOMER_CARE_PROFILES);
    
    if(seresaUsrProfiles != null && seresaUsrProfiles.Profiles__c != null){
      for(String profileName : seresaUsrProfiles.Profiles__c.split(',')){
        validSeresaProfiles.add(profileName.trim());
      }
    }
    if(serasaCustomerCareProfiles != null && serasaCustomerCareProfiles.Profiles__c != null){
      for(String profileName : serasaCustomerCareProfiles.Profiles__c.split(',')) {

        // Adding the profile name and the 'Experian Serasa ' prefix back to the profile name to the set
        validSeresaProfiles.add('Experian Serasa ' + profileName.trim());
      }
    }
    /*
    if(seresaUsrProfiles1 != null && seresaUsrProfiles1.Profiles__c != null){
      for(String profileName : seresaUsrProfiles1.Profiles__c.split(',')){
        validSeresaProfiles.add(profileName.trim());
      }
    }
    if(seresaUsrProfiles2 != null && seresaUsrProfiles2.Profiles__c != null){
      for(String profileName : seresaUsrProfiles2.Profiles__c.split(',')){
        validSeresaProfiles.add(profileName.trim());
      }
    }*/
 
 system.debug('Tyaga the complete set of profiles is this' +validSeresaProfiles ); 
    
    if (profilesAllowedForNewAccount.contains(usrRec.Profile.Name) || usrRec.PermissionSetAssignments.size() > 0) {
      showNewAccountButtons = true;
      return null;
    }
    else if (validSeresaProfiles.contains(usrRec.Profile.Name)) {
      returnURL = new PageReference('/apex/AccountCNPJSearchPage');
    }
    else {
      returnURL = newAccountAndAddress(); // Pass through the pagereference method to return the parameters if set
    }
    
    returnURL.setRedirect(true);
    return returnURL;
  }
  
  public PageReference newAccountAndAddress() {
    PageReference pr = new PageReference('/apex/ASS_AddOrCreateNewAccountAddress');
    if (recordTypeId != null) {
      pr.getParameters().put('RecordType',recordTypeId);
    }
    pr.setRedirect(true); 
    return pr;
  }
  
  public PageReference newAccount() {
    PageReference pr = newAccountAndAddress();
    pr.getParameters().put('bypassQAS','1');
    return pr;
  }
}