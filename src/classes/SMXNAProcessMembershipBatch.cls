/**=====================================================================
 * Experian
 * Name: SMXNAProcessMembershipBatch 
 * Description: 
 * Created Date: 18/01/2016
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 * 26th Jan, 2016    Paul Kissick       Removed check for experian.com on contact emails
 * 26th Jan, 2016    Paul Kissick       Modified to look for memberships sent 24 hours ago (delay)
 * 23th Feb, 2016    Diego Olarte       Modified to sent next day
 =====================================================================*/

global class SMXNAProcessMembershipBatch implements Database.Batchable<Membership__c>, Database.Stateful {
    
  global Datetime startTime;
  
  global static String strSurveyName = 'NA CS Membership';
  global static String strSurveyId = 'EXPERIAN_108225';
    
  global Iterable<Membership__c> start(database.batchablecontext BC){
    
    startTime = system.now();
    
    Datetime lastRunTime = BatchHelper.getBatchClassTimestamp('SMXNAProcessMembershipBatch');
    if (lastRunTime == null) {
      lastRunTime = Datetime.now();
    }
    // Overlapping to look within the last 48 to 24 hours
    Datetime mbStartDatetime = lastRunTime.addHours(-48);
    Datetime mbEndDatetime = lastRunTime.addHours(-24);
    
    return [
      SELECT Id, Contact_Name__r.Id, Contact_Name__r.Email, 
        Membership_Onboarding_Welcome_sent__c, Membership_Welcome_Activity_Sent_date__c, 
       (SELECT Id FROM Survey__r)
      FROM Membership__c 
      WHERE Membership_Onboarding_Welcome_sent__c = true       
      AND Membership_Welcome_Activity_Sent_date__c >= :mbStartDatetime
      AND Membership_Welcome_Activity_Sent_date__c < :mbEndDatetime
      AND Contact_Name__r.Id != null 
      AND Contact_Name__r.Id != ''
    ];
  }

  global void execute(Database.BatchableContext BC, List<Membership__c> scope){
 
    List <Feedback__c> feedbackList = new List<Feedback__c>();
    
    Long lgSeed = system.currentTimeMillis();

    for (Membership__c mb : scope){
      
      List<Feedback__c> lstSurvey = mb.Survey__r;

      if (lstSurvey.isEmpty() && (String.isNotBlank(mb.Contact_Name__r.Email))) {
        // && !mb.Contact_Name__r.Email.contains('@experian.com') PK Removed at request of Adil Ghafoor (26/01/16)
        lgSeed = lgSeed + 1;
        Feedback__c feedback = new Feedback__c();
        feedback.Membership__c = mb.Id;
        feedback.Name = 'P_' + lgSeed;
        feedback.Contact__c = mb.Contact_Name__r.Id; //ContactName
        feedback.Status__c = 'Nominated';
        feedback.DataCollectionName__c = strSurveyName;
        feedback.DataCollectionId__c = strSurveyId;
        feedbackList.add(feedback);
      }
    }
    insert feedbackList;
  }

  global void finish(Database.BatchableContext info){
    BatchHelper.setBatchClassTimestamp('SMXNAProcessMembershipBatch', startTime);
  }
  
}