/**=====================================================================
 * Name: AccountTeamMembersList_Full
 * Description: Case #01192655Show the Business Unit alongside each AE under Account Team
 * This is the Controller extension class for the AccountTeamMembersList_Full VF page that was written
 * to allow for the customisation of the list of AccountTeamMember records which was previously
 * being displayed using a standard page. It is called via a hyper-link on the AccountTeamMembersList VF page
 * on the Account layout.
 *
 * Created Date: Mar. 2nd, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By           Description of the update
 * Mar. 2nd, 2016        James Wills           Case #1192655: Show the Business Unit alongside each AE under Account Team - Created
 * Jun. 7th, 2016        James Wills           Case #1192655: Further updates
 * Aug. 30th, 2016       James Wills           CRM2:W-005718: Account Team View - Job Title
  =====================================================================*/
global with sharing class AccountTeamMembersList_Full {
  
  public List<accountTeamWrapper> accountTeamMembersToShowComplete {
    get{
      return fetchExistingAccountTeamMembers_Full();
    }  
    set;
  }
  
  public Account acc {get;set;}
  public ID accId;
  public ID selectedId {get;set;}
  public String atmListOrderParam {get;set;}  
  public String atmListAscDesc {get;set;}
  public String accountName {get;set;}

  public Boolean displayAccess {
    get{
      return getDisplayAccess();
    }  
    set;
  }

  //Property used by dynamic SOQL to sort and re-sort the ATM list
  public String  atmListOrder {
    get{
        if(atmListOrderParam == Label.AccountTeamMembersList_Team_Member){
          return 'User.Name';
        } else if(atmListOrderParam == Label.AccountTeamMembersList_Job_Title){
          return 'User.Title';
        } else if(atmListOrderParam == Label.AccountTeamMembersList_Team_Role){
          return 'TeamMemberRole';
        } else if(atmListOrderParam == Label.AccountTeamMembersList_Team_Member_Business_Unit){
          return 'User.Business_Unit__c';
        } else if(atmListOrderParam == Label.AccountTeamMembersList_Sales_Team){
          return 'User.Sales_Team__c';
        } else if(atmListOrderParam == Label.AccountTeamMembersList_Sales_Sub_Team){
          return 'User.Sales_Sub_Team__c';
        } else {
          return 'User.Name';
        }
    }
    set;
  }  
   
  public Boolean userhasEditAccess {
    get{
      if(accessType.contains(Label.AccountTeamMembersList_Edit)){
         return true;
      } else {
        return false;
      }
    }  
    set;
  }
  
  public Boolean userhasDeleteAccess {
    get{
      if(accessType.contains(Label.AccountTeamMembersList_Del)){
         return true;
      } else {
        return false;
      }
    }  
    set;
  }
  
  public String accessType{  
    get{
      return AccountPlanUtilities.checkAccess(accId);      
    }
    set;
  }
  
    
  //Objectives List Navigation Parameters
  public Integer pageOffset=0;
  
  //This is the initial number of records to display for this list and then the increment for displaying more records (if applicable).
  public Integer relatedListSize = 100; //20160608 - At time of writing the Account with the most ATMs has 51.
  
  public Integer atmLimit=relatedListSize;//maximum number to show before list expanded
  public Integer additionalATMsToView {
    get{
      if(endATMNumber+relatedListSize > totalATMs){
        return totalATMs-endATMNumber;
      } else {
        return relatedListSize;
      }      
    }
    set;    
  }
  public Integer startATMNumber {get;set;}
  public Integer endATMNumber {get;set;}
  public Integer totalATMs {get;set;}
  
  
  //============================================================================
  // Wrapper class to hold the Team Member record as well as their access levels
  //============================================================================
  global class accountTeamWrapper {
    public AccountTeamMember member {get;set;}
    public Boolean assignmentTeamLead {get;set;}
    public accountTeamWrapper() {
      member = new AccountTeamMember();
    }
    public String getCreateDate(){      
        return String.valueOf(member.createdDate.date().format());
    }
  }
  
  
  //==========================================================================
  // Constructor
  //==========================================================================
  public AccountTeamMembersList_Full ( ApexPages.Standardcontroller std) {
    
    Account accountFromPage = (Account)std.getRecord();
    accId = accountFromPage.id;
    
    acc = [SELECT Id,Name FROM Account WHERE Id = :accId];
    
    accountName=acc.Name;               

    atmListOrder   = 'User.Name'; //Default sort ATM of list by User.Name
    atmListAscDesc = 'ASC';       //Default sort ATM of list ascending
    
  }
  
  
  public Boolean getDisplayAccess(){
    Set<String> profileNamesToGrantAccess = new Set<String>{Constants.PROFILE_SYS_ADMIN,
                                                            Constants.PROFILE_EXP_SALES_ADMIN,     //Experian Sales Effectiveness 
                                                            Constants.PROFILE_EXP_SALES_SUPPORT,
                                                            Constants.PROFILE_EXP_CERT_SALES_ADMIN,//Experian Certified Sales Administrators
                                                            Constants.PROFILE_EXP_DQ_ADMIN         //Experian DQ Administration
                                                            };
    Set<ID> profilesHaveAccess_IDs = new Set<ID>();
    for (Profile p : [SELECT ID FROM Profile WHERE Name IN: profileNamesToGrantAccess]) {
      profilesHaveAccess_IDs.add(p.ID);
    }
    
    if(profilesHaveAccess_IDs.contains(UserInfo.getProfileId())){
      return true;
    } else {
      return false;
    }
  }
  
  public PageReference backToAccount(){
  
    PageReference account = new PageReference('/' + accId); 
    account.setRedirect(true); 

    return account;    
  
  }
  
  //Called from the AccountTeamMembersList_Full VF page
  public void atmListReorder(){
    if(atmListAscDesc == 'ASC'){
      atmListAscDesc = 'DESC';
    } else {
      atmListAscDesc = 'ASC';
    }
  
  }
    
  //Case #1192655 - new method to display all Account Team Members
  public List<accountTeamWrapper> fetchExistingAccountTeamMembers_Full () {

    List<accountTeamWrapper> accountTeamMembersToShowComplete = new List<accountTeamWrapper>();
    
    integer i = 0;
    map<Id,AccountShare> accShareMap = new map<Id,AccountShare>();
    for ( AccountShare accShare : [SELECT Id, AccountAccessLevel, OpportunityAccessLevel, CaseAccessLevel, UserOrGroupId
                                   FROM AccountShare
                                   WHERE AccountId = :acc.Id]) {
      accShareMap.put(accShare.UserOrGroupId,accShare);
    }
    
    List<AccountTeamMember> atmList = new List<AccountTeamMember>();
    
    String atmListQuery = 'SELECT Id,TeamMemberRole, UserId, AccountId, User.Sales_Sub_Team__c, User.Sales_Team__c, User.Name,' +
                          'User.Business_Unit__c, CreatedDate, CreatedBy.Alias, ' +
                          'User.Title ' + //CRM2:W-005718
                          'FROM AccountTeamMember ' +
                          'WHERE AccountId = :accId' +
                          ' ORDER BY '         + atmListOrder + 
                          ' '                  + atmListAscDesc + ' '+
                          ' LIMIT :atmLimit ' +
                          ' OFFSET :pageOffset';
    
    atmList = Database.query(atmListQuery);
    
    List<Account_Assignment_Team__c> aatList = [SELECT id, Assignment_Team__r.Account_Executive__c FROM Account_Assignment_Team__c WHERE Account__c = :accId];
            
    for ( AccountTeamMember atm : atmList) {

      accountTeamWrapper atWrap = new accountTeamWrapper();
      atWrap.member = atm;

      for(Account_Assignment_Team__c aat : aatList){
        if(aatList.size()>0 && atWrap.member.User.id == aat.Assignment_Team__r.Account_Executive__c){
          atWrap.assignmentTeamLead = true;
          Break;
        }
      }
      accountTeamMembersToShowComplete.add(atWrap);
    }

    //Now set values used for ATM list navigation
    if(accountTeamMembersToShowComplete.size()>0){
        startATMNumber = pageOffSet+1;
        endATMNumber = pageOffset + accountTeamMembersToShowComplete.size();  
        AggregateResult[] ATMs = [SELECT COUNT(id)TotalATMs FROM AccountTeamMember WHERE AccountId = :acc.Id];
        totalATMs  = (Integer)ATMs[0].get('TotalATMs');    
      } else {
        startATMNumber = 0;
        endATMNumber = 0;  
        totalATMs = 0;
      }


    return accountTeamMembersToShowComplete;
  }
  

  public void seeMoreATMs(){   
    atmLimit+=additionalATMsToView;
  }
 
  //==========================================================================
  // Method to delete the account team member records
  //==========================================================================
  public PageReference doDelete() {
    try{      
        AccountTeamMember atmToDel = [SELECT Id,UserId,AccountId FROM AccountTeamMember WHERE Id = :selectedId];
        
        // fetch the Confidential_Information__Share records to be deleted.
        List<Confidential_Information__Share> confidentialInfoShares = new List<Confidential_Information__Share>();
        for (Confidential_Information__Share CI_Share : [SELECT c.UserOrGroupId, c.RowCause, c.Parent.Account__c, c.ParentId 
                                                         FROM Confidential_Information__Share c
                                                         WHERE Parent.Account__c = :atmToDel.AccountId 
                                                         AND UserOrGroupId = :atmToDel.UserId
                                                         AND RowCause = :Constants.ROWCAUSE_ACCOUNT_TEAM]) {
          confidentialInfoShares.add(CI_Share);
        }
        
        AccountPlanUtilities.deleteAccountPlanTeams(atmToDel);
        delete atmToDel;
        
        if (!confidentialInfoShares.isEmpty() ) {
          delete confidentialInfoShares;
        }

    } catch ( exception ex) {
      apexLogHandler.createLogAndSave('AccountTeamMembersList','doDelete', ex.getStackTraceString(), ex);
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()));
    }
    return null;
  }
  
   
}