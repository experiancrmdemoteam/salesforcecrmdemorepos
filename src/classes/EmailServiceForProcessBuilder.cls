/**=============================================================================
 * Experian plc
 * Name: EmailServiceForProcessBuilder_Test
 * Description: Case #02296348 UAT - Repository, Distribution and Tracking
 * Created Date: Feb 23rd, 2017
 * Created By: Sanket Vaidya
 * 
 * Date Modified        Modified By                  Description of the update
 * 
 =============================================================================*/

public class EmailServiceForProcessBuilder
{
    
    @InvocableMethod(label='Email Testers of this Test Case')   
    public static void EmailTestersOnTestCase(List<Test_Case__c> testCases)
    {
        Map<Id, Test_Case__c> mapTestCases = new Map<Id, Test_Case__c>(testCases);

        List<Test_Case__C> testCaseList = [SELECT Test_Case_Name__c, Name, Short_Description__c,  Work__r.agf__Due_Date__c, Project__r.Name, UAT_Owner__r.Email, (Select Tester__r.Email from Test_Case_Teams__r) FROM Test_Case__c where ID =: mapTestCases.keySet() ];
        
        for(Test_Case__C tc : testCaseList)
        {
            Set<String> toAddresses = new Set<String>();
            String emailOfUATOwnerOfTestCase = '';
            
            for(Test_Case_Team__c teamC : tc.Test_Case_Teams__r)
            {
                if(String.isNotEmpty(teamC.Tester__r.Email))
                {
                    toAddresses.add(teamC.Tester__r.Email);
                }
            }

            emailOfUATOwnerOfTestCase = tc.UAT_Owner__r.Email;
            
            UATEmailReminderForAgileAccelerator.SendEmail(tc, new List<String>(toAddresses), emailOfUATOwnerOfTestCase);
        }
    }    
}