/**=====================================================================
 * Experian
 * Name: BatchWalletSyncUserLookups_Test
 * Description: 
 * Created Date: 6 Aug 2015
 * Created By: Paul Kissick
 *
 * Date Modified       Modified By          Description of the update
 * Aug 28th, 2015      Paul Kissick         Adding Previous Account Supervisor to tests
 * Oct 1st, 2015       Paul Kissick         Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c
 * Apr 7th, 2016       Paul Kissick         Case 01932085: Fixing Test User Email Domain
 * May 3rd, 2016       Paul Kissick         Case 01972593: Adding checks for Sales (not chatter) users.
 * Oct 19th, 2016      Manoj Gopu           Case 02172178: Replaced Employee number with the Serasa Employee Number
 =====================================================================*/
@isTest
private class BatchWalletSyncUserLookups_Test {

  static testMethod void testBatch() {
    
    List<WalletSync__c> walletSyncs = new List<WalletSync__c>();
    Integer i = 1;
    Integer employeeNumbers = 0;
    Integer totalEmps = 0;
    //02172178 changed employeeNumber to Serasa_Employee_Number__c - MG
    for (User u : [SELECT Id, Serasa_Employee_Number__c, Name FROM User WHERE Alias LIKE 'ZWQQ%']) {
      if (u.Serasa_Employee_Number__c != null) {

        employeeNumbers++;
      }
      // Note, in this test, all 3 user fields are the same. This shouldn't actually happen!
      //changed employeeNumber to Serasa_Employee_Number__c - MG
      walletSyncs.add(new WalletSync__c(
        Account_Owner_Employee_Code__c = (String.isNotBlank(u.Serasa_Employee_Number__c)) ? u.Serasa_Employee_Number__c : 'ANOTHERNUM'+String.valueOf(i),
        Account_Owner_Name__c = u.Name,
        Account_Supervisor_Emp_Code__c = (String.isNotBlank(u.Serasa_Employee_Number__c)) ? u.Serasa_Employee_Number__c : 'ANOTHERNUM'+String.valueOf(i),
        Account_Supervisor_Name__c = u.Name,
        Previous_Account_Owner_Employee_Code__c = (String.isNotBlank(u.Serasa_Employee_Number__c)) ? u.Serasa_Employee_Number__c : 'ANOTHERNUM'+String.valueOf(i),
        Previous_Account_Owner_Name__c = u.Name,
        Previous_Account_Supervisor_Emp_Code__c = (String.isNotBlank(u.Serasa_Employee_Number__c)) ? u.Serasa_Employee_Number__c : 'ANOTHERNUM'+String.valueOf(i),
        // Previous_Account_Owner_Name__c = u.Name,
        CNPJ_Number__c = '0192982092020'+String.valueOf(i),
        LegacyCRM_Account_ID__c = 'ANYTHING'+String.valueOf(i),
        Last_Processed_Date__c = Datetime.now().addHours(-1)
      )); 
      i++;
    }
    //changed employeeNumber to Serasa_Employee_Number__c - MG
    for (User u : [SELECT Id, Serasa_Employee_Number__c, Name FROM User WHERE Alias LIKE 'QSWA%' AND Serasa_Employee_Number__c != null]) {
      totalEmps++;
      // Note, in this test, all 3 user fields are the same. This shouldn't actually happen!
      walletSyncs.add(new WalletSync__c(
        Account_Owner_Employee_Code__c = (String.isNotBlank(u.Serasa_Employee_Number__c)) ? u.Serasa_Employee_Number__c : 'ANOTHERNUM'+String.valueOf(i),
        Account_Owner_Name__c = u.Name,
        Account_Supervisor_Emp_Code__c = (String.isNotBlank(u.Serasa_Employee_Number__c)) ? u.Serasa_Employee_Number__c : 'ANOTHERNUM'+String.valueOf(i),
        Account_Supervisor_Name__c = u.Name,
        Previous_Account_Owner_Employee_Code__c = (String.isNotBlank(u.Serasa_Employee_Number__c)) ? u.Serasa_Employee_Number__c : 'ANOTHERNUM'+String.valueOf(i),
        Previous_Account_Owner_Name__c = u.Name,
        Previous_Account_Supervisor_Emp_Code__c = (String.isNotBlank(u.Serasa_Employee_Number__c)) ? u.Serasa_Employee_Number__c : 'ANOTHERNUM'+String.valueOf(i),
        // Previous_Account_Owner_Name__c = u.Name,
        CNPJ_Number__c = '0192982092020'+String.valueOf(i),
        LegacyCRM_Account_ID__c = 'ANYTHING'+String.valueOf(i),
        Last_Processed_Date__c = Datetime.now().addHours(-1)
      ));
      i++;
    }
    
    insert walletSyncs;
    
    Test.startTest();
    BatchWalletSyncUserLookups b = new BatchWalletSyncUserLookups();
    Database.executeBatch(b);
    Test.stopTest();
    
    system.assertEquals(employeeNumbers, [SELECT COUNT() FROM WalletSync__c WHERE Account_Owner__c != null]);
    system.assertEquals(employeeNumbers, [SELECT COUNT() FROM WalletSync__c WHERE Account_Supervisor__c != null]);
    system.assertEquals(employeeNumbers, [SELECT COUNT() FROM WalletSync__c WHERE Previous_Account_Owner__c != null]);
    system.assertEquals(employeeNumbers, [SELECT COUNT() FROM WalletSync__c WHERE Previous_Account_Supervisor__c != null]);
  }
  
  @testSetup
  static void setupTestData() {
    IsDataAdmin__c ida = new IsDataAdmin__c(IsDataAdmin__c = true, SetupOwnerId = UserInfo.getUserId());
    insert ida;
    
    Profile userProf = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    Profile chatterProf = [SELECT Id FROM Profile WHERE UserType = 'CsnOnly' LIMIT 1];
    
    Integer userCount = 5;
    
    List<User> tstUsrs = Test_Utils.createUsers(userProf, 'test@experian.com', 'ZWQQ', userCount);
    List<User> chatUsrs = Test_Utils.createUsers(chatterProf, 'extra@experian.com', 'QSWA', userCount);
    // 02172178 changed employeeNumber to Serasa_Employee_Number__c - MG
    for (Integer i = 0; i < userCount; i++) {
      if (Math.mod(i,2) == 0) {
        tstUsrs[i].Serasa_Employee_Number__c = 'RANDOM' + i + 'RANDOM';
      }
      else {
        tstUsrs[i].Serasa_Employee_Number__c = null;
      }
    }
    for (Integer i = 0; i < userCount; i++) {
      if (Math.mod(i,2) == 0) {
        chatUsrs[i].Serasa_Employee_Number__c = 'EXTRAC' + i + 'EXTRAC';
      }
      else {
        chatUsrs[i].Serasa_Employee_Number__c = null;
      }
    }
    // Add a unique employee code to 3 of them...
    insert tstUsrs;
    insert chatUsrs;
    
    Global_Settings__c globalSetting = Global_Settings__c.getInstance('Global');
    globalSetting.Batch_Failures_Email__c = 'test@experian.com';
    globalSetting.Wallet_Sync_Processing_Last_Run__c = system.now().addDays(-1);
    update globalSetting;
    
  }


}