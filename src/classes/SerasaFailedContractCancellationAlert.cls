/**=====================================================================
  * Experian
  * Name: CaseCreateRedirectController
  * Description: W-007795: A schedualable class that can be called. This will be responsible to send 
  *              email alerts to supervisors about Failed Contract Cancellation.
  * Created Date: March 31 2017
  * Created By: Ryan (Weijie) Hu, UCInnovation
  *
  * Date Modified      Modified By                  Description of the update
  * May 3rd 2017       Ryan (Weijie) Hu             Custom labels for translation added
  *=====================================================================*/
global class SerasaFailedContractCancellationAlert implements Schedulable {

  public CronTrigger currentCron {get; private set;}
  public String scheduledHour {get; set;}
  public String newScheduledHour {get; set;}

  

  public SerasaFailedContractCancellationAlert(ApexPages.StandardSetController stdController) {

  }

  public SerasaFailedContractCancellationAlert() {
    
  }


  // Schedulable methods
  global void execute(SchedulableContext sc) {
    alertSupervisorsByEmail();
  }

  public void alertSupervisorsByEmail() {
    List<Contract_Cancellation_Request__c> cancellationList = [SELECT Id, Name, CNPJ_Number__c FROM Contract_Cancellation_Request__c WHERE Status__c = 'Failed' LIMIT 45000];
    List<Serasa_Contract_Cancellation_Supervisors__c> supervisorList = [SELECT Id, Name, Email__c FROM Serasa_Contract_Cancellation_Supervisors__c];
    //List<String> supervisorsEmailList = new List<String>{'Karyne.Silva@br.experian.com', 'Marcel.Silvio@br.experian.com', 'Nathalia.Suarez@br.experian.com'};

    // If no failed request, do not send email and return.
    if (cancellationList == null || cancellationList.size() == 0 || supervisorList == null || supervisorList.size() == 0) {
      return;

    }

    // Email body composing.
    String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
    String htmlEmailBodyStr = Label.SerasaFailedContractCancellationAlert_emailBody;

    for (Contract_Cancellation_Request__c ccr : cancellationList) {
      htmlEmailBodyStr += '<a href=\'' + sfdcBaseURL + '/' + ccr.Id + '\'>' + ccr.Name + ' [CNPJ: ' + ccr.CNPJ_Number__c + ']'+ '</a><br/><br/>';
    }


    for (Serasa_Contract_Cancellation_Supervisors__c sccs : supervisorList) {
      Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
      
      String finalizedEmailHtmlBody = Label.SerasaFailedContractCancellationAlert_emailGreeting + ' ' + sccs.Name + ',<br/><br/>' + htmlEmailBodyStr;

      String[] toaddress = new String[]{sccs.Email__c};

      email.setSubject(Label.SerasaFailedContractCancellationAlert_emailSubject);
      email.setHtmlBody(finalizedEmailHtmlBody);
      email.setToAddresses(toaddress);
      Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});
    }
    
  }

  // Helper class to reschedule a job.
  public static void rescheduleAlert() {
    
    abortScheduledJobs();
    scheduleMyJob(); 
  }

  // Helper class to scheudle a job.
  public static void scheduleMyJob() {

    // Get hour or hours from custom setting.
    List<Serasa_Contract_Cancellation_Schedule__c> selectedHours = [SELECT Id, Scheduled_Hour__c FROM Serasa_Contract_Cancellation_Schedule__c ORDER BY CreatedDate DESC NULLS LAST LIMIT 1];

    // If custom setting has hour defined
    if (selectedHours != null && selectedHours.size() != 0 && selectedHours.get(0).Scheduled_Hour__c != null && selectedHours.get(0).Scheduled_Hour__c != '') {
      SerasaFailedContractCancellationAlert sfcca = new SerasaFailedContractCancellationAlert();
      String sch = '0 0 ' + selectedHours.get(0).Scheduled_Hour__c + ' ? * *';

      System.schedule('Serasa Fialed Contract Cancellation Alert', sch, sfcca);
    }
  }

  // Helper method to abort a job
  public static void abortScheduledJobs() {
    List<CronTrigger> scheduleList = [SELECT CronJobDetail.Name, CronExpression, CronJobDetailId, Id FROM CronTrigger WHERE CronJobDetail.Name = 'Serasa Fialed Contract Cancellation Alert'];
    
    Savepoint nothingDone = null;
    for (CronTrigger t : scheduleList) {
      try {
         // checkpoint within the loop so we can log in the exception handler
        nothingDone = Database.setSavepoint();

        System.abortJob(t.Id);

        }
      catch (Exception e) {
        // can't schedule the job = aborted the wrong job = rollback!
        Database.rollback(nothingDone);
      } 
    }
  }

  // Helper method to check if there is an alert schedule exists
  public boolean getCurrentSchedule() {

    boolean exists = false;

    List<CronTrigger> scheduleList = [SELECT CronJobDetail.Name, CronExpression, CreatedById, CreatedDate, EndTime, NextFireTime, PreviousFireTime, StartTime, State, CronJobDetailId, TimesTriggered, TimeZoneSidKey, Id FROM CronTrigger WHERE CronJobDetail.Name = 'Serasa Fialed Contract Cancellation Alert'];

    if (scheduleList != null && scheduleList.size() > 0) {
      exists = true;

      currentCron = scheduleList.get(0);

      scheduledHour = currentCron.CronExpression.split(' ')[2];
    }

    return exists;
  }

  // Helper method to get the hour string from custom setting
  public String getCurrentCustomSetting() {
    // Get hour or hours from custom setting.
    List<Serasa_Contract_Cancellation_Schedule__c> selectedHours = [SELECT Id, Scheduled_Hour__c FROM Serasa_Contract_Cancellation_Schedule__c ORDER BY CreatedDate DESC NULLS LAST LIMIT 1];

    if (selectedHours != null && selectedHours.size() != 0 && selectedHours.get(0).Scheduled_Hour__c != null && selectedHours.get(0).Scheduled_Hour__c != '') {
      return selectedHours.get(0).Scheduled_Hour__c;
    }
    else {
      return '';
    }
  }

  public PageReference updateCustomSetting() {

    updateCustomSettingHelper();

    return null;
  }

  public void updateCustomSettingHelper() {
    List<Serasa_Contract_Cancellation_Schedule__c> selectedHours = [SELECT Id, Scheduled_Hour__c FROM Serasa_Contract_Cancellation_Schedule__c ORDER BY CreatedDate DESC NULLS LAST LIMIT 1];

    if (selectedHours != null && selectedHours.size() != 0 && selectedHours.get(0).Scheduled_Hour__c != null && selectedHours.get(0).Scheduled_Hour__c != '') {

      Serasa_Contract_Cancellation_Schedule__c setting = selectedHours.get(0);
      setting.Scheduled_Hour__c = newScheduledHour;

      update setting;
    }
    else {
      Serasa_Contract_Cancellation_Schedule__c newSetting = new Serasa_Contract_Cancellation_Schedule__c();
      newSetting.Name = 'Selected Hours';
      newSetting.Scheduled_Hour__c = newScheduledHour;

      insert newSetting;
    }
  }

  public PageReference pageScheduleMyJob() {
    updateCustomSettingHelper();
    SerasaFailedContractCancellationAlert.scheduleMyJob();

    return null;
  }

  public PageReference pageAbortScheduledJobs() {
    SerasaFailedContractCancellationAlert.abortScheduledJobs();

    return null;
  }

  public PageReference pageRescheduleAlert() {
    updateCustomSettingHelper();

    System.debug(Logginglevel.ERROR, scheduledHour);
    SerasaFailedContractCancellationAlert.rescheduleAlert();

    return null;
  }
}