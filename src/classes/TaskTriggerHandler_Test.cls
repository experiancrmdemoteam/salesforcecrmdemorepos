/**================================================================================
 * Experian plc
 * Name: TaskTriggerHandler_Test
 * Description: Test classes for TaskTriggerHandler
 * Created Date: 9th September 2014
 * Created By: James Weatherall
 * 
 * Date Modified        Modified By         Description of the update
 * Sep 8, 2014          James Weatherall    Case 1949 - Created tests for the new
 *                                          setCampaignWhenConverted method.
 * Apr 17th,2015        Arpita Bose(Appirio) Updated testSetCampaignWhenConverted() to fix failure
 * Jul 15th, 2015       Paul Kissick        Adding fixes for failing tests after Timed workflow added to leads
 * Jul 17th, 2015       Arpita Bose         T-419042: Added method testCalculateCampaignTasksCount()
 * Jul 20th, 2015       Arpita Bose         Added method test_DispatchNote() for increasing code coverage 
 * Sep 07th, 2015       Arpita Bose         Updated class to fix failure 
 * Oct 06th, 2015       Nathalie Le Guay    Fixed assert that was failing occasionally in test_Dispatch
 * Jun 9th, 2016        Paul Kissick        Case 01975883: New tests for SME Activities
 * Jul 28th, 2016(QA)   Manoj Gopu          CRM2:W-005289: Added new method testpopulateoppfields() to test functionality
 * Aug 10th, 2016       Manoj Gopu          CRM2:W-005424: Updated the method testpopulateoppfields for assignCountryfromContact 
 * Aug 12th, 2016       James Wills         Case #02093125 - added new test method test_ActivityTypeUpdate()
 * Aug 22nd, 2016       Manoj Gopu          CRM2:W-005599: Updated the testpopulateoppfields() to increase the test Coverage
 * Sep 02nd, 2016       Manoj Gopu          CRM2:W-005289: Added asserts to the test method.
 * Feb 09th, 2017       Manoj Gopu          Case #02227631:Updated the testpopulateoppfields() to improve code Coverage
 * Jun 06th, 2017       Manoj Gopu          Fixed TestClass Failure
=================================================================================**/

@isTest
private class TaskTriggerHandler_Test {
    
  
  //1. As per test scnario of T-311659: verify no Project Resource gets created
  static testMethod void test_no_ProjectResourceGetsCreated() {
    
    User testUser = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    
    system.runAs(testUser) {
      Account accnt = Test_Utils.insertAccount();
      Contact testContact = Test_Utils.insertContact(accnt.ID);

      Task testTask = Test_Utils.insertTask(testContact.ID, accnt.ID);
      //No project resource has been created.
      system.assertEquals([SELECT count() FROM Project_Resource__c], 0);
    }
  }
  
  //2. As per test scnario of T-311659: verify Project Resource gets created for each of the Task's Owner
  static testMethod void test_projectResourceGetsCreatedForEachOwner () {
    User testUser1 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    User testUser2 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    Project__c testProject;
    Delivery_Line__c dl;
    RecordType projDevRT = [Select Id From RecordType 
                                 WHERE SObjectType = 'Task' 
                                 AND Name = 'Project and Delivery'];
    
    system.runAs(testUser1) {
      testProject = Test_Utils.insertProject(true);
      delete [SELECT ID FROM Project_Resource__c];
      Task projectTask = new Task(WhatId = testProject.ID, RecordTypeId = projDevRT.ID);
      projectTask.Assign_Resource__c = true;
      insert projectTask;
    }
    
    Project_Resource__c assertCheck = [SELECT Resource__c, Project__c, Id  FROM Project_Resource__c WHERE Resource__c =: testUser1.ID];
    system.assertEquals(assertCheck.Resource__c, testUser1.ID);

    system.runAs(testUser2) {
      dl = Test_Utils.insertDeliveryLine(true, testProject.ID);
      delete [SELECT ID FROM Project_Resource__c];
      Task delLineTask = new Task(WhatId = dl.ID, RecordTypeId = projDevRT.ID);
      delLineTask.Assign_Resource__c = true;
      insert delLineTask;
    }
    
    assertCheck = [SELECT Resource__c, Project__c, Id  FROM Project_Resource__c WHERE Resource__c =: testUser2.ID];
    system.assertEquals(assertCheck.Resource__c, testUser2.ID);
  }
  
  //3. As per test scnario of T-311659: verify Project Resource should not get created for Task.Assign_Resource__c = false
  static testMethod void test_projectResourceNotCreatedForAssignResFalse () {
    User testUser1 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    User testUser2 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    Project__c testProject1;
    Project__c testProject2;
    Delivery_Line__c dl;
    RecordType projDevRT = [Select Id From RecordType 
                                 WHERE SObjectType = 'Task' 
                                 AND Name = 'Project and Delivery'];
    
    system.runAs(testUser1) {
      testProject1 = Test_Utils.insertProject(true);
      testProject2 = Test_Utils.insertProject(true);
      delete [SELECT ID FROM Project_Resource__c];
      Task projectTask = new Task(WhatId = testProject1.ID, RecordTypeId = projDevRT.ID);
      projectTask.Assign_Resource__c = false;
      insert projectTask;
    }
    //Should not have any Project Resource as Assign Resource = false
    system.assertEquals([SELECT Resource__c, Project__c, Id  FROM Project_Resource__c WHERE Resource__c =: testUser1.ID].size(), 0);

    system.runAs(testUser2) {
      dl = Test_Utils.insertDeliveryLine(true, testProject2.ID);
      delete [SELECT ID FROM Project_Resource__c];
      Task delLineTask = new Task(WhatId = dl.ID, RecordTypeId = projDevRT.ID);
      delLineTask.Assign_Resource__c = false;
      insert delLineTask;
    }
    //Should not have any Project Resource as Assign Resource = false
    system.assertEquals([SELECT Resource__c, Project__c, Id  FROM Project_Resource__c WHERE Resource__c =: testUser2.ID].size(), 0);
  }

  //4. As per test scnario of T-311659: Task updated and Owner get changed should create Project Resource
  static testMethod void test_projectResourceGetCreatedForNewUser() {
    User testUser1 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    User testUser2 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    Project__c testProject1;
    Delivery_Line__c dl;
    RecordType projDevRT = [Select Id From RecordType 
                                 WHERE SObjectType = 'Task' 
                                 AND Name = 'Project and Delivery'];
    
    system.runAs(testUser1) {
      testProject1 = Test_Utils.insertProject(true);
      delete [SELECT ID FROM Project_Resource__c];
      dl = Test_Utils.insertDeliveryLine(true, testProject1.ID);
      Task testTask = new Task(WhatId = dl.ID, RecordTypeId = projDevRT.ID);
      testTask.Assign_Resource__c = false;
      insert testTask;

      //Should have already project resource for testUser1 as the current owner.
      system.assertEquals([SELECT Resource__c, Project__c, Id  FROM Project_Resource__c WHERE Resource__c =: testUser2.ID].size(), 0);
  
      //Updating task and changing user.
      testTask.OwnerId = testUser2.ID;
      testTask.Assign_Resource__c = true;
      update testTask;
    }
    
    //Should create Project Resource as Assign Resource = true and Owner get changed.
    system.assertEquals([SELECT Resource__c, Project__c, Id  FROM Project_Resource__c WHERE Resource__c =: testUser2.ID].size(), 1);
  }

  //5. As per test scnario of T-311659: verify Project Resource get created for Task.Assign_Resource__c changes to true from false
  static testMethod void test_projectResourceCreatedForAssignResTrue () {
    User testUser1 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    Project__c testProject1;
    Delivery_Line__c dl;
    RecordType projDevRT = [Select Id From RecordType 
                                 WHERE SObjectType = 'Task' 
                                 AND Name = 'Project and Delivery'];
    
    system.runAs(testUser1) {
      testProject1 = Test_Utils.insertProject(true);
      delete [SELECT ID FROM Project_Resource__c];
      Task projectTask = new Task(WhatId = testProject1.ID, RecordTypeId = projDevRT.ID);
      projectTask.Assign_Resource__c = false;
      insert projectTask;
      //Should not have any Project Resource as Assign Resource = false
      system.assertEquals([SELECT Resource__c, Project__c, Id  FROM Project_Resource__c WHERE Resource__c =: testUser1.ID].size(), 0);

      projectTask.Assign_Resource__c = true;
      update projectTask;
    }
    
    //Should not have any Project Resource as Assign Resource = false
    system.assertEquals([SELECT Resource__c, Project__c, Id  FROM Project_Resource__c WHERE Resource__c =: testUser1.ID].size(), 1);
  }


  //6. As per test scnario of T-311659: verify Project resource gets created when RT is changed to Project and Delivery
  static testMethod void test_projectResourceCreatedForRecTypeProjDelvery() {
    User testUser1 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    Project__c testProject1;
    Delivery_Line__c dl;
    RecordType recordType = [Select Id From RecordType 
                                 WHERE SObjectType = 'Task' 
                                 AND Name != 'Project and Delivery' LIMIT 1];
    
    system.runAs(testUser1) {
      testProject1 = Test_Utils.insertProject(true);
      delete [SELECT ID FROM Project_Resource__c];
      Task projectTask = new Task(WhatId = testProject1.ID, RecordTypeId = recordType.ID);
      projectTask.Assign_Resource__c = true;
      insert projectTask;
      
      //Should not have any Project Resource as Assign Resource = false
      system.assertEquals([SELECT Resource__c, Project__c, Id  FROM Project_Resource__c WHERE Resource__c =: testUser1.ID].size(), 0);
      
      recordType = [Select Id From RecordType 
                                 WHERE SObjectType = 'Task' 
                                 AND Name = 'Project and Delivery' LIMIT 1];
      
      projectTask.RecordTypeId = recordType.ID;
      update projectTask;
    }
    
    //Should not have any Project Resource as Assign Resource = false
    system.assertEquals([SELECT Resource__c, Project__c, Id  FROM Project_Resource__c WHERE Resource__c =: testUser1.ID].size(), 1);
  }  
  
  //7. As per test scnario of T-311659: verify Project resource do not get created when RT is other than Project and Delivery and Owner changed
  static testMethod void test_projectResourceCreatedForRecTypeNonProjDelvery() {
    User testUser1 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    User testUser2 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    Project__c testProject1;
    Delivery_Line__c dl;
    RecordType recordType = [Select Id From RecordType 
                                 WHERE SObjectType = 'Task' 
                                 AND Name != 'Project and Delivery' LIMIT 1];
    
    system.runAs(testUser1) {
      testProject1 = Test_Utils.insertProject(true);
      delete [SELECT ID FROM Project_Resource__c];
      Task projectTask = new Task(WhatId = testProject1.ID, RecordTypeId = recordType.ID);
      projectTask.Assign_Resource__c = true;
      insert projectTask;
      
      //Should not have any Project Resource as Assign Resource = false
      system.assertEquals([SELECT Resource__c, Project__c, Id  FROM Project_Resource__c WHERE Resource__c =: testUser1.ID].size(), 0);
      
      
      projectTask.OwnerID = testUser2.ID;
      update projectTask;
    }
    
    //Should not have any Project Resource as Assign Resource = false
    system.assertEquals([SELECT Resource__c, Project__c, Id  FROM Project_Resource__c WHERE Resource__c =: testUser2.ID].size(), 0);
  } 
  
  //7. As per test scnario of T-311659: verify Project resource do not get created when RT is other than Project and Delivery and Owner changed
  static testMethod void test_refreshOppScoreCalculations() {
    User testUser1 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    User testUser2 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    Project__c testProject1;
    Delivery_Line__c dl;
    
    
    system.runAs(testUser1) {
      Account accnt = Test_Utils.insertAccount();
      Opportunity oppty = Test_Utils.insertOpportunity(accnt.ID);
      Opportunity_Plan__c oppPlan = Test_Utils.insertOpportunityPlan(true, oppty.ID);
      
      String oppPlanOldModifiedDate = String.valueOf(oppPlan.LastModifiedDate);
      
      Task projectTask = new Task(WhatId = oppPlan.ID);
      insert projectTask;
            
      oppPlan = [SELECT LastModifiedDate FROM Opportunity_Plan__c WHERE ID =: oppPlan.ID];
      
      //Should not have any Project Resource as Assign Resource = false
      system.assertNotEquals(oppPlanOldModifiedDate, String.valueOf(oppPlan.LastModifiedDate));
      
      delete projectTask;

      oppPlan = [SELECT LastModifiedDate FROM Opportunity_Plan__c WHERE ID =: oppPlan.ID];
      
      //Should not have any Project Resource as Assign Resource = false
      system.assertNotEquals(oppPlanOldModifiedDate, String.valueOf(oppPlan.LastModifiedDate));
    }
  } 

  public static testmethod void testSetCampaignWhenConverted() {
  
    Integer i = 0;  
    Id campaignId; 
    Id leadToConvertId;
    List<Lead> leads = new List<Lead>();
    List<CampaignMember> campaignMembers = new List<CampaignMember>();
      
    Test.startTest();
    
    while(i < 5) {
      leads.add(Test_Utils.createLead());
      i++;    
    }
    insert leads;  
    
    Campaign newCampaign = new Campaign(Name = 'Test Campaign', Business_Unit__c = 'Credit Services', /*Audience__c = 'Testing',*/ Campaign_Code__c = 'TEST-123', IsActive = true, StartDate = system.today(), EndDate = system.today().addDays(7));
    newCampaign.Audience__c = 'Test Audience';
    insert newCampaign;
      
    campaignId = newCampaign.Id;
      
    for (Lead l : leads) {
      campaignMembers.add(new CampaignMember(CampaignId = campaignId, LeadId = l.Id, Status = 'Sent'));
      l.Abort_Timed_Workflow__c = true; // PK: Adding to fix tests
    }
    insert campaignMembers;
    update leads;
    
    //Conversion process
    Database.leadConvert lc = new Database.leadConvert();  
    leadToConvertId = leads[0].Id; 
    lc.setLeadId(leadToConvertId);
    lc.setConvertedStatus('Converted');
    lc.setDoNotCreateOpportunity(true);
    Database.convertLead(lc);
      
    Lead testLead = [select Id, Status from Lead where Id =: leadToConvertId];
    Contact testContact = [select Id, Email from Contact where Email = 'test@test.com' and CreatedDate = TODAY];
    
    // Create Task as leadConvert() method doesn't create it be default
    Task testTask = new Task(WhoId = testContact.Id, Subject = 'Test Lead', Type = 'Other', ActivityDate = system.today());
    insert testTask;
    
    Task testInsertedTask = [select Id, WhatId from Task where Id = : testTask.Id];
    
    Test.stopTest();
      
    system.assertEquals('Converted', testLead.Status);
    system.assertEquals('test@test.com', testContact.Email);
    system.assertEquals(campaignId, testInsertedTask.WhatId);                      
  }

  // T-419042
  static testmethod void testCalculateCampaignTasksCount() {
    Campaign newCampaign = new Campaign(Name = 'Test Campaign', Business_Unit__c = 'Credit Services', /*Audience__c = 'Testing',*/ Campaign_Code__c = 'TEST-123', IsActive = true, StartDate = system.today(), EndDate = system.today().addDays(7));
    newCampaign.Audience__c = 'Test Audience';
    insert newCampaign;
    
    //for Campaign__c on Task
    Campaign cmp = new Campaign(Name = 'Test Campaign', Business_Unit__c = 'Credit Services', Audience__c = 'Testing', Campaign_Code__c = 'TEST-123', IsActive = true, StartDate = system.today(), EndDate = system.today().addDays(7));
    insert cmp;
    
    Account accnt = Test_Utils.insertAccount();
    Contact testContact = Test_Utils.insertContact(accnt.ID);
      
    Test.startTest();
    //insert one Task with Status = 'Not Started'
    Task testTask1 = new Task(WhoId = testContact.Id, WhatId = newCampaign.Id, Subject = 'Test Campaign', Type = 'Other', ActivityDate = system.today(), Status = 'Not Started', Campaign__c = newCampaign.Id);
    insert testTask1;
    
    Campaign testCampaign = [SELECT Id, Total_Assigned_Tasks__c, Total_Open_Tasks__c, Total_Completed_Tasks__c 
                               FROM Campaign
                               WHERE Id =: newCampaign.Id];
                               
    //asserts to verify count of Task on Campaign records                           
    system.assertEquals(testCampaign.Total_Assigned_Tasks__c, 1);
    system.assertEquals(testCampaign.Total_Open_Tasks__c, 1);
    system.assertEquals(testCampaign.Total_Completed_Tasks__c, 0);
    
    // insert one Task with Status='In Progress' and other with Status = 'Completed'
    Task testTask2 = new Task(WhoId = testContact.Id, WhatId = newCampaign.Id, Subject = 'Test Campaign', Type = 'Other', ActivityDate = system.today(), Status = 'In Progress', Campaign__c = newCampaign.Id);
    insert testTask2;
    
    Task testTask3 = new Task(WhoId = testContact.Id, WhatId = newCampaign.Id, Subject = 'Test Campaign', Type = 'Other', ActivityDate = system.today(), Status = 'Completed', Campaign__c = newCampaign.Id);
    insert testTask3;
    
    testCampaign = [SELECT Id, Total_Assigned_Tasks__c, Total_Open_Tasks__c, Total_Completed_Tasks__c 
                      FROM Campaign
                      WHERE Id =: newCampaign.Id];
                               
    //asserts to verify count of Task on Campaign records                             
    system.assertEquals(testCampaign.Total_Assigned_Tasks__c, 3);
    system.assertEquals(testCampaign.Total_Open_Tasks__c, 2);
    system.assertEquals(testCampaign.Total_Completed_Tasks__c, 1);
    
    //update status of Task to 'Completed'    
    testTask1.Status = 'Completed';
    update testTask1;
    
    testCampaign = [SELECT Id, Total_Assigned_Tasks__c, Total_Open_Tasks__c, Total_Completed_Tasks__c 
                      FROM Campaign
                      WHERE Id =: newCampaign.Id];
                      
    //asserts to verify count of Task on Campaign records after update on Task record                            
    system.assertEquals(testCampaign.Total_Assigned_Tasks__c, 3);
    system.assertEquals(testCampaign.Total_Open_Tasks__c, 1);
    system.assertEquals(testCampaign.Total_Completed_Tasks__c, 2);
    
    // delete one Task of campaign
    delete testTask1;
    
    //asserts to verify count of Task on Campaign records after delete of Task record
    testCampaign = [SELECT Id, Total_Assigned_Tasks__c, Total_Open_Tasks__c, Total_Completed_Tasks__c 
                      FROM Campaign
                      WHERE Id =: newCampaign.Id];
    system.debug('testCampaign>>>' +testCampaign) ;
                              
    system.assertEquals(testCampaign.Total_Assigned_Tasks__c, 2);
    system.assertEquals(testCampaign.Total_Open_Tasks__c, 1);
    system.assertEquals(testCampaign.Total_Completed_Tasks__c, 1);
    
    // Add a new Campaign on Task.Campaign__c   
    Task testCmp = new Task(WhoId = testContact.Id, WhatId = newCampaign.Id, Subject = 'Test Campaign', Type = 'Other', ActivityDate = system.today(), Status = 'Not Started', Campaign__c = cmp.Id);
    insert testCmp;
    
    Campaign testCampaign1 = [SELECT Id, Total_Assigned_Tasks__c, Total_Open_Tasks__c, Total_Completed_Tasks__c 
                               FROM Campaign
                               WHERE Id =: cmp.Id]; 
                               
    //asserts to verify count of Task on Campaign records                           
    system.assertEquals(testCampaign1.Total_Assigned_Tasks__c, 0);
    system.assertEquals(testCampaign1.Total_Open_Tasks__c, 0);
    system.assertEquals(testCampaign1.Total_Completed_Tasks__c, 0);                     
    Test.stopTest();
  }
  
  
  static testmethod void test_DispatchNote() {
    Conga_Quicklinks_Settings__c custSettings = new Conga_Quicklinks_Settings__c();
    custSettings.Name = 'Dispatch Note';
    custSettings.Subject__c = 'Test subject';
    insert custSettings;
    system.debug('custSettings>>>' +custSettings);
    
    User testUser1 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
  
    system.runAs(testUser1) {
      Account accnt = Test_Utils.insertAccount();
      Contact testContact = Test_Utils.insertContact(accnt.ID);
      Opportunity oppty = Test_Utils.createOpportunity(accnt.Id);
      insert oppty;
      
      Product2 product = Test_Utils.insertProduct();
      
      PricebookEntry stdPricebookEntryObj = Test_Utils.insertPricebookEntry(product.Id, Test.getStandardPriceBookId(), Constants.CURRENCY_USD);
      //insert OLI
      List<OpportunityLineItem> opptyLIs = new List<OpportunityLineItem>();
      opptyLIs.add(Test_Utils.createOpportunityLineItem(oppty.Id, stdPricebookEntryObj.Id, oppty.Type));
      insert opptyLIs;
            
      Order__c ordr = Test_Utils.createOrder();
      ordr.Opportunity__c = oppty.Id;
      ordr.Account__c = accnt.ID;
      insert ordr;
      system.debug('testOrder>>>' +ordr);
      
      Billing_Product__c billProd = Test_Utils.insertBillingProduct();
      
      Order_Line_Item__c ordrli = Test_Utils.insertOrderLineItems(true, ordr.id, billProd.Id);
      system.debug('ordrli>>>' +ordrli);
      
      Test.startTest();
           
      Task ordrTask = new Task(WhatId = ordr.ID, Subject = 'Test subject');
      insert ordrTask;
      
      //Asserts
      Order__c orderResult = [SELECT Id, Date_Shipped__c FROM Order__c WHERE Id = :ordr.ID];
      
      system.assertNotEquals(null, orderResult.Date_Shipped__c); // NLG 2015-10-06
      system.debug('orderResult>>>' +orderResult);
      
      Order_Line_Item__c lineItem = [ SELECT Id, Shipped__c , Order__c, Delivery_Method__c 
                                        FROM Order_Line_Item__c 
                                        WHERE Order__c = :ordr.ID AND Id =:ordrli.Id];
      system.assertEquals(lineItem.Shipped__c, true);
      system.debug('lineItem>>>' +lineItem);
      
      Test.stopTest();
    }
  }
  
  static testmethod void testCampaignContactAssociation() {  
    Integer i = 0;  
    Id campaignId;
    
    List<Contact> contactList = new List<Contact>();
    List<Task> taskList = new List<Task>();

    Test.startTest();
    Account accnt = Test_Utils.insertAccount();
    while(i < 5) {
      contactList.add(Test_Utils.createContact(accnt.Id));
      i++;    
    }  
    insert contactList;    
    
    Campaign TestCampaign = Test_Utils.CreateCampaign();
    for (Contact c : contactList) {
      taskList.add(new Task(WhoId = c.Id, WhatId = TestCampaign.Id, Subject = 'Test Campaign', Type = 'Other', ActivityDate = system.today(), Status = 'In Progress', Campaign__c = TestCampaign.Id));
    }
    
    insert taskList;     
    // One Assertions requried: 1. Check there are 5 campaign members.
    
    List<CampaignMember> CampMemCountTemp = [select Id from CampaignMember where campaignId =: TestCampaign.Id];
    Integer CampMemCount = CampMemCountTemp.size();

    Test.stopTest(); 
    system.assertEquals(5,CampMemCount);

  }
  
  // Case 01975883 - Adding tests for new SME activity sales process
  static testMethod void testSMEActivityToOrder() {
    
    Account acc = Test_Utils.insertAccount();
    Contact con = Test_Utils.insertContact(acc.Id);
    Product2 prd2 = Test_Utils.insertProduct();
    
    Task t = new Task();
    t.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Task' AND Name = :TaskTriggerHandler.SERASA_SME_ACTIVITY LIMIT 1].Id;
    t.ActivityDate = Date.today();
    t.Status = 'Not Started';
    t.Probability__c = '0%';
    insert t;
    
    system.assertEquals(0, [SELECT COUNT() FROM Order__c WHERE Account__c = :acc.Id], 'No orders should exist yet!');
    
    t.Status = Constants.OPPTY_STAGE_CLOSED_WON;
    try {
      update t;
    }
    catch (DMLException e) {
      system.assert(e.getMessage().contains(system.label.Serasa_SME_Task_Contact_Required));
    }
    
    system.assertEquals(0, [SELECT COUNT() FROM Order__c WHERE Account__c = :acc.Id], 'No orders should exist yet!');
    
    t.WhoId = con.Id;
    t.Offer__c = prd2.Id;
    t.Revenue__c = 1000;
    t.Revenue_Type__c = Constants.ORDER_LINE_ITEM_RENEWAL_ONE_OFF;
    t.CurrencyISOCode = 'USD';
    
    update t;
    
    system.assertEquals(1, [SELECT COUNT() FROM Order__c WHERE Account__c = :acc.Id], 'Order should exist!');
    
  }
  // CRM2.0 W- 005289 - Added by Manoj
  static testmethod void testpopulateoppfields() {  
        Test.startTest();
        Account accnt = Test_Utils.insertAccount();
        Opportunity opty = Test_Utils.insertOpportunity(accnt.ID);      
        
        Contact newcontact  = Test_Utils.insertContact(accnt.id);
        Address__c address = Test_Utils.insertAddress(true);
            
        //insert account address
        Account_Address__c accAddrs = Test_Utils.insertAccountAddress(true, address.Id, accnt.Id);

        Test_Utils.insertContactAddress(true, address.Id, newcontact.Id);
        
        Id marketingTaskRecId = [SELECT Id FROM RecordType WHERE SobjectType ='Task' and Name = 'Marketing Activity'].Id;
      
        list<Task> lstTasks=new list<Task>();
        Task ts=new Task();
        ts.Subject='Test Subject';
        ts.WhatId=opty.Id;
        ts.Status='Completed';
        ts.Outcomes__c='Received signed contract';
        ts.WhoId=newcontact.Id;
        ts.RecordTypeId=marketingTaskRecId;
        lstTasks.add(ts);
        
        Task ts1=new Task();
        ts1.Subject='Test Subject';
        ts1.WhatId=opty.Id;
        ts1.Status='Completed';
        ts1.Outcomes__c='Quote delivered';
        lstTasks.add(ts1);
        
        Task ts2=new Task();
        ts2.Subject='Test Subject';
        ts2.WhatId=opty.Id;
        ts2.Status='Completed';
        ts2.Outcomes__c='Selection Confirmed';
        lstTasks.add(ts2);
        
        Database.insert(lstTasks);
        
        Opportunity oppty=[select id,Received_signed_contract_date__c,Quote_delivered_date__c,Selection_confirmed_date__c from Opportunity where id=:opty.Id];
        
        system.assertNotEquals(oppty.Received_signed_contract_date__c,null);
        system.assertNotEquals(oppty.Quote_delivered_date__c,null);
        system.assertNotEquals(oppty.Selection_confirmed_date__c,null);
         
        lstTasks=new list<Task>();
        ts1.Outcomes__c='Received signed contract';
        lstTasks.add(ts1);
        
        ts.Outcomes__c='Quote delivered';
        lstTasks.add(ts);
        
        ts.Outcomes__c='Selection Confirmed';
        lstTasks.add(ts2);
        
        Database.update(lstTasks);                
        
        Test.stopTest();
  }
  
  //Case #02093125
  //This test method is testing the method updateActivityType() that is called from beforeInsert()
  private static testmethod void test_ActivityTypeUpdate(){   
    
    Account testAccount = Test_Utils.insertAccount();
    Contact testContact = Test_Utils.insertContact(testAccount.id);
    
    Task newTask = Test_Utils.createTask(testContact.id, testAccount.id);
    newTask.Type = 'Email';
    
    insert newTask;
    
    List<Task> newTasks = new List<Task>();
    
    newTasks.add(newTask);
    
    TaskTriggerHandler.updateActivityType(newTasks);
    
    System.Assert(newTask.Activity_Type__c == newTask.Type, 'test_ActivityTypeUpdate(): Activity_Type__c has not been set properly.' + newTask.Activity_Type__c + ' ' + newTask.Type);
    
  }
  
}