/**=====================================================================
 * Experian
 * Name: SMXNAProcessCaseBatch 
 * Description: 
 * Created Date: 12/03/2015
 * Created By: Diego Olarte
 * 
 * Date Modified      Modified By        Description of the update
 * Feb. 2nd 2016      James Wills        Case #01266714 - Updated reference to Case Record Types following name change.
 * Mar 1st, 2016      Paul Kissick       Case #01879223 - Extending contact email check to remove any ending in experian.com
 * July 12th,2016(QA) Tyaga Pati         CRM2:W-005363 - Added code to update the Account on Survey record as part of the nomination record
                                        creation process.
 =====================================================================*/

global class SMXNAProcessCaseBatch implements Database.Batchable<Case>, Database.AllowsCallouts{

  global Iterable<Case> start(database.batchablecontext BC){
    if (Test.isRunningTest()) {
      case cs = SMXNAProcessCaseBatchTest.prepareTestData();
      List<Case> casesToUpdate = new List<Case>();
      casesToUpdate.add(cs);
      return casesToUpdate; 
    }
    else{

      return [
        SELECT Id, ContactId, AccountId, Contact.Email, (SELECT Id FROM Survey__r)
        FROM Case 
        WHERE IsClosed = true 
        AND ClosedDate >= YESTERDAY 
        AND Status != 'Closed - Duplicate'
        AND Case_Owned_by_Queue__c = false
        AND EDQ_Support_Region__c = 'NA'
        AND RecordTypeId IN (
          SELECT Id 
          FROM RecordType 
          WHERE Name IN ('EDQ Technical Support', 'EDQ Commercial Support', 'EDQ GPD Support')//Case #01266714 James Wills;
        ) 
        AND ContactId != null 
        AND ContactId != ''
      ];
    }
    //return AssetsToUpdate;
  }

  global void execute(Database.BatchableContext BC, List<Case> scope){
 
    String strSurveyName = 'EDQ Support Experience (NA)';
    String strSurveyId = 'EXPERIAN_109584';
    
    List <Feedback__c> feedbackList = new List<Feedback__c>();
    Long lgSeed = System.currentTimeMillis();

    Boolean result = false;
    for(Case cs : scope){
      
      List<Feedback__c> lstSurvey = cs.Survey__r;

      if(lstSurvey.isEmpty() && (String.isNotBlank(cs.Contact.Email) && !cs.Contact.Email.endsWithIgnoreCase('experian.com'))) {
        lgSeed = lgSeed + 1;
        Feedback__c feedback = new Feedback__c();
        feedback.Case__c = cs.Id;
        feedback.Name = 'P_' + lgSeed;
        feedback.Contact__c = cs.ContactId; //ContactName 
        feedback.Status__c = 'Nominated';               
        feedback.DataCollectionName__c = strSurveyName;
        feedback.DataCollectionId__c = strSurveyId;
        feedback.Account__c = cs.AccountId;//CRM2:W-005363 
        feedbackList.add(feedback);        
      }
    }
    system.debug('The survey list is' + feedbackList);
    insert feedbackList;
  }

  global void finish(Database.BatchableContext info){
    if(Test.isRunningTest()){
      SMXProcessCaseBatchTest.ClearTestData();
    }
  }//global void finish loop
    
  
}