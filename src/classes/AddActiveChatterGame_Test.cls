/**********************************************************************************************
 * Experian 
 * Name         : AddActiveChatterGame_Test
 * Created By   : Diego Olarte (Experian)
 * Purpose      : Test class of scheduler class "ScheduleAddActiveChatterGame" & AddActiveChatterGame
 * Created Date : July 27, 2015
 *
 * Date Modified                Modified By                 Description of the update
 * February 25, 2016            Diego Olarte                Case#01838499: Added test data for 3 additional priorities
 * Apr 7th, 2016                Paul Kissick                Removed schedule test as it is in another class
***********************************************************************************************/

@isTest
private class AddActiveChatterGame_Test {
    
  private static testMethod void testBatchGame1() {
    Chatter_Game__c game1 = [SELECT Id, Active__c FROM Chatter_Game__c WHERE Name = 'Test Game 1'];
    game1.Active__c = true;
    update game1;
    Test.startTest();
    AddActiveChatterGame batchToProcess = new AddActiveChatterGame();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    system.assertEquals(1,[SELECT COUNT() FROM User_Chatter_Stats__c WHERE Chatter_Game__c <> null]);
  }
  
  private static testMethod void testBatchGame2() {
    Chatter_Game__c game2 = [SELECT Id, Active__c FROM Chatter_Game__c WHERE Name = 'Test Game 2'];
    game2.Active__c = true;
    update game2;
    Test.startTest();
    AddActiveChatterGame batchToProcess = new AddActiveChatterGame();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    system.assertEquals(1,[SELECT COUNT() FROM User_Chatter_Stats__c WHERE Chatter_Game__c <> null]);
  }
  
  private static testMethod void testBatchGame3() {
    Chatter_Game__c game3 = [SELECT Id, Active__c FROM Chatter_Game__c WHERE Name = 'Test Game 3'];
    game3.Active__c = true;
    update game3;
    Test.startTest();
    AddActiveChatterGame batchToProcess = new AddActiveChatterGame();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    system.assertEquals(1,[SELECT COUNT() FROM User_Chatter_Stats__c WHERE Chatter_Game__c <> null]);
  }
  
  private static testMethod void testBatchGame4() {
    Chatter_Game__c game4 = [SELECT Id, Active__c FROM Chatter_Game__c WHERE Name = 'Test Game 4'];
    game4.Active__c = true;
    update game4;
    Test.startTest();
    AddActiveChatterGame batchToProcess = new AddActiveChatterGame();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    system.assertEquals(1,[SELECT COUNT() FROM User_Chatter_Stats__c WHERE Chatter_Game__c <> null]);
  }
  
  private static testMethod void testBatchGame5() {
    Chatter_Game__c game5 = [SELECT Id, Active__c FROM Chatter_Game__c WHERE Name = 'Test Game 5'];
    game5.Active__c = true;
    update game5;
    Test.startTest();
    AddActiveChatterGame batchToProcess = new AddActiveChatterGame();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    system.assertEquals(1,[SELECT COUNT() FROM User_Chatter_Stats__c WHERE Chatter_Game__c <> null]);
  }
  
  private static testMethod void testBatchGame6() {
    Chatter_Game__c game6 = [SELECT Id, Active__c FROM Chatter_Game__c WHERE Name = 'Test Game 6'];
    game6.Active__c = true;
    update game6;
    Test.startTest();
    AddActiveChatterGame batchToProcess = new AddActiveChatterGame();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    system.assertEquals(1,[SELECT COUNT() FROM User_Chatter_Stats__c WHERE Chatter_Game__c <> null]);
  }
  
  private static testMethod void testBatchGame7() {
    Chatter_Game__c game7 = [SELECT Id, Active__c FROM Chatter_Game__c WHERE Name = 'Test Game 7'];
    game7.Active__c = true;
    update game7;
    Test.startTest();
    AddActiveChatterGame batchToProcess = new AddActiveChatterGame();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    system.assertEquals(1,[SELECT COUNT() FROM User_Chatter_Stats__c WHERE Chatter_Game__c <> null]);
  }
  
  @testSetup
  private static void setupData() {
    
    /*
    BatchSchedulingIDstorage__c setting = new BatchSchedulingIDstorage__c();
    setting.BSIDS04__c = Userinfo.getOrganizationId();
    insert setting;
    */
    
    // 1. Create our test users...
    //Create test user
       
    User tstUser1 = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser1.Region__c = 'APAC';
    tstUser1.Business_line__c = 'APAC Corporate';
    tstUser1.Function__c = 'Sales operations';
       
    User tstUser2 = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser2.Region__c = 'UK&I';
    tstUser2.Business_line__c = 'UK&I Decision Analytics';
    tstUser2.Function__c = 'Executives';
    
    User tstUser3 = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser3.Region__c = 'Latin America';
    tstUser3.Business_line__c = 'LATAM Corporate';
    tstUser3.Function__c = 'Finance';
       
    User tstUser4 = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser4.Region__c = 'Latin America';
    tstUser4.Business_line__c = 'LATAM Decision Analytics';
    tstUser4.Function__c = 'Billing';
    
    User tstUser5 = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser5.Region__c = 'EMEA';
    tstUser5.Business_line__c = 'EMEA Credit Services';
    tstUser5.Function__c = 'Client Services';
    
    User tstUser6 = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser6.Region__c = 'North America';
    tstUser6.Business_line__c = 'NA Marketing Services';
    tstUser6.Function__c = 'CRM/SE support';
    
    User tstUser7 = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser7.Region__c = 'Global';
    tstUser7.Business_line__c = 'Global Corporate';
    tstUser7.Function__c = 'Finance';
       
    insert new List<User>{tstUser1, tstUser2, tstUser3, tstUser4, tstUser5, tstUser6, tstUser7};
    
    Chatter_Game__c game1 = new Chatter_Game__c(
      Name = 'Test Game 1',
      Function__c = 'Sales operations',
      Game_Region__c = 'APAC',
      Business_line__c = 'APAC Corporate',
      Unique_Active_Game_name__c = 'APACAPAC CorporateSales operations',
      Active__c = false
    );
    Chatter_Game__c game2 = new Chatter_Game__c(
      Name = 'Test Game 2',
      Function__c = 'Executives',
      Game_Region__c = 'UK&I',
      Unique_Active_Game_name__c = 'ExecutivesUK&I',
      Active__c = false
    );
    Chatter_Game__c game3 = new Chatter_Game__c(
      Name = 'Test Game 3',
      Game_Region__c = 'Latin America',
      Business_line__c = 'LATAM Corporate',
      Unique_Active_Game_name__c = 'Latin AmericaLATAM Corporate',
      Active__c = false
    );
    Chatter_Game__c game4 = new Chatter_Game__c(
      Name = 'Test Game 4',
      Function__c = 'Billing',
      Business_line__c = 'LATAM Decision Analytics',
      Unique_Active_Game_name__c = 'BillingLATAM Decision Analytics',
      Active__c = false
    );
    Chatter_Game__c game5 = new Chatter_Game__c(
      Name = 'Test Game 5',
      Function__c = 'Client Services',
      Unique_Active_Game_name__c = 'Client Services',
      Active__c = false
    );
    Chatter_Game__c game6 = new Chatter_Game__c(
      Name = 'Test Game 6',
      Game_Region__c = 'North America',
      Unique_Active_Game_name__c = 'North America',
      Active__c = false
    );
    Chatter_Game__c game7 = new Chatter_Game__c(
      Name = 'Test Game 7',
      Business_line__c = 'Global Corporate',
      Unique_Active_Game_name__c = 'Global Corporate',
      Active__c = false
    );
    
    insert new list<Chatter_Game__c>{game1,game2,game3,game4,game5,game6,game7};
    
    User_Chatter_stats__c stat1 = new User_Chatter_stats__c(
      User__c = tstUser1.Id
    );
    User_Chatter_stats__c stat2 = new User_Chatter_stats__c(
      User__c = tstUser2.Id
    );
    User_Chatter_stats__c stat3 = new User_Chatter_stats__c(
      User__c = tstUser3.Id
    );
    User_Chatter_stats__c stat4 = new User_Chatter_stats__c(
      User__c = tstUser4.Id
    );
    User_Chatter_stats__c stat5 = new User_Chatter_stats__c(
      User__c = tstUser5.Id
    );
    User_Chatter_stats__c stat6 = new User_Chatter_stats__c(
      User__c = tstUser6.Id
    );
    User_Chatter_stats__c stat7 = new User_Chatter_stats__c(
      User__c = tstUser7.Id
    );
    
    insert new List<User_Chatter_stats__c>{stat1,stat2,stat3,stat4,stat5,stat6,stat7};
  }
  
}