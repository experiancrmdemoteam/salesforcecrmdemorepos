/**=====================================================================
 * Experian
 * Name: NominationsLandingPageCntlr
 * Description: Controller to nominationslandingpage page
 * Created Date: March 13th 2016
 * Created By: Richard Joseph
 *
 * Date Modified      Modified By                Description of the update
 * Sep 13th, 2016     Paul Kissick               W-005721: Adding support for group half year nominations.
 * Sep 20th, 2016     Paul Kissick               W-005721: Fixed problem with null user ids, and also fixed types
 =====================================================================*/
public class NominationsLandingPageCntlr {
  
  public NominationsLandingPageCntlr() {
    
  }
  /*
  private ApexPages.StandardController stdController;
  
  public NominationsLandingPageCntlr(ApexPages.StandardController s) {
    stdController = s;
  }
  
  // Custom error messages to return to the page in a more friendly format.
  public List<String> errorMessages {get{
    if (errorMessages == null) {
      errorMessages = new List<String>();
    }
    return errorMessages;
  }set;}
  
  public String getSubordinatesJson() {
    List<Id> subIds = new List<Id>();
    Map<Id, User> mySubs = new Map<Id, User>([
      SELECT Id
      FROM User
      WHERE IsActive = true
      AND ManagerId = :UserInfo.getUserId()
    ]);
    subIds.addAll(mySubs.keySet());
    return JSON.serialize(subIds);
  }
  
  
  public List<String> confirmMessages {get{
    if (confirmMessages == null) {
      confirmMessages = new List<String>();
    }
    return confirmMessages;
  }set;}
  
  public String justificationText {get;set;}
  public String singleNominee {get;set;}
  public String projectSponsor {get;set;}
  public String teamName {get;set;}
  
  public String spotAward {get;set;}
  
  public List<SelectOption> getSpotAwards() {
    List<SelectOption> so = new List<SelectOption>();
    so.add(new SelectOption('','Choose...'));
    
    String myCurr = UserInfo.getDefaultCurrency();
    Decimal exchRate = 1.0;
    if (myCurr != Constants.CORPORATE_ISOCODE) {
      exchRate = [SELECT IsoCode, ConversionRate FROM CurrencyType WHERE IsoCode = :myCurr].ConversionRate;
    }
    
    for (Schema.PicklistEntry pe : Nomination__c.Spot_Award_Amount__c.getDescribe().getPicklistValues()) {
      String peLabel = pe.getLabel();
      if (myCurr != Constants.CORPORATE_ISOCODE) {
        Decimal usdVal = Decimal.valueOf(peLabel.removeStart('$'));
        peLabel = myCurr + ' ' + (usdVal * exchRate).round(System.RoundingMode.HALF_UP);
      }
      so.add(new SelectOption(pe.getValue(), peLabel));
    }
    return so;
  }
  
  public Boolean isPerson {
    get {
      if (isPerson == null) isPerson = true;
      return isPerson;
    } 
    private set;
  }
  

  
  private String nomTypeSingle = 'Level 3 - Half Year Nomination (Individual)';
  private String nomTypeTeam = 'Level 3 - Half Year Nomination (Team)';
  
  //private Id singleNominationRtId = DescribeUtility.getRecordTypeIdByName('Nomination__c', nomTypeSingle);
  //private Id teamNominationRtId = DescribeUtility.getRecordTypeIdByName('Nomination__c', nomTypeTeam);
  
  private String nomStatus = 'Submitted';
  
  public Map<Integer, String> userIdMap {
    get {
      if (userIdMap == null) {
        userIdMap = new Map<Integer, String>{0 => ''};
      }
      return userIdMap;
    }
    set;
  }
  
  public String selectNomType {
    get{
      if (selectNomType == null) {
        selectNomType = '';
      }
      return selectNomType;
    }
    set;
  }
  
  // If the user has the Recognition_Coordinator custom permission (from the Permission Set ideally)
  // then elite awards can be created.
  private Boolean canAddElite {get{
    if (canAddElite == null) {
      CustomPermission cpAllow = [
        SELECT Id 
        FROM CustomPermission 
        WHERE DeveloperName = 'Recognition_Coordinator'
        LIMIT 1
      ];
      Integer testCount = [
        SELECT COUNT()FROM PermissionSetAssignment
        WHERE PermissionSetId IN (SELECT ParentId
          FROM SetupEntityAccess
          WHERE SetupEntityType = 'CustomPermission' AND SetupEntityId = :cpAllow.Id)
        AND AssigneeId = :UserInfo.getUserId()
      ];
      
      canAddElite = (testCount == 0) ? false : true;
    }
    return canAddElite;
  }set;}
    
  
  public List<SelectOption> getNominationTypes() {
    List<SelectOption> so = new List<SelectOption>();
    so.add(new SelectOption('', 'Choose...'));
    so.add(new SelectOption('1', 'Profile Badge'));
    if (isPerson) {
      so.add(new SelectOption('2', 'Spot Award'));
    }
    so.add(new SelectOption('3', 'Half Year Award'));
    if (isPerson && canAddElite) {
      so.add(new SelectOption('4', 'Elite Award'));
    }
    return so;
  }
  
  public List<Nomination__c> getRecentNominationList() {
    return [
      SELECT Nominee__c, Nominee__r.Name, Nominee__r.SmallPhotoUrl, Nominee__r.FullPhotoUrl, Type__c, Nominee__r.Title 
      FROM Nomination__c
      WHERE Nominee__c != null
      ORDER BY CreatedDate DESC
      LIMIT 5
    ];
  }

  public NominationsLandingPageCntlr() {
    
  }
  
  public List<WorkBadgeDefinition> allBadges {
    get {
      if (allBadges == null) {
        allBadges = [
          SELECT Id, Name, IsCompanyWide, Description, ImageUrl, IsActive, Recognition_Category__c
          FROM WorkBadgeDefinition
          WHERE IsActive = true 
          AND IsCompanyWide = true
          ORDER BY Badge_Order__c ASC NULLS LAST, Name
        ];
      }
      return allBadges;
    }
    set;
  }
  
  public String badgeId {
    get {
      if (badgeId == null) {
        badgeId = allBadges.get(0).Id;
      }
      return badgeId;
    }
    set;
  }

  public PageReference submitNominationRec() {
    
    Boolean errorFound = false;
    errorMessages = new List<String>();
    confirmMessages = new List<String>();
    
    //checkJustifText(errorFound);
    
    if (String.isBlank(userIdMap.get(0))) {
      errorFound = true;
      errorMessages.add('Add someone to thank or reward.');
    }
    
    if (errorFound) {
      return null;
    }
    
    Savepoint sp = Database.setSavepoint();
    
    try {
      
      //if (isPerson == true) {
      if ('1'.equals(selectNomType)) {
        
        Map<String, Id> recipIdMap = new Map<String, Id>();
        Map<String, Id> giverIdMap = new Map<String, Id>();
        Map<String, String> msgMap = new Map<String, String>();
        Map<String, Id> badgeIdMap = new Map<String, Id>();
        system.debug(userIdMap);
        for (String uId : userIdMap.values()) {
          if (String.isNotBlank(uId)) {
            recipIdMap.put(uId, uId);
            giverIdMap.put(uId, UserInfo.getUserId());
            msgMap.put(uId, justificationText);
            badgeIdMap.put(uId, badgeId);
          }
        }

        Boolean allGood = NominationHelper.postBadgeToProfilesMany(
          recipIdMap,
          giverIdMap,
          msgMap,
          badgeIdMap,
          null
        );
        if (allGood) {
          resetForm();
          confirmMessages.add('Badge(s) posted to profile.');
        }
        else {
          
        }
      }
      // SPOT AWARD
      if ('2'.equals(selectNomType)) {
        if (!isPerson) {
          errorMessages.add('Spot Awards can only be awarded to individuals.');
          return null;
        }
        else {
          Nomination__c nom = new Nomination__c(
            Type__c = 'Level 2 - Spot Award',
            Status__c = nomStatus,
            Requestor__c = UserInfo.getUserId(),
            Nominee__c = (Id)userIdMap.get(0),
            Badge__c = (Id)badgeId,
            Justification__c = justificationText
          );
          if (String.isNotBlank(getSubordinatesJson()) && getSubordinatesJson().contains(nom.Nominee__c)) {
            nom.Spot_Award_Amount__c = spotAward;
          }
          try {
            insert nom;
            resetForm();
            confirmMessages.add('Spot Award awarded successfully.');
          }
          catch (Exception e) {
            system.debug(e);
          }
        }
      }
      if ('3'.equals(selectNomType)) {
        if (isPerson) {
          Nomination__c nom = new Nomination__c(
            Type__c = 'Level 3 - Half Year Nomination (Individual)',
            Status__c = nomStatus,
            Requestor__c = UserInfo.getUserId(),
            Nominee__c = (Id)userIdMap.get(0),
            Badge__c = (Id)badgeId,
            Justification__c = justificationText
          );
          try {
            insert nom;
            resetForm();
            confirmMessages.add('Nomination successfully added.');
          }
          catch (Exception e) {
            system.debug(e);
          }
        }
        else {
          // TEAM NOMINATION....
          try {
            
            List<Nomination__c> newNoms = new List<Nomination__c>();
            
            Nomination__c masterNomination = new Nomination__c(
              Type__c = 'Level 3 - Half Year Nomination (Team)',
              Status__c = nomStatus,
              Requestor__c = UserInfo.getUserId(),
              Justification__c = justificationText,
              Project_Sponsor__c = (Id)projectSponsor,
              Team_Name__c = teamName,
              Badge__c = (Id)badgeId
            );
            insert masterNomination;
            
            Set<Id> userIdNom = new Set<Id>();

            for (Integer i : userIdMap.keySet()) {
              if (String.isNotBlank(userIdMap.get(i))) {
                Id userId = userIdMap.get(i);
                if (!userIdNom.contains(userId)) {
                  userIdNom.add(userId);
                  newNoms.add(new Nomination__c(
                    Nominee__c = userId,
                    Justification__c = justificationText,
                    Type__c = masterNomination.Type__c,
                    Status__c = masterNomination.Status__c,
                    Requestor__c = masterNomination.Requestor__c,
                    Master_Nomination__c = masterNomination.Id,
                    Project_Sponsor__c = masterNomination.Project_Sponsor__c,
                    Badge__c = masterNomination.Badge__c
                  ));
                }
              }
            }
            
            insert newNoms;
            resetForm();
            confirmMessages.add('Nomination submitted successfully.');
          }
          catch (Exception ex) {
            Database.rollBack(sp);
            errorMessages.add(ex.getMessage());
          }
        }
      }
      if ('4'.equals(selectNomType)) {
        if (!isPerson) {
          errorMessages.add('Elite Winner can only be awarded to individuals.');
          return null;
        }
        else {
	        Nomination__c nom = new Nomination__c(
		        Type__c = 'Level 4 - Elite',
		        Status__c = nomStatus,
		        Requestor__c = UserInfo.getUserId(),
		        Nominee__c = (Id)userIdMap.get(0),
		        Badge__c = (Id)badgeId,
		        Justification__c = justificationText
		      );
		      try {
		        insert nom;
		        resetForm();
		        confirmMessages.add('Elite Winner awarded successfully.');
		      }
		      catch (Exception ex) {
            Database.rollBack(sp);
            errorMessages.add(ex.getMessage());
		      }
        }
      }
    }
    catch (Exception ex) {
      errorMessages.add(ex.getMessage());
    }
    return null;
  }
  
  public PageReference doNothing() {
    return null;
  }
  
  public Integer getUserIdMapSize() {
    return userIdMap.size()-1;
  }
  
  
  public PageReference addAnother() {
    if (String.isNotBlank(userIdMap.get(userIdMap.size()-1))) {
      userIdMap.put(userIdMap.size(), '');
    }
    // If the number of people is > 1 and the first one isn't empty, then we are in team mode.
    isPerson = (String.isNotBlank(userIdMap.get(0)) && userIdMap.size() > 1) ? false : true;
      
    return null;
  }
  
  public PageReference submitTeamNomination() {
    Savepoint sp = Database.setSavepoint();
    
    errorMessages = new List<String>();
    confirmMessages = new List<String>();
    
    // first, let's validate the input...
    Boolean errorFound = false;
    
    Boolean nomsFound = false;
    for (Integer userMapKey : userIdMap.keySet()) {
      if (String.isNotBlank(userIdMap.get(userMapKey))) {
        nomsFound = true;
        break;
      }
    }
    if (nomsFound == false) {
      errorMessages.add('No Nominees were added.');
      errorFound = true;
    }
    
    //checkJustifText(errorFound);
    
    if (String.isBlank(projectSponsor)) {
      errorFound = true;
      //errorMessages.add(Nomination__c.Project_Sponsor__c.getDescribe().getLabel()+' is missing.');
    }
    
    if (errorFound) {
      return null;
    }
    
    try {
      Nomination__c masterNomination = new Nomination__c(
        Type__c = nomTypeTeam,
        Status__c = nomStatus,
        Requestor__c = UserInfo.getUserId(),
        Justification__c = justificationText
        //,Project_Sponsor__c = (Id)projectSponsor
        //,Team_Name__c = teamName
        //,RecordTypeId = teamNominationRtId
      );
      insert masterNomination;

      List<Nomination__c> childNoms = new List<Nomination__c>();
      Set<Id> userIdNom = new Set<Id>();

      for (Integer i : userIdMap.keySet()) {
        if (String.isBlank(userIdMap.get(i))) {
          continue;
        }
        Id userId = userIdMap.get(i);
        if (!userIdNom.contains(userId)) {
          userIdNom.add(userId);
          childNoms.add(new Nomination__c(
            Nominee__c = userId,
            Justification__c = justificationText,
            Type__c = masterNomination.Type__c,
            Status__c = masterNomination.Status__c,
            Requestor__c = masterNomination.Requestor__c,
            Master_Nomination__c = masterNomination.Id
            //,RecordTypeId = singleNominationRtId
            //,Project_Sponsor__c = masterNomination.Project_Sponsor__c
          ));
        }
      }
      
      insert childNoms;
      
      resetForm();
      
      confirmMessages.add('Nomination submitted successfully.');
      
      // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Nomination Submitted Sucessfully')); // TODO: Make this a label.    
    }
    catch (Exception ex) {
      Database.rollBack(sp);
      errorMessages.add(ex.getMessage());
    }
    return null;
  }
  
  public PageReference resetForm() {
    justificationText = null;
    singleNominee = null;
    projectSponsor = null;
    teamName = null;
    userIdMap = new Map<Integer,String>{0 => ''};
    //justificationTextMissing = false;
    errorMessages = new List<String>();
    badgeId = allBadges.get(0).Id;
    confirmMessages = new List<String>();
    selectNomType = null;
    return null;
  }
  
  public List<User> getManyUserList() {
    return [
      SELECT Id, Name, Email, Manager.Name, Title, IsActive
      FROM User
      WHERE Id IN :userIdMap.values()
    ];
  }
  */
 
}