/*=============================================================================
 * Experian
 * Name: AutoProjectCopadoProject
 * Description: Creates a Copado Project and set it on edit mode for extra Copado data to be completed
 * Created Date: 07/24/2017
 * Created By: Diego Olarte
 *
 * Date Modified      Modified By           Description of the update
 * 
 
 * 
 =============================================================================*/
global with sharing class AutoProjectCopadoProject {
   
   webservice static String autoProjectCP(String proID) {

      String returnResult = '';
      
      Project__c originProject = [Select id,Name,Start_Date__c,Project_End_Date__c,Description__c,Owner.Id FROM Project__c WHERE id = :proID];
                                 
      //Save point
      Savepoint sp = Database.setSavepoint();
    
      List<copado__Project__c> projectList = new List<copado__Project__c>();
            
      try {
      
          copado__Project__c newAutoProjectCP = new copado__Project__c (
          
          Name = originProject.Name,
          SMC_Project__c = originProject.id,
          copado__Start_Date__c = originProject.Start_Date__c,
          copado__End_Date__c = originProject.Project_End_Date__c,
          copado__Description__c = originProject.Description__c,          
          copado__Index_Metadata__c = true);
          projectList.add(newAutoProjectCP);
          
          insert projectList;
                
          returnResult = 'Click OK to continue with a new Copado Project.';          
      }
      
      catch (Exception e) {
      Database.rollback(sp);
      returnResult = 'Cannot add project because of error: ' + e.getMessage();
      system.debug('[AutoProjectCopadoProject: AutoProjectCopadoProject] Exception: ' + e.getMessage());
      ApexLogHandler.createLogAndSave('AutoProjectCopadoProject','AutoProjectCopadoProject', e.getStackTraceString(), e);
    }
    return returnResult;
      
   }
}