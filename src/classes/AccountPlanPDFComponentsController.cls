/**=====================================================================
 * Appirio, Inc;
 * Name: AccountPlanPDFComponentsController
 * Description: 
 * Created Date: 28/01/2017
 * Created By:
 * 
 * Date Modified      Modified By                  Description of the update
 * Jan. 28th, 2017    James Wills                  Case #01999757 : Created.
 *  =====================================================================*/
public with sharing class AccountPlanPDFComponentsController {
  
  public ID AccountPlanObjId {get;set;}
  
  public Boolean showExperianGoals {get;set;}
  public Boolean showExperianStrategy {get;set;}
  public Boolean showClientsCoreBusiness {get;set;}
  public Boolean showCapexInOurDomain {get;set;}
  public Boolean showOpexInOurDomain {get;set;}
  public Boolean showClientsStrategy {get;set;}
  public Boolean showClientsObjectives {get;set;}
  public Boolean showClientsChanges {get;set;}
  public Boolean showClientsCompetitors {get;set;}
  public Boolean showClientsPartners {get;set;}
  public Boolean showExperianAnnualisedRevenue {get;set;}
  public Boolean showSummaryRecentHistory {get;set;}
  public Boolean showRecentRelationshipSuccess {get;set;}
  public Boolean showRelationshipObjNotAchieved {get;set;}
  public Boolean showRelationshipLessonsLearned {get;set;}
  public Boolean showAdditionalInformation {get;set;}
    
  public Account_Plan__c accountPlanObj {get;set;}  
  public Id radarChartImageId {get;set;}
  
  public List<Account_Plan_Opportunity__c> accPlanOppList {get;set;}
  
  public List<Account_Plan_Critical_Success_Factor__c> criticalSuccessFacList {get{
    return[SELECT Id, IsDeleted, Name, Account_Plan__c, Account_Plan_SWOT__c, Description__c, SWOT_1_Description__c, 
             SWOT_1_Importance__c, SWOT_1__c, SWOT_2_Description__c, SWOT_2_Importance__c, SWOT_2__c, SWOT_3_Description__c,
             SWOT_3_Importance__c, SWOT_3__c, SWOT_4_Description__c, SWOT_4_Importance__c, SWOT_4__c, SWOT_5_Description__c, 
             SWOT_5_Importance__c, SWOT_5__c 
      FROM Account_Plan_Critical_Success_Factor__c 
      WHERE Account_Plan__c = :accountPlanObj.Id];}  
  set;}
  
  public List<Account_Plan_Competitor__c> competitorsList {get{
    return [SELECT Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, 
             SystemModstamp, LastActivityDate, Account_Plan__c, Comp_Adv_1_Str__c, Comp_Adv_1__c, Comp_Adv_2_Str__c, 
             Comp_Adv_2__c, Comp_Adv_3_Str__c, Comp_Adv_3__c, Comp_Adv_4_Str__c, Competitor__r.name, Comp_Adv_4__c, 
             Comp_Adv_5_Str__c, Comp_Adv_5__c, Competitor__c, Exp_Adv_1_Str__c, Exp_Adv_1__c, Exp_Adv_2_Str__c, 
             Exp_Adv_2__c, Exp_Adv_3_Str__c, Exp_Adv_3__c, Exp_Adv_4_Str__c, Exp_Adv_4__c, Exp_Adv_5_Str__c, Exp_Adv_5__c,
             Additional_Information__c, Areas_competing_against_Experian__c, Client_History_with_Competitor__c
      FROM Account_Plan_Competitor__c 
      WHERE Account_Plan__c = :accountPlanObj.Id];}  
  set;}
  
  public List<Account_Plan_Team__c> accPlanTeamList {get{  
    return [SELECT Id, IsDeleted, Name,  Account_Plan__c, Job_Title__c, User__c, User__r.name 
           FROM Account_Plan_Team__c 
           WHERE Account_Plan__c = :accountPlanObj.Id];}  
  set;}
  
  public List<Task> taskList {get{    
    //Case #2154715 - Get a Map of the Account Plan Objectives for the Account Plan so that their Tasks can be added to taskList (see below)
    Map<Id,Account_Plan_Objective__c> accountPlanObjectiveIdMap = new Map<Id, Account_Plan_Objective__c>([SELECT id from Account_Plan_Objective__c WHERE Account_Plan__c = :accountPlanObj.Id]);
  
    return [SELECT Id, Owner.Name, What.Name, Subject, ActivityDate, Status, Priority, Completed_Date__c, Result__c, Investment__c 
           FROM Task 
           WHERE WhatId = :accountPlanObj.Id
           OR
           WhatId IN :accountPlanObjectiveIDMap.keySet()
           ORDER BY ActivityDate];}
  set;}
  
  
  public List<Note> accPlanNotesList{get{
   return [SELECT Id, Title, Body, Owner.Name, LastModifiedDate 
           FROM Note 
           WHERE ParentId = :accountPlanObj.Id];}
  set;}

    
  public AccountPlanPDFComponentsController() {
    accountPlanObjId = ApexPages.currentPage().getParameters().get('id') ;
    accountPlanObj = [SELECT Account__r.Name, Value__c, SystemModstamp, Summary_Recent_History__c, 
                     Strategic_Direction__c, Stability__c, Solutions__c, Risks__c, Revenue__c, 
                     Reference__c, Recent_Successes__c, Primary_Contact__c, Payment_Issues__c, OwnerId,
                     Opportunities__c, One_Experian__c, Objectives_Not_Achieved__c, Name, NPS__c, 
                     Major_Wins__c, Major_Partners__c, Major_Competitors__c, Major_Changes__c, 
                     Lessons_Learned__c, Last_Review_Date__c, LastModifiedDate, LastModifiedById, 
                     LastActivityDate, IsDeleted, Internal_Account_Team__c, Industry_Expertise__c, Id, 
                     Health_Status__c, Face_to_Face__c, Expiration__c, Experian_Vision_for_Account__c, Experian_Strategy_for_Account__c,
                     Experian_OPEX_Total__c, Experian_OPEX_Share__c, Experian_Capex_Total__c, Experian_CAPEX_share__c, 
                     Experian_Annualised_Revenue__c, Executive__c, CreatedDate, CreatedById,Core_Business__c, 
                     Contracts__c, Contact_Plan__c, Client_Plan__c, Business_Objectives__c, Annual_OPEX_in_Experian_Domain__c, 
                     Annual_CAPEX_in_Experian_Domain__c, Account__c, Account_Sector__c, Account_Industry__c, Additional_Information__c
                     FROM Account_Plan__c
                     WHERE Id = :accountPlanObjId
                     LIMIT 1];
    
    AccountPlanObjId=accountPlanObj.id; 
  }
    
   
  public void fetchRadarChartImageId(){
    for(Attachment radarChart : [Select Id From Attachment Where ParentId = :accountPlanObj.id and Name = 'RadarChart']){
      radarChartImageId = radarChart.Id;
    } 
  }
 
  
}