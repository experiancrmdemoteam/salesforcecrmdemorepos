public with sharing class StratComm_CaseAttachmentList_Controller {
	
	//public static List<Attachment> caseAttachments = new List<Attachment>();

	@AuraEnabled
    public static Boolean checkIfCaseIsClosed(Id caseID) {
        Case thisCase = [SELECT id, isClosed FROM Case WHERE id=:caseID];
        if (thisCase.isClosed)
        {
            return true;
        }
        return false;
    }

    @AuraEnabled
    public static List<caseAttachmentWrapper> findCaseAttachments(Id caseID) {
        TimeZone userTimezone = UserInfo.getTimeZone();
        List<caseAttachmentWrapper> caseAttachmentList = new List<caseAttachmentWrapper>();
        Case thisCase = [SELECT id, CaseNumber, (SELECT id, Name, CreatedBy.Name, CreatedDate, BodyLength FROM attachments) FROM Case WHERE id=:caseID];

        //caseAttachments = [SELECT id, Body, Name, CreatedBy.Name, CreatedBy.ID, CreatedDate, BodyLength, TYPE OF ParentID WHEN Case THEN CaseNumber END FROM Attachment WHERE ParentID =: caseID ORDER BY CreatedDate DESC];

        for(Attachment a: thisCase.attachments)
        {
            caseAttachmentWrapper caw = new caseAttachmentWrapper();
            caw.FileName = a.Name;
            caw.CreatedDate = a.CreatedDate.format('yyyy-MM-dd HH:mm:ss', userTimezone.getID());
            caw.CreatorName = a.CreatedBy.Name;
            caw.BodyLength = a.BodyLength;
            caw.ParentCase = thisCase.CaseNumber;
            
            caseAttachmentList.add(caw);
        }

        return caseAttachmentList;
    }

    /****Wrapper Classes***/
    public class caseAttachmentWrapper{
        @AuraEnabled
        public String FileName {get; set;}
        @AuraEnabled
        public String CreatorName {get; set;}
        @AuraEnabled
        public String CreatedDate {get; set;}
        @AuraEnabled
        public String AttachmentLink {get; set;}
        @AuraEnabled
        public Integer BodyLength {get; set;}
        @AuraEnabled
        public String ParentCase {get; set;}

        public caseAttachmentWrapper(){

        }
    }
}