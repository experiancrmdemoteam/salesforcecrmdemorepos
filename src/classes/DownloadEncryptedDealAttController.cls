/**=====================================================================
 * Experian
 * Name: DownloadEncryptedDealController
 * Description: Controller to manage the decryption of the attachments on Deal Records
 * Created Date: Oct 28th, 2015
 * Created By: Paul Kissick 
 * 
 * Date Modified      Modified By                Description of the update
 =====================================================================*/ 
public with sharing class DownloadEncryptedDealAttController {
  
  Id attId;
  transient Attachment attData; 
  transient Blob decKey;
  
  public DownloadEncryptedDealAttController() {
    attId = ApexPages.currentPage().getParameters().get('Id');
    attData = [SELECT Name,Body,ContentType,ParentId FROM Attachment WHERE Id = :attId];
    decKey = EncodingUtil.base64Decode([SELECT Encryption_Key__c FROM Deal__c WHERE Id = :attData.ParentId].Encryption_Key__c);
  }
  
  public String getAttName() {
    return attData.Name.removeEnd('.enc');
  }
  
  public String getAttType() {
    return attData.ContentType;
  }
  
  public String getAttBody() {
    return EncodingUtil.base64encode(Crypto.decryptWithManagedIV('AES256', decKey, attData.Body));
  }
  
}