/**=====================================================================
 * Experian
 * Name: FailureNotificationUtility_Test
 * Description: Case 01234035 - Test class for FailureNotificationUtility
 * Created Date: 9 Nov 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update

 =====================================================================*/
@isTest
private class FailureNotificationUtility_Test {

  static testMethod void noSettingsTest() {
    
    system.assertEquals(1, FailureNotificationUtility.retrieveRecipients('BlahBlah').size());
    system.assertEquals(FailureNotificationUtility.defaultNoRecipEmail, FailureNotificationUtility.retrieveRecipients('BlahBlah')[0]);
    
    system.assertEquals(1, FailureNotificationUtility.retrieveRecipients('').size());
    system.assertEquals(FailureNotificationUtility.defaultNoRecipEmail, FailureNotificationUtility.retrieveRecipients('')[0]);
    
  }
  
  static testMethod void globalSettingsTest() {
    
    String testEmail1 = 'test@email.com';
    String testEmail2 = 'test2@email.com';
    
    Global_Settings__c gs = new Global_Settings__c(Name = Constants.GLOBAL_SETTING, Batch_Failures_Email__c = testEmail1);
    insert gs;
    
    system.assertEquals(1, FailureNotificationUtility.retrieveRecipients('BlahBlah').size());
    system.assertEquals(testEmail1, FailureNotificationUtility.retrieveRecipients('BlahBlah')[0]);
    
    gs.Batch_Failures_Email__c = testEmail1 + ', '+testEmail2;
    update gs;
    
    system.assertEquals(2, FailureNotificationUtility.retrieveRecipients('BlahBlah').size());
    system.assertEquals(testEmail1, FailureNotificationUtility.retrieveRecipients('BlahBlah')[0]);
    system.assertEquals(testEmail2, FailureNotificationUtility.retrieveRecipients('BlahBlah')[1]);
    
  }
  
  static testMethod void specificClassTest() {
    
    String testEmail1 = 'test@email.com';
    
    Global_Settings__c gs = new Global_Settings__c(Name = Constants.GLOBAL_SETTING, Batch_Failures_Email__c = testEmail1);
    insert gs;
    
    Batch_Class_Failure_Notifications__c bcfn1 = new Batch_Class_Failure_Notifications__c(Name = 'BlahBlah', Recipients__c = testEmail1);
    
    system.assertEquals(1, FailureNotificationUtility.retrieveRecipients('BlahBlah').size());
    system.assertEquals(testEmail1, FailureNotificationUtility.retrieveRecipients('BlahBlah')[0]);
    
  }
  
  @testSetup
  static void setupTestData() {
    
  }
}