/**=====================================================================
 * Appirio, Inc
 * Name: EmailMessageTriggerHandler
 * Description: T-311459: Trigger to update Case Sub Origin with email address
 * Created Date: Aug 11th, 2014
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Sep 3rd, 2014                Arpita Bose(Appirio)         T-316539: Updated code to populate Sub_Origin__c with ToAddress     
 * Sep 16th, 2014               Mohit Parnami                T-318433: Updated code to resolve the error of Duplicate Ids.                                       
 * Oct 20, 2014                 Nathalie Le Guay (Appirio)   Add Database.DMLOptions to resend the Auto-Response rules
 * Nov 20, 2014                 Noopur                       Added fields in the case query, so that they can be updated.
 * Apr 16th, 2015               Nathalie Le Guay             I-154983: Populate the Requestor_Email__c regardless of RT
 * Apr 17th, 2015               Noopur                       Added code to remove semi-colon if there is only one email address in sub origin.
 * May 20th, 2015               Noopur                       Added a check to remove semi colon only if there is a single email Address in sub-origin
 * Jul 10th, 2015               Paul Kissick                 Case #601525 - Extracting quote.
 * Jan 5th,  2016               James Wills                  Case 01266714: Updated text to reflect updates to Case Record Types (see comment below).
 * Apr 25th, 2016               Paul Kissick                 Case 01794099: Adding support to set accountId from inbound email domain/address. 
                                                             Also fixed multiple updating on cases.
 * Dic 13th, 2016               Diego Olarte                 Case #01265676 - Add method for EDQ Hard close case
 * May 10th, 2017               Diego Olarte                 Case #02320808 - Add EDQ Region to fix assignment issue
 * Jun 20th, 2017               Tyaga Pati                   case #12880437 - Fixed Nested Loop Situation.  
  =====================================================================*/
public class EmailMessageTriggerHandler {

  // Map holding case records to update after the handler is finished processing.
  static Map<Id, Case> casesToUpdate = new Map<Id, Case>();
  
  // Special method to populate casesToUpdate (above) and set a single field.
  private static void setFieldToUpdate(Id caseId, String fieldName, Object fieldValue) {
    if (!casesToUpdate.containsKey(caseId)) {
      casesToUpdate.put(caseId, new Case(Id = caseId));
    }
    Case c = casesToUpdate.get(caseId);
    c.put(fieldName, fieldValue);
    casesToUpdate.put(caseId, c);
    return;
  }
  
  // Building static property to hold all case record types. By DO
  // Using describe calls translates the record type names, if translations exist.
  public static Map<Id, RecordType> caseRecordTypeMap { get {
    if (caseRecordTypeMap == null) {
      caseRecordTypeMap = new Map<Id, RecordType>([
        SELECT Id, Name, DeveloperName 
        FROM RecordType 
        WHERE sObjectType = 'Case'
      ]);
    }
    return caseRecordTypeMap;
  } set; }
  
  public static Map<String, Id> recordTypeNameToIdMap { get {
    if (recordTypeNameToIdMap == null) {
      recordTypeNameToIdMap = new Map<String, Id> ();
      for (RecordType rt : caseRecordTypeMap.values()) {
        recordTypeNameToIdMap.put(rt.Name, rt.Id);
      }
    }
    return recordTypeNameToIdMap;
  } set; }  

  //=========================================================================
  // After Insert Call
  //=========================================================================
  public static void afterInsert (List<EmailMessage> newList) {
    
    populateSubOriginOnCase (newList);
    populateQuoteFromEmail (newList);
    populateAccountOnCaseFromEmail (newList);
    createEDQCaseforHardClose (newList);
    
    updateCases ();  // New method to update any cases after insertion of email message
    
  }
  
  //===========================================================================
  // updateCases - Updates any cases that require field changes. Previously
  //  each method updated cases after each execution. This way only 1 update 
  //  call is required.
  //===========================================================================
  private static void updateCases() {
    // Force autoresponse email during update.
    Database.DMLOptions dlo = new Database.DMLOptions();
    dlo.EmailHeader.triggerAutoResponseEmail = true;

    List<Database.SaveResult> srList = Database.update(casesToUpdate.values(), dlo);
    for(Database.SaveResult sr : srList) {
      if (!sr.isSuccess()) {
        system.debug('Failed to save.');
      }
    }
    casesToUpdate.clear();
  }
  
  //===========================================================================
  // emailAddressToAccountId - property to hold all email addresses linked to
  //  accounts.
  //===========================================================================
  private static Map<String, Id> emailAddressToAccountId {get{
    if (emailAddressToAccountId == null) {
      emailAddressToAccountId = new Map<String, Id> ();
      for (Account_Email_Address__c aea : [SELECT Email_Address__c, Account__c 
                                           FROM Account_Email_Address__c 
                                           WHERE Email_Address__c != null]) {
        emailAddressToAccountId.put(aea.Email_Address__c.toLowerCase(), aea.Account__c);
      }
    }
    return emailAddressToAccountId;
  }set;}
  
  //===========================================================================
  // emailDomainToAccountId - property to hold all email domains linked to
  //  accounts. 
  // TODO: Improve this to support a better caching technique, e.g. platform
  //  cache.
  //===========================================================================
  private static Map<String, Id> emailDomainToAccountId {get{
    if (emailDomainToAccountId == null) {
      emailDomainToAccountId = new Map<String, Id> ();
      for (Account_Email_Address__c aea : [SELECT Email_Domain__c, Account__c 
                                           FROM Account_Email_Address__c 
                                           WHERE Email_Domain__c != null]) {
        emailDomainToAccountId.put(aea.Email_Domain__c.toLowerCase(), aea.Account__c);
      }
    }
    return emailDomainToAccountId;
  }set;}
  
  //===========================================================================
  // populateSubOriginOnCase - Method to populate Sub Origin on Case.
  //  Also sets the 
  //===========================================================================
  public static void populateSubOriginOnCase(List<EmailMessage> newEmailMsg) {
    
    Map<String, String> mapParentIdTotoAddress = new Map<String, String>();
    Map<String, List<Case>> mapRequestorEmailToCaseIds = new Map<String, List<Case>>();   
    
    try {
      // Noopur - Fetch the values from custom settings
      EMSSupportEmailAddresses__c emailAddresses = EMSSupportEmailAddresses__c.getValues('Default');

      List<String> emailAddsFound;

      for (EmailMessage msg: newEmailMsg) {
        // Only run for inbound case linked emails.
        if (msg.Incoming == true && msg.ParentId != null && msg.ParentId.getsObjectType() == Case.sObjectType) {
          emailAddsFound = new List<String>();
          emailAddsFound.add(msg.ToAddress);
          if (String.isNotBlank(msg.CcAddress)) {
            emailAddsFound.add(msg.CcAddress);
          }
          mapParentIdTotoAddress.put(msg.ParentId, String.join(emailAddsFound,';'));
        }
      }
      
      // Bail out if nothing found to update.
      if (mapParentIdTotoAddress.isEmpty()) {
        return;
      }
      
      Map<String,BusinessHours> businessHoursMap = new Map<String,BusinessHours>();
      for (BusinessHours hour : [SELECT Id, Name FROM BusinessHours]) {
        businessHoursMap.put(hour.Name, hour);
      }
      
      Map<String, Case> caseMap = new Map<String, Case>([
        SELECT Id, SuppliedEmail, RecordTypeId, BusinessHoursId,
               Business_Hours_Afternoon__c
        FROM Case 
        WHERE Id IN: mapParentIdTotoAddress.keySet()
      ]);

      for (Case newCase : caseMap.values()) {
        if (!mapRequestorEmailToCaseIds.containsKey(newCase.SuppliedEmail)) {
          mapRequestorEmailToCaseIds.put(newCase.SuppliedEmail, new List<Case>());
        }
        mapRequestorEmailToCaseIds.get(newCase.SuppliedEmail).add(newcase);
      }

      for (Case newCase : caseMap.values()) {
        //newCase.Sub_Origin__c = mapParentIdTotoAddress.get(newCase.Id);
        
        String subOrigin = mapParentIdTotoAddress.get(newCase.Id);
        Integer soLength = Case.Sub_Origin__c.getDescribe().getLength();
        
        setFieldToUpdate(newCase.Id, 'Sub_Origin__c', subOrigin.mid(0,soLength));
        
        Id bhId;
        Id bhAId;
        
        if (emailAddresses != null) {
          Set<String> subOrigins = new Set<String>(subOrigin.split(';'));
          if (String.isNotBlank(emailAddresses.France_Emails__c)) {
            for (String franceEmail : emailAddresses.France_Emails__c.split(',')) {
              if (subOrigins.contains(franceEmail)) {
                bhId = businessHoursMap.containsKey('France Morning Hours') ? businessHoursMap.get('France Morning Hours').Id: null;
                bhAId = businessHoursMap.containsKey('France Afternoon Hours') ? businessHoursMap.get('France Afternoon Hours').Id: null;
              }
            }
          }
          if (String.isNotBlank(emailAddresses.Germany_Emails__c) && bhId == null) {
            for (String germanyEmail : emailAddresses.Germany_Emails__c.split(',')) {
              if (subOrigins.contains(germanyEmail)) {
                bhId = businessHoursMap.containsKey('Germany') ? businessHoursMap.get('Germany').Id: null;
              }
            }
          }
          if (String.isNotBlank(emailAddresses.Spain_Emails__c) && bhId == null) {
            for (String spainEmail : emailAddresses.Spain_Emails__c.split(',')) {
              if (subOrigins.contains(spainEmail)) {
                bhId = businessHoursMap.containsKey('Spain') ? businessHoursMap.get('Spain').Id: null;
              }
            }
          }
        }
        setFieldToUpdate(newCase.Id, 'BusinessHoursId', bhId);
        setFieldToUpdate(newCase.Id, 'Business_Hours_Afternoon__c', bhAId);
      }

      for (User user : [Select Id, Email, Phone From User Where Email IN : mapRequestorEmailToCaseIds.keySet()]) {
        if (mapRequestorEmailToCaseIds.containsKey(user.Email)) {
          for (Case newCase : mapRequestorEmailToCaseIds.get(user.Email)) {
            system.debug('newCase--'+newCase);
            setFieldToUpdate(newCase.Id, 'User_Requestor__c', newCase.SuppliedEmail);
            setFieldToUpdate(newCase.Id, 'Requestor__c', user.Id);
            setFieldToUpdate(newCase.Id, 'Requestor_Email__c', user.Email);
            setFieldToUpdate(newCase.Id, 'Requestor_Work_Phone__c', user.Phone);
          }
        }
      }
      // Updates moved to end of execution (updateCases)
    } 
    catch (DMLException e) {
      ApexLogHandler.createLogAndSave('EmailMessageTriggerHandler','populateSubOriginOnCase', e.getStackTraceString(), e);
      for (Integer i=0; i < e.getNumDml(); i++) {
        newEmailMsg.get(0).addError(e.getDMLMessage(i)); 
      }
    }   
  }
  
  //===========================================================================
  // Case 601525 - Extract quote ID from the email in from CPQ
  //===========================================================================
  public static void populateQuoteFromEmail(List<EmailMessage> newList) {
    // Only find 18 character salesforce Ids (might find other things too)
    String quotePrefix = Quote__c.sObjectType.getDescribe().getKeyPrefix();
    Pattern idPattern = Pattern.compile(quotePrefix+'[a-zA-Z0-9]{15}');
    
    Map<Id, Id> caseToQuoteIdMap = new Map<Id, Id> ();

    String htmlBody;
    Id quoteId;
    List<String> possibleIds = new List<String>();
    
    for(EmailMessage msg : newList) {
      try {
        // Only check inbound, with the starting subject 'Approval Request', with an HTML body, which hopefully all those are on the emails.
          if (msg.Incoming == true && 
              String.isNotBlank(msg.Subject) && 
              msg.Subject.startsWithIgnoreCase('Approval Request') &&
              String.isNotBlank(msg.htmlBody)) {
            
            htmlBody = msg.htmlBody;
            quoteId = null;
            possibleIds = new List<String>();
            
            // We need to check all the found strings against the object prefix (might vary between systems!)
            Matcher idMatcher = idPattern.matcher(htmlBody);
            while(idMatcher.find()) {
              // Add each found match into the list to check against later.
              String matchFound = idMatcher.group();
              possibleIds.add(matchFound);
              system.debug(' ** Match found: '+matchFound);
            }
            if (!possibleIds.isEmpty()) {
              for(String possibleId : possibleIds) {
                if (possibleId.startsWith(quotePrefix)) {
                  try {
                    quoteId = (Id)possibleId;
                  }
                  catch (Exception e) {
                    ApexLogHandler.createLogAndSave('EmailMessageTriggerHandler','populateQuoteFromEmail', e.getStackTraceString(), e);
                  }
                }
              }
            }
            if (quoteId != null) {
              // Quote id must have been found, so update the parent case with the new Lookup
              setFieldToUpdate(msg.ParentId, 'CPQ_Quote__c', quoteId);
            caseToQuoteIdMap.put(msg.ParentId, quoteId);
            }
          }
      }
      catch (Exception e) {
        ApexLogHandler.createLogAndSave('EmailMessageTriggerHandler','populateQuoteFromEmail', e.getStackTraceString(), e);
      }
    }
    
    // Leave if no quote Ids found in body
    if (caseToQuoteIdMap.isEmpty()) {
      return;
    }
    
    // For every quote, get the account and opportunity, and reset these on the case?
    Map<Id,Quote__c> quoteDataMap = new Map<Id,Quote__c>([
      SELECT Id, Opportunity__c, Opportunity__r.AccountId 
      FROM Quote__c 
      WHERE Id IN :caseToQuoteIdMap.values()
    ]);
    
    // Leave if not quotes found
    if (quoteDataMap.isEmpty()) {
      return;
    }
    
    for (Id cId : caseToQuoteIdMap.keySet()) {
      if (caseToQuoteIdMap.containsKey(cId) && quoteDataMap.containsKey(caseToQuoteIdMap.get(cId))) {
        Quote__c q = quoteDataMap.get(caseToQuoteIdMap.get(cId));
        if (q.Opportunity__c != null) {
          setFieldToUpdate(cId, 'Opportunity__c', q.Opportunity__c);
        }
        if (q.Opportunity__r.AccountId != null) {
          setFieldToUpdate(cId, 'Account__c', q.Opportunity__r.AccountId);
        }
      }
    }
    // Updates to cases happens in the updateCases method.
  }
  
  //===========================================================================
  // populateAccountOnCaseFromEmail - 
  //===========================================================================
  public static void populateAccountOnCaseFromEmail (List<EmailMessage> newList) {
    for (EmailMessage eml : newList) {
      // Inbound, from address known, and linked to a case...
      if (eml.Incoming == true && eml.FromAddress != null && eml.ParentId != null && eml.ParentId.getsObjectType() == Case.sObjectType) {
        String emailAdd = eml.FromAddress.toLowerCase();
        if (emailAddressToAccountId.containsKey(emailAdd)) {
          // matched on email address to an account...
          setFieldToUpdate(eml.ParentId, 'AccountId', emailAddressToAccountId.get(emailAdd));
        }
        else {
          // extract domain from address:
          if (emailAdd.contains('@')) {
            String emlDomain = emailAdd.subStringAfterLast('@');
            if (emailDomainToAccountId.containsKey(emlDomain)) {
              setFieldToUpdate(eml.ParentId, 'AccountId', emailDomainToAccountId.get(emlDomain));
            }
          }
        }
      }
    }
  }
  
  //===========================================================================
  //Case #01265676 - Add method for EDQ Hard close case - Added by Diego O
  //Case #02320808 - Add EDQ Region to fix assigment issue - Added by Diego O  
  //case #12880437 - Fixed Nested Loop Situation. Tyaga Pati, Created Map, reviewed with Diego. Still meets requirements 
  //===========================================================================
  
  public static void createEDQCaseforHardClose(List<EmailMessage> newList) {
    
    Id caseRecordTypeId = recordTypeNameToIdMap.get(Constants.RECORDTYPE_CASE_EDQ_TECH_SUPPORT);
    
    List<Case>  newCase = new List<Case>();
    Map<Id,Case> closeCaseMap = new Map<Id,Case>();
    
    for(Case caseOnTask: [SELECT Id, isClosed, RecordTypeId, Status, ClosedDate, EDQ_Support_Region__c FROM Case WHERE isClosed = true AND ClosedDate <> LAST_N_DAYS:5 AND ClosedDate >= LAST_N_MONTHS:12 AND
                                    (
                                    Status = :Constants.CASE_STATUS_CLOSED_COMPLETE OR
                                    Status = :Constants.CASE_STATUS_CLOSED_RESOLVED OR Status = :Constants.CASE_STATUS_CLOSED_RESOLVED OR
                                    Status = :Constants.CASE_STATUS_CLOSED_NO_ACTION OR Status = :Constants.CASE_STATUS_CLOSED_PENDING_ACCEPTANCE OR
                                    Status = :Constants.CASE_STATUS_CLOSED_NO_ACTION OR Status = :Constants.CASE_STATUS_CLOSED_NO_ACTION OR
                                    Status = :Constants.CASE_STATUS_CLOSED_NO_ACTION OR Status = :Constants.CASE_STATUS_CLOSED_CANCELLED OR
                                    Status = :Constants.CASE_STATUS_CLOSED_NO_ACTION OR Status = :Constants.CASE_STATUS_CLOSED_NOT_RESOLVED OR
                                    Status = :Constants.CASE_STATUS_CLOSED_NOT_RESOLVED OR Status = :Constants.CASE_STATUS_CLOSED_ESCALATED) AND 
                                    RecordTypeId = :caseRecordTypeId]){
                                    
          closeCaseMap.put(caseOnTask.Id,caseOnTask);                      
     }//Added this bracket, to close this loop.                              
    
    for(EmailMessage eM: [SELECT Id, ParentId, Subject, TextBody, ToAddress, Parent.CaseNumber, Parent.Account.Name, Parent.Contact.Name, Parent.EDQ_Support_Region__c
                             FROM EmailMessage 
                             WHERE Id in: newList
                             AND Incoming = true]) 
                             //AND ParentId in :caseOnTask.Id]) //commented out this line
          {                            
            if(closeCaseMap.containsKey(eM.ParentId))
                {              
                    If(eM.ParentId.getsObjectType() == Case.sObjectType){
                        Case newEDQCase = new Case();
                        
                        newEDQCase.RecordTypeId = caseRecordTypeId;
                        newEDQCase.AccountId = eM.Parent.AccountId;
                        newEDQCase.ParentId = eM.ParentId;
                        newEDQCase.ContactId = eM.Parent.ContactId;
                        newEDQCase.Origin = 'Email';
                        newEDQCase.Subject = eM.Subject;
                        newEDQCase.Description = eM.TextBody;
                        newEDQCase.Priority = 'P3';
                        newEDQCase.Status = 'New';
                        newEDQCase.Sub_Origin__c = eM.ToAddress;
                        //newEDQCase.EDQ_Support_Region__c = caseOnTask.EDQ_Support_Region__c;
                        newEDQCase.EDQ_Support_Region__c = closeCaseMap.get(eM.ParentId).EDQ_Support_Region__c;
                        
                        newCase.add(newEDQCase); 
                     }                     
            }
            
            else{
                 continue;
            }
    }
    if(!newCase.isEmpty()){
        insert newCase;
       } 
  }
    
}