/**=====================================================================
 * Experian
 * Name: NominationTriggerHandler
 * Description: Handler class for NominationTrigger
                Before Insert : To make the Nominee's Manager as owner
 * Created Date: March 29th 2016
 * Created By: Richard Joseph
 *
 * Date Modified      Modified By           Description of the update
 * Apr 18th, 2016     Paul Kissick          Updated formatting
 * Sep 2nd, 2016      Diego Olarte          Replaced Old justification with new RichJustification__c field
 * Sep 5th, 2016      Paul Kissick          Fixed bug found in failed tests.
 * Spe 7th, 2016      Paul Kissick          Removed payrollEmailUpdate.
 * Sep 7th, 2016      Diego Olarte          No longer need to replace Justification
 * Sep 7th, 2016      Richard Joseph        Added to have only active users as Nomination owner.
 * Sep 13th, 2016     Paul Kissick          W-005721: Adding support for team nominations.
 * Sep 20th, 2016     Paul Kissick          W-005721: Added support to currency from payroll reporting and amount calculating
 * Oct 20th, 2016     Paul Kissick          W-006128: Add a badge when set to Rejected with Badge
 * Oct 21st, 2016     Paul Kissick          Various updates to support new status values. Also added Nominees_Manager__c to hold for emails.
 * Oct 25th, 2016     Tyaga Pati            Updated populateApprover function to capture the approver for the Level3 Panel Approval. This will override the current approver value.
 * Oct 31st, 2016     Paul Kissick          Added sharing for Nominators to see the records they create.
 * Nov 7th, 2016      Paul Kissick          W-006354: Prevent deletion unless a Recognition_Data_Admin user
 * Apr 20th,2017      Manoj Gopu            Case:02374799 : Adding a check if Manager is inactive or not available then the status will be Updated to Manager Not available.
 =====================================================================*/

public class NominationTriggerHandler {
  
  public static Boolean allowStatusUpdate = false;
  
  // Will be used to verify running user has isDataAdmin rights
  public static Boolean isDataAdmin = false;
  
  // Constants taken from NominationHelper class.
  public static String STATUS_SUBMITTED = NominationHelper.NomConstants.get('Submitted');
  public static String STATUS_PENDING_APPROVAL = NominationHelper.NomConstants.get('PendingApproval');
  public static String STATUS_APPROVED = NominationHelper.NomConstants.get('Approved');
  public static String STATUS_WON = NominationHelper.NomConstants.get('Won');
  public static String STATUS_REJECTED_BADGE = NominationHelper.NomConstants.get('RejectedBadge');
  public static String STATUS_REJECTED = NominationHelper.NomConstants.get('Rejected');
  public static String STATUS_DOWNGRADED_TO_L2 = NominationHelper.NomConstants.get('DowngradedToLevel2');
  public static String STATUS_NOMINATED = NominationHelper.NomConstants.get('Nominated');
  public static String STATUS_PENDINGMANAGER = NominationHelper.NomConstants.get('PendingManagerApproval');
  public static String STATUS_PENDINGPANEL = NominationHelper.NomConstants.get('PendingPanelApproval');
  public static String STATUS_MANAGERAPPROVED = NominationHelper.NomConstants.get('ManagerApproved');
  
  private static Set<Id> nominationBadgesAddedIdSet = new Set<Id>();
  
  // Map holding case records to update after the handler is finished processing.
  static Map<Id, Nomination__c> nominationsToUpdate = new Map<Id, Nomination__c>();
  
  public static void beforeInsert (List<Nomination__c> newList) {
    populateRecipientMngrAsOwnerDetail (newList, null);
    populateRecognitionCategory (newList);
    populateApprover (newList, null);
    populateCurrency (newList); // Always set the currency of the record to USD.
  }
  
  public static void afterInsert (List<Nomination__c> newList) {
    
    calculateTeamTotal (newList, null);
    updateCurrencyFromPayroll (newList, null);
    insertThanksRecords (newList, null);
    insertCustomSharing (newList);
    updateNominations();
  }
  
  public static void beforeUpdate (Map<Id,Nomination__c> oldMap, List<Nomination__c> newList) {
    populateRecipientMngrAsOwnerDetail (newList, oldMap);
    calculateRewardAmount (newList, oldMap);
    populateApprover (newList, oldMap);
    checkTeamIsApproved (newList, oldMap);
    
  }
  
  public static void afterUpdate (Map<Id,Nomination__c> oldMap, List<Nomination__c> newList) {
    updateCurrencyFromPayroll (newList, oldMap);
    checkForMasterNominationStatusChange (newList, oldMap);
    calculateTeamTotal (newList, oldMap);
    syncTeamMasterFields(newList, oldMap);
    updateChildNomsToNotifyNominees(newList, oldMap);
    
    insertNomineeSharing(newList, oldMap);
    
    insertThanksRecords(newList, oldMap);  // Call last
    
    updateNominations();
  }
  
  public static void beforeDelete (Map<Id, Nomination__c> oldMap) {
    preventDeletions(oldMap);
  }
  
  public static void afterDelete (Map<Id, Nomination__c> oldMap) {
    calculateTeamTotal(oldMap.values(), null);
  }
  
  
  // Special method to populate casesToUpdate (above) and set a single field.
  private static void setFieldToUpdate(Id nomId, Schema.SObjectField fieldName, Object fieldValue) {
    if (!nominationsToUpdate.containsKey(nomId)) {
      nominationsToUpdate.put(nomId, new Nomination__c(Id = nomId));
    }
    Nomination__c n = nominationsToUpdate.get(nomId);
    n.put(fieldName, fieldValue);
    nominationsToUpdate.put(nomId, n);
    return;
  }
  
  public static void updateNominations() {
    // Force autoresponse email during update.
    List<Nomination__c> tmpNomList = new List<Nomination__c>(nominationsToUpdate.values());
    nominationsToUpdate.clear();
    List<Database.SaveResult> srList = Database.update(tmpNomList);
    for(Database.SaveResult sr : srList) {
      if (!sr.isSuccess()) {
        system.debug('Failed to save.');
      }
    }
    
  }
  
  //===========================================================================
  // For single nominations, replace the Owner with the recipient manager.
  // For team nominations, set the Owner as the Project Sponsor.
  //===========================================================================
  private static void populateRecipientMngrAsOwnerDetail (List<Nomination__c> newList, Map<Id, Nomination__c> oldMap) {
    
    Set<Id> nomineeIdSet = new Set<Id>();
    Set<Id> childNomToUpd = new Set<Id>();
    
    for (Nomination__c nom : newList) {
      // If inserting and nominee is empty, then prepare to get the nominee managers.
      if (oldMap == null) {
        if (nom.Nominee__c != null) {
          nomineeIdSet.add(nom.Nominee__c);
        }
        if (nom.Project_Sponsor__c != null && nom.Master_Nomination__c == null) {
          // Hack to fix the team master record ownership on insertion
          nomineeIdSet.add(nom.Project_Sponsor__c);
        }
      }
      if (oldMap != null && 
          oldMap.get(nom.Id).Status__c != nom.Status__c &&
          STATUS_PENDING_APPROVAL.equals(nom.Status__c) &&
          STATUS_SUBMITTED.equals(oldMap.get(nom.Id).Status__c) &&
          nom.Master_Nomination__c != null) {
        nomineeIdSet.add(nom.Nominee__c);
        childNomToUpd.add(nom.Id);
      }
    }
    
    if (nomineeIdSet.isEmpty()) {
      return;
    }
    
    Map<Id,User> userMap = new Map<Id,User>([
      SELECT Id, IsActive, ManagerId, Manager.IsActive, Manager.ManagerId, Manager.Manager.IsActive
      FROM User 
      WHERE Id IN :nomineeIdSet
    ]); 
    
    for (Nomination__c nom : newList) {
      if (oldMap == null || childNomToUpd.contains(nom.Id)) {
        // First, check if the nominee's are active...
        if (nom.Nominee__c != null && 
            userMap.containsKey(nom.Nominee__c) && 
            userMap.get(nom.Nominee__c).IsActive == false) {
          nom.addError(system.Label.Recognition_Nominee_Inactive);
        }
        
        if (nom.Nominee__c != null && 
            userMap.containsKey(nom.Nominee__c) &&
            userMap.get(nom.Nominee__c).IsActive == true) {
          if (userMap.get(nom.Nominee__c).ManagerId != null) {
            // Next, set as the nominees manager, but only if they are active...
            if (userMap.get(nom.Nominee__c).Manager.IsActive == true) {
              nom.OwnerId = userMap.get(nom.Nominee__c).ManagerId;
            }
            else {
              if (userMap.get(nom.Nominee__c).Manager.ManagerId != null) {
                // Otherwise, set as the nominees managers manager, but only if they are active...
                if (userMap.get(nom.Nominee__c).Manager.Manager.IsActive == true) {
                  nom.OwnerId = userMap.get(nom.Nominee__c).ManagerId;
                }
              }
              else {
                // 2 levels of manager are inactive
                nom.OwnerId = nom.Requestor__c;
                nom.Status__c = 'Nominee Manager Not available';
              }
            }
            //TODO: Fix this as there's a chance we'll get an incorrect manager
            nom.Nominees_Manager__c = nom.OwnerId;
          }
          else {
            // Manager unknown.
            nom.OwnerId = nom.Requestor__c;
            nom.Status__c = 'Nominee Manager Not available';
          }
        }
          // When creating the Team Nominations, set the owner of all entries to the Project Sponsor.
          if (NominationHelper.NomConstants.get('Level3Team').equals(nom.Type__c) && !childNomToUpd.contains(nom.Id)){
            if (nom.Project_Sponsor__c != null) {
              nom.OwnerId = nom.Project_Sponsor__c;
            }
          }
      }
    }
  }
  
  //===========================================================================
  // Always set the Currency to USD
  //===========================================================================
  public static void populateCurrency (List<Nomination__c> newList) {
    for (Nomination__c n : newList) {
      n.CurrencyIsoCode = Constants.CORPORATE_ISOCODE;
    }
  }
  
  //===========================================================================
  // If/When the status is changed to Approved, set the Approver (running user)
  //===========================================================================
  public static void populateApprover (List<Nomination__c> newList, Map<Id,Nomination__c> oldMap) {
    for (Nomination__c nom : newList) {
      if ((oldMap == null || (oldMap != null && oldMap.get(nom.Id).Status__c != nom.Status__c)) &&
          (STATUS_APPROVED.equals(nom.Status__c) || 
           STATUS_PENDINGPANEL.equals(nom.Status__c) || 
           STATUS_WON.equals(nom.Status__c))) {

        nom.Approver__c = UserInfo.getUserId();
      }
    }
  }
   
  //===========================================================================
  // Take the Recognition Category from the Badge, and place on the Nomination.
  //===========================================================================
  public static void populateRecognitionCategory (List<Nomination__c> newList) {
    
    Set<Id> badgeIdSet = new Set<Id>();
    
    for (Nomination__c nom : newList) {
      if (nom.Badge__c != null) {
        badgeIdSet.add(nom.Badge__c);
      }
    }
    
    if (badgeIdSet.isEmpty()) {
      return;
    }
    
    Map<Id, WorkBadgeDefinition> allBadges = new Map<Id, WorkBadgeDefinition>([
      SELECT Id, Recognition_Category__c
      FROM WorkBadgeDefinition
      WHERE Id IN :badgeIdSet
      AND Recognition_Category__c != null
    ]);
    
    for (Nomination__c nom : newList) {
      if (nom.Badge__c != null && allBadges.containsKey(nom.Badge__c)) {
        nom.Recognition_Category__c = allBadges.get(nom.Badge__c).Recognition_Category__c;
      }
    }
    
  }
  
  //===========================================================================
  // When a master nomination is updated, set the child nominations as appropriate
  //===========================================================================
  public static void checkForMasterNominationStatusChange (List<Nomination__c> newList, Map<Id, Nomination__c> oldMap) {
    
    Set<Id> approvedMasterNomIds = new Set<Id>();
    Set<Id> wonMasterNomIds = new Set<Id>();
    Set<Id> rejectedBadgeNomIds = new Set<Id>();
    Set<Id> downgradedL2NomIds = new Set<Id>();
    Set<Id> nominatedMasterNomIds = new Set<Id>();
    Set<Id> rejectedNomIds = new Set<Id>();
    
    Set<Id> masterNomIds = new Set<Id>();
    
    for (Nomination__c nom : newList) {
      if (nom.Status__c != oldMap.get(nom.Id).Status__c && 
          (STATUS_PENDINGMANAGER.equals(nom.Status__c) || 
           STATUS_WON.equals(nom.Status__c) || 
           STATUS_REJECTED_BADGE.equals(nom.Status__c) || 
           STATUS_DOWNGRADED_TO_L2.equals(nom.Status__c) ||
           STATUS_NOMINATED.equals(nom.Status__c) ||
           STATUS_REJECTED.equals(nom.Status__c)
          ) && 
          nom.Master_Nomination__c == null &&
          NominationHelper.NomConstants.get('Level3Team').equals(nom.Type__c)) {
        if (STATUS_PENDINGMANAGER.equals(nom.Status__c)) {
          approvedMasterNomIds.add(nom.Id);
          masterNomIds.add(nom.Id);
        }
        if (STATUS_WON.equals(nom.Status__c)) {
          wonMasterNomIds.add(nom.Id);
          masterNomIds.add(nom.Id);
        }
        // W-??????
        if (STATUS_REJECTED_BADGE.equals(nom.Status__c)) {
          rejectedBadgeNomIds.add(nom.Id);
          masterNomIds.add(nom.Id);
        }
        // W-006151 - Downgrade master to L2
        if (STATUS_DOWNGRADED_TO_L2.equals(nom.Status__c)) {
          downgradedL2NomIds.add(nom.Id);
          masterNomIds.add(nom.Id);
        }
        // W
        if (STATUS_NOMINATED.equals(nom.Status__c)) {
          nominatedMasterNomIds.add(nom.Id);
          masterNomIds.add(nom.Id);
        }
        if (STATUS_REJECTED.equals(nom.Status__c)) {
          rejectedNomIds.add(nom.Id);
          masterNomIds.add(nom.Id);
        }
      }
    }
    
    if (masterNomIds.isEmpty()) {
      return;
    }
    
    List<Nomination__c> childNomsToUpd = [
      SELECT Id, Status__c, Master_Nomination__c, Type__c, 
             Master_Nomination__r.Rejection_Reason__c, Master_Nomination__r.Notes__c
      FROM Nomination__c
      WHERE Master_Nomination__c IN :masterNomIds
      AND Type__c = :NominationHelper.NomConstants.get('Level3Team')
    ];
    
    for (Nomination__c cNom : childNomsToUpd) {
      if (approvedMasterNomIds.contains(cNom.Master_Nomination__c)) {
        if (STATUS_SUBMITTED.equals(cNom.Status__c)) {
          setFieldToUpdate(cNom.Id, Nomination__c.Status__c, STATUS_PENDING_APPROVAL);
        }
      }
      if (wonMasterNomIds.contains(cNom.Master_Nomination__c)) {
        if (STATUS_MANAGERAPPROVED.equals(cNom.Status__c)) {
          setFieldToUpdate(cNom.Id, Nomination__c.Status__c, STATUS_WON);
        }
      }
      // Set the children to Rejected with Badge, also pull down the reason/notes
      if (rejectedBadgeNomIds.contains(cNom.Master_Nomination__c)) {
        if (STATUS_SUBMITTED.equals(cNom.Status__c)) {
          setFieldToUpdate(cNom.Id, Nomination__c.Rejection_Reason__c, cNom.Master_Nomination__r.Rejection_Reason__c);
          setFieldToUpdate(cNom.Id, Nomination__c.Notes__c, cNom.Master_Nomination__r.Notes__c);
          setFieldToUpdate(cNom.Id, Nomination__c.Status__c, STATUS_REJECTED_BADGE);
        }
      }
      // W-006151: Set children to L2 Spot if set to Downgraded
      if (downgradedL2NomIds.contains(cNom.Master_Nomination__c)) {
        setFieldToUpdate(cNom.Id, Nomination__c.Type__c, NominationHelper.NomConstants.get('Level2SpotAward'));
        setFieldToUpdate(cNom.Id, Nomination__c.Status__c, STATUS_PENDING_APPROVAL);
      }
      
      if (nominatedMasterNomIds.contains(cNom.Master_Nomination__c)) {
        // Only update Manager approved children
        if (STATUS_MANAGERAPPROVED.equals(cNom.Status__c)) {
          setFieldToUpdate(cNom.Id, Nomination__c.Status__c, STATUS_NOMINATED);
        }
      }
      
      // If set to Rejected, set all child records to rejected.
      // Also have to add the reason/notes from the master...
      if (rejectedNomIds.contains(cNom.Master_Nomination__c)) {
        if (STATUS_SUBMITTED.equals(cNom.Status__c)) {
          setFieldToUpdate(cNom.Id, Nomination__c.Rejection_Reason__c, cNom.Master_Nomination__r.Rejection_Reason__c);
          setFieldToUpdate(cNom.Id, Nomination__c.Notes__c, cNom.Master_Nomination__r.Notes__c);
          setFieldToUpdate(cNom.Id, Nomination__c.Status__c, STATUS_REJECTED);
        }
      }
    }
    
  }
  
  //===========================================================================
  // Apply badges and thanks to those that are approved.
  //===========================================================================
  public static void insertThanksRecords (List<Nomination__c> newList, Map<Id, Nomination__c> oldMap) {
    
    Map<String, Id> refToUserMap = new Map<String, Id>();
    Map<String, Id> refToGiverMap = new Map<String, Id>();
    Map<String, String> refToMessageMap = new Map<String, String>();
    Map<String, Id> refToBadgeMap = new Map<String, Id>();
    Map<String, Id> refToNomMap = new Map<String, Id>();
    
    Map<Id, Nomination__c> nomsToCheckAndBadge = new Map<Id, Nomination__c>();
    Map<Id, Nomination__c> level1Noms = new Map<Id, Nomination__c>();
    
    for (Nomination__c nom : newList) {
      // Status updated, or inserted in 'Approved', 'Won' state.
      if ((oldMap == null || (oldMap != null && oldMap.get(nom.Id).Status__c != nom.Status__c)) && 
          nom.Nominee__c != null && 
          String.isNotBlank(nom.Type__c) &&
          String.isNotBlank(nom.Status__c) &&
          nom.Add_Badge_Automatically__c == true &&
          !nominationBadgesAddedIdSet.contains(nom.Id)) {
        // Check this 'state' is valid on the badges we can auto apply
        if (NominationHelper.stateToBadge.containsKey(nom.Recognition_Category__c + ',' + nom.Status__c + ',' + nom.Type__c)) {
          nomsToCheckAndBadge.put(nom.Id, nom);
        }
        // W-006128 - Support for badging when status set to rejected with badge
        else if (STATUS_REJECTED_BADGE.equals(nom.Status__c)) {
          level1Noms.put(nom.Id, nom);
        }
      }
    }
    
    // Check the existing nominations, to ensure only 1 per status..
    for (WorkBadge bdgToCheck : [SELECT Id, Definition.Nomination_State__c, Nomination__c, Definition.Recognition_Category__c
                                 FROM WorkBadge
                                 WHERE Nomination__c IN :nomsToCheckAndBadge.keySet()
                                 AND Definition.Nomination_State__c != null 
                                 AND Definition.Recognition_Category__c != null]) {
      if (!nomsToCheckAndBadge.containsKey(bdgToCheck.Nomination__c)) {
        continue;
      }
      Set<String> nomStateSet = new Set<String>();
      for (String nomState : bdgToCheck.Definition.Nomination_State__c.split(';')) {
        nomStateSet.add(bdgToCheck.Definition.Recognition_Category__c + ',' + nomState);
      }
      Nomination__c tmpNom = nomsToCheckAndBadge.get(bdgToCheck.Nomination__c);
      if (nomStateSet.contains(tmpNom.Recognition_Category__c + ',' + tmpNom.Status__c + ',' + tmpNom.Type__c)) {
        nomsToCheckAndBadge.remove(bdgToCheck.Nomination__c);
      }
    }
    
    Set<Id> finalNomIdSet = new Set<Id>();
    finalNomIdSet.addAll(nomsToCheckAndBadge.keySet());
    finalNomIdSet.addAll(level1Noms.keySet());
    
    for (Id nomId : finalNomIdSet) {
      nominationBadgesAddedIdSet.add(nomId);
      Nomination__c tmpNom;
      if (nomsToCheckAndBadge.containsKey(nomId)) {
        tmpNom = nomsToCheckAndBadge.get(nomId);
      }
      if (level1Noms.containsKey(nomId)) {
        tmpNom = level1Noms.get(nomId);
      }
      if (tmpNom == null) {
        continue;
      }
      refToUserMap.put(tmpNom.Id, tmpNom.Nominee__c);
      refToGiverMap.put(tmpNom.Id, tmpNom.Requestor__c);
      refToMessageMap.put(tmpNom.Id, tmpNom.Justification__c);
      refToNomMap.put(tmpNom.Id, tmpNom.Id);
      // Apply the level specific badges, if applicable.
      refToBadgeMap.put(tmpNom.Id, (nomsToCheckAndBadge.containsKey(nomId) ? NominationHelper.stateToBadge.get(tmpNom.Recognition_Category__c + ',' + tmpNom.Status__c + ',' + tmpNom.Type__c) : tmpNom.Badge__c));
    }

    if (!refToUserMap.isEmpty()) {
      NominationHelper.postBadgeToProfilesMany(refToUserMap, refToGiverMap, refToMessageMap, refToBadgeMap, refToNomMap);
    }
    
  }
  
  //===========================================================================
  // If we insert, update or delete a nomination with a parent, prepare to 
  // retotal the parent 'Team Total'
  //===========================================================================
  public static void calculateTeamTotal (List<Nomination__c> newList, Map<Id, Nomination__c> oldMap) {
    
    Map<Id, Integer> totalChildNoms = new Map<Id, Integer>();
    Map<Id, Integer> pendingChildNoms = new Map<Id, Integer>();
    Map<Id, Integer> rejectedChildNoms = new Map<Id, Integer>();
    Map<Id, List<String>> nomineeNames = new Map<Id, List<String>>();
    
    for (Nomination__c nom : newList) {
      if ((oldMap == null || 
          (oldMap != null && oldMap.get(nom.Id).Status__c != nom.Status__c)) && 
          NominationHelper.NomConstants.get('Level3Team').equals(nom.Type__c) &&
          nom.Master_Nomination__c != null) {
        totalChildNoms.put(nom.Master_Nomination__c, 0);
        pendingChildNoms.put(nom.Master_Nomination__c, 0);
        rejectedChildNoms.put(nom.Master_Nomination__c, 0);
        nomineeNames.put(nom.Master_Nomination__c, new List<String>());
      }
    }
    
    if (totalChildNoms.isEmpty()) {
      return;
    }
    
    // Filter the nominations to the ones that have been approved by the nomination approvers
    for (List<Nomination__c> nomsToTotal : [SELECT Id, Master_Nomination__c, Status__c, Nominee__c, Nominee__r.Name
                                            FROM Nomination__c
                                            WHERE Master_Nomination__c IN :totalChildNoms.keySet()
                                            AND Type__c = :NominationHelper.NomConstants.get('Level3Team')
                                            /* AND Status__c IN (:STATUS_APPROVED, :STATUS_WON) */
                                            AND Master_Nomination__c != null]) {
      for (Nomination__c nom : nomsToTotal) {
        // SET TO PENDING
        if (STATUS_PENDING_APPROVAL.equals(nom.Status__c)) {
          pendingChildNoms.put(nom.Master_Nomination__c, pendingChildNoms.get(nom.Master_Nomination__c) + 1);
        }
        // SET TO REJECTED
        else if (STATUS_REJECTED.equals(nom.Status__c)) {
          rejectedChildNoms.put(nom.Master_Nomination__c, rejectedChildNoms.get(nom.Master_Nomination__c) + 1);
        }
        // NOT SUBMITTED
        else if (!STATUS_SUBMITTED.equals(nom.Status__c)) {
          totalChildNoms.put(nom.Master_Nomination__c, totalChildNoms.get(nom.Master_Nomination__c) + 1);
        }
        // Add the names into a list.
        nomineeNames.get(nom.Master_Nomination__c).add(nom.Nominee__r.Name);
      }
    }
    
    for (Id masterNomId : totalChildNoms.keySet()) {
      setFieldToUpdate(masterNomId, Nomination__c.Team_Total__c, totalChildNoms.get(masterNomId));
      setFieldToUpdate(masterNomId, Nomination__c.Team_Pending__c, pendingChildNoms.get(masterNomId));
      setFieldToUpdate(masterNomId, Nomination__c.Team_Rejected__c, rejectedChildNoms.get(masterNomId));
      setFieldToUpdate(masterNomId, Nomination__c.Team_Members_List__c, String.join(nomineeNames.get(masterNomId), ', '));
    }
    
  }
  
  //===========================================================================
  // Once, pending = 0 for master nom (and team total)
  //===========================================================================
  public static void checkTeamIsApproved(List<Nomination__c> newList, Map<Id, Nomination__c> oldMap) {
    
    for (Nomination__c nom : newList) {
      if (NominationHelper.NomConstants.get('Level3Team').equals(nom.Type__c) && 
          STATUS_PENDINGMANAGER.equals(nom.Status__c) && 
          nom.Master_Nomination__c == null &&
          nom.Team_Pending__c != null && nom.Team_Pending__c == 0 && 
          oldMap.get(nom.Id).Team_Pending__c != null && oldMap.get(nom.Id).Team_Pending__c > 0) {
        // pending is now zero
        nom.Status__c = STATUS_PENDINGPANEL;
        if (nom.Team_Rejected__c != null && nom.Team_Rejected__c > 0) {
          // Some have been set as rejected, so set status to something else to get the coords involved.
          nom.Notify_Coordinators__c = true;
        }
      }
    }
    
    
  }
  
  public static void updateChildNomsToNotifyNominees(List<Nomination__c> newList, Map<Id, Nomination__c> oldMap) {
    
    Set<Id> masterNomIds = new Set<Id>();
    
    for (Nomination__c nom : newList) {
      if (NominationHelper.NomConstants.get('Level3Team').equals(nom.Type__c) &&
          STATUS_PENDINGPANEL.equals(nom.Status__c) &&
          STATUS_PENDINGMANAGER.equals(oldMap.get(nom.Id).Status__c) &&
          nom.Master_Nomination__c == null) {
        // Master Ids to update children....
        masterNomIds.add(nom.Id);
      }
    }
    
    if (masterNomIds.isEmpty()) {
      return;
    }
    
    for (Nomination__c cNom : [SELECT Id, Notify_Nominees__c FROM Nomination__c WHERE Master_Nomination__c IN :masterNomIds]) {
      setFieldToUpdate(cNom.Id, Nomination__c.Notify_Nominees__c, true);
    }
    
  }
  
  
  //===========================================================================
  // Sets the currency on the nomination record from the payroll name
  // Oracle_Payroll_Name__c is populated by workflow, so has to run afterUpdate/insert
  //===========================================================================
  public static void updateCurrencyFromPayroll (List<Nomination__c> newList, Map<Id, Nomination__c> oldMap) {
    
    // Payrolls to get and update...
    Set<String> payrollNames = new Set<String>();
    Set<Id> nomIdsToUpdate = new Set<Id>();
    
    List<Nomination__c> updNomList = new List<Nomination__c>();
    
    for (Nomination__c nom : newList) {
      if (oldMap == null || (
           oldMap != null && (
            nom.Oracle_Payroll_Name__c != oldMap.get(nom.Id).Oracle_Payroll_Name__c
          ))) {
        if (String.isNotBlank(nom.Oracle_Payroll_Name__c)) {
          payrollNames.add(nom.Oracle_Payroll_Name__c);
          nomIdsToUpdate.add(nom.Id);
        }
      }
    }
    
    if (payrollNames.isEmpty() && nomIdsToUpdate.isEmpty()) {
      return;
    }
    
    Map<String, String> payrollToCurrency = new Map<String, String>();
    Map<String, Decimal> payrollToAutoGross = new Map<String, Decimal>();
    for (Employee_Recognition_Payroll__mdt payrollCfg : [SELECT Payroll_Reference__c, Currency__c, Auto_Gross_Percentage__c, No_Gross_Up_Possible__c
                                                         FROM Employee_Recognition_Payroll__mdt
                                                         WHERE Payroll_Reference__c IN :payrollNames]) {
      payrollToCurrency.put(payrollCfg.Payroll_Reference__c, payrollCfg.Currency__c);
      if (payrollCfg.No_Gross_Up_Possible__c) {
        payrollToAutoGross.put(payrollCfg.Payroll_Reference__c, payrollCfg.Auto_Gross_Percentage__c);
      }
    }
    
    for (Nomination__c nom : newList) {
      if (!nomIdsToUpdate.contains(nom.Id)) {
        continue;
      }
      setFieldToUpdate(nom.Id, Nomination__c.Payroll_Currency__c, (String.isNotBlank(nom.Oracle_Payroll_Name__c) && payrollToCurrency.containsKey(nom.Oracle_Payroll_Name__c)) ? payrollToCurrency.get(nom.Oracle_Payroll_Name__c) : Constants.CORPORATE_ISOCODE);
      setFieldToUpdate(nom.Id, Nomination__c.Payroll_Gross_Percentage__c, (String.isNotBlank(nom.Oracle_Payroll_Name__c) && payrollToAutoGross.containsKey(nom.Oracle_Payroll_Name__c)) ? payrollToAutoGross.get(nom.Oracle_Payroll_Name__c) : 0.0);
    }
  }
  
  //===========================================================================
  // 
  //===========================================================================
  public static void calculateRewardAmount (List<Nomination__c> newList, Map<Id, Nomination__c> oldMap) {
    Set<String> currenciesToConvert = new Set<String>();
    Set<Id> nomIdsToUpdate = new Set<Id>();
    
    // either pull the reward from the Half_Year_Reward__c field, or the Spot_Award_Amount__c field...
    // But check to see if there are changes on Half_Year_Reward__c, Spot_Award_Amount__c, Payroll_Currency__c 
    // or Award_Amount_in_User_Currency__c is not already populated. 
    for (Nomination__c nom : newList) {
      if (oldMap == null || 
          (oldMap != null && (
            oldMap.get(nom.Id).Half_Year_Reward__c != nom.Half_Year_Reward__c ||
            oldMap.get(nom.Id).Spot_Award_Amount__c != nom.Spot_Award_Amount__c ||
            oldMap.get(nom.Id).Payroll_Currency__c != nom.Payroll_Currency__c ||
            nom.Award_Amount_in_User_Currency__c == null
           ))) {
        if (nom.Half_Year_Reward__c != null || String.isNotBlank(nom.Spot_Award_Amount__c)) {
          currenciesToConvert.add(nom.Payroll_Currency__c);
          nomIdsToUpdate.add(nom.Id);
        }
      }
    }
    
    if (currenciesToConvert.isEmpty() && nomIdsToUpdate.isEmpty()) {
      return;
    }
    
    CurrencyUtility cu = new CurrencyUtility(currenciesToConvert);
    
    for (Nomination__c nom : newList) {
      if (!nomIdsToUpdate.contains(nom.Id)) {
        continue;
      }
      Decimal valueToConvert = 0;
      if (nom.Half_Year_Reward__c != null) {
        valueToConvert = nom.Half_Year_Reward__c;
      }
      // Only applicable to Level 2, Spot Awards.
      if (String.isNotBlank(nom.Type__c) && 
          NominationHelper.NomConstants.get('Level2SpotAward').equals(nom.Type__c) && 
          String.isNotBlank(nom.Spot_Award_Amount__c)) {
        String holdValue = nom.Spot_Award_Amount__c;
        if (nom.Spot_Award_Amount__c.startsWith('$')) {
          holdValue = nom.Spot_Award_Amount__c.removeStart('$');
        }
        try {
          valueToConvert = Decimal.valueOf(holdValue);
        }
        catch (Exception e) {
          nom.Spot_Award_Amount__c.addError('Problem converting amount: '+e.getMessage());
        }
      }
      if (valueToConvert != 0) {
        Decimal payrollValue = cu.convertCurrencyProper(Constants.CORPORATE_ISOCODE, valueToConvert, nom.Payroll_Currency__c, (nom.Payroll_Award_Date__c != null) ? nom.Payroll_Award_Date__c : nom.CreatedDate.date());
        Decimal exchangeRate = payrollValue.divide(valueToConvert, 6);
        
        payrollValue = payrollValue.round(System.RoundingMode.HALF_UP);
        nom.Award_Amount_in_User_Currency__c = payrollValue;
        nom.Exchange_Rate__c = exchangeRate;
      }
    }
    
  }
  
  //===========================================================================
  // If the H1_or_H2__c field changes on a master nomination, sync this on the children
  // Also update the Team_Members_List__c
  //===========================================================================
  public static void syncTeamMasterFields (List<Nomination__c> newList, Map<Id,Nomination__c> oldMap) {
    
    Set<Id> masterNomIdSet = new Set<Id>();
    
    for (Nomination__c nom : newList) {
      // Check for a team nom...
      if ((oldMap.get(nom.Id).Status__c != nom.Status__c ||
           oldMap.get(nom.Id).H1_or_H2__c != nom.H1_or_H2__c || 
           oldMap.get(nom.Id).Team_Members_List__c != nom.Team_Members_List__c) &&
          nom.Master_Nomination__c == null && 
          NominationHelper.NomConstants.get('Level3Team').equals(nom.Type__c)) {
        // H1_or_H2__c updated...
        masterNomIdSet.add(nom.Id);
      }
    }
    
    if (masterNomIdSet.isEmpty()) {
      return;
    }
    
    List<Nomination__c> nomsToUpdate = [
      SELECT Id, H1_or_H2__c, Master_Nomination__r.H1_or_H2__c, Team_Members_List__c, Master_Nomination__r.Team_Members_List__c
      FROM Nomination__c
      WHERE Master_Nomination__c IN :masterNomIdSet
    ];
    
    for (Nomination__c nom : nomsToUpdate) {
      setFieldToUpdate(nom.Id, Nomination__c.H1_or_H2__c, nom.Master_Nomination__r.H1_or_H2__c);
      setFieldToUpdate(nom.Id, Nomination__c.Team_Members_List__c, nom.Master_Nomination__r.Team_Members_List__c);
    }
   
  }
  
  //===========================================================================
  // When inserted, any child records should be shared with the project sponsor
  // Also, the nominee managers need access to edit/see the master record.
  // And, the nominee managers need access to see the other member records.
  // And, nominators need access to see their nominations
  //===========================================================================
  public static void insertCustomSharing (List<Nomination__c> newList) {
    
    NominationHelper.createNominationShares(newList);
  }
  
  public static void insertNomineeSharing (List<Nomination__c> newList, Map<Id, Nomination__c> oldMap) {
    
    List<Nomination__c> nomsToCheck = new List<Nomination__c>();
    
    for (Nomination__c nom : newList) {
      if ((oldMap == null || (oldMap != null && oldMap.get(nom.Id).Nominee_Viewable__c != nom.Nominee_Viewable__c)) && nom.Nominee_Viewable__c == true) {
        nomsToCheck.add(nom);
      }
    }
    
    if (!nomsToCheck.isEmpty()) {
      NominationHelper.createNomineeShares(nomsToCheck);
    }
    
  }
  
  public static void preventDeletions(Map<Id, Nomination__c> oldMap) {
    if (!NominationHelper.isRecogDataAdmin) {
      for (Id nomId : oldMap.keySet()) {
        oldMap.get(nomId).addError('Only Recognition Data Admins can delete.');
      }
    }
  } 
  
}