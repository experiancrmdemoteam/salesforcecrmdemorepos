/*=============================================================================
 * Experian
 * Name: BatchAssignPermissionSetToManagers_Test
 * Description: Case:02301636 : test Class for BatchAssignPermissionSetToManagers
 * Created Date: 03rd Mar 2017
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
            
 ============================================================================*/
@isTest
private class BatchAssignPermissionSetToManagers_Test{
static string testUserEmail = 'asfkljhowhjrefoi987@experian.com';
    static testMethod void myUnitTest(){
        Test.startTest();
        
            PermissionSet obj = [select id from PermissionSet where Name = 'EITS_Manager_Permission_Set' limit 1];
            Custom_Fields_Ids__c settings = Custom_Fields_Ids__c.getOrgDefaults();
            settings.EITS_Permission_set_id__c = obj.Id;
            upsert settings;
            
            List<User> listTestUsers = [SELECT Id, Days_since_Activity__c FROM User WHERE Email = :testUserEmail];
                        
            ScheduleAssignPermissionSetToManagers objS =new ScheduleAssignPermissionSetToManagers(); 
            objS.testUsers = listTestUsers;
            objS.execute(null);         
        
        Test.stoptest();
    }
    
      @testSetup
      static void setupData() {
      
        User testUserforInsert1 = Test_Utils.createUser('Standard User');
        testUserforInsert1.Email = testUserEmail;
        testUserforInsert1.Days_since_Activity__c = 1;
        insert testUserforInsert1;
        
        User testUserforInsert2 = Test_Utils.createUser('Standard User');
        testUserforInsert2.Email = testUserEmail;
        testUserforInsert2.ManagerId = testUserforInsert1.Id;
        insert testUserforInsert2;
      }
}