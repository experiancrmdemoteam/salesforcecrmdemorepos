/**=====================================================================
 * Experian, Inc
 * Name: BatchCreateExperianContactfromNewUser
 * Description: The following batch class is designed to be scheduled to run once every hour.
                    This class will get all new users and create a contact for them in the Experian Cross BU Account
 * Created Date: 15/01/2016
 * Created By: Diego Olarte (Experian)
 * 
 * Date Modified    Modified By      Description of the update
 * Feb 3rd, 2017    Sanket Vaidya    Case #02131111: Added method includeContactsInCampaign() to copy contacts in a campaign.
 =====================================================================*/
global class BatchCreateExperianContactfromNewUser implements Database.Batchable<sObject>, Database.stateful {

  global static Id experianBUAccId = Global_Settings__c.getValues(Constants.GLOBAL_SETTING).Experian_Cross_BU_Acc_Id__c;
  global static Id campaignInternalContactsId = Global_Settings__c.getValues(Constants.GLOBAL_SETTING).Campaign_Internal_Contacts_Employees__c;
  
  global List<String> insertErrors;
  global List<String> campaignMembersInsertErrors;

  global Database.Querylocator start ( Database.BatchableContext bc ) {
    if (insertErrors == null) insertErrors = new List<String>();
    if (campaignMembersInsertErrors == null) campaignMembersInsertErrors = new List<String>();

    return Database.getQueryLocator([
      SELECT Id, FirstName, LastName, Title, Department, Phone, MobilePhone, Email, EmployeeNumber,
             ManagerId, UserType, IsActive, SystemModStamp, Profile.Name
      FROM User 
      WHERE IsActive = true 
      AND UserType IN ('Standard','CsnOnly')
      AND Id NOT IN (SELECT SFDC_User__c FROM Contact WHERE AccountId = :experianBUAccId)
      AND SystemModStamp >= LAST_N_DAYS:1
    ]);
    
  }

  global void execute (Database.BatchableContext bc, List<User> newListID) {
     
     if (Global_Settings__c.getAll().containsKey(Constants.GLOBAL_SETTING)) {
    
      List<Contact> contactsToInsert = new List<Contact>();
      
      for(User u : newListID) {
        // Create users where no matching Id is found
        if (u.Contacts__r == null || (u.Contacts__r != null && u.Contacts__r.size() == 0))
          contactsToInsert.add(
            new Contact(
              AccountId = experianBUAccId, 
              FirstName = u.FirstName, 
              LastName = u.LastName, 
              Title = u.Title, 
              Department = u.Department, 
              Phone = (u.Phone != '-') ? u.Phone : '',
              MobilePhone = (u.MobilePhone != '-') ? u.MobilePhone : '',
              Email = u.Email,
              Employee_Number__c = u.EmployeeNumber,
              Current_Manager__c = u.ManagerId,              
              SFDC_User__c = u.Id
            )
          );
        }
    
      
      if(contactsToInsert.size() > 0)
      {
        List<Database.SaveResult> insertSave = Database.insert(contactsToInsert,false);
            for (Database.SaveResult sr : insertSave) {
              if (!sr.isSuccess()) {
                insertErrors.addAll(parseErrors(sr.getErrors()));
              }
            }
      }

      this.includeContactsInCampaign(contactsToInsert);
    }
  }
  
  global void includeContactsInCampaign(List<Contact> contactsToInsert)
  {
    Campaign cmpgn = [SELECT ID FROM CAMPAIGN WHERE ID =: campaignInternalContactsId];

    List<CampaignMember> campaignMembersToInsert = new List<CampaignMember>();

    for(Contact c : contactsToInsert)
    {      
      if(c.Id != null)
      {
        campaignMembersToInsert.add(new CampaignMember(CampaignId = cmpgn.Id, ContactId = c.Id));                
      }    
    }        

    if(campaignMembersToInsert.size() > 0)
    {
        List<Database.SaveResult> insertSave = Database.insert(campaignMembersToInsert,false);
        for (Database.SaveResult sr : insertSave)
        {
          if (!sr.isSuccess())
          {
            campaignMembersInsertErrors.addAll(parseErrors(sr.getErrors()));
          }
        }
    }
  }
  
  private static List<String> parseErrors(List<Database.Error> dbErrors) {
    List<String> errorsFound = new List<String>();
    for(Database.Error err : dbErrors) {
      errorsFound.add('Code: '+err.getStatusCode()+' - Message:'+err.getMessage()+' - Fields:'+String.join(err.getFields(),','));
    }
    return errorsFound;
  }

    //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchCreateExperianContactfromNewUser', false);
    
    String emailBody = '';
    
    if (insertErrors.size() > 0 || Test.isRunningTest()) {
      
      bh.batchHasErrors = true;
      
      if ( insertErrors.size() > 0) {
        emailBody += '\nThe following errors were observed when inserting Contacts:\n';
        emailBody += String.join(insertErrors,'\n');
      }

      if ( campaignMembersInsertErrors.size() > 0) {
        emailBody += '\n\n The following errors were observed when inserting CampaingMembers :\n';
        emailBody += String.join(campaignMembersInsertErrors,'\n');
      }
      
      bh.emailBody += emailBody;
    }
    
    bh.sendEmail();
  }
    
}