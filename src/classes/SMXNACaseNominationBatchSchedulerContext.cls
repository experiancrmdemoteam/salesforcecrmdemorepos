global class SMXNACaseNominationBatchSchedulerContext implements Schedulable{
    global SMXNACaseNominationBatchSchedulerContext(){}
    global void execute(SchedulableContext ctx){
        SMXNAProcessCaseBatch b = new  SMXNAProcessCaseBatch();
        if(!Test.isrunningTest()){
        database.executebatch(b); 
        }       
    }
    
}