/**
* @description A page controller for IT Service Knowledge visualforce page
* This controller controls what languages of IT Service articles will be displaying to the currently logged in user
* This controller also group articles by their selected date category and sort them by article title alphabetically within 
* each sub-category under IT Service
*
* @author Ryan (Weijie) Hu, UC Innovation
*
* @date 02/14/2017
*/

public with sharing class ExpComm_ITServiceKnowledgePageController {

    /* Private Controler properties */

    private final Map<String, Wrapper> nodeMap;
    private final String DATA_CATEGORY_GROUPNAME = 'Experian_Internal';
    private final Integer MAX_NUMBER_OF_COLOR = 5;
    private Map<String, String> dataCategoryAPINameAndLabelMap;
    private Map<String, CategoryArticleWrapper> catApiToWrapperMap {get; set;}


    public List<CategoryArticleWrapper> dataCatAndArticlesList {get {
            return catApiToWrapperMap.values();
        } set;}

    public ExpComm_ITServiceKnowledgePageController() {

        // Get current user(viewer)'s USER INFO
        Id currentUserId = UserInfo.getUserId();
        // Query for the current user's locale setting
        User currentUser = [SELECT Id, LanguageLocaleKey FROM USER WHERE Id =:currentUserId];

        // Query all the articles that matches current user's language
        String queryStr = 'SELECT ArticleNumber, ArticleType, Id, IsLatestVersion, KnowledgeArticleId, Title, UrlName'
                                        + ' , (SELECT Id, DataCategoryName FROM DataCategorySelections) FROM Employee_Article__kav'
                                        + ' WHERE PublishStatus = \'Online\' AND Language = \'' + currentUser.LanguageLocaleKey + '\' AND IsLatestVersion = true'
                                        + ' WITH DATA CATEGORY Experian_Internal__c BELOW IT_Services__c LIMIT 10000';


        List<Employee_Article__kav> articleVersionList = Database.query(queryStr);
        nodeMap = new Map<String, Wrapper>();
        dataCategoryAPINameAndLabelMap = new Map<String, String>();

        catApiToWrapperMap = new Map<String, CategoryArticleWrapper>();
        
        getDescribeDataCategoryGroupStructureResults(nodeMap);

        // Remove the top level, only need the sub levels
        nodeMap.remove('IT_Services');

        Integer colorCount = 0;
        for (String key : nodeMap.keySet()) {
            dataCategoryAPINameAndLabelMap.put(nodeMap.get(key).apiName, nodeMap.get(key).label);
            CategoryArticleWrapper newCatArtWrp = new CategoryArticleWrapper();
            newCatArtWrp.categoryApiName = nodeMap.get(key).apiName;
            newCatArtWrp.categoryLable = nodeMap.get(key).label;
            newCatArtWrp.categoryImageIcon = 'it_service_' + newCatArtWrp.categoryApiName + '.png';
            newCatArtWrp.setColorOption(Math.mod(colorCount, MAX_NUMBER_OF_COLOR));

            catApiToWrapperMap.put(nodeMap.get(key).apiName, newCatArtWrp);

            colorCount++;
        }

        groupingArticles(catApiToWrapperMap, articleVersionList);
    }


    /**
     * A helper method that helps group each article into their category group and sort them by title afterward.
     *
     * @param   catApiToWrapperMap           A map that contains the mapping of (category API name and CategoryArticleWrapper) pair
     * @param   articleVersionList           The list that contains all the articles.
     * @return  None.
     *
     */
    private void groupingArticles(Map<String, CategoryArticleWrapper> catApiToWrapperMap, List<Employee_Article__kav> articleVersionList) {
        for (Employee_Article__kav oneArticle : articleVersionList) {
            if (oneArticle.DataCategorySelections != null && oneArticle.DataCategorySelections.size() > 0) {
                
                for (Employee_Article__DataCategorySelection dataCatType : oneArticle.DataCategorySelections) {
                    
                    //Employee_Article__DataCategorySelection dataCatType = oneArticle.DataCategorySelections.get(0);                   
                    
                    if (dataCatType.DataCategoryName == 'IT_Services') {
                        // Found an article belongs to Main Category, add it too all sub-category under main-category
                        //for (String key : catApiToWrapperMap.keySet()) {
                        //    catApiToWrapperMap.get(key).addArticleToList(oneArticle);
                        //}
                    }
                    else {
                        // Add to sub-category list if it is under any of the direct sub-category
                        if (catApiToWrapperMap.get(dataCatType.DataCategoryName) != null) {                         
                            catApiToWrapperMap.get(dataCatType.DataCategoryName).addArticleToList(oneArticle);                          
                        }
                    }
                }
            }
        }

        // Sort the articles by title in each sub-category
        for (String key : catApiToWrapperMap.keySet()) {
            List<Employee_Article__kav> oneSubCatArticles = catApiToWrapperMap.get(key).articleVersionList;

            if (oneSubCatArticles != null && oneSubCatArticles.size() > 1) {
                // Only sort if it contains more than 1 article in this sub-category

                List<ArticleSorter> artSortList = new List<ArticleSorter>();
                for (Employee_Article__kav oneArti : oneSubCatArticles) {
                    artSortList.add(new ArticleSorter(oneArti));
                }

                artSortList.sort();

                // clear the original list
                catApiToWrapperMap.get(key).clearArticleList();

                // re-add the article in order
                for (ArticleSorter sortedArti : artSortList) {
                    catApiToWrapperMap.get(key).addArticleToList(sortedArti.articleVersion);
                }
            }
        }
    }

    /**
     * A helper method that helps to build the data category structure map
     *
     * @param   nodeMap           the nodeMap that passed in and used to store the structure information.
     * @return  None.
     *
     */
    private void getDescribeDataCategoryGroupStructureResults(Map<String, Wrapper> nodeMap){
        List<DescribeDataCategoryGroupResult> describeCategoryResult;
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
        try {
            //Making the call to the describeDataCategoryGroups to
            //get the list of category groups associated
            List<String> objType = new List<String>();
            objType.add('KnowledgeArticleVersion');

            describeCategoryResult = Schema.describeDataCategoryGroups(objType);

            //Creating a list of pair objects to use as a parameter
            //for the describe call
            List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();

            //Looping throught the first describe result to create
            //the list of pairs for the second describe call
            for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult){
                DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
                p.setSobject(singleResult.getSobject());
                p.setDataCategoryGroupName(singleResult.getName());
                pairs.add(p);
            }

            //describeDataCategoryGroupStructures()
            describeCategoryStructureResult = 
            Schema.describeDataCategoryGroupStructures(pairs, false);

            //Getting data from the result
            for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult){
                //Get name of the associated Sobject
                singleResult.getSobject();

                //Get the name of the data category group
                singleResult.getName();

                //Get the name of the data category group
                singleResult.getLabel();

                //Get the description of the data category group
                singleResult.getDescription();

                //Get the top level categories
                DataCategory [] toplevelCategories = 
                singleResult.getTopCategories();

                //Recursively get all the categories
                List<DataCategory> allCategories = getAllCategories(toplevelCategories);

                List<DataCategory> categoryToIterate = new List<DataCategory>();


                // For this controller's visualforce page, only interested in 'IT_Services' Data Category
                for(DataCategory category: allCategories){
                    if(category.getName().equalsIgnoreCase('IT_Services')){

                        categoryToIterate.add(category);
                    }
                }

                buildCategoryStructureTree(nodeMap, categoryToIterate, null);
            }
        } 
        catch (Exception e){
        }
    }
    
    /**
     * A helper method that helps the above 'getDescribeDataCategoryGroupStructureResults' method
     */
    private DataCategory[] getAllCategories(DataCategory [] categories){
        if(categories.isEmpty()){
            return new DataCategory[]{};
        } else {
            DataCategory [] categoriesClone = categories.clone();
            DataCategory category = categoriesClone[0];
            DataCategory[] allCategories = new DataCategory[]{category};
            categoriesClone.remove(0);
            categoriesClone.addAll(category.getChildCategories());
            allCategories.addAll(getAllCategories(categoriesClone));
            return allCategories;
        }
    }

    /**
     * A helper method that use the data category structure map to build a tree structure of the data category
     *
     * @param   nodeMap             the nodeMap that passed in and used to store the structure information.
     * @param   allCategories       All child categories of their parent node
     * @param   parentNode          Parent node of the child categories
     * @return  None.
     *
     */
    private void buildCategoryStructureTree(Map<String, Wrapper> nodeMap, List<DataCategory> allCategories, Wrapper parentNode) {
        for (DataCategory oneCategory : allCategories) {

            Wrapper currentNode = new Wrapper();
            currentNode.label = oneCategory.getLabel();
            currentNode.apiName = oneCategory.getName();

            if (parentNode != null) {
                currentNode.parent = parentNode;
            }

            nodeMap.put(oneCategory.getName(), currentNode);

            List<DataCategory> children = oneCategory.getChildCategories();

            if (children.isEmpty() == false) {
                buildCategoryStructureTree(nodeMap, children, currentNode);
            }
        }
    }

    /**
     * A Wrapper class that helps to build the tree structure of the data categories
     *
     */
    public class Wrapper {
        public String label {get; set;}
        public String apiName {get; set;}
        public Wrapper parent {get; set;}
        public List<Wrapper> children {get; set;}

        public Wrapper() {
            children = new List<Wrapper>();
        }
    }


    /**
     * A Wrapper class that helps to show all sub data categories on visualforce page
     *
     */
    public class CategoryArticleWrapper {
        public String categoryApiName {get; set;}
        public String categoryLable {get; set;}
        public List<Employee_Article__kav> articleVersionList {get; set;}
        public Integer colorOption {get; set;}
        public String categoryImageIcon {get; set;}

        public CategoryArticleWrapper() {
            articleVersionList = new List<Employee_Article__kav>();
        }

        public void addArticleToList(Employee_Article__kav oneArticle) {
            articleVersionList.add(oneArticle);
        }

        public void setColorOption(Integer color) {
            colorOption = color;
        }

        public void clearArticleList() {
            articleVersionList.clear();
        }
    }

    /**
     * A Helper class that helps to sort the article list by article title
     *
     */
    public class ArticleSorter implements Comparable {
        public final Employee_Article__kav articleVersion;
        public final String recordTitle;

        public ArticleSorter(Employee_Article__kav articleVersion) {
            this.articleVersion = articleVersion;

            if (articleVersion.Title == null) {
                recordTitle = '';
            }
            else {
                recordTitle = articleVersion.Title;
            }
        }

        public Integer compareTo(Object compareTo) {
            ArticleSorter cmt = (ArticleSorter) compareTo;

            String articleTitle = cmt.recordTitle;
            
            if (articleTitle == null) {
                articleTitle = '';
            }

            return recordTitle.compareTo(articleTitle);
        }
    }
}