/**=====================================================================
 * Appirio, Inc
 * Name: UserTriggerHandler
 * Description: T-264687: To update ARIABillingAccount
 * Created Date: Apr 2nd, 2014
 * Created By: Naresh Kr Ojha (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Apr 9th, 2014                Arpita Bose(Appirio)         T-269989: Added method validateCurrencyFieldsOnUser() to
 *                                                           keep User's standard and custom Currency field in sync.
 * Aug 30th, 2014               Richard Joseph               Modified to created CPQ user and Update the CPQ User.
 *                                                           Call SFDCToCPQUserServiceClass.
 * Oct 06th, 2014               Naresh kr Ojha               T-322915: User validation for EDQ user to populate Office location,
 *                                                           added method validateEDQUserFields()
 * Oct 07th, 2014               Naresh Kr Ojha               Removed soql from validateEDQUserFields method.
 
 * Feb 2nd, 2015                Tyaga Pati                   Added Code to make track Loggined In User and Date when Activation and Deactivation occur.
 * Mar 27th, 2015               Arpita Bose                  T-373930: Added method recalculateSegmentFieldValues() and isChangedAnyField()
 * Apr 01st, 2015               Arpita Bose                  T-373930: Updated method recalculateSegmentFieldValues() to call AccountSegmentationUtility.updateOpportunitySegments()
 * Apr 08th, 2015               Arpita Bose                  T-373930: Added method createAccSegmentForNewAccount_User()
 * April 10th 2015              Richard Joseph               Added validation to skip null/empty CPQ User Type during deactivation of user.
 * April 10th 2015              Richard Joseph               Added condition to verify if Bill Plan field is changed or not.
 * April 28th, 2015             James Weatherall             Case #528796 Created two new methods createExperianContact(), updateExperianContactManager()
 *                                                           to create Contacts on the Experian BU Account and to update the Manager when changed on the User
 * Jul 8th, 2015                Paul Kissick                 Case #944218 - Adding changes to prevent mixed DML exceptions
 * Sep 14th, 2015               Noopur                       Added check to bypass the Account Segment functionality if the UserReOrgProcessed is checked
 * Sep 30th, 2015               Paul Kissick                 Added code from SaaSDev Richard Joseph added (updateCommunityContactInactive,updateCommunityContact)
 * Jan 14th, 2016               James Wills                  Case #01252420 - Added two new methods (checkIfIsactiveChanged and reAssessTeams) that are used to activate the ContactTeamTrigger if a User 
 *                                                           associated with a Contact Team (Contact Relationship) is de-activated/activated.  This in turn re-calculates the Contact parent field 
 *                                                           Active_Contact_Relationship_Count__c.  The method reAssessTeams uses the @future annotation to avoid the mixed DML error.
 * Jan 15th, 2016               Diego Olarte                 Case #01135171 Added new method updateExperianContact to update changes to the user's related contact
 * Jun 13th, 2016               Diego Olarte                 Case #01864697 Updated method updateExperianContact to update contact Additional Status field
 * Jul 19 2016                  Paul Kissick                 CRM2:W-005406: Cleaned up class a bit
 * Aug 9, 2016                  Cristian Torres              CRM2:W-005402: Re-define Contact Status
 * Aug 22nd, 2016               Paul Kissick                 CRM2:W-005402: Cleaned up inefficient methods
 * Aug 26th, 2016               Tyaga Pati                   CRM2:W-005402: Removed Reference to Inactive flag on Contact.
 * Sep 5th, 2016                Paul Kissick                 CRM2:W-005402: Fixed contact status for SFDC users
 * Fep 17th,2016                Richard Joseph               W-006487:To capture community Contact user activation.  
 * Apr 4th, 2017                Esteban Castro               Case: 02297812 - Please add custom fields to CPQ to enable better management of the approver listing
 * June 6th. 2017               Mauricio Murillo             Case  02191630: Updated updateExperianContact method to avoid MIXED DML error
 =====================================================================*/
public with sharing class UserTriggerHandler {
  
  // PK Case #944218 - Only run update once (was executing twice due to workflow updates)
  public static Boolean isAfterUpdateTriggerExecuted = false;
  
  //===========================================================================
  //After update
  //===========================================================================
  public static void afterUpdate(List<User> newList, Map<ID, User> oldMap) {
    // PK Case #944218 - Only run update once
    if (isAfterUpdateTriggerExecuted) {
      return;
    }
    isAfterUpdateTriggerExecuted = true;
    
    updateARIABillingAccounts(newList, oldMap);
    //[RJ]Changes by Richard Starts
    if (newList.size() == 1) { //Size is 1 since CPQ webservice can't handle bulk updates
      cpqUserUpdate(newList, oldMap); 
    }
    createAccSegmentForNewAccount_User(newList, oldMap);
    // T-373930
    recalculateSegmentFieldValues(newList, oldMap);
    //[RJ]To unmark the community contact flag in contact when community user is made inactive
    updateCommunityContactInactive(newList, oldMap);
    
    //Case #01252420 - Creation of a Contact DQ score field on the Contact object - James Wills
    checkIfIsactiveChanged( newList, oldMap); //DO Commented out 20160117 as giving error ContactTeamTriggerHandler does not exists - uncommented James Wills 20170118 after code update.
    
    //Case #01135171 : Diego Olarte Method to update the contact
    updateExperianContact(newList);
      
    //W-005402: Re-define Contact Status (Contact.User_SFDC__c)   
    redefineContactStatus(newList, oldMap);
  }
  
  //===========================================================================
  //Before Insert
  //===========================================================================
  public static void beforeInsert(List<User> newList){
    validateEDQUserFields(newList, null);
  }
  
  //===========================================================================
  //After Insert
  //===========================================================================
  public static void afterInsert(List<User> newList){
    validateCurrencyFieldsOnUser(newList);
    
    //[RJ]Changes by Richard Starts
    if (newList.size() == 1) {//Size is 1 since CPQ webservice can't handle bulk updates
      cpqUserCreate(newList);
    }
    
    //Richard created this below method to identify Community user contact.
    updateCommunityContact(newList);
    
  }
  
  //===========================================================================
  //Before Update
  //===========================================================================
  public static void beforeUpdate(Map<ID, User> newMap, Map<ID, User> oldMap){
    validateCurrencyFieldsOnUser(newMap, oldMap);
    validateEDQUserFields(newMap.values(), oldMap);
    
    updtUsrActivtDeactivtInfo(newMap.values(), oldMap); // TP Feb 2nd, 2015
      

  }

   

      
  //[RJ]Changes by Richard
  //===========================================================================
  //To Create user record in CPQ
  //===========================================================================
  private static void cpqUserCreate(List<User> newList) {
    for (User userRec : newList) {
      if (userRec.CPQ_User__c && userRec.CPQ_User_Type__c != null && userRec.isActive == true) {
        SFDCToCPQUserServiceClass.callCPQUserAdminSerivceAsync(userRec.Id);
      }
    }
  }

  //=====================Tyaga Pati==================================================
  //This Function will update the User Record with Details of the User Name 
  //and Time when perticular user is being Activated or Deactivated.
  //=======================================================================
  private static void updtUsrActivtDeactivtInfo(List<User> newList, Map<ID, User> oldMap) {
    List<User> newActvtdUsr = new List<User>();
    for (User usr : newList) {
      if (oldMap != null && oldMap.get(Usr.Id).IsActive == Usr.IsActive) {
        continue;
      }
      else {
        newActvtdUsr.add(usr);
      }
    }
    
    if (newActvtdUsr.size() > 0) {
      for (User usrNew: newActvtdUsr) {
        if (usrNew.IsActive) {
          usrNew.Date_Reactivated__c = date.today();
          usrNew.Reactivate_By_T__c = UserInfo.getName();
        }
        else if (!usrNew.IsActive) {
          usrNew.Date_Deactivated__c= date.today();
          usrNew.Deactivated_By_T__c = UserInfo.getName();
        }   
      }
    }
  } 
  // End Of Function to Update User Record during Activation and DeActivation 

  //===========================================================================
  //To update user record in CPQ
  //===========================================================================
  private static void cpqUserUpdate(List<User> newList, Map<ID, User> oldMap) {
    for (User userRec : newList) {
      if ( userRec.CPQ_User__c <> oldMap.get(userRec.ID).CPQ_User__c || 
              (userRec.CPQ_User__c && (
              userRec.CPQ_User_Type__c <> oldMap.get(userRec.ID).CPQ_User_Type__c ||
              userRec.isActive <> oldMap.get(userRec.ID).IsActive ||
              userRec.Country__c <> oldMap.get(userRec.ID).Country__c ||
              userRec.LastName <> oldMap.get(userRec.ID).LastName ||
              userRec.FirstName <> oldMap.get(userRec.ID).FirstName|| 
              userRec.email <> oldMap.get(userRec.ID).email ||
              userRec.Street<> oldMap.get(userRec.ID).Street ||
              userRec.city <> oldMap.get(userRec.ID).city ||
              userRec.state <> oldMap.get(userRec.ID).state ||
              userRec.phone <> oldMap.get(userRec.ID).phone ||
              userRec.Region__c <> oldMap.get(userRec.ID).Region__c ||
              userRec.Managers_Name__c <> oldMap.get(userRec.ID).Managers_Name__c || // EC Case: 02297812
              userRec.Sales_Sub_Team__c <> oldMap.get(userRec.ID).Sales_Sub_Team__c || // EC Case: 02297812
              userRec.CSDA_Bill_Plan_User__c <> oldMap.get(userRec.ID).CSDA_Bill_Plan_User__c || //  RJ - Added CSDA Bill Plan field.
              userRec.CSDA_Create_Quote_User__c <> oldMap.get(userRec.ID).CSDA_Create_Quote_User__c || //  RJ - Added CSDA Create Quote user field.
              userRec.PostalCode <> oldMap.get(userRec.ID).PostalCode))){
        SFDCToCPQUserServiceClass.callCPQUserAdminSerivceAsync(userRec.Id);
      }
    }
  }
  //[RJ]Changes ends  
  
  
  //===========================================================================
  //T-264687: To Update ARIA billingAccounts 
  //===========================================================================
  // PK: Case #944218 - Making into future method to prevent Mixed DML
  private static void updateARIABillingAccounts (List<User> newList, Map<Id, User> oldMap) {
    Set<ID> userIDs = new Set<ID>();
    //check whether User updated for Business_Unit__c
    for (User u : newList) {
      if (u.Business_Unit__c != null && u.Business_Unit__c != oldMap.get(u.ID).Business_Unit__c) {
        userIDs.add(u.ID);
      }
    }
    if (!userIDs.isEmpty()) {
      updateARIABillingAccountsFuture(userIDs);
    }
  }
  
  // PK: Case #944218 - Making future method to prevent Mixed DML
  public static void updateARIABillingAccountsFuture(Set<Id> userIds) {
    Map<ID, ARIA_Billing_Account__c> ARIABillingAccountMap = new Map<ID, ARIA_Billing_Account__c>();
    for (ARIA_Billing_Account__c billingAccount : [SELECT ID, Push_To_Aria__c, SendBillingAccntToAria__c 
                                                   FROM ARIA_Billing_Account__c 
                                                   WHERE Owner__c IN :userIDs 
                                                         AND Push_To_Aria__c =: Constants.PICKLISTVAL_YES]) {
      billingAccount.SendBillingAccntToAria__c = true;
      ARIABillingAccountMap.put(billingAccount.ID, billingAccount);
    }
    if (!ARIABillingAccountMap.isEmpty()) {
      update ARIABillingAccountMap.values();
    }
  }
  
  
  //===========================================================================
  //T-269989: Validation to keep User's standard and custom Currency field in sync
  //===========================================================================
  private static void validateCurrencyFieldsOnUser(List<User> newList) {
    for (User u : newList) {
      if (String.isNotBlank(u.Currency__c) && !u.Currency__c.equalsIgnoreCase(u.DefaultCurrencyIsoCode.substringBefore('-'))) {
        u.addError(Label.USER_CURRENCY_FIELDS_MUST_MATCH); 
      }
    }
  }
  
  //===========================================================================
  //T-269989: Validation to keep User's standard and custom Currency field in sync
  //===========================================================================
  private static void validateCurrencyFieldsOnUser(Map<ID, User> newMap, Map<ID, User> oldMap) {
    for(User u : newMap.values()) {
      if (String.isNotBlank(u.Currency__c) && !u.Currency__c.equalsIgnoreCase(u.DefaultCurrencyIsoCode.substringBefore('-'))) {
        u.addError(Label.USER_CURRENCY_FIELDS_MUST_MATCH); 
      }
    }
  }
  
  //===========================================================================
  //T-322915: Validate users to have Office location if user is EDQ user
  //===========================================================================
  private static void validateEDQUserFields(List<User> newList, Map<Id, User> oldMap) {

    Map<String, String> bu_GroupMap = new Map<String, String>();
    Set<String> businessUnitSet = new Set<String>();
    Map<String, User> userMap = new Map<String, User>();

    //if (oldMap != null) {
    for (User u : newList) {
      if (u.Business_Unit__c != null) {
        businessUnitSet.add(u.Business_Unit__c);
      }
      if (oldMap != null) {
        userMap.put(u.ID, u);
      }
    }

    //If no business unit filled return
    if (businessUnitSet.isEmpty()) {
      return;
    }

    bu_GroupMap = BusinessUnitUtility.getGroupNameForBusinessUnits(businessUnitSet);

    for (User u : newList) {
      if (oldMap != null && 
          userMap.containsKey(u.ID) && 
          userMap.get(u.ID).Business_Unit__c != null && 
          bu_GroupMap.containsKey(userMap.get(u.ID).Business_Unit__c)) {
        String groupName = bu_GroupMap.get(userMap.get(u.ID).Business_Unit__c);

        if (groupName.equalsIgnoreCase(Constants.EDQ) && (String.isBlank(u.Office_Location__c))) {
          u.addError(Label.EDQ_USER_REQD_FIELDS);
        }
      } 
      else if (u.Business_Unit__c != null && 
               bu_GroupMap.containsKey(u.Business_Unit__c) &&
               String.isBlank(u.Office_Location__c)) {
        String groupName = bu_GroupMap.get(u.Business_Unit__c);
        if (groupName.equalsIgnoreCase(Constants.EDQ)/* && String.isBlank(u.Office_Location__c)*/) {
          u.addError(Label.EDQ_USER_REQD_FIELDS);
        }
      }
    }
  }
  
  // Case #528796 : James Weatherall 28th April, 2015
  // Updates the Current Manager of the Experian Employee Contact records
  // Case #01135171 : Diego Olarte enabled the method
  // Case 02191630: Mauricio Murillo.  Updated method signature to avoid Mixed DML error
  private static void updateExperianContact(List<User> newList) {
  
    List<Id> userListIds = new List<Id>(); 
      
    for (User u : newList) {
        userListIds.add(u.Id);
    }
    
    updateExperianContactHelper(userListIds);
  }
  
  // Case # 02191630: Mauricio  Murillo June, 2017
  // Created future helper method to update contact information from user, to avoid MIXED DML error
  @future
  private static void updateExperianContactHelper(List<Id> userListIds) {
  
    Map<Id, Contact> updContMap = new Map<Id, Contact>();
          
    for (Contact c : [SELECT Id, SFDC_User__c, Current_Manager__c, FirstName, LastName, Title, Department, 
                             Phone, MobilePhone, Email, Employee_Number__c, //Inactive__c, 
                             Status__c,
                             SFDC_User__r.Id, SFDC_User__r.IsActive,  
                             SFDC_User__r.FirstName, SFDC_User__r.LastName, SFDC_User__r.Title,SFDC_User__r.Department, 
                             SFDC_User__r.Phone, SFDC_User__r.MobilePhone, SFDC_User__r.Email, SFDC_User__r.EmployeeNumber, 
                             SFDC_User__r.Additional_Status__c, SFDC_User__r.ManagerId  
                      FROM Contact 
                      WHERE SFDC_User__c IN: userListIds]) {
                      
      c.Current_Manager__c = c.SFDC_User__r.ManagerId;
      c.FirstName = c.SFDC_User__r.FirstName;
      c.LastName = c.SFDC_User__r.LastName;
      c.Title = c.SFDC_User__r.Title;
      c.Department = c.SFDC_User__r.Department;
      c.Phone = c.SFDC_User__r.Phone;
      c.MobilePhone = c.SFDC_User__r.MobilePhone;
      c.Email = c.SFDC_User__r.Email;
      c.Employee_Number__c = c.SFDC_User__r.EmployeeNumber;
      // Set the status to Left company if the additional status is 'Leaver' and user is now inactive.
      c.Status__c = (c.SFDC_User__r.IsActive == false && 'Leaver'.equals(c.SFDC_User__r.Additional_Status__c)) ? Constants.CONTACT_STATUS_LEFT : Constants.STATUS_ACTIVE;
      if (!updContMap.containsKey(c.Id)) {
        System.debug('Adding contact ' + c.Id);
        updContMap.put(c.Id, c);
      }
    }
 
    try {
      update updContMap.values();
    } 
    catch(DMLException e) {
      system.debug('UserTriggerHandler : updateExperianContactHelper: e = ' + e);          
    }
  }    


  //===========================ACCOUNT SEGMENTS=================================
  // T-373930 : Method to create Account_Segment__c records for new Account
  //============================================================================
  private static void createAccSegmentForNewAccount_User(List<User> newList, Map<ID, User> oldMap) {
    system.debug('======createAccSegmentForNewAccount_User>>>');
    
    Set<String> updatedFieldValues = new Set<String>();
    Set<String> userFieldNameSet = new Set<String>{'Business_Line__c', 'Business_Unit__c',
                                                   'Global_Business_Line__c','Region__c', 'Country__c'};
    Map<Id, User> newUserMap = new Map<Id, User>();

    for (User userRec : newList) {
      if (oldMap != null && !userRec.UserReOrgProcessed__c) {
        newUserMap.put(userRec.Id, userRec);
        updatedFieldValues.addAll(isChangedAnyField(userFieldNameSet, userRec, oldMap.get(userRec.ID))) ;
      }
    }
    //If there is no field changed in the user list, return
    if (updatedFieldValues.isEmpty()) {
      return;
    }
    
    system.debug('updatedFieldValues.size()>>>' +updatedFieldValues.size());

    //call method to create Account_Segment__c records
    AccountSegmentationUtility.createAccountSegmentRecordforUser(updatedFieldValues, newUserMap, userFieldNameSet);
  }

  //============================================================================
  // T-373930 : Method to recalculate Segment values
  //============================================================================
  private static void recalculateSegmentFieldValues(List<User> newList, Map<ID, User> oldMap) {
    Set<String> updatedFieldValues = new Set<String>();
    Set<String> userFieldNameSet = new Set<String>{
      'Business_Line__c', 'Business_Unit__c',
      'Global_Business_Line__c','Region__c', 'Country__c'
    };
    Map<Id, User> newQualifyingUsers = new Map<Id, User>();
    Map<Id, User> oldQualifyingUsers = new Map<Id, User>();

    for (User userRec : newList) {
      if (oldMap != null && !userRec.UserReOrgProcessed__c) {
        //add new and old user records in Map
        newQualifyingUsers.put(userRec.Id, userRec);
        updatedFieldValues.addAll(isChangedAnyField(userFieldNameSet, userRec, oldMap.get(userRec.ID))) ;
      }
    }

    //If there is no field changed in the user list, return
    if (updatedFieldValues.isEmpty()) {
      return;
    }
    system.debug('updatedFieldValues.size()>>>' +updatedFieldValues.size());

    // if there is change in values, call the method from AccountSegmentationUtility
    // When a user's BU/BL/GBL/Region/Country is updated, we will recalculate
    // the old and new segments representing those values

    /*AccountSegmentationUtility.updateOpportunitySegments(newQualifyingUsers, oldMap);
    AccountSegmentationUtility.updateOrderSegments(newQualifyingUsers, oldMap);*/
     
    AccountSegmentationUtility.segmentationMaintenance(updatedFieldValues, null);

  }

  //============================================================================
  // T-373930 : Method to check any field got changed
  //============================================================================
  private static Set<String> isChangedAnyField (Set<String> fieldNameSet, User newRecord, User oldRecord) {
    Set<String> updatedFieldValues = new Set<String>();

    for (String fieldName : fieldNameSet) {
      if (newRecord.get(fieldName) != oldRecord.get(fieldName)) {
        updatedFieldValues.add(String.valueOf(newRecord.get(fieldName)));
      }
    }
    return updatedFieldValues;
  }


  //[RJ]Changes by Richard
  //===========================================================================
  //To update flag in contact of the active community user 
  //===========================================================================
  private static void updateCommunityContact(List<User> newList) {
    set<id> activeContactIdSet = new set<Id> (); 
    for (User userRec : newList) {
      if (userRec.Contactid != null && userRec.IsActive == true) {
         activeContactIdSet .add(userRec.Contactid);

      }
    } 
    system.debug('Richard Test' +activeContactIdSet );
    //update toUpdateContactList;
    If (activeContactIdSet.size() >0 )
    contactActivationDateUpdate(activeContactIdSet,null);
  }

  //[RJ]Changes by Richard
  //===========================================================================
  //To update a contact flag as false when a user is made inactive
  //===========================================================================
  private static void updateCommunityContactInactive(List<User> newList, Map<ID, User> oldMap) {
  
    set<id> inActiveContactIdSet = new set<Id>(); 
    set<id> activeContactIdSet = new set<Id> (); 
    for (User userRec : newList) { 
      if (userRec.Contactid != null && userRec.IsActive == false) {
        if (userRec.IsActive != oldMap.get(userRec.ID).IsActive) {
              inActiveContactIdSet .add(userRec.Contactid);
        } 
      }
      else if (userRec.Contactid != null && userRec.IsActive == True) {
        if (userRec.IsActive != oldMap.get(userRec.ID).IsActive) {
          activeContactIdSet .add(userRec.Contactid);

        } 
      }
    }
    If (activeContactIdSet.size() >0 || inActiveContactIdSet.size() >0)
    contactActivationDateUpdate(activeContactIdSet,inActiveContactIdSet );
  }
 
  
  //Case #01252420 - Creation of a Contact DQ score field on the Contact object - James Wills
  public static void checkIfIsactiveChanged(List<User> newList, Map<ID, User> oldMap) {
    List<Id> checkUserIds = new List<Id>();
    
    for (User userToCheck : newList) {
      User oldUser = oldMap.get(userToCheck.Id);
      if (oldUser.IsActive != userToCheck.IsActive) {
        checkUserIds.add(userToCheck.id);
      }
    }
    if (!checkUserIds.isEmpty()) {
      reAssessTeams(checkUserIds);
    }
  }
  
  //Case #01252420 - Creation of a Contact DQ score field on the Contact object - James Wills
  //This update needs to be in an @future method to avoid mixed DML error
  @future 
  public static void reAssessTeams(List<ID> checkUserIds){  
    for(List<Contact_Team__c> contactTeamsToReassess : [SELECT Id, Contact__c 
                                                        FROM Contact_Team__c 
                                                        WHERE Relationship_Owner__c IN :checkUserIds]){
      ContactTeamTriggerHandler.updateCounts(contactTeamsToReassess);     
    }    
  }
//RJ Starts
//This update needs to be in an @future method to avoid mixed DML error
  @future 
  public static void contactActivationDateUpdate(set<ID> ActiveContactIdSet ,set<ID> inActiveContactIdSet){  
    List<Contact> toUpdateContactList = new list<Contact>();
   If(inActiveContactIdSet != null){
    For(Id ContId:inActiveContactIdSet ){
        toUpdateContactList.add(
            new Contact(
              Id = ContId,
              Is_Community_User__c = false,
              Community_User_Latest_Activated_Date__c=null //RJ added for ITSM to balck out Activated date
            )
          );
    }
    }
    if(activeContactIdSet != null){
    For(Id ContId:activeContactIdSet){
        toUpdateContactList.add(
            new Contact(
              Id = ContId,
              Is_Community_User__c = true,
              Community_User_Latest_Activated_Date__c=date.today()//RJ added for ITSM to balck out Activated date
            )
          );
    }
    }
    if(toUpdateContactList.size()>0)
    Update toUpdateContactList;
  }

//RJ Ends

  //===========================================================================
  //  W-005402: Re-define Contact Status 
  //  Calls the future method and saves a list of the contacts in which the isActive flag changed from 
  //  inactive to active so that the contact can later be updated to active
  //===========================================================================
  public static void redefineContactStatus(List<User> newList, Map<Id, User> oldMap) {
    //we will store the users that changed from Inactive to Active
    Set<Id> usersMadeActiveIdSet = new Set<Id>();
    Set<Id> usersMadeInactiveIdSet = new Set<Id>();
    
     //This checks what users changed from Inactive to Active since we only want the contact to be updated to active when there is a change on th e
     //user from Inactive to Active
     for (User u : newList) {
       if (u.IsActive == true && oldMap.get(u.Id).IsActive == false) {
         usersMadeActiveIdSet.add(u.Id);
      }
      if (u.IsActive == false && oldMap.get(u.Id).IsActive == true) {
        usersMadeInactiveIdSet.add(u.Id);
      }
      if (u.IsActive == false && u.Additional_Status__c != oldMap.get(u.Id).Additional_Status__c) {
        usersMadeInactiveIdSet.add(u.Id); // Also add if the users additional status has been changed whilst still inactive 
      }
    }
    if (!usersMadeInactiveIdSet.isEmpty() || !usersMadeActiveIdSet.isEmpty()) {
      contactStatus_SFDC_user(usersMadeInactiveIdSet, usersMadeActiveIdSet);
    }
  }
  
    
  //===========================================================================
  // W-005402: Re-define Contact Status 
  // Checks for the Contacts that have this user populated as SFDC User and updates the Contact Status
  // based on the Additional Status from the SFDC User - @future to avoid the MIXED_DML_OPERATION Error
  //===========================================================================
  @future 
  private static void contactStatus_SFDC_user(Set<Id> usersMadeInactiveIdSet, Set<Id> usersMadeActiveIdSet) {
    
    String leaverAddStatus = 'Leaver';
    
    //Query for the Contacts
    List<Contact> contactUsersList = [
      SELECT ID, SFDC_User__c, SFDC_User__r.Additional_Status__c, SFDC_User__r.IsActive, Status__c
      FROM Contact
      WHERE SFDC_User__c != null 
      AND (SFDC_User__c IN :usersMadeInactiveIdSet OR SFDC_User__c IN :usersMadeActiveIdSet)
    ];

    for (Contact c : contactUsersList) {
      // Has been made inactive???
      if (usersMadeInactiveIdSet.contains(c.SFDC_User__c) && c.SFDC_User__r.IsActive == false) {
        //Check if the user's addition status is leaver.
        if (c.SFDC_User__r.Additional_Status__c == leaverAddStatus && 
            c.Status__c != Constants.CONTACT_STATUS_LEFT) {
          c.Status__c = Constants.CONTACT_STATUS_LEFT;
        }
        //If the Additional Status is anything other than leaver then
        if (c.SFDC_User__r.Additional_Status__c != leaverAddStatus && 
            c.Status__c != Constants.CONTACT_STATUS_INACTIVE) {
          c.Status__c = Constants.CONTACT_STATUS_INACTIVE;
        }
      }
      
      //If trigger is changed from Inactive to Active then the Contact will have a status of Active
      if (usersMadeActiveIdSet.contains(c.SFDC_User__c) && c.SFDC_User__r.IsActive == true) {
        c.Status__c = Constants.STATUS_ACTIVE;
      }
    }
    update contactUsersList;
  }
  
    
}