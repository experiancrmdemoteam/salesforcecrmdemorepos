/* batch to assign User Permission License Assignment for 
 * active User with Salesforce Platform License
 *
 * By: Hay Mun Win
 * UCInnovation
 */

global class batch_UserPermissionAssignment implements Database.Batchable<sObject> {
    global final String query;
    
     
     /**
     * Definition of the constructor. 
     * Initial all variables.
     *
     * @param      None
     * @return     None
     * @since      1.0
     */ 
    global batch_UserPermissionAssignment() {
    
        if(Test.isRunningTest()) {
            query ='Select Id, License__c, IsActive from User where License__c = \'Salesforce Platform\' AND IsActive = true AND FirstName = \'Unique Test\'';    
        }else{
            query ='Select Id, License__c, IsActive from User where License__c = \'Salesforce Platform\' AND IsActive = true';    
        }
           
        
    }
    
    /**
     * query locator
     *
     * @param    BC                         A reference to the Database.BatchableContext object
     * @return   Database.QueryLocator            
     * @since               1.0
     */ 
     
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }
   
     
    global void execute(Database.BatchableContext BC, List<User> scope) {
        
        List<String> userID = new List<String>();
        
        //add user ID to the list
        for (User uID : scope) {
            userID.add(uID.ID);
        }
        
        Set<String> userSet = new Set<String>();
        List<PermissionSetLicenseAssign> assignmentList = new List<PermissionSetLicenseAssign>();
        String licenseID;
        userSet.addAll(userID);
        
        
        //find permissionSetLicenseAssignment to see if it's already assigned to the user
        for(PermissionSetLicenseAssign assignment: [SELECT AssigneeId, PermissionSetLicenseId FROM PermissionSetLicenseAssign WHERE PermissionSetLicense.MasterLabel = 'Company Community for Force.com' and AssigneeId IN: userID]) {       
             licenseID = assignment.PermissionSetLicenseId;
             userSet.remove(assignment.AssigneeId);
        }
        
        //license ID
        if (licenseID == null ) {
            List<PermissionSetLicense> permissions = [Select Id from PermissionSetLicense where MasterLabel = 'Company Community for Force.com'];
            licenseID = permissions[0].ID;
        }
    
        // if it's not null
        if (licenseID != null ) {
        
            if (userSet.size() > 0) {
            
                for(String assignee: userSet) {
                    PermissionSetLicenseAssign permissionAssgn = new PermissionSetLicenseAssign(AssigneeId = assignee, PermissionSetLicenseId = licenseID );
                    assignmentList.add(permissionAssgn);
                }
            }
        
        }
    
        if (assignmentList.size() > 0) {
            insert assignmentList;
        }
    }
    
    global void finish(Database.BatchableContext BC){ 
        
   }
  

}