/**=====================================================================
 * Experian
 * Name: BatchWalletSyncAccountLookups
 * Description: Batch to set the account lookup on the WalletSync records
 * Created Date: 20th Aug, 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Aug 25th, 2015               Paul Kissick                 Adding condition for no account found be cnpj lookup
 * Sep 10th, 2015               Paul Kissick                 Added try/catch around email sending & changed email wording.
 * Oct 1st, 2015                Paul Kissick                 Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c
 * Nov 9th, 2015                Paul Kissick                 Case 01234035: Adding FailureNotificationUtility
 * Nov 27th, 2015               Paul Kissick                 Case 01266075: Replacing Global_Setting__c with new Batch_Class_Timestamp__c
 =====================================================================*/

global class BatchWalletSyncAccountLookups implements  Database.Batchable<sObject>, Database.Stateful {

  global List<String> updateErrors;
  global Map<String,String> failedCnpjToDetailsMap;
  
  global Database.QueryLocator start(Database.BatchableContext BC) {
    // Initialise the global vars
    updateErrors = new List<String>();
    failedCnpjToDetailsMap = new Map<String,String>();
    
    Datetime lastStartDate = WalletSyncUtility.getWalletSyncProcessingLastRun();
    return Database.getQueryLocator([
      SELECT Id, CNPJ_Number__c, Account__c, LegacyCRM_Account_ID__c
      FROM WalletSync__c
      WHERE Last_Processed_Date__c >= :lastStartDate
    ]);
  }
  
  global void execute(Database.BatchableContext BC, List<WalletSync__c> walletSyncs) {
    // this will be batched in suitable sizes....
    Map<String,Id> cnpjNumberToAccountId = new Map<String,Id>();
    for(WalletSync__c ws : walletSyncs) {
      if (String.isNotBlank(ws.CNPJ_Number__c)) {
        cnpjNumberToAccountId.put(ws.CNPJ_Number__c,null);
      }
    }
    
    // Now we have some cnpj numbers, lets get the account ids...
    List<Account> accountList = [SELECT Id, CNPJ_Number__c FROM Account WHERE CNPJ_Number__c IN :cnpjNumberToAccountId.keySet()];
    for(Account a : accountList) {
      cnpjNumberToAccountId.put(a.CNPJ_Number__c,a.Id);
    }
    
    // Great, now lets get these walletsync records updated....
    for(WalletSync__c ws : walletSyncs) {
      if (String.isNotBlank(ws.CNPJ_Number__c)) {
        ws.Account__c = null;
        if (cnpjNumberToAccountId.containsKey(ws.CNPJ_Number__c) && 
              cnpjNumberToAccountId.get(ws.CNPJ_Number__c) != null ) {
          ws.Account__c = cnpjNumberToAccountId.get(ws.CNPJ_Number__c);
        }
        else {
          failedCnpjToDetailsMap.put(ws.CNPJ_Number__c,ws.LegacyCRM_Account_ID__c);
        }
      }
    }
    List<Database.SaveResult> wsUpdRes = Database.update(walletSyncs,false);
    for(Database.SaveResult sr : wsUpdRes) {
      if(!sr.isSuccess()) {
        for(Database.Error err : sr.getErrors()) {
          updateErrors.add(err.getMessage());
        }
      }
    }
  }
  
  global void finish(Database.BatchableContext BC) {
    try {
	    BatchHelper bh = new BatchHelper();
	    bh.checkBatch(BC.getJobId(), 'BatchWalletSyncAccountLookups', false);
	    
	    if (updateErrors.size() > 0) {
	      bh.batchHasErrors = true;
	      bh.emailBody += '\nThe following errors were observed when updating records:\n';
	      bh.emailBody += String.join(updateErrors,'\n');
	    }
	    
	    if (failedCnpjToDetailsMap.size() > 0) {
	      bh.batchHasErrors = true;
		    bh.emailBody += '\nThe following CNPJ numbers in WalletSync do not have a matching Account with CNPJ Number in Salesforce. '+
		      'Please make sure Accounts that interface through Wallet Integration exist in Salesforce.\n';
		    bh.emailBody += 'CNPJ Number : LegacyCRM Account ID\n----------------------------------\n';
		    for(String cnpjNumber : failedCnpjToDetailsMap.keySet()) {
		      bh.emailBody += cnpjNumber + ': ' +failedCnpjToDetailsMap.get(cnpjNumber) + '\n';
		    }
		  }
	    
	    bh.sendEmail();
    }
    catch (Exception e) {
      system.debug(e.getMessage()); 
    }
    if (!Test.isRunningTest()) { // Must be within this check as tests cannot start other batches
      // THIS BATCH, WHEN FINISHED, STARTS THE NEXT BATCH OF ACCOUNT INDUSTRY AND SECTOR SETTING
      system.scheduleBatch(new BatchWalletSyncAccountIndSect(), 'BatchWalletSyncAccountIndSect'+String.valueOf(Datetime.now().getTime()), 0, ScopeSizeUtility.getScopeSizeForClass('BatchWalletSyncAccountIndSect'));
    }
  }
  
}