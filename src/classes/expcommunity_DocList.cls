/******************************************************************************
 * Name: expcommunity_DocList.cls
 * Created Date: 4/20/2017
 * Created By: Hay Win
 * Description : Controller for expcomm_documentList_cmpt
 * Change Log- 
 ****************************************************************************/
 
public with sharing class expcommunity_DocList{ 
   
    public List<Document> docList{get;set;}
    public String DocfolderName {get;set;}
    
    public expcommunity_DocList() {        
        docList= new List<Document>();
    }
    
  
    
    public boolean getfindDocs(){
    
        if (DocfolderName != null && DocfolderName != '') {
            docList = [Select Id, Name, BODYLENGTH, LastModifiedDate from Document where Folder.Name =: DocfolderName];
            
            if (docList.size() > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }        
    }
}