/**=====================================================================
 * Experian, Inc
 * Name: ControllerCustomSettings
 * Description: Controller class for The VisualForce Page that Renders the 
                Custom Settings which are read to show information the Global Messaging Console.
 *
 * Created Date: Feb 3rd, 2016
 * Created By: Tyaga Pati (Experian)
 *
 * Date Modified            Modified By              Description of the update
 ======================================================================*/
public class ControllerCustomSettings {
    
    Public AdminMessages__c AdmSettings1; 

    //= AdminMessages__c.getOrgDefaults();
    public String Msghdr {get; set;}
    public String DisplayMessage {get; set;}
    Public Boolean IsActiveFlag  {get; set;}
    
    public PageReference save() {
    system.debug('Tyaga Constructor has been called 000000');
        AdmSettings1.Is_Message_Active__c = IsActiveFlag;
        AdmSettings1.Header__c = Msghdr;
        AdmSettings1.Admin_Message__c = DisplayMessage;
        Update AdmSettings1;
        return null;
    }
        
    Public ControllerCustomSettings (){
        AdmSettings1 = AdminMessages__c.getOrgDefaults();
        system.debug('Tyaga Constructor has been called');
        msghdr = AdmSettings1.Header__c; //Value to be Displayed in UI
        DisplayMessage = AdmSettings1.Admin_Message__c; //Value to be Displayed in UI
        IsActiveFlag  = AdmSettings1.Is_Message_Active__c; //Value to be Displayed in UI
        system.debug('Tyaga the values of the attributes are : msghdr is : '+   msghdr + 'display msg is :' + DisplayMessage   + 'active flag is ' + IsActiveFlag    );
    }
}