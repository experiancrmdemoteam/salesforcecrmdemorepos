/**=====================================================================
 * Experian
 * Name: BatchWalletSyncAccountIndSect
 * Description: Find all new WalletSync entries and update the Industry/Sector on Accounts based on the values.
 * Created Date: 11th Aug 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Sep 10th, 2015               Paul Kissick                 Added try/catch around email sending
 * Sep 25th, 2015               Paul Kissick                 I-182166: Adding LATAM Segment from Wallet Sync to Account
 * Oct 1st, 2015                Paul Kissick                 Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c
 * Nov 9th, 2015                Paul Kissick                 Case 01234035: Adding FailureNotificationUtility
 * Nov 27th, 2015               Paul Kissick                 Case 01266075: Replacing Global_Setting__c with new Batch_Class_Timestamp__c
 =====================================================================*/

global class BatchWalletSyncAccountIndSect implements  Database.Batchable<sObject>, Database.Stateful {

  global List<String> updateErrors;
  global Map<String,Serasa_Segments_Map__c> allSegmentsMap; // Will hold all the segments to use later...
  
  global Database.QueryLocator start(Database.BatchableContext BC) {
    updateErrors = new List<String>();
    Datetime lastStartDate = WalletSyncUtility.getWalletSyncProcessingLastRun();
    allSegmentsMap = new Map<String,Serasa_Segments_Map__c>();
    
    for(Serasa_Segments_Map__c ssm : [SELECT Segment1__c, Segment2__c, Segment3__c, Industry__c, Sector__c FROM Serasa_Segments_Map__c]) {
      allSegmentsMap.put(ssm.Segment1__c+'|'+ssm.Segment2__c+'|'+ssm.Segment3__c,ssm);
    }
        
    return Database.getQueryLocator([
      SELECT Id, Segment1__c, Segment2__c, Segment3__c, Account__c, Sector__c
      FROM WalletSync__c
      WHERE Segment1__c != null
      AND Segment2__c != null
      AND Segment3__c != null
      AND Account__c != null
      AND Last_Processed_Date__c >= :lastStartDate
    ]);
  }
  
  
  global void execute(Database.BatchableContext BC, List<WalletSync__c> scope) {
    Map<Id,String> accIdToSegmentMap = new Map<Id,String>();
    Map<Id,String> accIdToLatamSectorMap = new Map<Id,String>();
    for(WalletSync__c ws : scope) {
      accIdToSegmentMap.put(ws.Account__c, ws.Segment1__c+'|'+ws.Segment2__c+'|'+ws.Segment3__c);
      accIdToLatamSectorMap.put(ws.Account__c, ws.Sector__c);
    }
    
    for(List<Account> accsToUpdate : [SELECT Industry, Sector__c, LATAM_Sector__c FROM Account WHERE Id IN :accIdToSegmentMap.keySet()]) {
      for(Account a : accsToUpdate) {
        String segmentKey = accIdToSegmentMap.get(a.Id);
        if (allSegmentsMap.containsKey(segmentKey)) {
          Serasa_Segments_Map__c tmpSsm = allSegmentsMap.get(segmentKey);
          a.Industry = tmpSsm.Industry__c;
          a.Sector__c = tmpSsm.Sector__c;
        }
        if (accIdToLatamSectorMap.containsKey(a.Id)) {
          a.LATAM_Sector__c = accIdToLatamSectorMap.get(a.Id);
        }
      }
      List<Database.SaveResult> saveRes = Database.update(accsToUpdate,false);
      for(Database.SaveResult sr : saveRes) {
        if(!sr.isSuccess()) {
          for(Database.Error err : sr.getErrors()) {
            updateErrors.add(err.getMessage());
          }
        }
      }
    }
  }
  
  global void finish(Database.BatchableContext BC) {
    try {
	    BatchHelper bh = new BatchHelper();
	    bh.checkBatch(BC.getJobId(), 'BatchWalletSyncAccountIndSect', false);
	    
	    if (updateErrors.size() > 0) {
	      bh.batchHasErrors = true;
	      bh.emailBody += '\nThe following errors were observed when updating records:\n';
	      bh.emailBody += String.join(updateErrors,'\n');
	    }
	    
	    bh.sendEmail();
    }
    catch (Exception e) {
      system.debug(e.getMessage()); 
    }
    if (!Test.isRunningTest()) { // Must be within this check as tests cannot start other batches
      // THIS BATCH, WHEN FINISHED, STARTS THE NEXT BATCH OF ACCOUNT OWNERSHIP
      system.scheduleBatch(new BatchWalletSyncAccountOwners(),'BatchWalletSyncAccountOwners'+String.valueOf(Datetime.now().getTime()),0,ScopeSizeUtility.getScopeSizeForClass('BatchWalletSyncAccountOwners'));
    }
  }

}