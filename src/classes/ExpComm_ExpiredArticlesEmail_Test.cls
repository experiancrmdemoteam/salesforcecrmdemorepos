@isTest
private class ExpComm_ExpiredArticlesEmail_Test {
    
    @isTest static void test_method_en_US() {
        List<User> userList = new List<User>();
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        Profile p =[Select Id from Profile where NAME='System Administrator'];

        System.runAs (thisUser) {

        user mgrUser = new User(Alias = 'testMGR', Email='sampleuser2@test.in.experian.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, phone = '555555555',
        TimeZoneSidKey='America/Los_Angeles', UserName='sampleUser2@test.in.experian.com', Department ='Test');
       
       insert mgrUser;

        user userWithManager = new User(Alias = 'UserWMGR', Email='sampleuser3@test.in.experian.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, phone = '555555555',
        TimeZoneSidKey='America/Los_Angeles', UserName='sampleUser3@test.in.experian.com', Department ='Test');

        insert userWithManager;
        userWithManager.isActive = false;
        update userWithManager;

        user userWithOutManager = new User(Alias = 'UWOMGR', Email='sampleuser4@test.in.experian.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, phone = '555555555',
        TimeZoneSidKey='America/Los_Angeles', UserName='sampleUser4@test.in.experian.com', Department ='Test');

        insert userWithOutManager;
        userWithOutManager.isActive = false;
        update userWithOutManager;

        user activeUser =  new User(Alias = 'ActUser', Email='sampleuser5@test.in.experian.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, phone = '555555555',
        TimeZoneSidKey='America/Los_Angeles', UserName='sampleUser5@test.in.experian.com', Department ='Test');

        insert activeUser;
        activeUser.isActive = true;
        update activeUser;

        
        Employee_Article__kav ea1 = new Employee_Article__kav(Expiration_Date__c = Date.Today()+2, Title = 'Test Article1',
        Description__c = 'Example', language='en_US', urlName ='test1', Article_Owner__c = activeUser.Id);
        insert ea1;
         ea1 = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :ea1.Id];
        KbManagement.PublishingService.publishArticle(ea1.KnowledgeArticleId, false);
        
        
        
        Employee_Article__kav ea2 = new Employee_Article__kav(Expiration_Date__c = Date.Today()+2, Title = 'Test Article2',
        Description__c = 'Example', language='en_US', urlName ='test2', Article_Owner__c = userWithManager.Id);
        insert ea2;
        ea2 = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :ea2.Id];
        KbManagement.PublishingService.publishArticle(ea2.KnowledgeArticleId, false);
        

        
        Employee_Article__kav ea3 = new Employee_Article__kav(Expiration_Date__c = Date.Today()+2, Title = 'Test Article3',
        Description__c = 'Example', language='en_US', urlName ='test3', Article_Owner__c = userWithOutManager.Id);
        insert ea3;
        ea3 = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :ea3.Id];
        KbManagement.PublishingService.publishArticle(ea3.KnowledgeArticleId, false);

        /****Spanish Articles****/
        Employee_Article__kav ea1_es = new Employee_Article__kav(Expiration_Date__c = Date.Today()+2, Title = 'Test Article1',
        Description__c = 'Example', language='es', urlName ='test1', Article_Owner__c = activeUser.Id);
        insert ea1_es;
         ea1_es = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :ea1_es.Id];
        KbManagement.PublishingService.publishArticle(ea1_es.KnowledgeArticleId, false);
        
        
        
        Employee_Article__kav ea2_es = new Employee_Article__kav(Expiration_Date__c = Date.Today()+2, Title = 'Test Article2',
        Description__c = 'Example', language='es', urlName ='test2', Article_Owner__c = userWithManager.Id);
        insert ea2_es;
        ea2_es = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :ea2_es.Id];
        KbManagement.PublishingService.publishArticle(ea2_es.KnowledgeArticleId, false);
        
        
        Employee_Article__kav ea3_es = new Employee_Article__kav(Expiration_Date__c = Date.Today()+2, Title = 'Test Article3',
        Description__c = 'Example', language='es', urlName ='test3', Article_Owner__c = userWithOutManager.Id);
        insert ea3_es;
        ea3_es = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :ea3_es.Id];
        KbManagement.PublishingService.publishArticle(ea3_es.KnowledgeArticleId, false);
        
        /****Portuguese Articles****/
        Employee_Article__kav ea1_pt = new Employee_Article__kav(Expiration_Date__c = Date.Today()+2, Title = 'Test Article1',
        Description__c = 'Example', language='pt_BR', urlName ='test1', Article_Owner__c = activeUser.Id);
        insert ea1_pt;
         ea1_pt = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :ea1_pt.Id];
        KbManagement.PublishingService.publishArticle(ea1_pt.KnowledgeArticleId, false);
        
        
        
        Employee_Article__kav ea2_pt = new Employee_Article__kav(Expiration_Date__c = Date.Today()+2, Title = 'Test Article2',
        Description__c = 'Example', language='pt_BR', urlName ='test2', Article_Owner__c = userWithManager.Id);
        insert ea2_pt;
        ea2_pt = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :ea2_pt.Id];
        KbManagement.PublishingService.publishArticle(ea2_pt.KnowledgeArticleId, false);
        
        
        Employee_Article__kav ea3_pt = new Employee_Article__kav(Expiration_Date__c = Date.Today()+2, Title = 'Test Article3',
        Description__c = 'Example', language='pt_BR', urlName ='test3', Article_Owner__c = userWithOutManager.Id);
        insert ea3_pt;
        ea3_pt = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :ea3_pt.Id];
        KbManagement.PublishingService.publishArticle(ea3_pt.KnowledgeArticleId, false);
        
        Test.startTest();
        ExpComm_ExpiredArticlesEmail_Schedule.runScheduler('test','0 0 0 15 3 ? 2022');
         Test.stopTest(); 
        }
    }
    
    
}