/**=====================================================================
 * Name: AccountPlanControllerExtension
 * Description: See case #01848189 - Account Planning Enhancements: Test Methods for the All the Opportunity List Views on the Vf Page. Revenue Seggregation test by Product BL and 
                GBL. 
 * Created Date: Mar. 10th, 2016
 * Created By: James Wills/Tyaga Pati
 *
 * Date Modified       Modified By          Description of the update
 * Mar. 10th, 2016     James Wills          Case #01848189: Created
 * Apr. 22nd, 2016     James Wills          Added test_generateLastYearsWonRevenueList()
 * Apr  22nd, 2016     Tyaga Pati           Added functions to Test the Opty and Revenue functions.DetermineTCVforFiscalNew,DetermineTCVforFiscalNewRen, DetermineTCVforFiscal
                                            AccountPlanWinBackOpps, AccountPlanAIdentifyOpps, AccountPlanAllKeyDeals, AccountPlanAllOpps                                             
 * Jun 21st, 2016      Tyaga Pati           Case 01976432 : Test Edit Functionality. 
 * Jun 27th, 2016      Manoj Gopu           Case #01947180 - Remove EDQ specific Contact object fields - MG COMMENTED CODE OUT
 * Oct 20th, 2016      James Wills          Case #02088090 - Resolved issues with tests following validation rule change
 * Aug. 1st, 2017     James Wills           Case 02079142 - Updated class for new validation.
  =====================================================================*/
@isTest
public with sharing class AccountPlanControllerExtension_Test{
  public static testmethod void test_GenericSaveAndWonRevenueMethods(){
    Account testAccount1 = Test_Utils.insertAccount();
    Account_Plan__c newAccountPlan = new Account_Plan__c(Account__c = testAccount1.id);
    newAccountPlan.Name = 'Test Account Plan1';
    insert newAccountPlan;
    PageReference pageRef = Page.AccountPlanTabPanel;
    Test.setCurrentPageReference(pageRef);
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)newAccountPlan);
    ID newAccountPlanID = [SELECT ID FROM Account_Plan__c WHERE id = :newAccountPlan.id].id;
    Test.startTest();
    ApexPages.currentPage().getParameters().put('Id', newAccountPlanID);
    AccountPlanControllerExtension controller = new AccountPlanControllerExtension(stdController);
    controller.saveAccountPlan();
    controller.generateLastYearsWonRevenueList();
    Test.stopTest();
  }
    
 public static testmethod void test_generateLastYearsWonRevenueList(){
    Account testAccount1 = Test_Utils.insertAccount();
    Account testAccount2 = Test_Utils.insertAccount();
    testAccount2.Ultimate_Parent_Account__c = testAccount1.id;
    update testAccount2;
    Contact newContact1 =  Test_Utils.insertContact(testAccount1.id);
    List<Account> selectedAccountsList = new List<Account>();
    selectedAccountsList.add(testAccount1);
    selectedAccountsList.add(testAccount2);
    Account_Plan__c newAccountPlan = new Account_Plan__c(Account__c = testAccount1.id);
    newAccountPlan.Name = 'Test Account Plan1';
    insert newAccountPlan;
    List<Account_Account_Plan_Junction__c> accPlanJuncList = new List<Account_Account_Plan_Junction__c>();
    for(Account acc: selectedAccountsList){
      Account_Account_Plan_Junction__c accAccPlan = new Account_Account_Plan_Junction__c();
      accAccPlan.Account__c = acc.Id;
      accAccPlan.Account_Plan__c = newAccountPlan.Id;
      accPlanJuncList.add(accAccPlan);
    }
    insert accPlanJuncList;
    PageReference pageRef = Page.AccountPlanTabPanel;
    Test.setCurrentPageReference(pageRef);
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)newAccountPlan);
    ID newAccountPlanID = [SELECT ID FROM Account_Plan__c WHERE id = :newAccountPlan.id].id;
    DateTime eventDate1 = DateTime.now();
    eventDate1 = eventDate1.addDays(-365);
    Date closeDatePrevFY = date.newInstance(eventDate1.year(), eventDate1.month(), eventDate1.day());
    Opportunity newOppty1 = Test_Utils.insertOpportunity(testAccount1.id);    
    Order__c newOrder1 = Test_Utils.insertOrder (true, testAccount1.id, newContact1.id, newOppty1.id);
    newOrder1.Close_Date__c = closeDatePrevFY;
    update newOrder1;
    Billing_Product__c billingProduct = Test_Utils.insertBillingProduct();
    Order_Line_Item__c oli1 = Test_Utils.insertOrderLineItems(true, newOrder1.id, billingProduct.id);
    Test.startTest();
    List<Account_Account_Plan_Junction__c> apj = [SELECT id FROM Account_Account_Plan_Junction__c];
    system.assert(apj.size()>0, 'Cannot find any Account_Account_Plan_Junction__c Objects');
    ApexPages.currentPage().getParameters().put('Id', newAccountPlanID);
    AccountPlanControllerExtension controller = new AccountPlanControllerExtension(stdController);
    controller.saveAccountPlan();
    controller.generateLastYearsWonRevenueList();
    Test.stopTest();
  }

    @isTest//(SeeAllData=true)
      public static void test_OpenOpps_Prop(){
        // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        system.runAs(testUser1) {
            Test.startTest();
            //Step1: Insert Account
              Account acc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
            //Step2: Insert Contact  
              Contact con = new Contact (FirstName = 'Larry', LastName = 'Ellison', Title = 'Mr',
                                         AccountId = acc.Id, Email = 'larrye11@experian.com', //EDQ_On_Demand__c = true,
                                         Phone = '9799559433', EDQ_Integration_Id__c = 'TestIds123');
              insert con;
            //Step3: Insert Opportunity
              Opportunity TestOpp = Test_Utils.createOpportunity(acc.Id);
              Insert TestOpp ;
            //Update Opty to Refelect Correct Stage
              TestOpp.StageName = 'Qualify';
              Update TestOpp;
            //Step4: Insert Product 
              Product2 Prod = Test_Utils.createProduct();
              Insert Prod;
            //Update Product to Reflect Global Business Line and Business Line
              Prod.Global_Business_Line__c = 'Decision Analytics' ; 
              Prod.Business_Line__c = 'Software'; 
              Update Prod;
            //Step5: Query a Price Book entry from the Object and Insert PriceBook 
            Id prcBookId = Test.getStandardPricebookId();
              PricebookEntry PrcBookEntry = Test_Utils.createPricebookEntry(Prod.Id,prcBookId ,'USD');
              Insert PrcBookEntry;
            //Step6: Insert OpportunityLineItem  
              OpportunityLineItem OptylineItem = Test_Utils.createOpportunityLineItem(TestOpp.Id,PrcBookEntry.Id,'Renewal');
              Insert OptylineItem ; 
            //Step7: Create a String containing the Id of the Account Created above and pass it to the Accountplanhelper Class to Create the plan.   
              List<string> selectedAcnts = New List<String>();
              selectedAcnts.add(acc.Id);
              Account_Plan__c acPlan = AccountPlanHelperClass.CreateAccountPlan(selectedAcnts,acc.Id);
            //Step 8: Set the Account Id on Page to be able to use in the AccountPlanControllerExtension class.
              ApexPages.currentPage().getParameters().put('Id', acPlan.Id); 
              //apexpages.standardcontroller testaccPlan = New apexpages.standardcontroller(acPlan); 
             //AccountPlanControllerExtension controller1 = new AccountPlanControllerExtension(testaccPlan);
              AccountPlanControllerExtension controller1 = new AccountPlanControllerExtension();
              List<Opportunity> OptyList = New List<Opportunity>();
            //Step 9: Assign the OpenOpps property to the OptyList.  
              OptyList = controller1.OpenOpps;
              Test.stopTest();  
           System.assertEquals(OptyList.size()>0, true);//Final Assert for this Test.
      }
    }//End of test_OpenOpps_Prop

// Scenario 2: Test to Ensure the ClosedOpps Prop has the Open Opportunities List Inside it.
    @isTest//(SeeAllData=true)
      public static void test_ClosedOpps_Prop(){
       
        // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        system.runAs(testUser1) {
            
            Test.startTest();
            //Step1: Insert Account
              Account acc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
              Address__c addrs1 = Test_Utils.insertAddress(true);
              Account_Address__c accAddrs = Test_Utils.insertAccountAddress(true, addrs1.Id, acc.Id);
            //Step2: Insert Contact  
              Contact con = new Contact (FirstName = 'Larry', LastName = 'Ellison', Title = 'Mr',
                                         AccountId = acc.Id, Email = 'larrye11@experian.com', //EDQ_On_Demand__c = true,
                                         Phone = '9799559433', EDQ_Integration_Id__c = 'TestIds123');
              insert con;
            //Step3: Insert Opportunity
              Opportunity TestOpp1 = Test_Utils.createOpportunity(acc.Id);
              Insert TestOpp1;
              Product2 Prod = Test_Utils.createProduct();
              Insert Prod;
              Prod.Global_Business_Line__c = 'Decision Analytics' ; 
              Prod.Business_Line__c = 'Software'; 
              Update Prod;
              Id prcBookId = Test.getStandardPricebookId();
              PricebookEntry PrcBookEntry = Test_Utils.createPricebookEntry(Prod.Id,prcBookId ,'USD');
              Insert PrcBookEntry;
              OpportunityLineItem OptylineItem = Test_Utils.createOpportunityLineItem(TestOpp1.Id,PrcBookEntry.Id,'Renewal');
              Insert OptylineItem ;           
              TestOpp1.StageName = Constants.OPPTY_STAGE_7;
              //testOpp.Status__c = Constants.OPPTY_CLOSED_WON;
              TestOpp1.Primary_Reason_W_L__c = constants.PRIMARY_REASON_WLC_DATA_QUALITY;
              TestOpp1.Channel_Type__c = Constants.OPPTY_CHANNEL_TYPE_DIRECT;
              TestOpp1.Amount = 100;
              TestOpp1.Has_Senior_Approval__c = true;
              TestOpp1.Starting_Stage__c = Constants.OPPTY_STAGE_6;
              TestOpp1.CloseDate = date.today()-10;
              TestOpp1.Contract_Start_Date__c = date.today().addDays(1);
              TestOpp1.Contract_End_Date__c = date.today().addYears(1);
              TestOpp1.Channel_Type__c = Constants.OPPTY_CHANNEL_TYPE_DIRECT;
              TestOpp1.Type = Constants.OPPTY_NEW_FROM_NEW; //ttk
              update TestOpp1;
            //Step7: Create a String containing the Id of the Account Created above and pass it to the Accountplanhelper Class to Create the plan.   
              List<string> selectedAcnts = New List<String>();
              selectedAcnts.add(acc.Id);
              Account_Plan__c acPlan = AccountPlanHelperClass.CreateAccountPlan(selectedAcnts,acc.Id);
            //Step 8: Set the Account Id on Page to be able to use in the AccountPlanControllerExtension class.
              ApexPages.currentPage().getParameters().put('Id', acPlan.Id); 
              //apexpages.standardcontroller testaccPlan = New apexpages.standardcontroller(acPlan); 
              //AccountPlanControllerExtension controller1 = new AccountPlanControllerExtension(testaccPlan);  
              AccountPlanControllerExtension controller1 = new AccountPlanControllerExtension();
              List<Opportunity> OptyList = New List<Opportunity>();
            //Step 9: Assign the OpenOpps property to the OptyList. 
              OptyList = controller1.ClosedOpps;
              Test.stopTest();
           System.assertEquals(OptyList.size()>0, true);//Final Assert for this Test.
          }
    }//End of test_ClosedOpps_Prop


// Scenario 3: Test to Ensure the KeyDeals Prop has the Key Deals List in it.
    @isTest//(SeeAllData=true)
      public static void test_KeyDeals_Prop(){       
        
        // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        system.runAs(testUser1) {
        
        Test.startTest();
        //Step1: Insert Account
          Account acc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
        //Step2: Insert Contact  
          Contact con = new Contact (FirstName = 'Larry', LastName = 'Ellison', Title = 'Mr',
                                     AccountId = acc.Id, Email = 'larrye11@experian.com', //EDQ_On_Demand__c = true,
                                     Phone = '9799559433', EDQ_Integration_Id__c = 'TestIds123');
          insert con;
        //Step3: Insert Opportunity
          Opportunity TestOpp = Test_Utils.createOpportunity(acc.Id);
          Insert TestOpp ;
        //Update Opty to Refelect Correct Stage
          TestOpp.StageName = 'Qualify';
          TestOpp.Key_Deal__c = true;
          Update TestOpp;
        //Step7: Create a String containing the Id of the Account Created above and pass it to the Accountplanhelper Class to Create the plan.   
          List<string> selectedAcnts = New List<String>();
          selectedAcnts.add(acc.Id);
          Account_Plan__c acPlan = AccountPlanHelperClass.CreateAccountPlan(selectedAcnts,acc.Id);
        //Step 8: Set the Account Id on Page to be able to use in the AccountPlanControllerExtension class.
          ApexPages.currentPage().getParameters().put('Id', acPlan.Id);   
          AccountPlanControllerExtension controller1 = new AccountPlanControllerExtension();
          List<Opportunity> OptyList = New List<Opportunity>();
        //Step 9: Assign the OpenOpps property to the OptyList.  
          OptyList = controller1.KeyDeals;
          Test.stopTest();
          
       System.assertEquals(OptyList.size()>0, true);//Final Assert for this Test.
      }
    }//End of test_OpenOpps_Prop

 // Scenario 4: Test to Ensure the Identify Prop has the Open Opportunities List Inside it.
    @isTest//(SeeAllData=true)
      public static void test_Identify_Prop(){
       
        // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        system.runAs(testUser1) {
            Test.startTest();
            //Step1: Insert Account
              Account acc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
            //Step2: Insert Contact  
              Contact con = new Contact (FirstName = 'Larry', LastName = 'Ellison', Title = 'Mr',
                                         AccountId = acc.Id, Email = 'larrye11@experian.com', //EDQ_On_Demand__c = true,
                                         Phone = '9799559433', EDQ_Integration_Id__c = 'TestIds123');
              insert con;
            //Step3: Insert Opportunity
              Opportunity TestOpp = Test_Utils.createOpportunity(acc.Id);
              Insert TestOpp ;
              TestOpp.StageName = 'Identify';
              Update TestOpp;
            //Step4: Insert Product 
              List<string> selectedAcnts = New List<String>();
              selectedAcnts.add(acc.Id);
              Account_Plan__c acPlan = AccountPlanHelperClass.CreateAccountPlan(selectedAcnts,acc.Id);
            //Step 8: Set the Account Id on Page to be able to use in the AccountPlanControllerExtension class.
              ApexPages.currentPage().getParameters().put('Id', acPlan.Id);   
              AccountPlanControllerExtension controller1 = new AccountPlanControllerExtension();
              List<Opportunity> OptyList = New List<Opportunity>();
            //Step 9: Assign the OpenOpps property to the OptyList.  
              OptyList = controller1.IdentifyOpps;
              Test.stopTest();  
           System.assertEquals(OptyList.size()>0, true);//Final Assert for this Test.
      }
    }//End of test_IdentifyOpps_Prop


// Scenario 5: Test to Ensure the Identify Prop has the Open Opportunities List Inside it.
    @isTest//(SeeAllData=true)
      public static void test_WinBackOpps_Prop(){
       
        // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        system.runAs(testUser1) {
            Test.startTest();
            //Step1: Insert Account
              Account acc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
            //Step2: Insert Contact  
              Contact con = new Contact (FirstName = 'Larry', LastName = 'Ellison', Title = 'Mr',
                                         AccountId = acc.Id, Email = 'larrye11@experian.com', //EDQ_On_Demand__c = true,
                                         Phone = '9799559433', EDQ_Integration_Id__c = 'TestIds123');
              insert con;
            //Step3: Insert Opportunity
              Opportunity TestOpp = Test_Utils.createOpportunity(acc.Id);
              Insert TestOpp ;
            //Update Opty to Refelect Correct Stage
              TestOpp.StageName = 'Closed Lost';
              TestOpp.Win_Back_Date__c= date.today().addDays(15);
              TestOpp.CloseDate  = date.today().addDays(-20);
              TestOpp.Other_Closed_Reason__c = 'The deal could not be completed.';//Case 02079142
              Update TestOpp;
            //Step7: Create a String containing the Id of the Account Created above and pass it to the Accountplanhelper Class to Create the plan.   
              List<string> selectedAcnts = New List<String>();
              selectedAcnts.add(acc.Id);
              Account_Plan__c acPlan = AccountPlanHelperClass.CreateAccountPlan(selectedAcnts,acc.Id);
            //Step 8: Set the Account Id on Page to be able to use in the AccountPlanControllerExtension class.
              ApexPages.currentPage().getParameters().put('Id', acPlan.Id);   
              AccountPlanControllerExtension controller1 = new AccountPlanControllerExtension();
              List<Opportunity> OptyList = New List<Opportunity>();
            //Step 9: Assign the OpenOpps property to the OptyList.  
              OptyList = controller1.WinBackOpps;
              Test.stopTest();   
           System.assertEquals(OptyList.size()>0, true);//Final Assert for this Test.
      }
    }//End of test_WinBackOpps_Prop

// Scenario 6: Test to Ensure the Opportunity List for PipelineData seggregated by Product GBL and BL has values in it
    @isTest//(SeeAllData=true)
      public static void test_PipelineNewRen_Prop(){
        // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        system.runAs(testUser1) {
            
            Test.startTest();
            //Step1: Insert Account
              Account acc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
              Address__c addrs1 = Test_Utils.insertAddress(true);
              Account_Address__c accAddrs = Test_Utils.insertAccountAddress(true, addrs1.Id, acc.Id);
            //Step2: Insert Contact  
              Contact con = new Contact (FirstName = 'Larry', LastName = 'Ellison', Title = 'Mr',
                                         AccountId = acc.Id, Email = 'larrye11@experian.com', //EDQ_On_Demand__c = true,
                                         Phone = '9799559433', EDQ_Integration_Id__c = 'TestIds123');
              insert con;
            //Step3: Insert Opportunity
              Opportunity TestOpp1 = Test_Utils.createOpportunity(acc.Id);
              Insert TestOpp1;
              Opportunity TestOpp2 = Test_Utils.createOpportunity(acc.Id);
              Insert TestOpp2;
              Product2 Prod = Test_Utils.createProduct();
              Insert Prod;
              Prod.Global_Business_Line__c = 'Decision Analytics' ; 
              Prod.Business_Line__c = 'Software'; 
              Update Prod;
              Id prcBookId = Test.getStandardPricebookId();
              PricebookEntry PrcBookEntry = Test_Utils.createPricebookEntry(Prod.Id,prcBookId ,'USD');
              Insert PrcBookEntry;
              OpportunityLineItem OptylineItem = Test_Utils.createOpportunityLineItem(TestOpp1.Id,PrcBookEntry.Id,'Renewal');
              TestOpp2.CloseDate = date.today()-10;
              TestOpp2.Type = 'Renewal';
              update TestOpp2;
              TestOpp1.CloseDate = date.today()-10;
              TestOpp1.Type = Constants.OPPTY_NEW_FROM_NEW;
              update TestOpp1;  
              OpportunityLineItem OptylineItem1 = Test_Utils.createOpportunityLineItem(TestOpp2.Id,PrcBookEntry.Id,'Renewal');
              
              optyLineItem.Type__c = Constants.OPPTY_LINE_ITEM_TYPE_RENEWAL;//Case #02088090              
              optyLineItem1.Type__c = Constants.OPPTY_LINE_ITEM_TYPE_RENEWAL;//Case #02088090
              
              Insert OptylineItem;
              Insert OptylineItem1 ;
            //Step7: Create a String containing the Id of the Account Created above and pass it to the Accountplanhelper Class to Create the plan.   
              List<string> selectedAcnts = New List<String>();
              selectedAcnts.add(acc.Id);
              Account_Plan__c acPlan = AccountPlanHelperClass.CreateAccountPlan(selectedAcnts,acc.Id);
            //Step 8: Set the Account Id on Page to be able to use in the AccountPlanControllerExtension class.
              ApexPages.currentPage().getParameters().put('Id', acPlan.Id);   
              AccountPlanControllerExtension controller1 = new AccountPlanControllerExtension();
            //Step 9: Assign the OpenOpps property to the OptyList. 
              Test.stopTest();

           System.assertEquals(controller1.BLPipelineSeggrLst.size()>0, true);//Final Assert for this Test.
           System.assertEquals(controller1.BLPipelineSeggrLstNew.size()>0, true);//Final Assert for this Test.
           System.assertEquals(controller1.BLPipelineSeggrLstRenewal.size()>0, true);//Final Assert for this Test.
          }
    }//End of to Determine the Pipeline for New and Renewal opty are built by product GBL and BL.


 // Scenario 7: Determine if the function is running.
    @isTest//(SeeAllData=true)
      public static void test_To_Detemine_FiscalDates(){
       // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        system.runAs(testUser1) {
            
            Test.startTest();
            //Step1: Insert Account
              Account acc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
              Address__c addrs1 = Test_Utils.insertAddress(true);
              Account_Address__c accAddrs = Test_Utils.insertAccountAddress(true, addrs1.Id, acc.Id);
            //Step2: Insert Contact  
              Contact con = new Contact (FirstName = 'Larry', LastName = 'Ellison', Title = 'Mr',
                                         AccountId = acc.Id, Email = 'larrye11@experian.com', //EDQ_On_Demand__c = true,
                                         Phone = '9799559433', EDQ_Integration_Id__c = 'TestIds123');
              insert con;
            //Step3: Insert Opportunity
              Opportunity TestOpp1 = Test_Utils.createOpportunity(acc.Id);
              Insert TestOpp1;
              Opportunity TestOpp2 = Test_Utils.createOpportunity(acc.Id);
              Insert TestOpp2;
              Product2 Prod = Test_Utils.createProduct();
              Insert Prod;
              Prod.Global_Business_Line__c = 'Decision Analytics' ; 
              Prod.Business_Line__c = 'Software'; 
              Update Prod;
              Id prcBookId = Test.getStandardPricebookId();
              PricebookEntry PrcBookEntry = Test_Utils.createPricebookEntry(Prod.Id,prcBookId ,'USD');
              Insert PrcBookEntry;
              OpportunityLineItem OptylineItem = Test_Utils.createOpportunityLineItem(TestOpp1.Id,PrcBookEntry.Id,'Renewal');
              TestOpp2.CloseDate = date.today()-10;
              TestOpp2.Type = 'Renewal';
              update TestOpp2;
              TestOpp1.CloseDate = date.today()-10;
              TestOpp1.Type = Constants.OPPTY_NEW_FROM_NEW;
              update TestOpp1;  
              OpportunityLineItem OptylineItem1 = Test_Utils.createOpportunityLineItem(TestOpp2.Id,PrcBookEntry.Id,'Renewal');
              
              optyLineItem.Type__c = Constants.OPPTY_LINE_ITEM_TYPE_RENEWAL;//Case #02088090              
              optyLineItem1.Type__c = Constants.OPPTY_LINE_ITEM_TYPE_RENEWAL;//Case #02088090
              
              Insert OptylineItem;
              Insert OptylineItem1 ;
            //Step7: Create a String containing the Id of the Account Created above and pass it to the Accountplanhelper Class to Create the plan.   
              List<string> selectedAcnts = New List<String>();
              selectedAcnts.add(acc.Id);
              Account_Plan__c acPlan = AccountPlanHelperClass.CreateAccountPlan(selectedAcnts,acc.Id);
            //Step 8: Set the Account Id on Page to be able to use in the AccountPlanControllerExtension class.
              ApexPages.currentPage().getParameters().put('Id', acPlan.Id); 
              Date DateToday = Date.today();
              Integer month = DateToday.month();
              Integer previousYear =  DateToday.year()-1;
              Date previousFiscalStartDt;
              Date previousFiscalEndDt;
              if(month < 4){
                 previousFiscalStartDt = Date.newinstance(previousYear-1, 4, 1);
                 previousFiscalEndDt = Date.newinstance(previousYear, 3, 31) ;
                 //previousFY = 'FY ' + (previousYear-1) + '/' + previousYear;
              }  
             //Determine Fiscal Year Dates when Month Falls between Apr 1st and Dec 31st
             if(month >=4){
               previousFiscalStartDt = Date.newinstance(previousYear, 4, 1);
               //CurrentFiscalStartDt = CurrentFiscalStartDt.format('yyyy-mm-dd');
               previousFiscalEndDt = Date.newinstance(previousYear+1, 3, 31) ;
               //CurrentFiscalEndDt = CurrentFiscalEndDt.format('yyyy-mm-dd');
               //previousFY = 'FY ' + previousYear + '/' + (previousYear+1);
              }  
      AccountPlanControllerExtension controller1 = new AccountPlanControllerExtension();  
      controller1.determinePreviousFiscalStrtAndEndDate();
         
      System.assertEquals(controller1.previousFiscalStartDt,previousFiscalStartDt);//Final Assert for this Test.
      System.assertEquals(controller1.previousFiscalEndDt ,previousFiscalEndDt);//Final Assert for this Test.
    }//End of Test for function to Determine Previous Fiscal Dates

 }
 // TP added test Class: Scenario 8: Test class Function to Ensure coverage for editplan function.
    @isTest
      public static void test_editPlan_Fuction(){
       // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        system.runAs(testUser1) {
            Test.startTest();
            //Step1: Insert Account
              Account acc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
            //Step7: Create a String containing the Id of the Account Created above and pass it to the Accountplanhelper Class to Create the plan.   
              List<string> selectedAcnts = New List<String>();
              selectedAcnts.add(acc.Id);
              Account_Plan__c acPlan = AccountPlanHelperClass.CreateAccountPlan(selectedAcnts,acc.Id);
            //Step 8: Set the Account Id on Page to be able to use in the AccountPlanControllerExtension class.
              ApexPages.currentPage().getParameters().put('Id', acPlan.Id); 
              }  
              AccountPlanControllerExtension controller1 = new AccountPlanControllerExtension();
              PageReference PegRef = controller1.editplan();
             Test.stopTest();
    
        System.assertEquals(PegRef!=null,True);

    }//End of Test class Function to Ensure coverage for editplan function.



}