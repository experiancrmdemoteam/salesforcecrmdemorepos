/*==================================================================
 * Experian
 * Created By: Paul Kissick
 * Created Date: Oct 1st, 2015
 * 
 * Description: T-437698 Extension to page to intercept the New button on Opportunity tab
 *   and redirect only if presented with the conid parameter, referenced in URLRedirect class/page
 *  Caused by Winter16 changes.
 * 
 * Modified Date          By                      Description Of Change
 * 
 *==================================================================*/
@isTest
private class OpportunityNewPageExtension_Test {

  private static testMethod void testRedirectFromContact() {
    
    Account a = Test_Utils.insertAccount();
    Contact c = Test_Utils.insertContact(a.Id);

    PageReference pageRef = Page.OpportunityNewPage;
    Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('conid', c.Id);    
  
    Opportunity o = new Opportunity();
    
    OpportunityNewPageExtension onp = new OpportunityNewPageExtension(new ApexPages.standardController(o));
    
    system.assertNotEquals(null,onp.prepareToRedirect(),'Failed to redirect');
    
  }
  
  private static testMethod void testNewOpportunityButton() {
    
    PageReference pageRef = Page.OpportunityNewPage;
    Test.setCurrentPage(pageRef);
  
    Opportunity o = new Opportunity();
    
    OpportunityNewPageExtension onp = new OpportunityNewPageExtension(new ApexPages.standardController(o));
    
    system.assertEquals(null,onp.prepareToRedirect(),'Redirected and should not have!');
    
  }

}