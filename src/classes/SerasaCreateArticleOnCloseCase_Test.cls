/**=====================================================================
  * Experian
  * Name: SerasaCreateArticleOnCloseCase
  * Description: W-007962: Test class for SerasaCreateArticleOnCloseCase
  * Created Date: April 10 2017
  * Created By: Ryan (Weijie) Hu, UCInnovation
  *
  * Date Modified      Modified By                  Description of the update
  * 
  *=====================================================================*/
@isTest
private class SerasaCreateArticleOnCloseCase_Test {
	
	@isTest static void test_method_one() {
		Case newCase = new Case();
		newCase.Description = 'test desc';
		newCase.Subject = 'test sub';
		insert newCase;

		String caseId = newCase.Id;

		ApexPages.currentPage().getParameters().put('sourceId', caseId);
		ApexPages.currentPage().getParameters().put('sfdc.override', '1');


		FAQ__kav newArticle = new FAQ__kav();

		ApexPages.KnowledgeArticleVersionStandardController ctrl = new ApexPages.KnowledgeArticleVersionStandardController((sObject) newArticle);
		new SerasaCreateArticleOnCloseCase(ctrl);
	}
	
}