/*=============================================================================
 * Experian
 * Name: CommunityCaseListViewExt
 * Description: Extension class to handle simple custom community list views
 * Created Date: 31 Aug 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

public with sharing class CommunityCaseListViewExt {
  
  public CommunityCaseListViewExt(ApexPages.StandardController con) {
  }
  public CommunityCaseListViewExt(ApexPages.StandardSetController setCon) {
  }
  
  public String showList {get{if (showList==null) showList = '1'; return showList;}set;}
  
  @isTest
  private static void testExt() {
    
    CommunityCaseListViewExt cve1 = new CommunityCaseListViewExt(new ApexPages.StandardController(new Case()));
    CommunityCaseListViewExt cve2 = new CommunityCaseListViewExt(new ApexPages.StandardSetController(new List<Case>()));
    
    cve1.showList = '1';
    cve1.showList = '2';
    
    system.assertEquals('2', cve1.showList);
    
  } 
  
}