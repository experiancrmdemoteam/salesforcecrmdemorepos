/**=====================================================================
 * Appirio, Inc
 * Name: AddPreviousChatterUserStat
 * Description: The following batch class is designed to be scheduled to run once every Sunday.
                    This class will get all new Chatter user Stats from an analytic snapshot and assign a Chatter game to them
 * Created Date: 5/5/2015
 * Created By: Diego Olarte (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 * 03/15/2016                   Diego Olarte                 Case#01838499: Set queue system
 * Apr 7th, 2016                Paul Kissick                 Fixed failing tests - Replaced finish method content
 =====================================================================*/
global class AddPreviousChatterUserStat implements Database.Batchable<sObject> {

  global Database.Querylocator start ( Database.BatchableContext bc ) {
  
    String query = 'Select Id, User__c, Chatter_Game__c, User_Region__c, User_Function__c, User_Business_Line__c, X4th_Priority__c FROM User_Chatter_stats__c';
   
    return Database.getQueryLocator (query);
  }

  global void execute (Database.BatchableContext bc, List<User_Chatter_stats__c> scope) {

    List<User_Chatter_stats__c> newchatterUserStatList = [Select Id,Previous_User_Chatter_stat__c, Record_Locator_for_User__c, LastModifiedDate FROM User_Chatter_stats__c WHERE  Previous_User_Chatter_stat__c = '' AND LastModifiedDate = TODAY AND Id in :scope];
        
    //Set off all User Chatter Stat ids
    
    Set<String> newStatId = new Set<String>();
    
    for (User_Chatter_stats__c newUserChatterStat : newchatterUserStatList) {
    if (newUserChatterStat.Record_Locator_for_User__c != null) {
      newStatId.add(newUserChatterStat.Record_Locator_for_User__c);}
    }
    system.debug('Diego newStatId:'+newStatId);
    //List of all Previous Chatter User Stats
    
    List<User_Chatter_stats__c> previousUserChatterStat = [Select Id,Record_Locator_for_User__c, Last_Week_s_Stat__c FROM User_Chatter_stats__c WHERE  Last_Week_s_Stat__c = true AND Record_Locator_for_User__c in :newStatId];
    
    //Map to search previous stats
    
    Map<String, User_Chatter_stats__c> pucsToUse = new Map<String, User_Chatter_stats__c>();
    
    for (User_Chatter_stats__c pucs : previousUserChatterStat) {
      pucsToUse.put(pucs.Record_Locator_for_User__c, pucs);
    }
    system.debug('Diego pucsToUse:'+pucsToUse);
    //List of records to update
    
    List<User_Chatter_stats__c> newUserStatListtoUpdate = new List<User_Chatter_stats__c>{};
    
    //Set the Previous Chatter User Stat
    
    if (!pucsToUse.isEmpty()) {    
      for (User_Chatter_stats__c newUserChatterStat : newchatterUserStatList){
        if (newUserChatterStat.Record_Locator_for_User__c != null) {
          User_Chatter_stats__c pid = pucsToUse.get(newUserChatterStat.Record_Locator_for_User__c);
          if (pid != null) {
            newUserChatterStat.Previous_User_Chatter_stat__c = pid.Id;
            newUserStatListtoUpdate.add(newUserChatterStat);
          }
        }
      }
    }
    try {
      update newUserStatListtoUpdate;
    }
    catch(Exception e) {
      system.debug(e.getMessage());
    }            
  }  

  //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'AddPreviousChatterUserStat', true);
    
    /*
    // PK Removed this and replaced with above as it's much easier to manage.
    BatchSchedulingIDstorage__c BSIDS = BatchSchedulingIDstorage__c.getOrgDefaults();
      
      if (!Test.isRunningTest()) {
      
      System.abortJob(BSIDS.BSIDS05__c );
      
      }
    
      AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                               TotalJobItems, CreatedBy.Email
                        FROM AsyncApexJob WHERE Id =: BC.getJobId()];
      
      System.debug('\n[AddPreviousChatterUserStat: finish]: [The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.]]');
    */
  }
    
}