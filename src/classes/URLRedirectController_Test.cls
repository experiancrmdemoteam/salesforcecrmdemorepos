/**========================================================================================
 * Appirio, Inc
 * Name: URLRedirectController_Test
 * Description: This class contains test method to test URLRedirectController
 * Created Date: March 13th, 2014
 * Created By: Nathalie LE GUAY (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 * Jul 28th, 2014               James Weatherall             Case #2323 Added testOppFromOpportunity() method
 * Feb 26th, 2015               James Weatherall             Case #9748 Added testPreSalesRequestFromOpportunity() method
 * Aug 27th, 2015               Paul Kissick                 S-210749/T-430758: Adding checks for 'Inactive' users.
 * Jul 18th, 2016               Manoj Gopu(QA)               CRM2.0W-005436 added testcontactroleupdate() method
 * Aug 23rd, 2016               Manoj Gopu                   CRM2.0W-005421: Added New Method testcreateorderfromcpq()
 * Aug 26th, 2016               Tyaga Pati                   CRM2.0W-005402: Removed References to Inactive__c in testOppFromInactiveContact because field will not be used define Contact Status anymore.
 * Oct 06th, 2016               Manoj Gopu                   CRM2.0W-005952: Added testOppFromMarketingTask() method
 * Dec 14th, 2016               Manoj Gopu                   Case:02108066: Added decision Parameter to improve the code coverage 
 ========================================================================================*/
@isTest
private class URLRedirectController_Test {

  @testSetup
  private static void prepareTestData() {
    Custom_Fields_Ids__c customFields = new Custom_Fields_Ids__c(
      SetupOwnerId = Userinfo.getOrganizationId(),
      Order_Account__c = '00N123456789123',
      Order_Campaign__c = '00N123456789124',
      Order_Contact__c = '00N123456789125'
    );
    insert customFields;

    Record_Type_Ids__c recordTypeIds = new Record_Type_Ids__c(
      SetupOwnerId = Userinfo.getOrganizationId()
    );
    insert recordTypeIds;
  }

  static testMethod void testOppFromContact() {
    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOpportunity');
    pageRef.getParameters().put('Id', cont.Id);

    Test.startTest();
    Test.setCurrentPage(pageRef);
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    system.assertEquals(0, ApexPages.getMessages().size());
    Test.stopTest();
  }

  static testMethod void testOppFromInactiveContact() {
    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);
    //cont.Inactive__c = true;//W-005402
    cont.Status__c = 'No Longer With Company';//TP Added this line to set Contact Status for opp creation testing
    update cont;

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOpportunity');
    pageRef.getParameters().put('Id', cont.Id);

    Test.startTest();
    Test.setCurrentPage(pageRef);
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    system.assertEquals(1, ApexPages.getMessages().size());
    Test.stopTest();
  }

  static testMethod void testOppFromOpportunity() {
    Account acc = Test_Utils.insertAccount();
    Contact con = Test_Utils.insertContact(acc.Id);
    Opportunity opp = Test_Utils.insertOpportunity(acc.Id);
    Test_Utils.insertOpportunityCR(true, con.Id, opp.Id);

    system.debug('opp.Id = ' + opp.Id);
    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOpportunity');
    pageRef.getParameters().put('Name', opp.Name);
    pageRef.getParameters().put('Id', opp.Id);

    Test.startTest();
    Test.setCurrentPage(pageRef);
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    system.assertEquals(0, ApexPages.getMessages().size());
    Test.stopTest();
  }

  static testMethod void testOppFromEvent() {
    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);
    Event evt = Test_Utils.insertEvent(cont.Id, null);

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOpportunity');
    pageRef.getParameters().put('Id', evt.Id);
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    system.assertEquals(0, ApexPages.getMessages().size());
    Test.stopTest();
  }

  static testMethod void testOppFromEventLinkedToLead() {
    Lead lead = Test_Utils.insertLead();
    Event evt = Test_Utils.insertEvent(lead.Id, null);

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOpportunity');
    pageRef.getParameters().put('Id', evt.Id);
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    ApexPages.Message[] messages = ApexPages.getMessages();
    system.assertNotEquals(0, messages.size());
    system.assertEquals(Label.URL_Redirect_Missing_Contact, messages.get(0).getSummary());
    Test.stopTest();
  }

  static testMethod void testOppFromTask() {
    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);
    Task task = Test_Utils.insertTask(cont.Id, null);

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOpportunity');
    pageRef.getParameters().put('Id', task.Id);
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    system.assertEquals(0, ApexPages.getMessages().size());
    Test.stopTest();
  }

  static testMethod void testOppFromMarketingTask() {
    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);
    Task task = new Task();
    task.WhoId = cont.Id;
    task.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.RECORDTYPE_MARKETING_TASK).getRecordTypeId();
    insert task;

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOpportunity');
    pageRef.getParameters().put('Id', task.Id);
    pageRef.getParameters().put('Decision', 'yes'); // Case:02108066 Added by MG
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    
    system.assertEquals(0, ApexPages.getMessages().size());
    Test.stopTest();
  }

  static testMethod void testOppFromMarketingActivity() {
    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);
    ELQA_Marketing_Activity__c mktgActivity = new ELQA_Marketing_Activity__c(Account__c = acc.Id, Contact__c = cont.Id);
    insert mktgActivity;

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOpportunity');
    pageRef.getParameters().put('Id', mktgActivity.Id);
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    system.assertEquals(0, ApexPages.getMessages().size());
    Test.stopTest();
  }

  static testMethod void testOppFromMarketingActivityMissingContact() {
    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);
    ELQA_Marketing_Activity__c mktgActivity = new ELQA_Marketing_Activity__c(Account__c = acc.Id);
    insert mktgActivity;

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOpportunity');
    pageRef.getParameters().put('Id', mktgActivity.Id);
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    ApexPages.Message[] messages = ApexPages.getMessages();
    system.assertNotEquals(0, messages.size());
    system.assertEquals(Label.URL_Redirect_Missing_Contact_or_Account, messages.get(0).getSummary());
    Test.stopTest();
  }

  static testMethod void testOrderFromContact() {
    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOrder');
    pageRef.getParameters().put('Id', cont.Id);
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    system.assertEquals(0, ApexPages.getMessages().size());
    Test.stopTest();
  }

  static testMethod void testOrderFromEvent() {
    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);
    Event evt = Test_Utils.insertEvent(cont.Id, null);

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOrder');
    pageRef.getParameters().put('Id', evt.Id);
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    system.assertEquals(0, ApexPages.getMessages().size());
    Test.stopTest();
  }

  static testMethod void testOrderFromTask() {
    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);
    Task task = Test_Utils.insertTask(cont.Id, null);

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOrder');
    pageRef.getParameters().put('Id', task.Id);
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    system.assertEquals(0, ApexPages.getMessages().size());
    Test.stopTest();
  }

  static testMethod void testOrderFromTaskLinkedToLead() {
    Lead lead = Test_Utils.insertLead();
    Task task = Test_Utils.insertTask(lead.Id, null);

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOrder');
    pageRef.getParameters().put('Id', task.Id);
    pageRef.getParameters().put('Decision', 'yes'); // Case:02108066 Added by MG
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    ApexPages.Message[] messages = ApexPages.getMessages();
    system.assertNotEquals(0, messages.size());
    system.assertEquals(Label.URL_Redirect_Missing_Contact, messages.get(0).getSummary());
    Test.stopTest();
  }

  static testMethod void testOrderFromMarketingActivity() {
    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);
    ELQA_Marketing_Activity__c mktgActivity = new ELQA_Marketing_Activity__c(Account__c = acc.Id, Contact__c = cont.Id);
    insert mktgActivity;

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOrder');
    pageRef.getParameters().put('Id', mktgActivity.Id);
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    system.assertEquals(0, ApexPages.getMessages().size());
    Test.stopTest();
  }

  static testMethod void testOrderFromUnsupportedObject() {
    Lead lead = Test_Utils.insertLead();

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOpportunity');
    pageRef.getParameters().put('Id', lead.Id);
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    ApexPages.Message[] messages = ApexPages.getMessages();
    system.assertNotEquals(0, messages.size());
    system.assertEquals(Label.URL_Redirect_Opp_From_Object_Not_Supported, messages.get(0).getSummary());
    Test.stopTest();
  }

  static testMethod void testPreSalesRequestFromOpportunity() {
    Account account = Test_Utils.insertAccount();
    Opportunity opportunity = Test_Utils.insertOpportunity(account.Id);

    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createPreSalesSupport');
    pageRef.getParameters().put('Id', opportunity.Id);
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    ApexPages.Message[] messages = ApexPages.getMessages();

    system.assertEquals(0, messages.size());
    Test.stopTest();
  }

  // TODO: Add assertions!
  static testMethod void testContactRoleUpdate() {
    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);
    cont.Decision_Maker__c=true;
    update cont;
    PageReference pageRef = Page.URLRedirect;
    pageRef.getParameters().put('action', 'createOpportunity');
    pageRef.getParameters().put('Id', cont.Id);
    pageRef.getParameters().put('Decision', 'Yes');
    Test.setCurrentPage(pageRef);

    Test.startTest();
    URLRedirectController urlRedirect = new URLRedirectController();
    urlRedirect.getURL();
    Test.stopTest();
  }


  // TODO: Add assertions!
  static testMethod void testCreateOrderFromCPQ() {
    Test.startTest();

    CPQ_Settings__c objC=new CPQ_Settings__c();
    objC.Name='CPQ';
    objC.Company_Code__c='Experian';
    objC.CPQ_API_Endpoint__c='salesforce.com';
    insert objC;

    User testUser1 = Test_Utils.createUser('system Administrator');
    testUser1.Business_Unit__c = 'APAC Marketing Services India';
    testUser1.Business_Line__c = 'India Credit Services';
    testUser1.Global_Business_Line__c = 'Credit Services';
    testUser1.Sales_Team__c = 'APAC MS India';
    testUser1.Sales_Sub_Team__c = 'EMEA CS SA Financial Services';
    testUser1.Region__c = 'India';
    testUser1.Country__c = 'India';
    testUser1.CPQ_User__c=true;
    testUser1.CPQ_User_type__c='CFO';
    insert testUser1;

    Account acc = Test_Utils.insertAccount();
    Contact cont = Test_Utils.insertContact(acc.Id);

    system.runAs(testUser1) {
      PageReference pageRef = Page.URLRedirect;
      pageRef.getParameters().put('action', 'createOrder');
      pageRef.getParameters().put('Id', cont.Id);
      Test.setCurrentPage(pageRef);

      URLRedirectController urlRedirect = new URLRedirectController();
      urlRedirect.getURL();
    }
    Test.stopTest();
  }

}