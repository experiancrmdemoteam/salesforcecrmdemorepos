/**********************************************************************************
 * Experian
 * Name: CaseComponentTriggerHandler
 * Description: Trigger Handler for Case_Component__c
 *             
 * Created Date: Oct 21st, 2015
 * Created By: Paul Kissick
 * 
 * Date Modified       Modified By                  Description of the update
 *********************************************************************************/
public with sharing class CaseComponentTriggerHandler {

  private static List<Deployment_Component__c> deployComps;

  public static void beforeDelete(Map<Id,Case_Component__c> oldMap) {
    prepareDeploymentComponents(oldMap);
  }

  public static void afterDelete(Map<Id,Case_Component__c> oldMap) {
    cleanupDeploymentComponents(oldMap);
  }
  
  
  //===========================================================================
  // Find and store all deployment components related to these case components
  //===========================================================================
  public static void prepareDeploymentComponents(Map<Id,Case_Component__c> caseComps) {
    deployComps = new List<Deployment_Component__c>();
    deployComps = [
      SELECT Id FROM Deployment_Component__c
      WHERE Case_Component__c IN :caseComps.keySet()
    ];
  }
  
  
  //===========================================================================
  // Delete any deployment components related to the case components
  //===========================================================================
  public static void cleanupDeploymentComponents(Map<Id,Case_Component__c> caseComps) {
    
    if (deployComps != null && deployComps.size() > 0) {
    
      List<Deployment_Component__c> deployCompsToDelete = deployComps;

      try {
        delete deployCompsToDelete;
      }
      catch (Exception e) {
        system.debug('CaseComponentTriggerHandler in method cleanupDeploymentComponents() Error:' + e.getMessage());
        ApexLogHandler.createLogAndSave('CaseComponentTriggerHandler','cleanupDeploymentComponents', e.getStackTraceString(), e);
      }
    }
    
  }

}