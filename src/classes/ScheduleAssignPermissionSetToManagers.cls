/**=====================================================================
 * Experian
 * Name: ScheduleAssignPermissionSetToManagers
 * Description: Schedule BatchAssignPermissionSetToManagers to run daily
 * Created Date: 02nd Mar 2017
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 ============================================================================*/
global class ScheduleAssignPermissionSetToManagers implements Schedulable {
   @testVisible global List<User> testUsers;    
  global void execute(SchedulableContext SC) {
    BatchAssignPermissionSetToManagers batch = new BatchAssignPermissionSetToManagers(); 
     if (Test.isRunningTest()) {
      batch.testUsers = testUsers;
    }
    Database.executeBatch(batch, 200); 
  }
}
      // Code to run in dev console 
     //  System.schedule('AssignPermissionSetToManagers', '0 0 23 * * ?', new ScheduleAssignPermissionSetToManagers());