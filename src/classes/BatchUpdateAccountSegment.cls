/**=====================================================================
 * Appirio, Inc
 * Name: BatchUpdateAccountSegment
 * Description: Batch job for updating Account Segment if Hierarchy__c has Apply_Parent_Change_to_Account_Segments__c set to true
 * Created Date: Aug 17th, 2015
 * Created By: Noopur Sundriyal
 *
 * Date Modified                Modified By                  Description of the update
 * 15th Sep, 2015               Noopur                       I-180509 - Modified the code to resolve the issue. Changed the maps used in the code.
 * Sep 15th, 2015               Nathalie Le Guay             I-180509: Added Relationship_Type__c and CurrencyIsoCode to new Account_Segment__c
 * Sep 23th, 2015               Noopur                       I-180754 : Modified the logic to remove the duplicate Account Segment record creation.
 * Nov 9th, 2015                Paul Kissick                 Case 01234035: Adding FailureNotificationUtility
 * Aug 3rd, 2016                Paul Kissick                 CRM2:W-005332: Adding optimisations
 =======================================================================*/
global class BatchUpdateAccountSegment implements Database.Batchable<sObject>, Database.Stateful {
  
  global Set<String> failedAccountSegmentsErrors;
  global Set<Id> hierarchyIdsToUpdate ;
  
  //========================================================================================
  // Start
  //========================================================================================
  global Database.QueryLocator start(Database.BatchableContext BC) {
    hierarchyIdsToUpdate = new set<Id>();
    failedAccountSegmentsErrors = new Set<String>();
    //String qryString = 'SELECT Id,Apply_Parent_Change_to_Account_Segments__c,Parent__c,Value__c,Type__c,Unique_Key__c,Processed_Reparenting__c FROM Hierarchy__c WHERE Apply_Parent_Change_to_Account_Segments__c = \'true\' AND Old_Parent__c != null';
    String qryString = 'SELECT Id ,Value__c,Parent_Account_Segment__c,Account__r.Name,Type__c,Account__c, Parent_Account_Segment__r.Value__c,Segment__r.Parent__r.Type__c,'+
                        'Segment__c ,Segment__r.Apply_Parent_Change_to_Account_Segments__c,Segment__r.Old_Parent__c,Segment__r.Parent__c,Segment__r.Parent__r.Value__c'+
                        ' FROM Account_Segment__c WHERE Segment__r.Apply_Parent_Change_to_Account_Segments__c = true AND Segment__r.Old_Parent__c != null';
    return Database.getQueryLocator(qryString);
  }
  
  //========================================================================================
  // Execute
  //========================================================================================
  global void execute(Database.BatchableContext BC, List<sObject> scope) {
    set<Id> hierarchies = new set<Id>();
    set<String> ParentSet = new set<String>();
    set<Id> parentHierarchies = new set<Id>();
    set<String> accountIds = new set<String>();
    map<Id,Id> hierachyParentMap = new map<Id,Id>();
    map<String, Hierarchy__c> hierarchyMap = new map<String,Hierarchy__c>();
    map<Id,Hierarchy__c> hierarchyMap2 = new map<Id,Hierarchy__c>();
    
    map<String,Account_Segment__c> AccSegments = new map<String,Account_Segment__c> ();
    map<String,Account_Segment__c> AccSegmentsParent = new map<String,Account_Segment__c> ();
    
    for (Account_Segment__c accSeg : (List<Account_Segment__c>)scope) {
      if (accSeg.Segment__c != null) {
        hierarchies.add(accSeg.Segment__c);
      }
      if (accSeg.Segment__r.Parent__c != null) {
        parentHierarchies.add(accSeg.Segment__r.Parent__c);
      }
      if (accSeg.Segment__c != null) {
        hierachyParentMap.put(accSeg.Segment__c,accSeg.Segment__r.Parent__c);
      }
      accountIds.add(accSeg.Account__c);
      AccSegments.put(accSeg.Type__c + '-'+ accSeg.Value__c,accSeg);
    }
    system.debug('---accountIds---'+accountIds);
    system.debug('--hierarchies---'+hierarchies);
    system.debug('--parentHierarchies---'+parentHierarchies);
    
    for (Hierarchy__c hrcy : [SELECT Id, Unique_Key__c , Value__c , Type__c 
                              FROM Hierarchy__c
                              WHERE Id IN :parentHierarchies
                              OR Id IN :hierarchies]) {
      if (parentHierarchies.contains(hrcy.Id)) {
        parentSet.add(hrcy.Value__c);
        hierarchyMap.put(hrcy.Unique_Key__c, hrcy);
        hierarchyMap2.put(hrcy.Id,hrcy);
      }
    }
    system.debug('----parentSet----'+parentSet);
    
    for (Account_Segment__c accSeg : [SELECT Id, Value__c, Parent_Account_Segment__c, Type__c,
                                             Parent_Account_Segment__r.Value__c, Account__c, Account__r.Name
                                      FROM Account_Segment__c 
                                      WHERE Account__c IN :accountIds
                                      AND Value__c IN :parentSet]) {
      AccSegmentsParent.put(accSeg.Type__c + '-'+ accSeg.Value__c+'-'+accSeg.Account__c,accSeg);
    }
    system.debug('--AccSegmentsParent---'+AccSegmentsParent);
    
    // Noopur - changed the type of maps for resolving the issue I-180509
    map<String,Account_Segment__c> accSegmentParentMap = new map<String,Account_Segment__c>();
    map<String,Account_Segment__c> accSegmentParentChildMap = new map<String,Account_Segment__c>();
    List<Account_Segment__c> accSegmentToInsert = new List<Account_Segment__c>();
    List<Account_Segment__c> accSegsToUpdate = new List<Account_Segment__c>();
    system.debug('--hierarchyMap---'+hierarchyMap);
    system.debug('--hierarchyMap2---'+hierarchyMap2);
    for (Account_Segment__c accSeg : (List<Account_Segment__c>)scope) {
      system.debug('==accSeg==='+accSeg);
      system.debug('-------'+accSeg.Segment__r.Parent__r.Type__c + '-'+ accSeg.Segment__r.Parent__r.Value__c+'-'+accSeg.Account__c+'======'+AccSegmentsParent.containsKey(accSeg.Segment__r.Parent__r.Type__c + '-'+ accSeg.Segment__r.Parent__r.Value__c+'-'+accSeg.Account__c));
      if (AccSegmentsParent.containsKey(accSeg.Segment__r.Parent__r.Type__c + '-'+ accSeg.Segment__r.Parent__r.Value__c+'-'+accSeg.Account__c)) {//AccSegmentsParent.containsKey(accSeg.Type__c + '-'+ accSeg.Value__c)) {
        accSeg.Parent_Account_Segment__c = AccSegmentsParent.get(accSeg.Segment__r.Parent__r.Type__c + '-'+ accSeg.Segment__r.Parent__r.Value__c+'-'+accSeg.Account__c).Id;
        accSegsToUpdate.add(accSeg);
      }
      else if (!AccSegmentsParent.containsKey(accSeg.Segment__r.Parent__r.Type__c + '-'+ accSeg.Segment__r.Parent__r.Value__c + '-' + accSeg.Account__c) && hierarchyMap2.containsKey(accSeg.Segment__r.Parent__c)) {
        Hierarchy__c hrcy = hierarchyMap2.get(accSeg.Segment__r.Parent__c);
        String accSegName = AccountSegmentationUtility.buildSegmentName(accSeg.Account__r.Name, hrcy.Value__c);
        Account_Segment__c accSegNew = AccountSegmentationUtility.buildNewAccountSegment(accSegName, accSeg.Account__c, hrcy.Id, Constants.BU_RELATIONSHIP_TYPE_PROSPECT, hrcy.Type__c);
        accSegNew.Value__c = hrcy.Value__c;
        accSegmentParentMap.put(accSegName, accSegNew);
        accSegmentParentChildMap.put(accSegName, accSeg);
        accSegmentToInsert.add(accSegNew);
      }
      else {
       	hierarchyIdsToUpdate.add(accSeg.Segment__c);
      }
    }
    if (!accSegmentParentMap.isEmpty() && !accSegmentToInsert.isEmpty()) {
      insert accSegmentToInsert;
    }
    for (String accSeg : accSegmentParentChildMap.keySet()) {
        Account_Segment__c accSegUpdate = accSegmentParentChildMap.get(accSeg);
        Account_Segment__c parentAccSeg = accSegmentParentMap.get(accSeg);
        system.debug('--accSegUpdate---'+accSegUpdate);
        system.debug('==accSeg==='+accSeg);
        accSegUpdate.Parent_Account_Segment__c = parentAccSeg.Id;
        accSegsToUpdate.add(accSegUpdate);
    }
    if (!accSegsToUpdate.isEmpty()) {
      List<Database.SaveResult> updatedAccountSegments = Database.update(accSegsToUpdate, false);
      for (Integer i = 0; i < updatedAccountSegments.size(); i++) {
        if (!updatedAccountSegments.get(i).isSuccess()) {
          // DML operation failed
          Database.Error error = updatedAccountSegments.get(i).getErrors().get(0);
          String failedDML = error.getMessage();
          system.debug('---failedDML---'+failedDML+'---'+accSegsToUpdate.get(i).Id);
          String failedStr = '<tr><td>' + accSegsToUpdate.get(i).Id + '</td><td>' + accSegsToUpdate.get(i).Name + '</td><td>' + failedDML + '</td></tr>';
          failedAccountSegmentsErrors.add(failedStr);
        }
        else {
          hierarchyIdsToUpdate.add(accSegsToUpdate.get(i).Segment__c);
        }
      }
    }
  }
  
  //========================================================================================
  // Finish
  //========================================================================================
  global void finish(Database.BatchableContext BC) {
    List<Hierarchy__c> hierarchyUpdate = new List<Hierarchy__c>();
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchUpdateAccountSegment', false);
    
    String emailBody = '';
    
    if (failedAccountSegmentsErrors != null && !failedAccountSegmentsErrors.isEmpty()) {
      bh.batchHasErrors = true;
      emailBody += '\n*** Account Segment record update failed:\n';  
      for (String currentRow : failedAccountSegmentsErrors) {
        emailBody += currentRow+'\n\n';
      }
      emailBody += '\n\n';
    }
        
    bh.emailBody += emailBody;

    system.debug('--hierarchyIdsToUpdate ---'+hierarchyIdsToUpdate);

    // Update the Hierarchy records for which the Account Segments have been updated
    if (hierarchyIdsToUpdate != null && !hierarchyIdsToUpdate.isEmpty()) {
      for (Hierarchy__c hrchy : [SELECT Id, Old_Parent__c, Processed_Reparenting__c
                                 FROM Hierarchy__c
                                 WHERE Id IN :hierarchyIdsToUpdate]) {
        hrchy.Old_Parent__c = null;
        hrchy.Processed_Reparenting__c = true;
        hierarchyUpdate.add(hrchy);
      }
    }
    
    update hierarchyUpdate;
    
    bh.sendEmail();
  }

}