/**=====================================================================
 * Experian
 * Name: SMXEDQGPDProcessCaseBatch 
 * Description: Finds cases related to Global EDQ GPD - Internal and creates survey nomination records
 * Created Date: 19/04/2017
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 =====================================================================*/

global class SMXEDQGPDProcessCaseBatch implements Database.Batchable<Case> {

  global static String strSurveyName = 'Global EDQ GPD - Internal';
  global static String strSurveyId = 'EXPERIAN_113966';

  global Iterable<Case> start(database.batchablecontext BC) {
    
    return [
      SELECT Id, ContactId, Contact.Email, (SELECT Id FROM Survey__r)
      FROM Case 
      WHERE IsClosed = true 
      AND ClosedDate > YESTERDAY
      AND Status IN ('Closed Resolved','Closed No Action','Closed Not Resolved','Closed Escalated','Closed - Duplicate')
      AND Case_Owned_by_Queue__c = false
      AND Reason != 'Hosted Services'
      AND Requestor_Email__c LIKE '%experian.com'
      AND RecordTypeId IN (
        SELECT Id 
        FROM RecordType 
        WHERE Name IN ('EDQ GPD Support')
      ) 
      AND ContactId != null 
      AND ContactId != ''
    ];
  }

  global void execute(Database.BatchableContext BC, List<Case> scope){
 
    List <Feedback__c> feedbackList = new List<Feedback__c>();
    
    Long lgSeed = System.currentTimeMillis();

    for(Case cs : scope){
      
      List<Feedback__c> lstSurvey = cs.Survey__r;

      if (lstSurvey.isEmpty() && String.isNotBlank(cs.Contact.Email)) {
        lgSeed = lgSeed + 1;
        Feedback__c feedback = new Feedback__c();
        feedback.Case__c = cs.Id;
        feedback.Name = 'P_' + lgSeed;
        feedback.Contact__c = cs.ContactId; //ContactName 
        feedback.Status__c = 'Nominated';               
        feedback.DataCollectionName__c = strSurveyName;
        feedback.DataCollectionId__c = strSurveyId;
        feedbackList.add(feedback);        
      }
    }
    insert feedbackList;
  }

  global void finish(Database.BatchableContext info){
  }
    
  
}