/**====================================================================================
 * Experian
 * Name: EventTriggerHandler
 * Description: EventTriggerHandler - Created for case #1010967
 * Created Date: 15th July, 2015
 * Created By: Paul Kissick 
 * 
 * Modified Date        Modified By      Description of the update
 ======================================================================================*/ 
public class EventTriggerHandler {

  public static void beforeInsert(List<Event> newList){
    setRelatedToObjectField(newList);
  }
  
  public static void beforeUpdate(List<Event> newList, Map<Id,Event> oldMap) {
    setRelatedToObjectField(newList);
  }
  
  // PK: Case #01010967 - Adding for knowing what the object is.
  // Note this is very similar in the TaskTriggerHandler class!
  public static void setRelatedToObjectField(List<Event> events) {
    for(Event t : events) {
      Id checkId;
      t.Related_To__c = null;
      if (t.WhatId != null) {
        checkId = t.WhatId;
      }
      else if (t.WhoId != null) {
        checkId = t.WhoId;
      }
      if (checkId != null) {
        try {
          String objectLabel = checkId.getSobjectType().getDescribe().getLabel();
          t.Related_To__c = objectLabel;
        }
        catch (Exception ex) {
          ApexLogHandler.createLogAndSave('EventTriggerHandler','setRelatedToObjectField', ex.getStackTraceString(), ex);
        }
      }
    }
  }
}