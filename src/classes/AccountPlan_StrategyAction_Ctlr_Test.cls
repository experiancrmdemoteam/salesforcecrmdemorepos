/**=====================================================================
 * Name: AccountPlan_StrategyAction_Ctlr_Test
 * Description: See case #01848189 - Account Planning Enhancements
 * Created Date: Apr. 20th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By            Description of the update
 * Apr. 20th, 2016       James Wills            Case #01848189: Created
 * May 6th, 2016         James Wills            Changed modifiers on class and test method 
 * May 9th, 2016         James Wills            Re-organised class and added test for adding/removing Account Plan Objectives
 * June 7th, 2016        James Wills            Put in changes for setting up test data to prevent mixed DML error messages.
 * June 7th, 2016        Sadar Yacob            Changes due to test class failure in prod due to user1 being generic added where clause to use alias 
  =====================================================================*/
@isTest
private class AccountPlan_StrategyAction_Ctlr_Test{  

  private static testmethod void test_AdditionAndDeletionOfObjectives(){

    Account testAccount1           = [SELECT id FROM Account LIMIT 1];
    Contact newContact1            = [SELECT id FROM Contact LIMIT 1];    
    Account_Plan__c accountPlan    = [SELECT id,Name FROM Account_Plan__c LIMIT 1];    
    User user1                     = [SELECT id FROM User WHERE alias = 'Test1' LIMIT 1];


    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)accountPlan);
    AccountPlan_StrategyAction_Controller controller = new AccountPlan_StrategyAction_Controller(stdController);
    
    DateTime eventDate1 = DateTime.now();
    
    String dueDate = string.valueOfGmt(eventDate1);
      
    Test.startTest();    
      Integer sizeOfObjectivesList = controller.accountPlanObjectivesList.size();
      
      //1. Test creation of Objective
      //Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Objective_Rec__c
      //Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Objective__c
      //accountPlan.Name
      //accountPlanId      
      //Custom_Fields_Ids__c cust =  new Custom_Fields_Ids__c();
      

      PageReference updatedAccountPlanPage1 = controller.newAccountPlanObjective();
      Test.setCurrentPage(updatedAccountPlanPage1);

      
      ApexPages.currentPage().getParameters().put('Name','Testing adding an Objective.');//Name
      ApexPages.currentPage().getParameters().put('CF00N56000000deLm_lkid',accountPlan.id);//Plan
      ApexPages.currentPage().getParameters().put('00N56000000deLr','Testing adding an Objective.');//Detail
      ApexPages.currentPage().getParameters().put('00N56000000deLq', dueDate);//DueDate
 
      PageReference newPage = updatedAccountPlanPage1.setRedirect(true);
      
      
      list<Account_Plan_Objective__c> apoAdded = [SELECT id, Name, Account_Plan__c, Objective_Detail__c FROM Account_Plan_Objective__c];


      ApexPages.StandardController stdController2 = new ApexPages.StandardController((sObject)accountPlan);
      AccountPlan_StrategyAction_Controller controller2 = new AccountPlan_StrategyAction_Controller(stdController2);
      
      //Assertion 
      //system.assert(controller2.accountPlanObjectivesList.size() > sizeOfObjectivesList, 'AccountPlan_StrategyAction_Ctlr_Test.test_AdditionAndDeletionOfObjectives: Account_Plan_Objective__c not created: ' + controller2.accountPlanObjectivesList.size() + ' ' + sizeOfObjectivesList + ' ' + apoAdded.size() + ' ' + apoAdded[0].Name + ' ' + apoAdded[1].Name);
      //Assertion
      
      
      //2. Test deletion of Objective
      List<Account_Plan_Objective__c> apoList = [SELECT id FROM Account_Plan_Objective__c WHERE Account_Plan__c =:accountPlan.id];
      sizeOfObjectivesList = apoList.size();
      
      controller.selectedIdToDelete = [SELECT id FROM Account_Plan_Objective__c WHERE Account_Plan__c =:accountPlan.id LIMIT 1].id; 
           
      PageReference updatedAccountPlanPage2 = controller.doDeleteObjective();
      
      apoList = [SELECT id FROM Account_Plan_Objective__c WHERE Account_Plan__c =:accountPlan.id];

      //Assertion 
      system.assert(apoList.size() < sizeOfObjectivesList, 'AccountPlan_StrategyAction_Ctlr_Test.test_AdditionAndDeletionOfObjectives: Account_Plan_Objective__c not deleted.'
                    + ' ' + controller.accountPlanObjectivesList.size() + ' ' + sizeOfObjectivesList);
      //Assertion
      
    Test.stopTest();
    
  }


  private static testmethod void test_AdditionOf_TasksAndEvents_ForActivitiesList(){

    Account testAccount1           = [SELECT id FROM Account LIMIT 1];
    Contact newContact1            = [SELECT id FROM Contact LIMIT 1];    
    Account_Plan__c accountPlan    = [SELECT id FROM Account_Plan__c LIMIT 1];    
    User user1                     = [SELECT id FROM User WHERE alias = 'Test1' LIMIT 1];


    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)accountPlan);
    AccountPlan_StrategyAction_Controller controller = new AccountPlan_StrategyAction_Controller(stdController);
    
    DateTime eventDate1 = DateTime.now();
    
    Test.startTest();
      
      controller.buildStrategyAndActionActivitiesList();     
      //Assertion 
      system.assert(controller.strategyAndActionActivitiesList.size()>0, 'AccountPlan_StrategyAction_Ctlr_Test: AccountPlan_StrategyAction_Ctlr_Test: No Open Activities found.');
      //Assertion
      
      //1. Test creation of Task
      controller.buildStrategyAndActionActivitiesList();
      Integer sizeOfActivitiesList = controller.strategyAndActionActivitiesList.size();
          
      PageReference newTaskPage = controller.createTask();

      Task newTaskForDisplay1 = new Task(WhatId=accountPlan.id,
                                         WhoID = newContact1.id,
                                         OwnerId=user1.id,
                                         Priority='Normal',
                                         CreatedById=user1.id,
                                         CreatedDate=DateTime.now(),
                                         TaskSubtype='Task',
                                         Related_To__c='Account Plan',                                          
                                         Status='Not Started');
      insert newTaskForDisplay1;
         
      controller.buildStrategyAndActionActivitiesList();
      
      //Assertion
      //system.assert(controller.strategyAndActionActivitiesList.size() != sizeOfActivitiesList, 'AccountPlan_StrategyAction_Ctlr_Test: AccountPlan_StrategyAction_Ctlr_Test: Task not created. '
      //             + controller.strategyAndActionActivitiesList.size() + ' ' + sizeOfActivitiesList);
      //Assertion
      
      
      //2. Test creation of Event
      controller.buildStrategyAndActionActivitiesList();
      sizeOfActivitiesList = controller.strategyAndActionActivitiesList.size();
      
      PageReference newEventPage        = controller.createEvent();

      
      Event newEventForForDisplay1 = new Event(WhatId=accountPlan.id,
                                             DurationInMinutes=20,
                                             ActivityDateTime=eventDate1,
                                             Subject='Test',
                                             OwnerId=user1.id
                                             );
      insert newEventForForDisplay1;
      
      controller.buildStrategyAndActionActivitiesList();
      
      //Assertion
      //system.assert(controller.strategyAndActionActivitiesList.size() != sizeOfActivitiesList, 'AccountPlan_StrategyAction_Ctlr_Test: AccountPlan_StrategyAction_Ctlr_Test: Event not created.'
      //              + controller.strategyAndActionActivitiesList.size() + ' ' + sizeOfActivitiesList);
      //Assertion
      

      test.stopTest(); 
    
    }
       
       
       
  private static testmethod void test_CloseAndDelete_TasksAndEvents_1(){

    Account testAccount1           = [SELECT id FROM Account LIMIT 1];
    Contact newContact1            = [SELECT id FROM Contact LIMIT 1];    
    Account_Plan__c accountPlan    = [SELECT id FROM Account_Plan__c LIMIT 1];    
    User user1                     = [SELECT id FROM User WHERE alias = 'Test1' LIMIT 1];


    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)accountPlan);
    AccountPlan_StrategyAction_Controller controller = new AccountPlan_StrategyAction_Controller(stdController);
    
    DateTime eventDate1 = DateTime.now();
    
    Test.startTest();
      
      //1. Test close Task associated with Account Plan
      controller.buildStrategyAndActionActivitiesHistoryList(); 
      //Assertion   
      system.assert(controller.strategyAndActionActivityHistoryList.size()>0, 'AccountPlan_StrategyAction_Ctlr_Test: AccountPlan_StrategyAction_Ctlr_Test: No Closed Activities found.');
      //Assertion


      controller.buildStrategyAndActionActivitiesList();
      Integer sizeOfStrategyAndActionActivitiesHistoryList = controller.strategyAndActionActivityHistoryList.size();          
      
      List<Task> taskList2 = [SELECT id FROM Task WHERE WhatId=:accountPlan.id];    
      controller.selectedId=taskList2[0].id;     
      PageReference closeActivityPage  = controller.doCloseActivity();
      
      controller.buildStrategyAndActionActivitiesHistoryList();
      
      //Assertion
      system.assert(controller.strategyAndActionActivityHistoryList.size() != sizeOfStrategyAndActionActivitiesHistoryList, 'AccountPlan_StrategyAction_Ctlr_Test: AccountPlan_StrategyAction_Ctlr_Test: Task not closed.'
                    + controller.strategyAndActionActivityHistoryList.size() + ' ' + sizeOfStrategyAndActionActivitiesHistoryList);
      //Assertion
      
      //2. Test close Task associated with Account Plan Objective
      controller.buildStrategyAndActionActivitiesList();
      sizeOfStrategyAndActionActivitiesHistoryList = controller.strategyAndActionActivityHistoryList.size();   
      
      List<Task> taskList3 = [SELECT id FROM Task WHERE WhatId=:[SELECT id FROM Account_Plan_Objective__c LIMIT 1].id];    
      controller.selectedId=taskList3[0].id;     
      PageReference closeActivityPage2   = controller.doCloseActivity();
      
      controller.buildStrategyAndActionActivitiesHistoryList();
      
      //Assertion
      system.assert(controller.strategyAndActionActivityHistoryList.size() != sizeOfStrategyAndActionActivitiesHistoryList, 'AccountPlan_StrategyAction_Ctlr_Test: AccountPlan_StrategyAction_Ctlr_Test: Task not closed.'
                    + controller.strategyAndActionActivityHistoryList.size() + ' ' + sizeOfStrategyAndActionActivitiesHistoryList);
      //Assertion

      
      //3. Test delete Task from Activities History List
      controller.buildStrategyAndActionActivitiesHistoryList(); 
      sizeOfStrategyAndActionActivitiesHistoryList = controller.strategyAndActionActivityHistoryList.size();   
      
      controller.selectedIdToDelete=taskList2[0].id;
 
      PageReference doDeleteActHistPage = controller.doDeleteActHist();
      
      controller.buildStrategyAndActionActivitiesHistoryList();
      
      //Assertion
      system.assert(controller.strategyAndActionActivityHistoryList.size() != sizeOfStrategyAndActionActivitiesHistoryList, 'AccountPlan_StrategyAction_Ctlr_Test: AccountPlan_StrategyAction_Ctlr_Test: Task not deleted.'
                    + controller.strategyAndActionActivityHistoryList.size() + ' ' + sizeOfStrategyAndActionActivitiesHistoryList);
      //Assertion

      //4. Test delete Event from Activity History List
      controller.buildStrategyAndActionActivitiesHistoryList(); 
      sizeOfStrategyAndActionActivitiesHistoryList = controller.strategyAndActionActivityHistoryList.size();  
      
      //DateTime eventDate1 = DateTime.now();

      list<Event> eventList1 = [SELECT id FROM Event WHERE WhatId=:accountPlan.id AND ActivityDateTime < :eventDate1 ];
      
      controller.selectedIdToDelete=eventList1[0].id;
      
      PageReference doDeleteActivityPage = controller.doDeleteActivity();
      
      controller.buildStrategyAndActionActivitiesHistoryList();
      
      //Assertion
      system.assert(controller.strategyAndActionActivityHistoryList.size() != sizeOfStrategyAndActionActivitiesHistoryList, 'AccountPlan_StrategyAction_Ctlr_Test: AccountPlan_StrategyAction_Ctlr_Test: Event not deleted.'
                    + controller.strategyAndActionActivityHistoryList.size() + ' ' + sizeOfStrategyAndActionActivitiesHistoryList);
      //Assertion
      
      //5
      controller.editMember();
      
      //6
      controller.buildAccountPlanObjectivesList();
      Integer sizeOfObjectivesList = controller.accountPlanObjectivesList.size();
      controller.seeMoreObjectives();
      controller.buildAccountPlanObjectivesList();
      system.assert(sizeOfObjectivesList + (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Objectives_List__c == controller.accountPlanObjectivesList.size(), 'AccountPlan_StrategyAction_Ctlr_Test: There was a problem with the method seeMoreObjectives(): ' 
                   + sizeOfObjectivesList + ' ' + controller.accountPlanObjectivesList.size());
      
      Test.stopTest();

      /*
      //7
      controller.viewAccountPlanObjectivesListFull();      
      controller.viewAccountPlanActivitiesListFull();      
      controller.viewAccountPlanHistoryListFull();
      
      //8
      controller.buildStrategyAndActionActivitiesList();
      Integer sizeOfActivitiesList = controller.strategyAndActionActivitiesList.size();
      controller.seeMoreActivities();
      controller.buildStrategyAndActionActivitiesList();
      system.assert(sizeOfActivitiesList + (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Open_Activities_List__c != controller.strategyAndActionActivitiesList.size(), 'AccountPlan_StrategyAction_Ctlr_Test: There was a problem with the method seeMoreActivities(): ' 
                   + sizeOfActivitiesList + ' ' + controller.strategyAndActionActivitiesList.size());  
      
      //9
      controller.buildStrategyAndActionActivitiesHistoryList();
      Integer sizeOfClosedActivitiesList = controller.strategyAndActionActivityHistoryList.size();
      controller.seeMoreClosedActivities();
      controller.buildStrategyAndActionActivitiesHistoryList();
      system.assert(sizeOfClosedActivitiesList + (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Closed_Activities_List__c != controller.strategyAndActionActivityHistoryList.size(), 'AccountPlan_StrategyAction_Ctlr_Test: There was a problem with the method seeMoreClosedActivities(): ' 
                   + sizeOfClosedActivitiesList + ' ' + controller.strategyAndActionActivityHistoryList.size());  
      
      
      //10
      controller.backToAccountPlan();
      
    Test.stopTest(); 
    */  
  }
  
   //Split above method in this one since there was 'Too many SOQL Queries: 101' exception - May23rd 2017 - Sanket Vaidya.
   private static testmethod void test_CloseAndDelete_TasksAndEvents_2()
   {

    Account testAccount1           = [SELECT id FROM Account LIMIT 1];
    Contact newContact1            = [SELECT id FROM Contact LIMIT 1];    
    Account_Plan__c accountPlan    = [SELECT id FROM Account_Plan__c LIMIT 1];    
    User user1                     = [SELECT id FROM User WHERE alias = 'Test1' LIMIT 1];


    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)accountPlan);
    AccountPlan_StrategyAction_Controller controller = new AccountPlan_StrategyAction_Controller(stdController);
    
    DateTime eventDate1 = DateTime.now();
    
    Test.startTest();

      //7
      controller.viewAccountPlanObjectivesListFull();      
      controller.viewAccountPlanActivitiesListFull();      
      controller.viewAccountPlanHistoryListFull();
      
      //8
      controller.buildStrategyAndActionActivitiesList();
      Integer sizeOfActivitiesList = controller.strategyAndActionActivitiesList.size();
      controller.seeMoreActivities();
      controller.buildStrategyAndActionActivitiesList();
      system.assert(sizeOfActivitiesList + (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Open_Activities_List__c != controller.strategyAndActionActivitiesList.size(), 'AccountPlan_StrategyAction_Ctlr_Test: There was a problem with the method seeMoreActivities(): ' 
                   + sizeOfActivitiesList + ' ' + controller.strategyAndActionActivitiesList.size());  
      
      //9
      controller.buildStrategyAndActionActivitiesHistoryList();
      Integer sizeOfClosedActivitiesList = controller.strategyAndActionActivityHistoryList.size();
      controller.seeMoreClosedActivities();
      controller.buildStrategyAndActionActivitiesHistoryList();
      system.assert(sizeOfClosedActivitiesList + (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Closed_Activities_List__c != controller.strategyAndActionActivityHistoryList.size(), 'AccountPlan_StrategyAction_Ctlr_Test: There was a problem with the method seeMoreClosedActivities(): ' 
                   + sizeOfClosedActivitiesList + ' ' + controller.strategyAndActionActivityHistoryList.size());  
      
      //10
      controller.backToAccountPlan();
      
    Test.stopTest(); 
  }

  @testSetup
  private static void setUpTestData(){
    
    User thisUser = [SELECT Id FROM User WHERE Id =:UserInfo.getUserId()];
    
    //Create data
    Profile profile1 = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    User user1 = Test_Utils.createUser(profile1, 'test@experian.com', 'Test1');
    Account testAccount1 = Test_Utils.createAccount();  
    Contact newContact1 =  Test_Utils.createContact(testAccount1.id);
    
    System.runAs(thisUser){
      insert user1;
      insert testAccount1;
      insert newContact1;
    }
    
    
    
    Account_Plan__c newAccountPlan = new Account_Plan__c(Account__c = testAccount1.id);
    insert newAccountPlan;

    DateTime eventDate1 = DateTime.now();
    
    
    List<Account_Plan_Objective__c> apoList = new List<Account_plan_Objective__c>();

    Integer i=0;
    for(i=0;i<25;i++){
      Account_Plan_Objective__c accPlObj1 = new Account_Plan_Objective__c(Account_Plan__c = newAccountPlan.id);
      apoList.add(accPlObj1);
    }    
    insert apoList;
    
    
    System.runAs(thisUser){
    
    Task newTask1 = Test_Utils.createTask(newContact1.id, newAccountPlan.id);
    
    Task newTask2 = Test_Utils.createTask(newContact1.id, apoList[0].id);
    
    List<Task> taskList = new List<Task>();
    Integer i2 = 0;
    for(i2=0;i2<20;i2++){
      Task  newTaskForAccountPlan1  = new Task(WhatId=newAccountPlan.id,
                                               WhoID = newContact1.id,
                                               OwnerId=user1.id,
                                               Priority='Normal',
                                               CreatedById=user1.id,
                                               CreatedDate=eventDate1,
                                               TaskSubtype='Task',
                                               Related_To__c='Account Plan',                                          
                                               Status='Not Started');
      taskList.add(newTaskForAccountPlan1);      
    }
    insert taskList;
     
    Task  newTaskForAccountPlan2  = new Task(WhatId=apoList[0].id,
                                             WhoID = newContact1.id,
                                             OwnerId=user1.id,
                                             Priority='Normal',
                                             CreatedById=user1.id,
                                             CreatedDate=eventDate1,
                                             TaskSubtype='Task',
                                             Related_To__c='Account Plan',                                          
                                             Status='Not Started');
    insert newTaskForAccountPlan2;
    
    
    eventDate1 = eventDate1.addDays(7);
     
 
    List<Event> eventList = new List<Event>();
    Integer i3 = 0;
    for(i3=0;i3<20;i3++){ 
      Event newEventForAccountPlan1 = new Event(WhatId=newAccountPlan.id,
                                               DurationInMinutes=20,
                                               ActivityDateTime=eventDate1,
                                               Subject='Test',
                                               OwnerId=user1.id
                                               );
      eventList.add(newEventForAccountPlan1);
    }
    insert eventList;
    
    //This Event should go straight into the Activities History List
    DateTime eventDate2 = DateTime.now();
    eventDate2 = eventDate2.addDays(-1);
    Event newEventForAccountPlan2 = new Event(WhatId=newAccountPlan.id,
                                             DurationInMinutes=0,
                                             ActivityDateTime=eventDate2,
                                             Subject='Test',
                                             OwnerId=user1.id,
                                             EndDateTime=eventDate2
                                             );
    insert newEventForAccountPlan2;


    DateTime eventDate3 = DateTime.now();
    eventDate3 = eventDate3.addDays(7);
    Event newEventForAccountPlanObjective1 = new Event(WhatId=apoList[0].id,
                                             DurationInMinutes=20,
                                             ActivityDateTime=eventDate1,
                                             Subject='Test',
                                             OwnerId=user1.id
                                             );
    insert newEventForAccountPlanObjective1;
    
    //This Event should go straight into the Activities History List
    DateTime eventDate4 = DateTime.now();
    eventDate4 = eventDate4.addDays(-2);
    Event newEventForAccountPlanObjective2 = new Event(WhatId=apoList[0].id,
                                             DurationInMinutes=0,
                                             ActivityDateTime=eventDate4,
                                             Subject='Test',
                                             OwnerId=user1.id,
                                             EndDateTime=eventDate4
                                             );
    insert newEventForAccountPlanObjective2;
    
    }
  
    Custom_Fields_Ids__c customFields = new Custom_Fields_Ids__c(
      AP_Account_Plan_Objective_Rec__c = 'a59',
      AP_Account_Plan_Objective__c  = 'CF00N56000000deLm'
    );
    insert customFields;
  
    Account_Plan_Related_List_Sizes__c custListSizes = new Account_Plan_Related_List_Sizes__c(
      Account_Plan_Objectives_List__c = 10,
      Account_Plan_Open_Activities_List__c = 10,
      Account_Plan_Closed_Activities_List__c = 10
    );
    insert custListSizes;
  
  
  }
  
  
}