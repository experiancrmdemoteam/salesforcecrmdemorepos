/*=============================================================================
 * Experian
 * Name: OpportunityCreateHelper
 * Description: Created simply to check the user can create an opportunity.
 * Created Date: 1 Jun 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 
 =============================================================================*/

global with sharing class OpportunityCreateHelper {
  
  webservice static Boolean canCreate(Id contactId) {
    return OpportunityUtility.canCreateOpportunity(contactId);
  }
  
}