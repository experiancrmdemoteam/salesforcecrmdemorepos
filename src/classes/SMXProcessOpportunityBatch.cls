/**=====================================================================
 * Experian
 * Name: SMXProcessOpportunityBatch
 * Description:
 * Created Date: Unknown
 * Created By: Unknown
 *
 * Date Modified     Modified By        Description of the update
 * Jul 22nd, 2015    Paul Kissick       Case #01040868 - Fixing nested soql query within for loop
 * Sep 18th, 2015     Paul Kissick      I-179463: Duplicate Management Failures
 * July 12th,2016(QA) Tyaga Pati        CRM2:W-005363 - Added code to update the Account on Survey record as part of the nomination record
                                        creation process.
 * Aug 10th,2016(QA) Tyaga Pati         CRM2:W-005363 - Added code to update the Account on Survey record To cover for Test scenario.
 =====================================================================*/

global class SMXProcessOpportunityBatch implements Database.Batchable<Opportunity>, Database.AllowsCallouts {

  global Iterable<Opportunity> start(database.batchablecontext BC) {
    List<String> oppStage = new List<String>();
    List<String> businessUnitStr = new List<String>();
    //String[] oppIds;
    //Integer i=0;
    List<OpportunityStage> oppStageList = [
      SELECT MasterLabel
      FROM OpportunityStage
      WHERE IsActive = true
      AND IsWon = true
    ];
    for (OpportunityStage oopStage1 : oppStageList) {
      oppStage.add(oopStage1.MasterLabel);
    }
    List<Business_Unit_Group_Mapping__c> businessUnitList = [
      SELECT User_Business_Unit__c
      FROM Business_Unit_Group_Mapping__c
      WHERE Common_Group_Name__c = 'EDQ'
    ];
    for (Business_Unit_Group_Mapping__c businessUnit : businessUnitList) {
      businessUnitStr.add(businessUnit.User_Business_Unit__c);
    }
    List<Opportunity> OpportunityToUpdate = [
      SELECT Id,
        (SELECT Id FROM Survey__r),
        (SELECT ContactId, Contact.AccountId FROM OpportunityContactRoles)
      FROM Opportunity
      WHERE Id IN (
        SELECT OpportunityId
        FROM OpportunityHistory
        WHERE StageName IN :oppStage
        AND CreatedDate >= YESTERDAY
      )
      AND Owner_s_Business_Unit__c IN :businessUnitStr
      AND Type NOT IN ('Credited', 'Existing Business', 'Free Trail', 'Renewal', 'Trial')
      AND Owner_s_Region__c IN ('UK&I','North America')
    ];
    if (Test.isRunningTest()) {
      OpportunityToUpdate = [
          SELECT Id,
            (SELECT Id FROM Survey__r),
            (SELECT ContactId, Contact.AccountId FROM OpportunityContactRoles)
          FROM Opportunity
          WHERE Owner_s_Business_Unit__c IN :businessUnitStr
          AND Type NOT IN ('Credited', 'Existing Business', 'Free Trail', 'Renewal', 'Trial')
          AND Owner_s_Region__c IN ('UK&I','North America')
        ];
    }
    return OpportunityToUpdate;
  }

  global void execute(Database.BatchableContext BC, List<Opportunity> scope){
    String strSurveyName = 'EDQ Purchase Experience';
    String strSurveyId = 'EXPERIAN_106613';

    List <Feedback__c> feedbackList = new List<Feedback__c>();

    Long lgSeed = System.currentTimeMillis();

    // Boolean result = false;

    for(Opportunity opp : scope){
       // PK: Removing and placing into initial query. Saves SOQL calls
      List<Feedback__c> lstSurvey = opp.Survey__r;

      if (lstSurvey.isEmpty()) {
        // Changing to reference sub query
        for (OpportunityContactRole oppContact : opp.OpportunityContactRoles) {
          lgSeed = lgSeed + 1;
          Feedback__c feedback = new Feedback__c();
          feedback.Opportunity__c = opp.Id;
          feedback.Name = 'P_' + lgSeed;
          feedback.Contact__c = oppContact.ContactId; //ContactName
          feedback.Status__c = 'Nominated';
          feedback.DataCollectionName__c = strSurveyName;
          feedback.DataCollectionId__c = strSurveyId;
          feedback.Account__c = oppContact.Contact.Accountid;
          feedbackList.add(feedback);
        }
      }
    }
    insert feedbackList;
  }
  
  global void finish (Database.BatchableContext info) {
  }
  //global void finish loop

}