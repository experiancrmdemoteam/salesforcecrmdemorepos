/**=====================================================================
 * Appirio Inc
 * Name: AddOrCreateNewContactAddressController.cls
 * Description: 
 * Created Date: Nov 15, 2013
 * Created By: Mohammed Irfan (Appirio)
 *
 * Date Modified         Modified By                   Description of the update
 * January 21st, 2014    Nathalie Le Guay              Adding duplicate check
 * Feb 13th, 2014        Jinesh Goyal(Appirio)         T-232763: Added Exception Logging 
 * Mar 04th, 2014        Arpita Bose(Appirio)          T-243282: Added Constants in place of String
 * Mar 06th, 2014        Naresh Kr Ojha(Appirio)       T-256339: Use of Labels for messages and used keyPrefix to get
 *                                                     Account's keyPrefix.
 * Mar 19th, 2014        Jinesh Goyal(Appirio)         T-251967
 * Apr 07th, 2014        Arpita Bose                   T-269372: Added addError() in try-catch block
 * Apr 10th, 2014        Jinesh Goyal(Appirio)         I-109889: Added accountUpdateId()
 * Apr 28th, 2014        Arpita Bose                   T-275717: Added LIMIT to fix the Force.com Security Scanner Results
 * May 1st, 2014         Arpita Bose                   Updated catch(Exception) to catch(DMLException) to use getDMLException
 * Feb 19, 2015          Noopur                        T-363891: added the logic to retrive the bypassQAS parameter from the URL 
 *                                                     to check if the user has clicked on "New Contact" button.
 * Feb 24th, 2015        Naresh Kr Ojha                T-364970: Added isSaaS option when goes to create new Contact.
 * Jul 10th, 2015        Arpita Bose                   T-418184: Added code for deduplication on Contact
 * Jul 16th, 2015        Noopur                        T-418184: Made changes to the performSave() and changed page version as well.
 * Aug 11th, 2015        Arpita Bose                   T-424805: Updated to populate CPF__c on Contact for serasa user
 * Aug 25th, 2015        Arpita Bose                   Updated to populate Account & removed reference of Serasa Profiles
 * Sep 4th, 2015         Paul Kissick                  S-291639: Improvements to the Duplication Management
 * Sep 15th, 2015        Paul Kissick                  I-180579: Adding changes for allowing 'No Address' for some users
 * Sep 24th, 2015        Arpita Bose                   I-181491: Updated code to Add Contact address on Contact
 * Oct 1st, 2015         Paul Kissick                  T-437699: Redirecting to alert page if creating from the Contact tab (no account id provided)
 * Oct 2nd, 2015         Noopur                        I-182131: Added check if the Address Id is not null, then bypass the duplicate check.
 * Nov 11th, 2015        Paul Kissick                  Case 01209492: Adding additional field for Serasa Mainframe Integration
 * Jun 24th, 2016        Manoj Gopu                    Case #01947180 - Remove EDQ specific Contact object fields - MG COMMENTED CODE OUT
 * Nov 28th, 2016        Sadar Yacob                   Removed Reference to QAS v4 components
 * Mar 14th, 2017        Ryan (Weijie) Hu              W-007090: Added support for consumer contact record type
 * Mar 29th, 2017        Ryan (Weijie) Hu              W-007431: Added support for consumer account record type when creating dummy acct for W-007090
 * May 1st, 2017         Ryan (Weijie) Hu              W-007121 (I952): Add Source System for consumer contact as "Salesorce.com"
 * May 30th, 2017        Charlie Park                  I-1105: Add mother's maiden name to contact.
 * May 31st, 2017        Ryan (Weijie) Hu              I1124 - Consumer contact can ignore address
 * Jun 1st, 2017         Charlie Park                  I-1105: Add a boolean to indicate if user is a serasa profile
 =====================================================================*/

public class AddOrCreateNewContactAddressController {
    
  public Contact contact {get;set;}
  public Address__c address {get;set;} 
  public String retURL {get;set;}
  public String action {get;set;}
  public boolean enableEditMode{get;set;}
  public boolean enableManualAddressSelection {get;set;}
  public boolean isAddressPopupOnload{get;set;}
  public Account_Address__c accountAddress {get;set;}
  public Contact_Address__c contactAddress {get;set;}
  public boolean bypassQAS {get;set;}
  // public Map<ID, Contact> duplicateContacts {get;set;}
  public List<Contact> duplicateContacts {get;set;}
  
  private Boolean allowSaveOverride; // Used to override the save rule for duplicates
  public Boolean saveButtonOverride {get; private set;}
  
  private String contactId;
  private String addressId;
  private Boolean isClone;
  //MIrfan. TypeDown SessionId Property.
/*  public String QASTypedownSessionToken { COMMENTED OUT SY 11/28/16
    get {
      for(QAS_NA__QAS_CA_Account__c accountSObject : [SELECT  QAS_NA__ValidationSessionToken__c FROM QAS_NA__QAS_CA_Account__c 
                                                      limit 10000]) {
        QASTypedownSessionToken = accountSObject.QAS_NA__ValidationSessionToken__c; 
      }
      return QASTypedownSessionToken;    
    }
    private set;
  } */
  //added by JG for T-251967
  public String addressIdFrmComponent {get;set;}
  public String accountName {get;set;}
  public String accountId {get;set;}
  private String accountCnpj;
  
  private Boolean accountMissing = false; // T-437699
  
  //public static final set<String> searasaProfiles = new set<String>();
  //static {
  //  searasaProfiles.add(Constants.PROFILE_EXP_SERASA_FINANCE);
  //  searasaProfiles.add(Constants.PROFILE_EXP_SERASA_SALES_EXEC);
  //  searasaProfiles.add(Constants.PROFILE_EXP_SERASA_SALES_MANAGER);
  //}
    
  // DUPLICATES
  // Initialize a list to hold any duplicate records
  //private List<sObject> duplicateRecords;
  // Define variable that’s true if there are duplicate records
  public boolean hasDuplicateResult{get;set;}

  public boolean isConsumerContact {get; set;}

  public boolean isSerasaUser {get; set;}

  public boolean ignoreAddressForConsumerContact {get; set;}
    
  //Constructor   
  public AddOrCreateNewContactAddressController(ApexPages.StandardController controller) {
    // DUPLICATES
    // duplicateRecords = new List<sObject>();
    hasDuplicateResult = false;
    duplicateContacts = new List<Contact>();
    saveButtonOverride = false;
    isConsumerContact = false;
    isSerasaUser = false;
    ignoreAddressForConsumerContact = false;

    // I-1105
    List<Profile> currentUserProfile = [SELECT id, Name FROM Profile WHERE id=:userinfo.getProfileId() LIMIT 1];
    String curUserProfileName;

    if (!currentUserProfile.isEmpty())
    {
      curUserProfileName = currentUserProfile[0].Name;
      if (curUserProfileName.contains('Experian Serasa '))
      {
        curUserProfileName = curUserProfileName.remove('Experian Serasa ');
      }
    }

    Serasa_User_Profiles__c serasaProfilesCS = [SELECT Name, Profiles__c FROM Serasa_User_Profiles__c WHERE Name=:Constants.SERASA_CUSTOMER_CARE_PROFILES];
    Set<String> serasaProfiles = new Set<String>(serasaProfilesCS.Profiles__c.Split(', '));

    if (serasaProfiles.contains(curUserProfileName))
    {
      isSerasaUser = true;
    }

    addressId = ApexPages.currentPage().getParameters().get('addrId'); 
    String recordType = ApexPages.currentPage().getParameters().get('RecordType');
    String isSaaS = ApexPages.currentPage().getParameters().get('isSaaS');
      
    // T-424805
    String cpfNum;
    if (ApexPages.currentPage().getParameters().containsKey('cpfNum')) {
      cpfNum = ApexPages.currentPage().getParameters().get('cpfNum');
    }
    
    //Case 01209492
    String dateOfBirth;
    if (ApexPages.currentPage().getParameters().containsKey('dateOfBirth')) {
      dateOfBirth = ApexPages.currentPage().getParameters().get('dateOfBirth');
    }
    
    contactId = ApexPages.currentPage().getParameters().get('conId');

    isClone = false;
    if(ApexPages.currentPage().getParameters().containsKey('clone')){
      isClone = true;
    }
    if(ApexPages.currentPage().getParameters().containsKey('accId')){
      accountId = ApexPages.currentPage().getParameters().get('accId');
    }
    // T-437699
    else {
      accountMissing = true;
    }
    //  action = ApexPages.currentPage().getParameters().get('action');
    // I-181491
    if (!ApexPages.currentPage().getParameters().containsKey('action')) {
      action = Label.CSS_Operation_NewContactAddress; // Always add contact and address if no action..
    }
    else {
      action = ApexPages.currentPage().getParameters().get('action');
    }
    // action = Label.CSS_Operation_NewContactAddress;
    
    bypassQAS = false;
    if (ApexPages.currentPage().getParameters().get('bypassQAS') != '' && ApexPages.currentPage().getParameters().get('bypassQAS') == '1'){
      bypassQAS = true;
    }

    contact = new Contact();
    address = new Address__c(Authenticated_Address__c=true);
    contactAddress = new Contact_Address__c();
    enableEditMode = true;
    isAddressPopupOnload = true; //Set false as per T-251967 : nojha
    enableManualAddressSelection = true;


    // Added by Ryan Hu (3.14.2017)
    // Check for if the current creating contact is a consumer contact record type
    RecordType consumer_contact_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Consumer_Contact'];
    if (!String.isBlank(recordType) && consumer_contact_rt.Id == Id.valueOf(recordType)) {
      
      isConsumerContact = true;
      //bypassQAS = true;

      String activeSerasaConsumerContAcctIdStr =ApexPages.currentPage().getParameters().get('accId');
      Account currentActiveSerasaConsumerContAcct = [SELECT Id FROM Account WHERE Id =: activeSerasaConsumerContAcctIdStr LIMIT 1];

      contact.RecordTypeId = recordType;
      contact.Account = currentActiveSerasaConsumerContAcct;
      contact.Source_System__c = 'Salesforce.com';
    }


    User usrRec = [Select Country__c, Profile.Name from User where id=:UserInfo.getUserId()];
    if (usrRec.Country__c != null) {
      address.Country__c = usrRec.Country__c;
    }
      
    if (!String.isBlank(recordType)) {
      //contact.RecordTypeId = recordType;
    }
      
    /*if (!String.isBlank(isSaaS) && isSaaS == '1') {
      contact.SaaS__c = true;
    }*/
      
    // T-424805 
    if (!String.isBlank(cpfNum)) {
      contact.CPF__c = cpfNum;
    }
    
    // Case 01209492
    if (!String.isBlank(dateOfBirth)) {
      contact.Birthdate = (Date)Json.deserialize(dateOfBirth, Date.class);
    }

    if (ApexPages.currentPage().getParameters().get('accId') != null && ApexPages.currentPage().getParameters().get('accName') != null) {
      contact.AccountId = ApexPages.currentPage().getParameters().get('accId'); 
    }
      
    if (contactId != null) {
      if(!isClone){
        enableEditMode=false;
        contact.Id = null;
      }
      String Qry = 'SELECT ';
      for (Schema.FieldSetMember f : SObjectType.Contact.FieldSets.ContactInfoSectionFieldSet.getFields()) {
        Qry += f.getFieldPath() + ', ';
      }
      Qry += 'Id FROM Contact  where id=:contactId';
      Contact con1 = Database.query(Qry );
      if ( !isClone ) {
        contact = con1;
      }
      else {
        contact = con1.clone(false,true);
        
        if (con1.RecordTypeId == consumer_contact_rt.Id) {
          isConsumerContact = true;
        }
        else {
          isConsumerContact = false;
        }

      }
      if (isClone && accountId == null){
        contact.accountId = null;
      }
      else if (isClone && accountId != null){
        contact.AccountId = accountId;
      }
    } 
    else {
      contact.FirstName=ApexPages.currentPage().getParameters().get('conFName'); 
      contact.LastName=ApexPages.currentPage().getParameters().get('conLName'); 
      contact.Mother_s_Maiden_Name__c = ApexPages.currentPage().getParameters().get('mothersMaidenName');
      contact.Email=ApexPages.currentPage().getParameters().get('conEmail');
      if(ApexPages.currentPage().getParameters().containsKey('accId') && ApexPages.currentPage().getParameters().get('accId') != null && ApexPages.currentPage().getParameters().get('accId') != ''){
        contact.AccountId=ApexPages.currentPage().getParameters().get('accId'); 
      }
    }
    if (addressId != null) {
      String Qry = 'SELECT ';
      String[] strArr = addressId.split(';');
      String addrId = strArr[1];
      contactAddress = [select id,Address_Type__c  from Contact_Address__c where Id=:strArr[0] limit 1];
      Map<String, Schema.SObjectField> addressFieldMap = DescribeUtility.getFieldMap(new Address__c());
      List<String> addressFieldList = new List<String>();
      addressFieldList.addAll(addressFieldMap.keySet());
      Qry += String.join(addressFieldList,',') + ' FROM Address__c WHERE Id = :addrId';
      address = Database.query(Qry);
    }
    
    //Added getkeyprefix to remove hardcoded /001/o from below code.
    Schema.DescribeSObjectResult dsr = Account.SObjectType.getDescribe();
    String AccountKeyPrefix = dsr.getKeyPrefix();
    
    retURL =  ApexPages.currentPage().getParameters().get(Constants.PARAM_NAME_RETURL); 
    //retURL = (retURL==null)?'/001/o':retURL;
    retURL = (retURL==null)?'/'+AccountKeyPrefix+'/o':retURL;
    addressIdFrmComponent = accountName = '';
    accountId = ApexPages.currentPage().getParameters().get('accId');
    
    if (String.isNotBlank(accountId)) {
      accountCnpj = getAccountCnpj(accountId);
    }
    
  }
  
  // T-437699
  public PageReference checkForAccount() {
    if (accountMissing) {
      PageReference pr = Page.ContactNewAlert;
      pr.setRedirect(true);
      return pr;
    }
    return null;
  }
  
  public PageReference performSaveAnyway() {
    allowSaveOverride = true;
    return performSave();
  }
      
  //Save method to save records    
  public Pagereference performSave() {
    system.debug('XXXX inside performSave');
    Savepoint sp;
    //Master Try/Catch
    try {
      isAddressPopupOnload=false;
      //validate data before saving.
      if (address.Id == null && (address.Validation_Status__c == null || address.Validation_Status__c == '') && bypassQAS == false && isConsumerContact == false) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.MSG_ADDRESS_IS_BLANK)); 
        return null;
      }
      system.debug('PK: THE ACTION IS: '+action);
      // Setting save point to roll back in case of error. 
      sp = Database.setSavepoint();
      //Create Contact.
      if ((action.equalsIgnoreCase(Label.CSS_Operation_AddContact) || action.equalsIgnoreCase(Label.CSS_Operation_NewContactAddress))) {
        //insert contact;
        system.debug(LoggingLevel.INFO, '#########before contact###########'+contact);
        // DUPLICATES
        contact.Account_CNPJ__c = accountCnpj; // Adding this before save...
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = allowSaveOverride;
        Database.SaveResult saveResult = Database.insert(contact, dml);
        if (!saveResult.isSuccess()) {
          Set<Id> contactIds = new Set<Id>();
          for (Database.Error error : saveResult.getErrors()) {
            system.debug( '===error===' + error);
            if (error instanceof Database.DuplicateError) {
              Database.DuplicateError duplicateError = (Database.DuplicateError)error;
              Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
              system.debug(LoggingLevel.INFO, duplicateResult.getDuplicateRule());
              // Display duplicate error message as defined in the duplicate rule
              saveButtonOverride = false;
              // Display duplicate error message as defined in the duplicate rule
              if (duplicateResult.isAllowSave()) {
                saveButtonOverride = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, System.Label.New_Record_Possible_Duplicates + ': ' + duplicateResult.getErrorMessage()));
              }
              else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.New_Record_Duplicates_Found + ': ' + duplicateResult.getErrorMessage()));
              }
              // Get duplicate records
              duplicateContacts = new List<Contact>();
 
              // Return only match results of matching rules that 
              //  find duplicate records
              Datacloud.MatchResult[] matchResults = duplicateResult.getMatchResults();

              // Just grab first match result (which contains the duplicate record found and other match info)
              Datacloud.MatchResult matchResult = matchResults[0];
              Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();
              system.debug('###matchRecords###'+matchRecords);
              
              // Add matched record to the duplicate records variable
              for (Datacloud.MatchRecord matchRecord : matchRecords) {
                contactIds.add(matchRecord.getRecord().ID);
                system.debug('MatchRecord: ' + matchRecord.getRecord());
                //duplicateContacts.add(matchRecord.getRecord());
                //if (!duplicateContacts.containsKey(matchRecord.getRecord().ID)) {
                  // duplicateContacts.put(matchRecord.getRecord().ID, null);
                //}
              }
              hasDuplicateResult = !contactIds.isEmpty();
            }
            // PK: Adding to show the errors if not related to duplicates.
            else {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error.getMessage())); 
            }
          }
          if (contactIds.size() > 0) {
            duplicateContacts = [SELECT Id, Name, Phone, Owner.Name, OwnerId, MobilePhone, MailingStreet, MailingState, MailingPostalCode, 
                                       MailingCountry, MailingCity, FirstName, LastName, LastModifiedDate, LastModifiedById, 
                                       Email, AccountId, Account.Name, CPF__c
                                FROM Contact c 
                                WHERE ID IN: contactIds];
          }
          //If there’s a duplicate record, stay on the page
          return null;
        }
      }

      if (bypassQAS == false || (isConsumerContact == true)) {

          if (ignoreAddressForConsumerContact == false && isConsumerContact == true && address != null && contactAddress.Address_Type__c != 'Registered' && contactAddress.Address_Type__c != 'Secondary') {
            Database.rollback(sp);
            contact.id = null;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Address_type_required_if_address_exists));
            return null;
          }

          //Create Address.
          if (address.Id == null && (action.equalsIgnoreCase(Label.CSS_Operation_AddAddress) || action.equalsIgnoreCase(Label.CSS_Operation_NewContactAddress))) {
            address = AddressUtility.checkDuplicateAddress(address); // NLG Jan 21st 2014
            if (address.Id == null) {
              insert address;
            }
          }
                    
          if (address.Id != null) {
            // Creating a Contact association with the selected address id.  
            Contact_Address__c newConAdrRec = new Contact_Address__c(Address_Type__c=contactAddress.Address_Type__c);
            newConAdrRec.Contact__c = contact.Id;
            newConAdrRec.Address__c = address.Id;
            insert newConAdrRec;
          }
      }        
      retURL='/'+contact.id;
    } 
    catch(DMLException e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,  e.getMessage())); 
      ApexLogHandler.createLogAndSave('AddOrCreateNewContactAddressController','performSave', e.getStackTraceString(), e);
      return null;
      for (Integer i = 0; i < e.getNumDml(); i++) {
        address.addError(e.getDmlMessage(i));
      }
            
    }//END: Master Try/Catch
    catch(Exception e) { 
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.EXCEPTION_WHILE_SAVING_REC + e.getMessage())); 
      // Rollback if is there any error. 
      Database.rollback(sp); 
      ApexLogHandler.createLogAndSave('AddOrCreateNewContactAddressController','performSave', e.getStackTraceString(), e);   
      address.addError(Label.EXCEPTION_WHILE_SAVING_REC);
      return null;
    }
    //Back to where came from.
    return new PageReference(retURL);
  }

  //On cancel return to Contact tab.
  public Pagereference cancel() {
    return new Pagereference(retURL);
  }
  
  //Blank call method  
  public pagereference blankCall() {
    return null;
  }
  
  //added by JG for T-251967
  //method to update the address Id (for the address selected) coming from the address component on the page
  public void updateAddress () {
    if (addressIdFrmComponent == '' || addressIdFrmComponent == null) {
      address.Id = null;
    } else if (addressIdFrmComponent != null) {
      address.Id = addressIdFrmComponent;
    }
  }//END OF METHOD 'updateAddress'
  
  //Get Account Id for the latest account selected on Contact creation page
  public void accountUpdateId() {
    Boolean resultFound = false;
    Account tempAcc = new Account();
    for(Account acc : [SELECT Id, CNPJ_Number__c FROM Account
                       WHERE Name = :accountName 
                       LIMIT 1]) {
        resultFound = true;      
        tempAcc = acc;
    }
    //update values to be used on page
    if (!resultFound) {
      accountId = null;
      accountCnpj = '';
    }else {
      accountId = tempAcc.Id;
      accountCnpj = tempAcc.CNPJ_Number__c;
    }
  }
  
  private String getAccountCnpj(Id accId) {
    String accountCnpj = '';
    if (String.isNotBlank(accId)) {
      Account a = [SELECT CNPJ_Number__c FROM Account WHERE Id = :accId];
      accountCnpj = a.CNPJ_Number__c;
    }
    return accountCnpj;
  }
  
  
}