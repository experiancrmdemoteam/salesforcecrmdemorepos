/**=====================================================================
 * Experian
 * Name: BatchWalletSyncOpportunityOwners_Test
 * Description: Designed to update the ownership of opportunities based on the changes to accounts from the WalletSync process
 *              Incorporates tasks on the opportunities too.
 * Created Date: 3rd, August, 2015
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By                Description of the update
 * Oct 1st, 2015      Paul Kissick               Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
@isTest
private class BatchWalletSyncOpportunityOwners_Test {

  static testMethod void testBatch1() {
    
    List<WalletSync__c> wsObjs = new List<WalletSync__c>();
    Account acc1 = [SELECT Id, CNPJ_Number__c FROM Account LIMIT 1];
    User usr1 = [SELECT Id FROM User WHERE Alias = 'ZWQQ0' LIMIT 1];
    User usr2 = [SELECT Id FROM User WHERE Alias = 'ZWQQ1' LIMIT 1];
    // Check how many open/closed opps are there...
    Integer acc1OpenUser1 = [SELECT COUNT() FROM Opportunity WHERE AccountId = :acc1.Id AND IsClosed = false AND OwnerId = :usr1.Id];
    Integer acc1ClsdUser1 = [SELECT COUNT() FROM Opportunity WHERE AccountId = :acc1.Id AND IsClosed = true AND OwnerId = :usr1.Id];
    Integer acc1OpenUser2 = [SELECT COUNT() FROM Opportunity WHERE AccountId = :acc1.Id AND IsClosed = false AND OwnerId = :usr2.Id];
    Integer acc1ClsdUser2 = [SELECT COUNT() FROM Opportunity WHERE AccountId = :acc1.Id AND IsClosed = true AND OwnerId = :usr2.Id];
        
    WalletSync__c ws1 = new WalletSync__c(
      Account_Owner__c = usr1.Id,
      Previous_Account_Owner__c = usr2.Id,
      CNPJ_Number__c = acc1.CNPJ_Number__c,
      Account__c = acc1.Id,
      LegacyCRM_Account_ID__c = 'ANYTHING1',
      Last_Processed_Date__c = Datetime.now().addHours(-1)
    );
    
    insert ws1;
    
    Test.startTest();
    BatchWalletSyncOpportunityOwners b = new BatchWalletSyncOpportunityOwners();
    Database.executeBatch(b);
    Test.stopTest();
    
    Integer acc1OpenUser1Aft = [SELECT COUNT() FROM Opportunity WHERE AccountId = :acc1.Id AND IsClosed = false AND OwnerId = :usr1.Id];
    Integer acc1ClsdUser1Aft = [SELECT COUNT() FROM Opportunity WHERE AccountId = :acc1.Id AND IsClosed = true AND OwnerId = :usr1.Id];
    Integer acc1OpenUser2Aft = [SELECT COUNT() FROM Opportunity WHERE AccountId = :acc1.Id AND IsClosed = false AND OwnerId = :usr2.Id];
    Integer acc1ClsdUser2Aft = [SELECT COUNT() FROM Opportunity WHERE AccountId = :acc1.Id AND IsClosed = true AND OwnerId = :usr2.Id];
    
    system.assertEquals(acc1ClsdUser1,acc1ClsdUser1Aft);
    system.assertEquals(acc1ClsdUser2,acc1ClsdUser2Aft);
    system.assertEquals(acc1OpenUser1Aft,acc1OpenUser1 + acc1OpenUser2);
    system.assertEquals(0,acc1OpenUser2Aft);
  }

  @testSetup
  static void setupTestData() {
    
    IsDataAdmin__c ida = new IsDataAdmin__c(IsDataAdmin__c = true, SetupOwnerId = UserInfo.getUserId());
    insert ida;
    
    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];
    List<User> tstUsrs = Test_Utils.createUsers(p,'test@experian.com','ZWQQ',2);
    insert tstUsrs;
    
    system.runAs(tstUsrs[0]) {
      List<Account> newAccsList = new List<Account>();
      Account acc1 = Test_utils.createAccount();
      acc1.CNPJ_Number__c = '62307946000152';
      newAccsList.add(acc1);
      Account acc2 = Test_utils.createAccount();
      acc2.CNPJ_Number__c = '17240483000103';
      newAccsList.add(acc2);
      Account acc3 = Test_utils.createAccount();
      acc3.CNPJ_Number__c = '43161803000130';
      newAccsList.add(acc3);
      Account acc4 = Test_utils.createAccount();
      acc4.CNPJ_Number__c = '34895621000101';
      newAccsList.add(acc4);
      Account acc5 = Test_utils.createAccount();
      acc5.CNPJ_Number__c = '18068852000186';
      newAccsList.add(acc5);
      insert newAccsList;
    }
    
    List<Account> newAccs = [SELECT Id, CNPJ_Number__c FROM Account];
    
    List<Contact> newConts = new List<Contact>();
    for(Account a : newAccs) {
      newConts.add(Test_Utils.createContact(a.Id));
    }
    
    insert newConts;
    
    // quick map of account id to cont id...
    Map<Id,Id> accToContMap = new Map<Id,Id>();
    for(Contact c : newConts) {
      accToContMap.put(c.AccountId,c.Id);
    }
    
    List<Opportunity> newOpps = new List<Opportunity>();
    
    for(Account a : newAccs) {
      for(Integer i = 0; i < 5; i++) {
        Opportunity opp = Test_Utils.createOpportunity(a.Id);
        opp.OwnerId = tstUsrs[0].Id;
        opp.StageName = Constants.OPPTY_STAGE_3; // : Constants.OPPTY_CLOSED_LOST;
        newOpps.add(opp);
      }
      for(Integer i = 0; i < 5; i++) {
        Opportunity opp = Test_Utils.createOpportunity(a.Id);
        opp.OwnerId = tstUsrs[1].Id;
        opp.StageName = Constants.OPPTY_STAGE_3; // : Constants.OPPTY_CLOSED_LOST;
        newOpps.add(opp);
      }
      for(Integer i = 0; i < 5; i++) {
        Opportunity opp = Test_Utils.createOpportunity(a.Id);
        opp.OwnerId = tstUsrs[0].Id;
        opp.StageName = Constants.OPPTY_CLOSED_LOST; // : Constants.OPPTY_CLOSED_LOST;
        newOpps.add(opp);
      }
      for(Integer i = 0; i < 5; i++) {
        Opportunity opp = Test_Utils.createOpportunity(a.Id);
        opp.OwnerId = tstUsrs[1].Id;
        opp.StageName = Constants.OPPTY_CLOSED_LOST; // : Constants.OPPTY_CLOSED_LOST;
        newOpps.add(opp);
      }
    }
    insert newOpps;
    
    // Then for each opp, add 1 open and 1 closed task...
    
    List<Task> newTasks = new List<Task>();
    for(Opportunity opp : newOpps) {
      Task openTask = new Task(
        ActivityDate = Date.today().addDays(1), 
        WhoId = accToContMap.get(opp.AccountId), 
        WhatId = opp.Id,
        Status = 'In Progress',
        Subject = 'Call Back',
        OwnerId = opp.OwnerId
      );
      newTasks.add(openTask);
      Task closedTask = new Task(
        ActivityDate = Date.today().addDays(-1), 
        WhoId = accToContMap.get(opp.AccountId), 
        WhatId = opp.Id,
        Status = 'Completed',
        Subject = 'Called',
        OwnerId = opp.OwnerId
      );
      newTasks.add(closedTask);
    }
    insert newTasks;
    
    Global_Settings__c globalSetting = Global_Settings__c.getInstance('Global');
    globalSetting.Batch_Failures_Email__c = 'test@experian.com';
    globalSetting.Wallet_Sync_Processing_Last_Run__c = system.now().addDays(-1);
    update globalSetting;
    
    delete ida;
    
  }
  
  
}