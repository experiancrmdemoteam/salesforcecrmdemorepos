/**=====================================================================
* Experian
* Name: SimpleQuoteRenewalExtCntlr
* Description: Unknown
* Created Date: Unknown
* Created By: Richard Joseph
*
* Date Modified  Modified By    Description of the update
* 2017-06-10     Esteban Castro   Enable the page to load CPQQuoteEdit
=====================================================================*/
public with sharing class SimpleQuoteRenewalExtCntlr {
  
  //public string newRenewalQuoteURL {get; set;}
  public string newRenewalQuote {get; set;}
  public string userName {get; set;}
  public boolean isError {get; set;}
  public string errorString {get; set;}
  public string opptyId {get; set;}
  
  public SimpleQuoteRenewalExtCntlr() {
    isError = false;
    opptyId = ApexPages.currentPage().getParameters().get('Id');
    if (opptyId != null) {
      Boolean haveAccess = false;
      Boolean contractDateError = false;
      CPQ_Settings__c cpqSettings;
      cpqSettings = CPQ_Settings__c.getInstance('CPQ');
      String BIS_Manager = cpqSettings.CPQ_USERTYPE_BIS_MANAGER__c;
      String BIS_Strategic_Pricing = cpqSettings.CPQ_USERTYPE_BIS_STRATEGIC_PRICING__c;
      String BIS_SVP_Sales = cpqSettings.CPQ_USERTYPE_BIS_SVP_SALES__c;
      String BIS_VP_Finance = cpqSettings.CPQ_USERTYPE_BIS_VP_FINANCE__c;
      //Added by RJ for Automotive
      String Auto_Strat_Pricing  = cpqSettings.CPQ_USERTYPE_Automotive_Strat_Pricing_Mg__c;
      String Auto_Compliance = cpqSettings.CPQ_USERTYPE_Automotive_Compliance__c;
      String Automotive_Manager = cpqSettings.CPQ_USERTYPE_Automotive_Manager__c;
      Set<String> errorMessages = new Set<String>();
      Boolean isOCR_ShipTo_Available, isOCR_PurLed_Available, isOCR_Commercial_Available;
      isOCR_ShipTo_Available = isOCR_PurLed_Available = isOCR_Commercial_Available = false;
      String groupName = BusinessUnitUtility.getBusinessUnit(UserInfo.getUserId());
      Set<String> allowedCPQUserTypes = new Set<String>{Constants.EDQ_USER_TYPE_SALES_MANAGER, Constants.EDQ_USER_TYPE_CUSTOMER_OPERATIONS, 
        Constants.EDQ_USER_TYPE_COMM_TECH_LEGAL, Constants.EDQ_USER_TYPE_FINANCE_MANAGER, 
        Constants.EDQ_USER_TYPE_PROFESSIONAL_SERVICES,
        Constants.CPQ_USERTYPE_CSDA_AE_MANAGER, Constants.CPQ_USERTYPE_CSDA_STRATEGIC_PRICING,
        BIS_Manager , BIS_Strategic_Pricing,BIS_SVP_Sales,BIS_VP_Finance ,Auto_Strat_Pricing  ,Auto_Compliance,Automotive_Manager  };//I-156139   //Added by RJ for Automotive  
          User currentUser = [
            SELECT Id, Name, CPQ_User__c, CPQ_User_Type__c 
            FROM User 
            WHERE Id =: UserInfo.getUserId()
          ];
      if (!currentUser.CPQ_User__c) {
        isError = true;
        errorMessages.add(Label.CPQ_Quote_Create_Edit_Message_To_User);
      } 
      //As per task T-336011
      if (currentUser.CPQ_User__c && allowedCPQUserTypes.contains(currentUser.CPQ_User_Type__c)) {
        haveAccess = true;
      }
      system.debug((!isError)+'~~~~debugaccess~~~~'+haveAccess);
      if (!isError) {
        /*
        quote = (Quote__c)controller.getRecord(); 
        if (quote.Opportunity__c != null) {
        opptyID = quote.Opportunity__c;
        }
        */
        if (opptyId != null) {  
          for (Opportunity opp : [SELECT ID, Invoice_to_End_User__c, Contract_Start_Date__c, Contract_End_Date__c,
                      (Select Id, UserId From OpportunityTeamMembers WHERE UserId =: UserInfo.getUserId()), 
                      (Select ContactId, Role From OpportunityContactRoles 
                       WHERE Role = :Constants.OPPTY_CONTACT_ROLE_SHIPTO OR 
                       Role = :Constants.OPPTY_CONTACT_ROLE_PURCHASE_LEDGER OR 
                       Role = :Constants.OPPTY_CONTACT_ROLE_COMMERCIAL) 
                      FROM Opportunity 
                      WHERE ID = :opptyId]) {
                        //Check weather current user is on Opportunity team or not
                        if (!haveAccess && opp.OpportunityTeamMembers.size() > 0) {
                          haveAccess = true;
                        }
                        
                        if (haveAccess && (opp.Contract_Start_Date__c == null || opp.Contract_End_Date__c == null)) {
                          errorMessages.add(Label.CPQ_Quote_Create_Edit_Require_Term_Dates);
                        }
                        if (groupName == 'EDQ') {
                          // NLG - April 15th, 2015 - check is for EDQ users only
                          if (String.isBlank(opp.Invoice_to_End_User__c)) {
                            errorMessages.add(Label.CPQ_Quote_Missing_fields_on_Opp);
                          }
                          //Check if ShipTo / Bill-To (old Purchase Ledger) / Commercial Contact role is on Opportunity
                          for (OpportunityContactRole ocr : opp.OpportunityContactRoles) {
                            if (ocr.Role == Constants.OPPTY_CONTACT_ROLE_SHIPTO) {
                              isOCR_ShipTo_Available = true;
                            } 
                            if (ocr.Role == Constants.OPPTY_CONTACT_ROLE_PURCHASE_LEDGER) {
                              isOCR_PurLed_Available = true;
                            } 
                            if (ocr.Role == Constants.OPPTY_CONTACT_ROLE_COMMERCIAL) {
                              isOCR_Commercial_Available = true;
                            }
                          }
                        }
                      }
          //Adding errors
          if (!haveAccess) {
            errorMessages.add(Label.CPQ_Quote_Create_Edit_Message_To_User);
          }
          
          String rolesMissing = '';
          //Error creation to show on page.
          if (groupName == 'EDQ') {
            if (!isOCR_ShipTo_Available) {
              rolesMissing += Label.CPQ_Quote_Missing_Contact_Role_Ship;
            }
            if (!isOCR_PurLed_Available) {
              rolesMissing += (String.isBlank(rolesMissing)) ? Label.CPQ_Quote_Missing_Contact_Role_Purchase_Ledger : ', ' + Label.CPQ_Quote_Missing_Contact_Role_Purchase_Ledger;
            }
            if (!isOCR_Commercial_Available) {
              rolesMissing += (String.isBlank(rolesMissing)) ? Label.CPQ_Quote_Missing_Contact_Role_Commercial : ', ' + Label.CPQ_Quote_Missing_Contact_Role_Commercial;
            }
            if (!String.isBlank(rolesMissing)) {
              errorMessages.add(Label.CPQ_Quote_Missing_Contact_Roles + ' ' + rolesMissing);
            }
          }
          
          //Mapking isError true to be available at page
          if (errorMessages.size() > 0) {
            isError = true;
          }
        }
      }
      if (!isError) {
        userName = userinfo.getUserName();
        system.debug('OpptyId: ' + opptyId + ' UserName: ' + userName);
        
        String resturnString = SFDCToCPQSimpleRenewalServiceClass.callCPQSimpleRenewalService(opptyId,userName);
        
        //if (resturnString != null && resturnString != '' && resturnString.containsIgnoreCase('http://')) {
        if (!String.isEmpty(resturnString) && resturnString.isNumeric()) {
          //newRenewalQuoteURL=resturnString.replace('http://', 'https://');
          system.debug('ResturnString: ' + resturnString);
          newRenewalQuote=resturnString;
        }
        else {
          errorMessages.add('Technical Error: ' + ' ' + resturnString);  
          isError = true; 
        }
      }
      //Adding all error on page
      if (isError) {
        for (String err : errorMessages) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, err));
        }
      }
    }  
  }
  
  // load the CPQQuoteEdit. This is done when calling the quote ID + /e
  public PageReference loadSimpleRenewalQuote() {
    try{
      Quote__c quote = [SELECT Id FROM Quote__c WHERE name = :newRenewalQuote];
      PageReference acctPage = new PageReference('/' + quote.Id + '/e?retURL=' + opptyId);
      acctPage.setRedirect(true);
      return acctPage;
    } catch (Exception e) {
      isError = true;
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Quote not found in Salesforce, unable to load'));
      return null;
    }
  }
}