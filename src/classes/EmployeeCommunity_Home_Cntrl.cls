/******************************************************************************
 * Name: EmployeeCommunity_Home_Cntrl 
 * Created Date: Oct 13th, 2016
 * Created By: Richard Joseph
 * Description : Controller to suppliment Employee Community
 * Change Log-   RJ - Added it popUp
 ******************/
public with sharing class EmployeeCommunity_Home_Cntrl {

 public List<WorkBadge> badgeList {get;set;}
    public List<User> curUser {get;set;}
    public List<Employee_Community_News__c> featured {get;set;}
    public List<Employee_Community_News__c> articles {get;set;}
    Public List<Employee_Community_News__c> activeArticles {get;set;}
    Public Employee_Community_News__c activeArticleDetail {get;set;}
    public AggregateResult[] groupedResults  {get;set;}
    public List<Data> data {get;set;}
    public static Data[] dataArray {get;set;}
     
     //RJ Added
    public boolean displayPopup {get; set;}     
    
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
   
   
    public EmployeeCommunity_Home_Cntrl () {
        string userID = UserInfo.getUserId();
        curUser = [Select ID, fullphotourl,smallphotourl from user where id =: userID ];
        displayPopup=false;
        
         
        
    }
    
    public void ReadActiveNewsDetail() {
        string artId = ApexPages.currentPage().getParameters().get('id');
        activeArticleDetail = [Select ID, Detail__c, Headline__c, Image_Link__c, isActive__c, isFeaturedArticle__c, Synopsis__c from Employee_Community_News__c where Id=:artId];
    }
    
    
    Public void ReadAllActiveNews(){
        activeArticles = new  List<Employee_Community_News__c> ([Select ID, Detail__c, Days_Ago__c,Headline__c, Image_Link__c, isActive__c, isFeaturedArticle__c, Synopsis__c from Employee_Community_News__c where isActive__c = true AND Application_Name__c = 'EITSHome' AND Content_Type__c = 'News' ORDER BY CreatedDate desc limit 100]);
    
    }
    
//Read all Active news from Employee_Community_News__c  object - Inactive not used anywhere.
/*
    Public void ReadAllActiveNews()
    {
        activeArticles = new  List<Employee_Community_News__c> ([Select ID, Detail__c, Headline__c, Image_Link__c, isActive__c, isFeaturedArticle__c, Synopsis__c from Employee_Community_News__c where isActive__c = true  ORDER BY CreatedDate desc limit 100]);
    
    }
    */
 
 //Load Home page badge details, News articles   
    Public Void LoadHomePage(){
        badgeList = [Select Description, Message, ImageUrl, Giver.Name,Recipient.Name  from WorkBadge Order By CreatedDate desc Limit 3];
        featured = [Select ID, Detail__c, Headline__c, Image_Link__c, isActive__c, isFeaturedArticle__c, Synopsis__c from Employee_Community_News__c where isFeaturedArticle__c = true and isActive__c = true ORDER BY CreatedDate desc Limit 1];
        articles = [Select ID, Detail__c, Headline__c, Image_Link__c, isActive__c, isFeaturedArticle__c, Synopsis__c from Employee_Community_News__c where isFeaturedArticle__c != true and isActive__c = true ORDER BY CreatedDate desc Limit 4];
   
    }
 
    
    public void LoadRecognitionDate(){
    
    
    //badgeList = [Select Description, Message, ImageUrl, Giver.Name,Recipient.Name  from WorkBadge Order By CreatedDate desc Limit 3];
    
    groupedResults = [SELECT Recipient.Name,Count(ID) BadgeCount  FROM WorkBadge GROUP BY Recipient.Name Order By Count(ID) desc Limit 3];
    
    /* data = new List<Data>();
    for (AggregateResult ar : groupedResults )  {
            data.add(new Data((string)ar.get('Name'),(integer) ar.get('BadgeCount')));

    }        
    
    system.debug(groupedResults );
    system.debug(data);
    EmployeeCommunity_Home_Cntrl.dataArray =data;
*/

}

public class Data {
    public String name { get; set; }
    public Integer BadgeCount { get; set; }
    public Data(String name, Integer BadgeCount ){
    
    this.name = name;

            this.BadgeCount = BadgeCount ;

    
    }
    
}

    
    @RemoteAction  
    public static data[] loadThisWeekData() {
       AggregateResult []  groupedResultsTmp = [SELECT Recipient.Name,Count(ID) BadgeCount  FROM WorkBadge where createddate=THIS_WEEK GROUP BY Recipient.Name Order By Count(ID) desc Limit 5];
    
     List<Data> datatmp = new List<Data>();
    for (AggregateResult ar : groupedResultsTmp )  {
            datatmp.add(new Data((string)ar.get('Name'),(integer) ar.get('BadgeCount')));

    }        
    
    EmployeeCommunity_Home_Cntrl.dataArray =datatmp ;
            system.debug(EmployeeCommunity_Home_Cntrl.dataArray);
        return EmployeeCommunity_Home_Cntrl.dataArray ;

    }  
    
    @RemoteAction  
    public static data[] loadThisQuaterData() {
       AggregateResult []  groupedResultsTmp = [SELECT Recipient.Name,Count(ID) BadgeCount  FROM WorkBadge where createddate=THIS_QUARTER GROUP BY Recipient.Name Order By Count(ID) desc Limit 5];
    
     List<Data> datatmp = new List<Data>();
    for (AggregateResult ar : groupedResultsTmp )  {
            datatmp.add(new Data((string)ar.get('Name'),(integer) ar.get('BadgeCount')));

    }        
    
    EmployeeCommunity_Home_Cntrl.dataArray =datatmp ;
            system.debug(EmployeeCommunity_Home_Cntrl.dataArray);
        return EmployeeCommunity_Home_Cntrl.dataArray ;

    }  
    
    @RemoteAction  
    public static data[] loadThisMonthData() {
       AggregateResult []  groupedResultsTmp = [SELECT Recipient.Name,Count(ID) BadgeCount  FROM WorkBadge where createddate=THIS_MONTH GROUP BY Recipient.Name Order By Count(ID) desc Limit 5];
    
     List<Data> datatmp = new List<Data>();
    for (AggregateResult ar : groupedResultsTmp )  {
            datatmp.add(new Data((string)ar.get('Name'),(integer) ar.get('BadgeCount')));

    }        
    
   
    EmployeeCommunity_Home_Cntrl.dataArray =datatmp ;
            system.debug(EmployeeCommunity_Home_Cntrl.dataArray);
        return EmployeeCommunity_Home_Cntrl.dataArray ;

    }  

//Inactive Not used
/*
   @RemoteAction  
    public static data[] loadRegionData() {
       AggregateResult []  groupedResultsTmp = [SELECT Recipient.country,Count(ID) BadgeCount  FROM WorkBadge where createddate=THIS_MONTH GROUP BY Recipient.country Order By Count(ID) ];
    
     List<Data> datatmp = new List<Data>();
    for (AggregateResult ar : groupedResultsTmp )  {
            datatmp.add(new Data((string)ar.get('country'),(integer) ar.get('BadgeCount')));

    }        
    
    system.debug(groupedResultsTmp );
    system.debug(datatmp );
    EmployeeCommunity_Home_Cntrl.dataArray =datatmp ;
            system.debug(EmployeeCommunity_Home_Cntrl.dataArray);
        return EmployeeCommunity_Home_Cntrl.dataArray ;

    }  
    
*/
}