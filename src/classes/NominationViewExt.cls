/*=============================================================================
 * Experian
 * Name: NominationViwExt
 * Description: 
 * Created Date: 4 Oct 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 21 Oct 2016        Paul Kissick          W-006151: Allow downgrading Team Nom to L2 Spot
 * 7 Nov, 2016        Paul Kissick          Reorganised class and added comments
 * 12 jul 2017        Manoj Gopu            skip to reset the standard controller while running the test class for updateRecord method
 * 25 Jul 2017        Manish Singh          Case #02294471 - Added an exception on approve button to allow Recognition Coordinators to approve L2 and L3 awards 
 * When referring to Status or Types in this code, please use the NominationHelper.NomConstants map
 * instead of hard coding the values here. That class is the source of all values should any require
 * updating.
 *
 * Comparisions of values is dealt with using String.equals(field) as this is much more 
 * robust than using == or != e.g. field == stringvalue 
 =============================================================================*/

public with sharing class NominationViewExt {
  
  // Custom Inner class for holding picklist options.
  public class PicklistOption {
    public String val {get;set;}
    public String label {get;set;}
    public PicklistOption (String v, String l) {
      val = v;
      label = l;
    }
  }
  
  // Inner class for specific history entries 
  public class CustomFieldHistory {
    public String createdBy {get;set;}
    public Datetime createdDate {get;set;}
    public String getCreatedDateStr() {
      return createdDate.format();
    }
    public String fieldApiName {get;set;}
    public String getFieldLabel () {
      return (Schema.SObjectType.Nomination__c.fields.getMap().containsKey(fieldApiName) ? Schema.SObjectType.Nomination__c.fields.getMap().get(fieldApiName).getDescribe().getLabel() : fieldApiName);
    }
    public Object oldValue {get;set;}
    public Object newValue {get;set;}
    public CustomFieldHistory() {
    }
  }

  // PRIVATE PROPERTIES
  private ApexPages.StandardController stdCon;
  
  // These hold the original values of the status and type should a problem occur whilst saving the record,
  // to revert these values back for the page to show the correct data.
  transient private String statusBefore;
  transient private String typeBefore;
  
  // Limits the history audit trail to just these fields.
  private Set<String> historyFieldFilter = new Set<String>{
    'Status__c', 'Type__c'
  };
  
  // PUBLIC PROPERTIES
  public Nomination__c currentRecord {get;set;}
  
  // Error and Success message lists
  public List<String> errorMessages { get { if (errorMessages == null) errorMessages = new List<String>(); return errorMessages; } set; }  
  public List<String> successMessages {get { if (successMessages == null) successMessages = new List<String>(); return successMessages; } set; }
  
  // Constructor
  public NominationViewExt(ApexPages.StandardController con) {
    stdCon = con;
    if (!Test.isRunningTest()) {
      // Load some fields for use later.
      con.addFields(new List<String>{'OwnerId','Status__c','Master_Nomination__c','Upgraded_to_Level_3__c','Downgraded_to_Level_2__c'});
    }
    currentRecord = (Nomination__c)con.getRecord();
  }
  
  
  // METHODS
  
  //===========================================================================
  // Return the Owner/Manager information
  //===========================================================================
  public User getOwnerDetails() {
    return [SELECT Id, FullPhotoUrl, SmallPhotoUrl, Name, Title FROM User WHERE Id = :currentRecord.OwnerId];
  }
 
  //===========================================================================
  // Clears the success/error lists
  //===========================================================================
  private void clearErrorMessages() {
    errorMessages = new List<String>();
    successMessages = new List<String>();
  }
  
  //===========================================================================
  // Is the current user the owner of the record (i.e. the manager)
  //===========================================================================
  public void checkOwnerIsUser(String actionName) {
      
    List <PermissionSetAssignment> permId = [SELECT Id FROM PermissionSetAssignment WHERE AssigneeId =: UserInfo.getUserId() AND PermissionSet.Name = 'Recognition_Coordinator']; //Will help check if the current user is a recognition coordinator
    if(actionName == 'approve')
    {
        if (currentRecord.OwnerId != UserInfo.getUserId() && permId.isEmpty()) {
            errorMessages.add('Unable to '+actionName+'. Only '+getOwnerDetails().Name + ' can '+actionName+' this nomination.'); // TODO: Replace with Label
        }
    }
    else
    {
        if (currentRecord.OwnerId != UserInfo.getUserId()){
            errorMessages.add('Unable to '+actionName+'. Only '+getOwnerDetails().Name + ' can '+actionName+' this nomination.'); // TODO: Replace with Label
        }
    }
  }
  
  //===========================================================================
  // Return the rejection reasons using custom class
  //===========================================================================
  public List<PicklistOption> getRejectReasons() {
    List<PicklistOption> rejReasons = new List<PicklistOption>();
    for (Schema.PicklistEntry pe: Nomination__c.Rejection_Reason__c.getDescribe().getPicklistValues()) {
      rejReasons.add(new PicklistOption(pe.getValue(), pe.getLabel()));
    }
    return rejReasons;
  }
  
  //===========================================================================
  // Returns the list of field history entries in customised format.
  // NOTE: This is filtered using the historyFieldFilter property.
  //===========================================================================
  public List<CustomFieldHistory> getNominationHistory() {
    List<CustomFieldHistory> cfh = new List<CustomFieldHistory>();
    for (Nomination__History nh : [SELECT CreatedBy.Name, CreatedDate, Field, OldValue, NewValue 
                                   FROM Nomination__History 
                                   WHERE ParentId = :currentRecord.Id 
                                   ORDER BY CreatedDate DESC]) {
      
      CustomFieldHistory fh = new CustomFieldHistory();
      // Any fields with IDs will be omitted, but also only keep if it matches the filter 
      if ((nh.OldValue instanceof Id) == false && historyFieldFilter.contains(nh.Field)) {
        fh.createdBy = nh.CreatedBy.Name;
        fh.createdDate = nh.CreatedDate;
        fh.fieldApiName = nh.Field;
        fh.oldValue = nh.OldValue;
        fh.newValue = nh.NewValue;
        cfh.add(fh);
      }
    }
    return cfh;
  }
  
  //===========================================================================
  // Reject any of the nominations.
  //===========================================================================
  public PageReference rejectNomination() {
    clearErrorMessages();
    checkOwnerIsUser('reject'); // TODO: Replace with Label
    if (!errorMessages.isEmpty()) {
      return null;
    }
    if (NominationHelper.NomConstants.get('PendingApproval').equals(currentRecord.Status__c)) {
      if (// currentRecord.Type__c == NominationHelper.NomConstants.get('Level1ProfileBadge') ||
          NominationHelper.NomConstants.get('Level2SpotAward').equals(currentRecord.Type__c) || 
          NominationHelper.NomConstants.get('Level3Individual').equals(currentRecord.Type__c) ||
          NominationHelper.NomConstants.get('Level3Team').equals(currentRecord.Type__c)) {
        statusBefore = currentRecord.Status__c;
        currentRecord.Status__c = NominationHelper.NomConstants.get('Rejected');
        updateRecord('Rejected'); // TODO: Replace with Label
      }
    }
    return null;
  }
  
  //===========================================================================
  // Upgrades a Level 2 to Level 3 Indiv only.
  //===========================================================================
  public PageReference upgradeToLevel3Ind() {
    clearErrorMessages();
    checkOwnerIsUser('upgrade'); // TODO: Replace with Label
    if (!errorMessages.isEmpty()) {
      return null;
    }
    if (NominationHelper.NomConstants.get('PendingApproval').equals(currentRecord.Status__c)) {
      if (NominationHelper.NomConstants.get('Level2SpotAward').equals(currentRecord.Type__c)) {
        typeBefore = currentRecord.Type__c;
        currentRecord.Type__c = NominationHelper.NomConstants.get('Level3Individual');
        updateRecord('Upgraded'); // TODO: Replace with Label
      }
    }
    return null;
  }
  
  //===========================================================================
  // Downgrade a level 3 indiv to level 2
  //===========================================================================
  public PageReference downgradeToLevel2() {
    clearErrorMessages();
    checkOwnerIsUser('downgrade'); // TODO: Replace with Label
    if (!errorMessages.isEmpty()) {
      return null;
    }
    if (NominationHelper.NomConstants.get('PendingApproval').equals(currentRecord.Status__c)) {
      if (NominationHelper.NomConstants.get('Level3Individual').equals(currentRecord.Type__c)) {
        typeBefore = currentRecord.Type__c;
        currentRecord.Type__c = NominationHelper.NomConstants.get('Level2SpotAward');
        updateRecord('Downgraded'); // TODO: Replace with Label
      }
    }
    return null;
  }
  
  //===========================================================================
  // W-006151: Sponsor can downgrade all (for team) to L2 Spot Awards
  //===========================================================================
  public PageReference downgradeTeamToLevel2() {
    clearErrorMessages();
    checkOwnerIsUser('downgrade');
    if (!errorMessages.isEmpty()) {
      return null;
    }
    if (NominationHelper.NomConstants.get('PendingApproval').equals(currentRecord.Status__c)) {
      if (NominationHelper.NomConstants.get('Level3Team').equals(currentRecord.Type__c)) {
        statusBefore = currentRecord.Status__c;
        currentRecord.Status__c = NominationHelper.NomConstants.get('DowngradedToLevel2');
        updateRecord('Downgraded');
      }
    } 
    return null;
  }
  
  //===========================================================================
  // Reject a Nomination with a badge. Applies to Level 2 & both Level 3 
  //===========================================================================
  public PageReference rejectWithBadge() {
    clearErrorMessages();
    checkOwnerIsUser('reject with a badge');
    if (!errorMessages.isEmpty()) {
      return null;
    }
    if (NominationHelper.NomConstants.get('PendingApproval').equals(currentRecord.Status__c)) {
      if (NominationHelper.NomConstants.get('Level2SpotAward').equals(currentRecord.Type__c) ||
          NominationHelper.NomConstants.get('Level3Individual').equals(currentRecord.Type__c) ||
          NominationHelper.NomConstants.get('Level3Team').equals(currentRecord.Type__c)) {
        statusBefore = currentRecord.Status__c;
        currentRecord.Status__c = NominationHelper.NomConstants.get('RejectedBadge');
        updateRecord('Rejected With a Badge'); // TODO: Replace with Label
      }
    }
    return null;
  }
  
  //===========================================================================
  // Approve the nomination.
  //===========================================================================
  public PageReference approveNomination() {
    clearErrorMessages();
    checkOwnerIsUser('approve'); // TODO: Replace with Label
    if (!errorMessages.isEmpty()) {
      return null;
    }
    // When manager approves, if level1/2
    if (NominationHelper.NomConstants.get('PendingApproval').equals(currentRecord.Status__c)) {
      statusBefore = currentRecord.Status__c;
      if (//currentRecord.Type__c == NominationHelper.NomConstants.get('Level1ProfileBadge') || 
          NominationHelper.NomConstants.get('Level2SpotAward').equals(currentRecord.Type__c)) {
        currentRecord.Status__c = NominationHelper.NomConstants.get('Approved');
        updateRecord('Approved'); // TODO: Replace with Label
      }
      // W-006145
      else if (NominationHelper.NomConstants.get('Level3Team').equals(currentRecord.Type__c) && currentRecord.Master_Nomination__c == null) {
        currentRecord.Status__c = NominationHelper.NomConstants.get('PendingManagerApproval');
        updateRecord('Approved By Project Sponsor'); // TODO: Replace with Label
      }
      else if (NominationHelper.NomConstants.get('Level3Team').equals(currentRecord.Type__c) && currentRecord.Master_Nomination__c != null) {
        currentRecord.Status__c = NominationHelper.NomConstants.get('ManagerApproved');
        updateRecord('Approved By Manager'); // TODO: Replace with Label
      }
      else if (NominationHelper.NomConstants.get('Level3Individual').equals(currentRecord.Type__c)) {
        currentRecord.Status__c = NominationHelper.NomConstants.get('PendingPanelApproval');
        updateRecord('Approved By Manager'); // TODO: Replace with Label
      }   
    }
    return null;
  }
  
  //===========================================================================
  // Update to Won - Only Coordinators can see the button to do this.
  //===========================================================================
  public PageReference wonNomination() {
    clearErrorMessages();
    if (NominationHelper.NomConstants.get('PendingPanelApproval').equals(currentRecord.Status__c)) {
      if (NominationHelper.NomConstants.get('Level3Individual').equals(currentRecord.Type__c) || 
          NominationHelper.NomConstants.get('Level3Team').equals(currentRecord.Type__c)) {
        statusBefore = currentRecord.Status__c;
        currentRecord.Status__c = NominationHelper.NomConstants.get('Won');
        updateRecord('Won'); // TODO: Replace with Label
      }
    }
    return null;
  }
  
  //===========================================================================
  // Update to Nominated. - Only coordinators can see the button to do this.
  //===========================================================================
  public PageReference runnerUpNomination() {
    clearErrorMessages();
    if (NominationHelper.NomConstants.get('PendingPanelApproval').equals(currentRecord.Status__c)) {
      if (NominationHelper.NomConstants.get('Level3Individual').equals(currentRecord.Type__c) || 
          NominationHelper.NomConstants.get('Level3Team').equals(currentRecord.Type__c)) {
        statusBefore = currentRecord.Status__c;
        currentRecord.Status__c = NominationHelper.NomConstants.get('Nominated');
        updateRecord('Nominated'); // TODO: Replace with Label
      }
    }
    return null;
  }
  
  //===========================================================================
  // Generic update record method - Returns errors/success messages.
  //===========================================================================
  public void updateRecord(String actionName) {
    try {
      update currentRecord;
      successMessages.add('Nomination '+actionName); // TODO: Replace with Label
      if(!Test.isRunningtest())//skip to reset the standard controller while running the test class
          stdCon.reset();
    }
    catch (DMLException ex) {
      for (ApexPages.Message msg : ApexPages.getMessages()) {
        errorMessages.add(msg.getSummary());
      }
      if (String.isNotBlank(statusBefore)) {
        currentRecord.Status__c = statusBefore;
      }
      if (String.isNotBlank(typeBefore)) {
        currentRecord.Type__c = typeBefore;
      }
    }
  }
  
  //===========================================================================
  // Redirect to the standard salesforce edit page - Only admins can do this.
  //===========================================================================
  public PageReference edit() {
      PageReference pr = stdCon.edit();
      pr.getParameters().put('nooverride','1');
      pr.getParameters().put('retURL','/'+stdCon.getId());
      pr.setRedirect(true);
      return pr;
  }
  
  //===========================================================================
  // Return Child Nominations for a Master Nomination
  //===========================================================================
  public List<Nomination__c> getChildNominations() {
    return [
      SELECT Id, Nominee__r.FullPhotoUrl, Nominee__r.SmallPhotoUrl, Nominee__c, Nominee__r.Title, 
             Status__c, Nominees_Manager__r.Name, Payroll_Award_Date__c, Nominee__r.Name, Manager_Approved_Date__c
      FROM Nomination__c
      WHERE Master_Nomination__c = :currentRecord.Id
      ORDER BY Nominee__r.Name ASC
    ];
  }
  
  //===========================================================================
  // Return other child/team nominations, not the current one.
  //===========================================================================
  public List<Nomination__c> getOtherTeamNominations() {
    return [
      SELECT Id, Nominee__r.FullPhotoUrl, Nominee__r.SmallPhotoUrl, Nominee__c, Nominee__r.Title, 
             Status__c, Nominees_Manager__r.Name, Payroll_Award_Date__c, Nominee__r.Name
      FROM Nomination__c
      WHERE Master_Nomination__c = :currentRecord.Master_Nomination__c
      AND Id != :currentRecord.Id
      ORDER BY Nominee__r.Name ASC
    ];
  }
  
}