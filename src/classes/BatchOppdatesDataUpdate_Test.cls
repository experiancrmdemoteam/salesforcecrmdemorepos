/*=============================================================================
 * Experian
 * Name: BatchOppdatesDataUpdate_Test
 * Description: W-005289 : Test class for BatchOppdatesDataUpdate
 *                         
 * Created Date: Sep2 nd 2016
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
  22nd Feb 2017       Manoj Gopu            Case:02227631 Updated the test class 
 ============================================================================*/
@isTest
private class BatchOppdatesDataUpdate_Test{
    static testMethod void myUnitTest() {
    Test.startTest();
        Opportunity opty=new Opportunity();
        opty.Name='Test Opportunity';
        opty.Selection_confirmed_date__c=null;
        opty.StageName='Qualify';
        
        opty.CloseDate=system.today().addDays(10);
        insert opty;
        
        Task objTask=new Task();
        objTask.WhatId=opty.Id;
        objTask.Subject='Test Subject';
        objTask.Outcomes__c=Constants.ACTIVITY_TYPE_SELECTION_CONFIRMED; 
        objTask.Status='Completed';
        insert objTask;
        BatchOppdatesDataUpdate obj=new BatchOppdatesDataUpdate();
        Database.executeBatch(obj);
       Test.stopTest(); 
        Opportunity oppty=[select id,Selection_confirmed_date__c from Opportunity where id=:opty.Id];
        
        system.assertNotEquals(oppty.Selection_confirmed_date__c,null);
       
        
    
    }
}