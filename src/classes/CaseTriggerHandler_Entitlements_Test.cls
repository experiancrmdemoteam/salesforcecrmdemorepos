/*=============================================================================
 * Experian
 * Name: CaseTriggerHandler_Entitlements_Test
 * Description: 
 * Created Date: 29 Apr 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

@isTest
private class CaseTriggerHandler_Entitlements_Test {

  static testMethod void testAddActiveEntitlements() {
    
    List<Case> allCases = new List<Case>();
    
    Map<Id,Account> accs = new Map<Id, Account>([
      SELECT Id, (SELECT Id FROM Entitlements WHERE Type = 'Email Support') 
      FROM Account
    ]);
    for (Account a : accs.values()) {
      Case newCase = Test_Utils.insertCase(false, a.Id);
      allCases.add(newCase);
    }
    
    CaseTriggerHandler_Entitlements.addActiveEntitlements(allCases, null);
    
    for (Case c : allCases) {
      if (accs.containsKey(c.AccountId)) {
        Account a = accs.get(c.AccountId);
        if (a.Entitlements != null && !a.Entitlements.isEmpty()) {
          system.assertEquals(a.Entitlements[0].Id, c.EntitlementId);
        }
        else {
          system.assertEquals(null, c.EntitlementId);
        }
      }
      else {
        system.assertEquals(null, c.EntitlementId);
      }
    }
        
  }
  
  static testMethod void testAddEntitlementRelatedFields() {
    
    List<Case> allCases = new List<Case>();
    List<Account> accs = [SELECT Id, (SELECT Id FROM Entitlements WHERE Type = 'Email Support') FROM Account];
    for (Account a : accs) {
      Case newCase = Test_Utils.insertCase(false, a.Id);
      List<Entitlement> ents = a.Entitlements;
      if (ents != null && !ents.isEmpty()) {
        Entitlement e = ents[0];
        newCase.EntitlementId = e.Id;
      }
      allCases.add(newCase);
    }
    
    insert allCases;
    
    Test.startTest();
    
    Map<Id, Case> beforeCases = new Map<Id, Case>();
    for (Case c : allCases) {
      beforeCases.put(c.Id, c.clone());
    }
    
    for (Case c : allCases) {
      c.AccountId = null;
      c.EntitlementId = null;
    }
    CaseTriggerHandler_Entitlements.executedEntitlementRelatedFields = false;
    CaseTriggerHandler_Entitlements.addEntitlementRelatedFields(allCases, beforeCases);
    for (Case c : allCases) {
      system.assertEquals(null, c.EMS_Customer_Type__c);
    }
    
    Test.stopTest();
    
  }
  
  static testMethod void testSetCaseMilestonesIfClosed() {
    
    List<Case> allCases = new List<Case>();
    List<Account> accs = [SELECT Id, (SELECT Id FROM Entitlements WHERE Type = 'Email Support') FROM Account];
    for (Account a : accs) {
      Case newCase = Test_Utils.insertCase(false, a.Id);
      List<Entitlement> ents = a.Entitlements;
      if (ents != null && !ents.isEmpty()) {
        Entitlement e = ents[0];
        newCase.EntitlementId = e.Id;
        allCases.add(newCase);
      }
    }
    
    insert allCases;
    
    Test.startTest();
    
    List<Case> allCaseList = [SELECT AccountId, EntitlementId, SlaStartDate, SlaExitDate, Status FROM Case WHERE Id IN : allCases];
    
    List<CaseMilestone> cmList1 = [SELECT Id, CompletionDate FROM CaseMilestone WHERE CaseId IN : allCaseList];
    system.debug(cmList1);
    system.assert(cmList1.size() > 0);
    
    for (CaseMilestone cm : cmList1) {
      system.assertEquals(null, cm.CompletionDate);
    }
    
    Map<Id, Case> beforeCases = new Map<Id, Case>();
    for (Case c : allCaseList) {
      beforeCases.put(c.Id, c.clone());
    }
    
    List<String> closedStatusList = new List<String>(CaseTriggerHandler.getClosedStatusSet());
    
    for (Case c : allCaseList) {
      c.Status = closedStatusList[0];
    }
    
    CaseTriggerHandler_Entitlements.setCaseMilestonesIfClosed(allCaseList, beforeCases);
    
    for (CaseMilestone cm : [SELECT Id, CompletionDate FROM CaseMilestone WHERE CaseId IN : allCaseList]) {
      system.assertNotEquals(null, cm.CompletionDate);
    }
    
    CaseTriggerHandler_Entitlements.asyncSetTargetResolutionFromEntitlement(beforeCases.keySet());
    
    Test.stopTest();
    
    List<Case> checkTargetClosureDate = [SELECT Target_Resolution_Time__c FROM Case WHERE Id IN :beforeCases.keySet()];
    for (Case c : checkTargetClosureDate) {
      system.assertNotEquals(null, c.Target_Resolution_Time__c, 'Target_Resolution_Time__c is null!');
    }
    
  }
  
  @testSetup
  private static void setupData() {
    // Initialise data for test here.
    
    User testUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert testUser;
    
    system.runAs(testUser) {
      
      Account acc1 = Test_Utils.createAccount();
      acc1.Name = 'Test Account T1';
      Account acc2 = Test_Utils.createAccount();
      acc2.Name = 'Test Account T2';
      Account acc3 = Test_Utils.createAccount();
      acc3.Name = 'Test Account T3';
      insert new List<Account>{acc1, acc2, acc3};
      
      SlaProcess stdProcess = [SELECT Id FROM SlaProcess WHERE Name = 'Standard Entitlement Process' AND IsActive = true LIMIT 1];
      
      Entitlement ent = new Entitlement(
        AccountId = acc1.Id,
        Escalation_Manager__c = UserInfo.getUserId(),
        Case_Resolution_Time__c = 50,
        Name = acc1.Name + ' Email',
        Type = 'Email Support', 
        StartDate = Date.today().addDays(-40),
        EndDate = Date.today().addDays(40),
        SlaProcessId = stdProcess.Id
      );
      
      Entitlement ent3 = new Entitlement(
        AccountId = acc3.Id,
        Escalation_Manager__c = UserInfo.getUserId(),
        Case_Resolution_Time__c = 50,
        Name = acc3.Name + ' Email',
        Type = 'Email Support', 
        StartDate = Date.today().addDays(-40),
        EndDate = Date.today().addDays(40),
        SlaProcessId = stdProcess.Id
      );
      
      insert new List<Entitlement>{ent, ent3};
      
    }
    
  }
  
}