/**=====================================================================
 * Appirio, Inc
 * Name: BatchWalletSyncAccountIndSect_Test
 * Description: Test class to verify behaviour of the batch wallet sync account industry and sector test.
 * Created Date: 11th Aug 2015
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By                Description of the update
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 * June 24th, 2016    James Wills                June 28th Sprint Release: Added code to prevent DML error in prepareData() method
 =====================================================================*/
@isTest
private class BatchWalletSyncAccountIndSect_Test {

  static testMethod void testGoodAllAccounts() {
    
    system.assertEquals(0, [SELECT COUNT() FROM Account WHERE Industry != null AND Sector__c != null]);
    
    Test.startTest();
    
    BatchWalletSyncAccountIndSect b = new BatchWalletSyncAccountIndSect();
    Database.executeBatch(b);
    
    Test.stopTest();
    
    system.assertEquals(5, [SELECT COUNT() FROM Account WHERE Industry != null AND Sector__c != null]);
    system.assertEquals(5, [SELECT COUNT() FROM Account WHERE LATAM_Sector__c != null]);
  }
  
  
  @testSetup
  private static void prepareData() {
    
    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];
    List<User> tstUsrs = Test_Utils.createUsers(p,'test@experian.com','ZWQQ',1);
    
    User userForTest = [SELECT id FROM User WHERE id = :UserInfo.getUserId() LIMIT 1];
    
    System.runAs(userForTest){
      insert tstUsrs;
    }//June 28th Sprint Release
    
    List<Serasa_Segments_Map__c> ssmList = new List<Serasa_Segments_Map__c>();
    for(Integer i = 1; i <=5; i++) {
      Serasa_Segments_Map__c ssm = new Serasa_Segments_Map__c(
        Segment1__c = 'SEG1'+String.valueOf(i), 
        Segment2__c = 'SEG2'+String.valueOf(i), 
        Segment3__c = 'SEG3'+String.valueOf(i), 
        Industry__c = 'Industry '+String.valueOf(i), 
        Sector__c = 'Sector ' +String.valueOf(i)
      );
      ssmList.add(ssm);
    }
    insert ssmList;
    
    List<Account> newAccs = new List<Account>();
    User u = [SELECT Id FROM User WHERE Id = :tstUsrs[0].Id LIMIT 1];
    system.runAs(u) {
      Account acc1 = Test_utils.createAccount();
      acc1.CNPJ_Number__c = '62307946000152';
      newAccs.add(acc1);
      Account acc2 = Test_utils.createAccount();
      acc2.CNPJ_Number__c = '17240483000103';
      newAccs.add(acc2);
      Account acc3 = Test_utils.createAccount();
      acc3.CNPJ_Number__c = '43161803000130';
      newAccs.add(acc3);
      Account acc4 = Test_utils.createAccount();
      acc4.CNPJ_Number__c = '34895621000101';
      newAccs.add(acc4);
      Account acc5 = Test_utils.createAccount();
      acc5.CNPJ_Number__c = '18068852000186';
      newAccs.add(acc5);
      insert newAccs;
    }
    Global_Settings__c globalSetting = Global_Settings__c.getInstance('Global');
    globalSetting.Batch_Failures_Email__c = 'test@experian.com';
    globalSetting.Last_Wallet_Sync_Upload_Started__c = system.now().addHours(-1);
    update globalSetting;
    List<Account> allAccs = [SELECT Id, CNPJ_Number__c FROM Account];
    List<WalletSync__c> wsList = new List<WalletSync__c>();
    for(Integer j = 1; j <=5; j++) {
      WalletSync__c ws = new WalletSync__c(
        Segment1__c = 'SEG1'+String.valueOf(j), 
        Segment2__c = 'SEG2'+String.valueOf(j), 
        Segment3__c = 'SEG3'+String.valueOf(j), 
        Sector__c = 'SomeSector',
        CNPJ_Number__c = allAccs[j-1].CNPJ_Number__c,
        Account__c = allAccs[j-1].Id,
        Last_Processed_Date__c = system.now(),
        LegacyCRM_Account_ID__c = 'ANYTHING1'+String.valueOf(j)
      );
      wsList.add(ws);
    }
    insert wsList;
    
  }
  
}