/**=====================================================================
 * Appirio, Inc
 * Name: OpportunityExitCriteriaStatusExtension_Test
 * Description: This test class is for testing the 'OpportunityExitCriteriaStatusExtension.cls' class
 * Created Date: Dec 17th, 2013
 * Created By: Mohammed Irfan (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Jan 30th, 2014               Jinesh Goyal(Appirio)        T-232760: Homogenize the comments
 * Apr 15th, 2014               Arpita Bose(Appirio)         T-271695: Removed reference to Below_Review_Thresholds__c field
 * Apr 16th, 2014               Arpita Bose                  T-271695: Renamed field Has_Stage_5_Approval__c to Has_Senior_Approval__c
 * Oct 20th, 2014               Arpita Bose                  Updated method createTestData() to remove IsDataAdmin__c
 * Feb 2nd, 2015                Gaurav Kumar Chadha          Updated method createTestData() to insert new opportunity from existing
                                                             one and testOpptyExitCriteria() to update the opp to stage 7   
 * Jul 7th, 2015                Naresh kr Ojha               T-405529: Updated test class.
 * Apr 5, 2016                  Paul Kissick                 Case 01028611: Adding new requirement to stage 4
 * Apr 25th, 2017               Sanket Vaidya                Case 02150014: CRM 2.0- Opportunity Competitor Information [Added Is_competitor__c flag to true for account]  
 =====================================================================*/

@isTest
private class OpportunityExitCriteriaStatusExten_Test {
  
  static testmethod void testOpptyExitCriteria1(){
    
    Test.startTest();
    
    Opportunity oppty = [
      SELECT Id, Type FROM Opportunity LIMIT 1
    ];
    
    PageReference pgRef = Page.OpportunityExitCriteriaStatus;
    Test.setCurrentPage(pgRef);
    
    OpportunityExitCriteriaStatusExtension cls = new OpportunityExitCriteriaStatusExtension(new ApexPages.StandardController(oppty));

    system.assertEquals(true, cls.hasSignedContract);
    system.assertEquals(true, cls.hasSelectionConfirmed);
    system.assertEquals(true, cls.hasQuoteDelivered);
    
    Test.stopTest();
  
  }

  static testmethod void testOpptyExitCriteria2() {
    
    Test.startTest();
    
    Opportunity oppty = [
      SELECT Id, StageName, Type FROM Opportunity LIMIT 1
    ];
    
    oppty.StageName = Constants.OPPTY_STAGE_4;
    update oppty;

    PageReference pgRef = Page.OpportunityExitCriteriaStatus;
    Test.setCurrentPage(pgRef);
    OpportunityExitCriteriaStatusExtension cls = new OpportunityExitCriteriaStatusExtension(new ApexPages.StandardController(oppty));
        
    system.assertEquals(true, cls.hasSignedContract);
    system.assertEquals(true, cls.hasSelectionConfirmed);
    system.assertEquals(true, cls.hasQuoteDelivered);
    system.assertEquals(false, cls.hasTechSuppFieldRequirement);
    
    Test.stopTest();

  }
  
  static testmethod void testOpptyExitCriteria3() {
    
    User testUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    testUser.Region__c = Constants.REGION_EMEA;
    insert testUser;
    
    Test.startTest();
    
    Opportunity oppty = [
      SELECT Id, StageName, OwnerId, Type FROM Opportunity LIMIT 1
    ];
    oppty.OwnerId = testUser.Id;
    oppty.StageName = Constants.OPPTY_STAGE_4;
    update oppty;

    PageReference pgRef = Page.OpportunityExitCriteriaStatus;
    Test.setCurrentPage(pgRef);
    OpportunityExitCriteriaStatusExtension cls = new OpportunityExitCriteriaStatusExtension(new ApexPages.StandardController(oppty));
        
    system.assertEquals(true, cls.hasSignedContract);
    system.assertEquals(true, cls.hasSelectionConfirmed);
    system.assertEquals(true, cls.hasQuoteDelivered);
    system.assertEquals(true, cls.hasTechSuppFieldRequirement);
    
    Test.stopTest();

  }
  
  @testSetup
  static void createTestData(){
    IsDataAdmin__c ida = new IsDataAdmin__c(SetupOwnerId = UserInfo.getUserId(), IsDataAdmin__c = true);
    insert ida;
      
    // Create an account    
    Account testAccount = Test_Utils.insertAccount();
    testAccount.Is_Competitor__c = true;
    update testAccount;

    Address__c addrs1 = Test_Utils.insertAddress(true);
    //insert account address
    Account_Address__c accAddrs = Test_Utils.insertAccountAddress(true, addrs1.Id, testAccount.Id);
    // Create Contact
    Contact testContact = Test_Utils.insertContact(testAccount.id);
    // insert contact address
    Test_Utils.insertContactAddress(true, Test_Utils.insertAddress(true).id, testContact.Id);
    
    // Create an opportunity
    Opportunity oppty = Test_Utils.createOpportunity(testAccount.Id);
    oppty.Type = Constants.OPPTY_NEW_FROM_NEW;
    oppty.Has_Senior_Approval__c = true;
    oppty.StageName = Constants.OPPTY_STAGE_3;
    
    //oppty.Below_Review_Thresholds__c = 'Yes';
    oppty.Amount = 500;
    oppty.Channel_Type__c = Constants.OPPTY_CHANNEL_TYPE_INDIRECT;
    oppty.Starting_Stage__c = Constants.OPPTY_STAGE_3;
    insert oppty;
        
    //Create OpportunityLineItem
    Product2 product = Test_Utils.insertProduct();
    product.RevenueScheduleType = Constants.REVENUE_SCHEDULED_TYPE_REPEAT;     
    product.RevenueInstallmentPeriod = Constants.INSTALLMENT_PERIOD_DAILY;   
    product.NumberOfRevenueInstallments = 2;        
    product.CanUseRevenueSchedule = true;
    update product;
    
    Product2 productb = Test_Utils.insertProduct();
    productb.RevenueScheduleType = Constants.REVENUE_SCHEDULED_TYPE_REPEAT;     
    productb.RevenueInstallmentPeriod = Constants.INSTALLMENT_PERIOD_DAILY;   
    productb.NumberOfRevenueInstallments = 2;        
    productb.CanUseRevenueSchedule = true;
    productb.Global_Business_Line__c = Constants.USER_GBL_DECISION_ANALYTICS;
    productb.Business_Line__c = 'Software';
    productb.Types_of_Sale__c = 'Maintenance';
    update productb;

    //Pricebook2 standardPricebook = Test_Utils.getPriceBook2(Constants.STANDARD_PRICE_BOOK);
    PricebookEntry stdPricebookEntry = Test_Utils.insertPricebookEntry(product.Id, Test.getStandardPriceBookId(), Constants.CURRENCY_USD);
    PricebookEntry stdPricebookEntry2 = Test_Utils.insertPricebookEntry(productb.Id, Test.getStandardPriceBookId(), Constants.CURRENCY_USD);
    
    //insert OLI
    OpportunityLineItem opportunityLineItem2 = Test_Utils.createOpportunityLineItem(oppty.Id, stdPricebookEntry.Id, oppty.Type);
    opportunityLineItem2.Start_Date__c = Date.today().addDays(5);
    opportunityLineItem2.End_Date__c = System.today().addDays(10);
    opportunityLineItem2.Type__c = Constants.OPPTY_TYPE_RENEWAL;
    opportunityLineItem2.CPQ_Quantity__c = 1000;
    insert opportunityLineItem2;
    
    //insert OLI
    OpportunityLineItem opportunityLineItemB = Test_Utils.createOpportunityLineItem(oppty.Id, stdPricebookEntry2.Id, oppty.Type);
    opportunityLineItemB.Start_Date__c = Date.today().addDays(5);
    opportunityLineItemB.End_Date__c = System.today().addDays(10);
    opportunityLineItemB.Type__c = Constants.OPPTY_TYPE_RENEWAL;
    opportunityLineItemB.CPQ_Quantity__c = 1000;
    opportunityLineItemB.Type_Of_Sale__c = 'Maintenance';
    insert opportunityLineItemB;

    system.debug('****opportunityLineItem2***'+opportunityLineItem2+'******oppty****'+oppty);
    
    // insert Coompleted Tasks
    list<Task> tasks = Test_Utils.createOpptyTasks(oppty.id , true);
    // insert competitor
    Competitor__c competitor = Test_Utils.insertCompetitor(oppty.id);
    
    // insert opportunity contact role
    OpportunityContactRole oppConRole = Test_Utils.insertOpportunityContactRole(true , String.valueof(oppty.id),String.valueOf(testContact.id) , Constants.DECIDER , true );
    
    delete ida;
  }
  
}