/**=====================================================================
 * Appirio, Inc
 * Name: Account_Plan_OpportunTriggerHandler_Test
 * Description: Test Class for Account_Plan_OpportunityTriggerHandler (for T-276706)
 * Created Date: May 7th, 2014
 * Created By: Rahul Jain (Appirio)
 *
 * Date Modified      Modified By                Description of the update
 * Oct 27th, 2014     Arpita Bose(Appirio)       Updated to remove IsDataAdmin__c
 * Sept 22nd,2015     Jagjeet Singh(Appirio)     Added test methods for ValidateRecordsToBeDeleted
 * sept 29th,2015     Jagjeet Singh(Appirio)     Getting the error message from Custom Label
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
@isTest
private class Account_Plan_OpportunTriggerHandler_Test {

 static testMethod void testCreateRelatedAccountPlanOppSwotEntries() {
    
    // create User
    Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;

    system.runAs(testUser1) {
      TriggerSettings__c insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.ACCOUNT_PLAN_OPPORTUNITY_TRIGGER);
      insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.ACCOUNT_PLAN_SWOT_TRIGGER);
      
      Account_Plan_Opportunity__c accountPlanOpp;
      
      // insert account
      Account account = Test_Utils.insertAccount();
      
      Test.startTest();

      // create account plan
      Account_Plan__c accountPlan = Test_Utils.insertAccountPlan(false, account.id);
      insert accountPlan;

      List<Account_Plan_SWOT__c> accPlanSwots = new List<Account_Plan_SWOT__c>();

      Account_Plan_SWOT__c accountPlanSwot = Test_Utils.insertAccountPlanSwot(false, accountPlan.id);
      accPlanSwots.add(accountPlanSwot);
      Account_Plan_SWOT__c accountPlanSwot1 = Test_Utils.insertAccountPlanSwot(false, accountPlan.id);
      accPlanSwots.add(accountPlanSwot1);
      insert accPlanSwots;

      List<Account_Plan_Opportunity_SWOT__c> accPlanOppSwots1 = [
        SELECT Account_Plan_Opportunity__c, id
        FROM Account_Plan_Opportunity_SWOT__c
        WHERE Account_Plan_SWOT__c IN : accPlanSwots
      ];

      system.assertEquals(0, accPlanOppSwots1.size(), 'There should be no account plan opp swot records created as there is no acc plan opp exists');

       // Create an opportunity
      Opportunity opp1 = Test_Utils.createOpportunity(account.Id);
      opp1.Has_Senior_Approval__c = true;
      opp1.StageName = Constants.OPPTY_STAGE_3;
      opp1.Amount = 500;
      opp1.Starting_Stage__c = Constants.OPPTY_STAGE_7;
      insert opp1;

      List<Account_Plan_Opportunity__c> accountPlanOppList = [
        SELECT Id
        FROM Account_Plan_Opportunity__c
        WHERE Account_Plan__c = :accountPlan.Id 
        AND Opportunity__c = :opp1.Id
      ];

      if (accountPlanOppList.size() > 0) {
        accountPlanOpp = accountPlanOppList[0];
      }
      else {
        accountPlanOpp = Test_Utils.insertAccountPlanOpp(false, accountPlan.id, opp1.id);
        insert accountPlanOpp;
      }

      accPlanOppSwots1 = [
        SELECT Account_Plan_Opportunity__c, Id
        FROM Account_Plan_Opportunity_SWOT__c
        WHERE Account_Plan_SWOT__c = :accPlanSwots
      ];

      system.assertEquals(2, accPlanOppSwots1.size(), 'insertion of account plan opp should have created two account plan opp swot entries for both swots');

      Test.stopTest();
    }

  }

  static testMethod void testUpdateRelatedOpptyShouldUpdateFields() {

    // create User
    Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;

    system.runAs(testUser1) {
      
      TriggerSettings__c insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.ACCOUNT_PLAN_OPPORTUNITY_TRIGGER);
      insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.ACCOUNT_PLAN_SWOT_TRIGGER);
      //IsDataAdmin__c isDateAdmin = Test_Utils.insertIsDataAdmin(false);
      Account_Plan_Opportunity__c accountPlanOpp;
      // insert account
      Account account = Test_Utils.insertAccount();

      // create account plan
      Account_Plan__c accountPlan = Test_Utils.insertAccountPlan(false, account.id);
      insert accountPlan;
        
      //insert the current user to the AccountPlanTeam
      Account_Plan_Team__c accPlanTeamMem = Test_Utils.insertAccountPlanTeam(true,accountPlan.Id,testUser1.Id);

      Opportunity opp2 = Test_Utils.createOpportunity(account.Id);
      opp2.Has_Senior_Approval__c = true;
      opp2.StageName = Constants.OPPTY_STAGE_3;
      opp2.Amount = 1500;
      opp2.Starting_Stage__c = Constants.OPPTY_STAGE_7;
      opp2.Contract_Start_Date__c = Date.today().addDays(10);
      opp2.Contract_End_Date__c = Date.today().addDays(20);
      insert opp2;

      Test.startTest();

        accountPlanOpp = Test_Utils.insertAccountPlanOpp(false, accountPlan.id, null);
      insert accountPlanOpp;

      //Not equal to opportunity amount
      system.assertNotEquals(opp2.Amount, [SELECT TCV__C, Id FROM Account_Plan_Opportunity__c WHERE Id =: accountPlanOPp.Id].TCV__c);

      accountPlanOpp.Opportunity__c = opp2.Id;
      update accountPlanOpp;

      //Equal to opportunity amount
      system.assertEquals(opp2.Amount, [SELECT TCV__C, Id FROM Account_Plan_Opportunity__c WHERE Id =: accountPlanOPp.Id].TCV__c);
      Test.stopTest();

      delete accountPlanOpp;
      undelete accountPlanOpp;
    }
  }

  static testMethod void testUpdateAccountPlanParentOpportunity() {

    // create User
    Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;

    system.runAs(testUser1) {
      TriggerSettings__c insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.ACCOUNT_PLAN_OPPORTUNITY_TRIGGER);
      insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.ACCOUNT_PLAN_SWOT_TRIGGER);
      IsDataAdmin__c isDateAdmin = Test_Utils.insertIsDataAdmin(false);
      Account_Plan_Opportunity__c accountPlanOpp;

      // insert account
      Account account = Test_Utils.insertAccount();

      // create account plan
      Account_Plan__c accountPlan = Test_Utils.insertAccountPlan(false, account.id);
      insert accountPlan;

      Opportunity opp2 = Test_Utils.createOpportunity(account.Id);
      opp2.Has_Senior_Approval__c = true;
      opp2.StageName = Constants.OPPTY_STAGE_3;
      opp2.Amount = 1500;
      opp2.Starting_Stage__c = Constants.OPPTY_STAGE_3;
      opp2.Contract_Start_Date__c = Date.today().addDays(10);
      opp2.Contract_End_Date__c = Date.today().addDays(20);
      insert opp2;

      Opportunity opp3 = Test_Utils.createOpportunity(account.Id);
      opp3.Has_Senior_Approval__c = true;
      opp3.StageName = Constants.OPPTY_STAGE_3;
      opp3.Amount = 1500;
      opp3.Starting_Stage__c = Constants.OPPTY_STAGE_3;
      opp3.Contract_Start_Date__c = Date.today().addDays(10);
      opp3.Contract_End_Date__c = Date.today().addDays(20);
      insert opp3;

      Test.startTest();
           
        accountPlanOpp = Test_Utils.insertAccountPlanOpp(false, accountPlan.id, null);
      insert accountPlanOpp;

      accountplanOpp.Opportunity__c = opp2.Id;
      update accountPlanOpp;

      Account_Plan_Opportunity__c accountPlanOpp2 = Test_Utils.insertAccountPlanOpp(false, accountPlan.id, null);
      insert accountPlanOpp2;

      accountplanOpp2.Opportunity__c = opp3.id;
      update accountPlanOpp2;

      Account_Plan_Parent_Opportunities__c appo = new Account_Plan_Parent_Opportunities__c();
      appo.Account_Plan__c = accountPlan.Id;
      insert appo;

      accountPlanOpp.Account_Plan_Parent_Opportunity__c  = appo.Id;
      update accountPlanOpp;

      accountPlanOpp2.Account_Plan_Parent_Opportunity__c  = appo.Id;
      update accountPlanOpp2;

      List<Account_Plan_Parent_Opportunities__c> listappo = [
        SELECT Annualised_Open_Revenue__c, Annualised_Won_Revenue__c, Id
        FROM Account_Plan_Parent_Opportunities__c 
        WHERE Id = :appo.Id
      ];
      system.debug('listappo>>>' +listappo);
      
      // Is Opp Amount added to Parent Opp
      system.assertEquals(null, listappo[0].Annualised_Won_Revenue__c);
      system.assertNotEquals(null, listappo[0].Annualised_Open_Revenue__c);

      Account_Plan_Parent_Opportunities__c appo2 = new Account_Plan_Parent_Opportunities__c();
      appo2.Account_Plan__c = accountPlan.Id;
      insert appo2;

      accountPlanOpp.Account_Plan_Parent_Opportunity__c  = appo2.Id;
      update accountPlanOpp;

      accountPlanOpp2.Account_Plan_Parent_Opportunity__c  = appo2.Id;
      update accountPlanOpp2;

      //Is Opp amount removed
      system.assertNotEquals(listappo[0].Annualised_Open_Revenue__c, [SELECT Annualised_Open_Revenue__c, Id FROM Account_Plan_Parent_Opportunities__c WHERE Id =: appo.Id].Annualised_Open_Revenue__c);

      accountPlanOpp.Account_Plan_Parent_Opportunity__c  = null;
      update accountPlanOpp;

      accountPlanOpp2.Account_Plan_Parent_Opportunity__c  = null;
      update accountPlanOpp2;

      //Is Opp amount removed
      system.assertEquals(0.00, [SELECT Annualised_Open_Revenue__c, Id FROM Account_Plan_Parent_Opportunities__c WHERE Id =: appo.Id].Annualised_Open_Revenue__c);
    }
  }
    
  //test the Validate Records Negative Scenario.
  static testMethod void testValidateRecordsToBeDeleted_Negative(){
    //start test
    Test.startTest();
    User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert testUser1; 
      
    // insert account
    Account account = Test_Utils.insertAccount();
      
    // create account plan
    Account_Plan__c accountPlan = Test_Utils.insertAccountPlan(false, account.id);
    accountPlan.Name = 'TestAccountPlan';
    insert accountPlan;
      
    Account_Plan_Opportunity__c accPlanOpp = Test_Utils.insertAccountPlanOpp(true, accountPlan.id, null);
    String expectedMsg = Label.ACCOUNTPLANNING_VALIDATION_MSG_ON_DELETE;
    assertErrorOnDelete(accPlanOpp,expectedMsg);
    Test.stopTest();
  }
     
     
  //test the Validate Records Positive Scenario.
  static testMethod void testValidateRecordsToBeDeleted_Positive(){
    //start Test
    Test.startTest();
    User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert testUser1; 
      
    // insert account
    Account account = Test_Utils.insertAccount();
    Id currentUserId = UserInfo.getUserId();
  
    //insert the accountTeamMember
    AccountTeamMember accTeamMem = Test_Utils.insertAccountTeamMember(true,account.Id,currentUserId,'test');
      
    // create account plan
    Account_Plan__c accountPlan = Test_Utils.insertAccountPlan(false, account.id);
    accountPlan.Name = 'TestAccountPlan';
    insert accountPlan;
      
    Account_Plan_Team__c accPlanTeamMem = Test_Utils.insertAccountPlanTeam(true,accountPlan.Id,currentUserId);
    
    Account_Plan_Opportunity__c accPlanOpp = Test_Utils.insertAccountPlanOpp(true, accountPlan.id, null);
    delete accPlanOpp;
    List<Account_Plan_Opportunity__c> accPlanOppDB = [select Id from Account_Plan_Opportunity__c where Id = :accPlanOpp.Id]; 
    //assert
    system.assertEquals(accPlanOppDB.size(),0,'Account Plan Opportunity should be deleted.');
    Test.stopTest();
  }
    
  //assert error message on delete when the current user is not in AccountTeamMember.
  private static void assertErrorOnDelete(SObject sObj, String expectedMsg) {
    try {
      delete sObj;
      //system.assert(false, 'exception expected for SObject: ' + sObj); // RJ Commented for test class failure 
    }
    catch (Exception e) {
      system.assert(e.getMessage().contains(expectedMsg), 'message=' + e.getMessage());
    }
  }
  
}