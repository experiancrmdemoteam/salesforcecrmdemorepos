/**=====================================================================
 * Name: AccountPlanPDFGeneratorExtension
 * Description: Code used to dynamically generate the AccountPlanPDFSWOTAnalysis VF Component
 * Created Date: Jan. 28th, 2016
 * Created By: James Wills
 * 
 * Date Modified         Modified By            Description of the update
 * Jan. 28th, 2017       James Wills            Created.
 * =====================================================================*/


public class AccountPlanPDFSWOTAnalysisController{

  public List<Account_Plan_SWOT__c> strengthAccPlanSWOTs{get;set;}
  public List<Account_Plan_SWOT__c> weaknessAccPlanSWOTs{get;set;}
  public List<Account_Plan_SWOT__c> opportunityAccPlanSWOTs{get;set;}
  public List<Account_Plan_SWOT__c> threatAccPlanSWOTs{get;set;}    
    
  public List<Account_Plan_SWOT__c> clientstrengthAccPlanSWOTs{get;set;}
  public List<Account_Plan_SWOT__c> clientweaknessAccPlanSWOTs{get;set;}
  public List<Account_Plan_SWOT__c> clientopportunityAccPlanSWOTs{get;set;}
  public List<Account_Plan_SWOT__c> clientthreatAccPlanSWOTs{get;set;}
  
  public ID AccountPlanObjId {get;set;}
  public Account_Plan__c AccountPlanObj {get;set;}

    public AccountPlanPDFSWOTAnalysisController(){
     
     accountPlanObjId = ApexPages.currentPage().getParameters().get('id') ;
      accountPlanObj = [
        SELECT Account__r.Name, Value__c, SystemModstamp, Summary_Recent_History__c, 
             Strategic_Direction__c, Stability__c, Solutions__c, Risks__c, Revenue__c, 
             Reference__c, Recent_Successes__c, Primary_Contact__c, Payment_Issues__c, OwnerId,
             Opportunities__c, One_Experian__c, Objectives_Not_Achieved__c, Name, NPS__c, 
             Major_Wins__c, Major_Partners__c, Major_Competitors__c, Major_Changes__c, 
             Lessons_Learned__c, Last_Review_Date__c, LastModifiedDate, LastModifiedById, 
             LastActivityDate, IsDeleted, Internal_Account_Team__c, Industry_Expertise__c, Id, 
             Health_Status__c, Face_to_Face__c, Expiration__c, Experian_Vision_for_Account__c, Experian_Strategy_for_Account__c,
             Experian_OPEX_Total__c, Experian_OPEX_Share__c, Experian_Capex_Total__c, Experian_CAPEX_share__c, 
             Experian_Annualised_Revenue__c, Executive__c, CreatedDate, CreatedById,Core_Business__c, 
             Contracts__c, Contact_Plan__c, Client_Plan__c, Business_Objectives__c, Annual_OPEX_in_Experian_Domain__c, 
             Annual_CAPEX_in_Experian_Domain__c, Account__c, Account_Sector__c, Account_Industry__c, Additional_Information__c
      FROM Account_Plan__c a
      WHERE Id = :accountPlanObjId
      LIMIT 1
    ];
     
     setAccPlanRecs();
     
    }

    public void setAccPlanRecs(){
    
    strengthAccPlanSWOTs = new List<Account_Plan_SWOT__c>();
    weaknessAccPlanSWOTs = new List<Account_Plan_SWOT__c>();
    opportunityAccPlanSWOTs = new List<Account_Plan_SWOT__c>();
    threatAccPlanSWOTs = new List<Account_Plan_SWOT__c>();
    clientstrengthAccPlanSWOTs = new List<Account_Plan_SWOT__c>();
    clientweaknessAccPlanSWOTs = new List<Account_Plan_SWOT__c>();
    clientopportunityAccPlanSWOTs = new List<Account_Plan_SWOT__c>();
    clientthreatAccPlanSWOTs = new List<Account_Plan_SWOT__c>();
    
    
    for (Account_Plan_SWOT__c singleAccPlanSWOT : [SELECT Id, Description__c, Impact__c, Who__c, Type__c, Importance__c 
                                                  FROM Account_Plan_SWOT__c
                                                  WHERE Account_Plan__c = :accountPlanObj.Id
                                                  ORDER BY Importance__c DESC]){
      if (singleAccPlanSWOT.who__c == 'Experian') {         
        if (singleAccPlanSWOT.Type__c == 'Strength') {
          strengthAccPlanSWOTs.add(singleAccPlanSWOT);
        }
        else if(singleAccPlanSWOT.Type__c == 'Weakness') {
          weaknessAccPlanSWOTs.add(singleAccPlanSWOT);
        }
        else if(singleAccPlanSWOT.Type__c == 'Opportunity') {
          opportunityAccPlanSWOTs.add(singleAccPlanSWOT);
        }
        else if(singleAccPlanSWOT.Type__c == 'Threat') {
          threatAccPlanSWOTs.add(singleAccPlanSWOT);
        }
      }
      else if(singleAccPlanSWOT.who__c == 'Client') {            
        if(singleAccPlanSWOT.Type__c == 'Strength') {
          clientstrengthAccPlanSWOTs.add(singleAccPlanSWOT);
        }
        else if(singleAccPlanSWOT.Type__c == 'Weakness') {
          clientweaknessAccPlanSWOTs.add(singleAccPlanSWOT);
        }
        else if(singleAccPlanSWOT.Type__c == 'Opportunity') {
          clientopportunityAccPlanSWOTs.add(singleAccPlanSWOT);
        }
        else if(singleAccPlanSWOT.Type__c == 'Threat') {
          clientthreatAccPlanSWOTs.add(singleAccPlanSWOT);
        }
      }
    }
    
    for (Integer i = 1 ; i <= 6 ; i++) {
      if(strengthAccPlanSWOTs.size() < i) {
        strengthAccPlanSWOTs.add(new Account_Plan_SWOT__c());
      }
      if(weaknessAccPlanSWOTs.size() < i) {
        weaknessAccPlanSWOTs.add(new Account_Plan_SWOT__c());
      }
      if(opportunityAccPlanSWOTs.size() < i) {
        opportunityAccPlanSWOTs.add(new Account_Plan_SWOT__c());
      }
      if(threatAccPlanSWOTs.size() < i) {
        threatAccPlanSWOTs.add(new Account_Plan_SWOT__c());
      }
      if(clientstrengthAccPlanSWOTs.size() < i) {
        clientstrengthAccPlanSWOTs.add(new Account_Plan_SWOT__c());
      }
      if(clientweaknessAccPlanSWOTs.size() < i) {
        clientweaknessAccPlanSWOTs.add(new Account_Plan_SWOT__c());
      }
      if(clientopportunityAccPlanSWOTs.size() < i) {
        clientopportunityAccPlanSWOTs.add(new Account_Plan_SWOT__c());
      }
      if(clientthreatAccPlanSWOTs.size() < i) {
        clientthreatAccPlanSWOTs.add(new Account_Plan_SWOT__c());
      }
    }
  }

}