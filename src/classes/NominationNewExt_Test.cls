/*=============================================================================
 * Experian
 * Name: NominationNewExt_Test
 * Description: 
 * Created Date: 7 Nov 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 31 Jul 2017        Manish Singh          Added dummy Oracle person type in all methods except level 1 noms to bypass 
                                            the blank person type validation rule condition in rule 
                                            Nomination__c.Cash_Awards_Not_Available_to_Contingents for 
                                            Case 02427918 
 =============================================================================*/

@isTest
private class NominationNewExt_Test {

  static String recipEmail = 'ksdjfhnajkdsfn88u98u@experian.com';
  static String recip2Email = 'skdjfnsadkjfnds89007@experian.com';
  static String giverEmail = 'aslkfnaslkdfn99992@experian.com';
  static String managEmail = 'ijsdhfkjbfsdkjf098098@experian.com';
  static String coordEmail = 'kjnasdfkjanf89u987u@experian.com';
  static String dataAdminEmail = 'kasjnfaskjsfsakjfb82820@experian.com';
  
  
  @isTest
  private static void testNoLevel() {
    
    NominationNewExt nomNew = new NominationNewExt(new ApexPages.StandardController(new Nomination__c()));
    
    system.assertEquals(WorkBadgeDefinition.Recognition_Category__c.getDescribe().getPicklistValues().size(), nomNew.recogBadges.size());
    system.assertNotEquals(0, nomNew.getNominationTypes().size());
    system.assertNotEquals(0, nomNew.getSpotAwards().size());
    system.assertNotEquals(0, nomNew.getCategories().size());
    
    system.assertEquals(null, nomNew.submitNominationRec());
    system.assert(nomNew.errorMessages.size() > 0);
    
  }
  
  @isTest
  private static void testLevel1() {
    
    User recipUser = [SELECT Id FROM User WHERE Email = :recipEmail LIMIT 1];
    
    User giverUser = [SELECT Id FROM User WHERE Email = :giverEmail LIMIT 1];
    
    system.runAs(giverUser) {
    
      NominationNewExt nomNew = new NominationNewExt(new ApexPages.StandardController(new Nomination__c()));
      nomNew.allNomEntries.get(0).nomId = recipUser.Id;
      nomNew.justificationText = 'Testing Level 1 Single Thanks';
      nomNew.selectNomType = '1'; // Level 1
      nomNew.badgeId = nomNew.recogBadges.get(0).Id;
      nomNew.submitNominationRec();
      
      system.assertEquals(1, [SELECT COUNT() FROM WorkBadge WHERE RecipientId = :recipUser.Id AND GiverId = :giverUser.Id]);
      
    }
  }
  
  @isTest
  private static void testLevel1Multi() {
    
    User recipUser = [SELECT Id FROM User WHERE Email = :recipEmail LIMIT 1];
    User managUser = [SELECT Id FROM User WHERE Email = :managEmail LIMIT 1];
    
    User giverUser = [SELECT Id FROM User WHERE Email = :giverEmail LIMIT 1];
    
    system.runAs(giverUser) {
    
      NominationNewExt nomNew = new NominationNewExt(new ApexPages.StandardController(new Nomination__c()));
      nomNew.allNomEntries.get(0).nomId = recipUser.Id;
      nomNew.addAnother();
      nomNew.allNomEntries.get(1).nomId = managUser.Id;
      nomNew.justificationText = 'Testing Level 1 Single Thanks';
      nomNew.selectNomType = '1'; // Level 1
      nomNew.badgeId = nomNew.recogBadges.get(0).Id;
      nomNew.submitNominationRec();
      
      system.assertEquals(2, [SELECT COUNT() FROM WorkBadge WHERE GiverId = :giverUser.Id]);
      
    }
  }
  
  @isTest
  private static void testLevel2FromManager() {
    
    User recipUser = [SELECT Id FROM User WHERE Email = :recipEmail LIMIT 1];
    recipUser.Oracle_User_Person_Type__c = 'ABC';
    update recipUser; //added to bypass the validation rule changes to not allow nominations with blank user person type 
    User managUser = [SELECT Id FROM User WHERE Email = :managEmail LIMIT 1];
    
    system.runAs(managUser) {
    
      NominationNewExt nomNew = new NominationNewExt(new ApexPages.StandardController(new Nomination__c()));
      system.assertNotEquals(JSON.serialize(new List<String>()), nomNew.getSubordinatesJson());
      nomNew.allNomEntries.get(0).nomId = recipUser.Id;
      nomNew.justificationText = 'Testing Level 2 Spot from Manager';
      nomNew.selectNomType = '2'; // Level 1
      nomNew.badgeId = nomNew.recogBadges.get(0).Id;
      nomNew.spotAward = '$25';
      
      nomNew.submitNominationRec();
      
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Type__c = :NominationHelper.NomConstants.get('Level2SpotAward') AND Nominee__c = :recipUser.Id AND Requestor__c = :managUser.Id]);
      
    }
  }
  
  @isTest
  private static void testLevel2NotManager() {
    
    User recipUser = [SELECT Id FROM User WHERE Email = :recipEmail LIMIT 1];
    recipUser.Oracle_User_Person_Type__c = 'ABC';
    update recipUser; //added to bypass the validation rule changes to not allow nominations with blank user person type 
    User giverUser = [SELECT Id FROM User WHERE Email = :giverEmail LIMIT 1];
    
    system.runAs(giverUser) {
    
      NominationNewExt nomNew = new NominationNewExt(new ApexPages.StandardController(new Nomination__c()));
      system.assertEquals(JSON.serialize(new List<String>()), nomNew.getSubordinatesJson());
      nomNew.allNomEntries.get(0).nomId = recipUser.Id;
      nomNew.justificationText = 'Testing Level 2 Spot from Not Manager';
      nomNew.selectNomType = '2'; // Level 1
      nomNew.badgeId = nomNew.recogBadges.get(0).Id;
      
      nomNew.submitNominationRec();
      
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Type__c = :NominationHelper.NomConstants.get('Level2SpotAward') AND Nominee__c = :recipUser.Id AND Requestor__c = :giverUser.Id]);
      
    }
  }
  
  @isTest
  private static void testLevel3aFromManager() {
    
    User recipUser = [SELECT Id FROM User WHERE Email = :recipEmail LIMIT 1];
    recipUser.Oracle_User_Person_Type__c = 'ABC';
    update recipUser; //added to bypass the validation rule changes to not allow nominations with blank user person type 
    User managUser = [SELECT Id FROM User WHERE Email = :managEmail LIMIT 1];
    
    system.runAs(managUser) {
    
      NominationNewExt nomNew = new NominationNewExt(new ApexPages.StandardController(new Nomination__c()));
      system.assertNotEquals(JSON.serialize(new List<String>()), nomNew.getSubordinatesJson());
      nomNew.allNomEntries.get(0).nomId = recipUser.Id;
      nomNew.justificationText = 'Testing Level 3 Individual from Manager';
      nomNew.selectNomType = '3a'; // Level 1
      nomNew.badgeId = nomNew.recogBadges.get(0).Id;
      
      nomNew.submitNominationRec();
      
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Type__c = :NominationHelper.NomConstants.get('Level3Individual') AND Nominee__c = :recipUser.Id AND Requestor__c = :managUser.Id]);
      
    }
  }
  
  @isTest
  private static void testLevel3bFromManager() {
    
    User recipUser = [SELECT Id FROM User WHERE Email = :recipEmail LIMIT 1];
    User recip2User = [SELECT Id FROM User WHERE Email = :recip2Email LIMIT 1];
    recipUser.Oracle_User_Person_Type__c = 'ABC';
    recip2User.Oracle_User_Person_Type__c = 'XYZ';
    update recip2User; //added to bypass the validation rule changes to not allow nominations with blank user person type 
    update recipUser; //added to bypass the validation rule changes to not allow nominations with blank user person type 
    User managUser = [SELECT Id FROM User WHERE Email = :managEmail LIMIT 1];
    User giverUser = [SELECT Id FROM User WHERE Email = :giverEmail LIMIT 1];
    
    system.runAs(giverUser) {
    
      NominationNewExt nomNew = new NominationNewExt(new ApexPages.StandardController(new Nomination__c()));
      nomNew.allNomEntries.get(0).nomId = recipUser.Id;
      nomNew.addAnother();
      nomNew.allNomEntries.get(1).nomId = recip2User.Id;
      nomNew.justificationText = 'Testing Level 3 Team from Manager';
      nomNew.selectNomType = '3b'; // Level 1
      nomNew.badgeId = nomNew.recogBadges.get(0).Id;
      nomNew.teamName = 'Testing Team Name';
      nomNew.sponsorEntry.nomId = managUser.Id;
      
      nomNew.submitNominationRec();
      
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Type__c = :NominationHelper.NomConstants.get('Level3Team') AND Nominee__c = :recipUser.Id AND Requestor__c = :giverUser.Id]);
      // THere should be a master, plus 2 for each nominee
      system.assertEquals(3, [SELECT COUNT() FROM Nomination__c WHERE Type__c = :NominationHelper.NomConstants.get('Level3Team') AND Project_Sponsor__c = :managUser.Id]);
      
    }
  }
  
  @isTest
  private static void testLevel4FromCoordinator() {
    
    User recipUser = [SELECT Id FROM User WHERE Email = :recipEmail LIMIT 1];
    recipUser.Oracle_User_Person_Type__c = 'ABC';
    update recipUser; //added to bypass the validation rule changes to not allow nominations with blank user person type 
    User coordUser = [SELECT Id FROM User WHERE Email = :coordEmail LIMIT 1];
    
    system.runAs(coordUser) {
    
      NominationNewExt nomNew = new NominationNewExt(new ApexPages.StandardController(new Nomination__c()));
      Boolean foundLevel4 = false;
      for (SelectOption so : nomNew.getNominationTypes()) {
        if (so.getValue().equals('4')) {
          foundLevel4 = true;
        }
      }
      system.assertEquals(true, foundLevel4);
      nomNew.allNomEntries.get(0).nomId = recipUser.Id;
      nomNew.justificationText = 'Testing Level 4 from Coordinator';
      nomNew.selectNomType = '4'; // Level 4
      nomNew.badgeId = nomNew.recogBadges.get(0).Id;
      
      nomNew.submitNominationRec();
      
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Type__c = :NominationHelper.NomConstants.get('Level4Elite') AND Nominee__c = :recipUser.Id AND Requestor__c = :coordUser.Id]);
      
    }
  }
  
  @isTest
  private static void testDataAdminUser() {
    
    User dataAdminUser = [SELECT Id FROM User WHERE Email = :dataAdminEmail LIMIT 1];
    
    system.runAs(dataAdminUser) {
    
      NominationNewExt nomNew = new NominationNewExt(new ApexPages.StandardController(new Nomination__c()));
      system.assertNotEquals(null, nomNew.checkDataAdmin());
      
    }
  }
  
  @testSetup
  private static void setupData() {
    IsDataAdmin__c ida = new IsDataAdmin__c(SetupOwnerId = UserInfo.getUserId(), IsDataAdmin__c = true);
    insert ida;
    
    Document badgeImage = new Document(
      FolderId = UserInfo.getUserId(),
      Name = 'BadgeImage',
      DeveloperName = 'BadgeImage',
      Body = Blob.valueOf('Test Image')
    );
    insert badgeImage;
    
    List<WorkBadgeDefinition> newBadges = new List<WorkBadgeDefinition>();
    
    for (Schema.PickListEntry pe : WorkBadgeDefinition.Recognition_Category__c.getDescribe().getPicklistValues()) {
      
      newBadges.add(new WorkBadgeDefinition(
        Name = pe.getValue(),
        IsActive = true,
        IsCompanyWide = true,
        Description = 'Details: '+pe.getValue(),
        IsLimitPerUser = false,
        ImageUrl = badgeImage.Id,
        Recognition_Category__c = pe.getValue()
      ));
      
    }
    
    insert newBadges;
    
    User recipUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    recipUser.Email = recipEmail;
    
    User recipUser2 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    recipUser2.Email = recip2Email;
    
    User giverUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    giverUser.Email = giverEmail;
    
    User managUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    managUser.Email = managEmail;
    managUser.Oracle_IsManager__c = true;
    
    User coordUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    coordUser.Email = coordEmail;
    
    User dataAdminUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    dataAdminUser.Email = dataAdminEmail;
    
    insert new List<User>{recipUser, recipUser2, giverUser, managUser, coordUser, dataAdminUser};
    
    recipUser.ManagerId = managUser.Id;
    recipUser2.ManagerId = managUser.Id;
    update recipUser;
    
    PermissionSet psda = [SELECT Id FROM PermissionSet WHERE Name = 'Recognition_Data_Admin' LIMIT 1];
    PermissionSet pscoord = [SELECT Id FROM PermissionSet WHERE Name = 'Recognition_Coordinator' LIMIT 1];
    system.runAs(new User(Id = UserInfo.getUserId())) {
      PermissionSetAssignment psa1 = new PermissionSetAssignment(
        PermissionSetId = psda.Id,
        AssigneeId = dataAdminUser.Id
      );
      PermissionSetAssignment psa2 = new PermissionSetAssignment(
        PermissionSetId = pscoord.Id,
        AssigneeId = coordUser.Id
      );
      insert new List<PermissionSetAssignment>{psa1, psa2};
    }
    
    delete ida;
    
  }

}