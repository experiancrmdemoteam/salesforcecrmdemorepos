/*========================================================================================================
 * Appirio, Inc
 * Name: SMX_EMS_ProcessCaseBatchTest
 * Description: T-375155
 * Created Date: 12th May, 2015
 * Created By: Arpita Bose (Appirio)
 *
 * Date Modified      Modified By                  Description of the update
 * Sep 18th, 2015     Paul Kissick                 I-179463: Duplicate Management Failures
 * Apr 14th, 2016     Sadar Yacob                  Country and State picklist implementation : use standard picklist values
=========================================================================================================*/
@isTest
global class SMX_EMS_ProcessCaseBatchTest{
  
  @testSetup
  private static void prepareTestData()  {
    Account a = Test_utils.createAccount();
    a.Name = 'AAA1';
    a.BillingPostalCode = '211212';
    a.BillingStreet = 'TestStreet';
    a.BillingCity = 'TestCity';
    a.BillingCountry = 'United States of America';    
    insert a;  
    Contact c = new Contact(
      FirstName = 'BBB1',
      LastName = 'SMX2SMX',
      AccountId = a.Id, 
      Email = 'test123@experian.com'
    );
    insert c;    
    Case cs = new Case(
      Subject = 'CCC1', 
      AccountId = a.id, 
      Status = 'Closed Resolved', 
      ContactId = c.Id,
      ClosedDate = Datetime.now().addHours(-12),
      CreatedDate = Datetime.now().addHours(-24),
      RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('EMS').getRecordTypeId()
    );
    insert cs;
  }

  static testmethod void SmxcaseScheduler(){
    User testUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    testUser.Global_Business_Line__c = 'Corporate';
    testUser.Business_Line__c = 'Corporate';
    testUser.Business_Unit__c = 'APAC:SE';
    testUser.Region__c = 'North America';
    insert testUser;
    system.runAs(testUser) {
      Test.startTest();
      SMX_EMS_ProcessCaseBatch b = new  SMX_EMS_ProcessCaseBatch();
      Database.executebatch(b);
      Test.stopTest();
    }
    system.assertEquals(1,[SELECT COUNT() FROM Feedback__c]);
    
  }
}