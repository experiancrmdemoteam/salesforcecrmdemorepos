/*=============================================================================
 * Experian
 * Name: BatchAccEDQDependencyExistUpdate 
 * Description: Case 02136424 : If the Account contains EDQ Asset and check for the Partner Accounts and update the EDQ dependency to True for the partner Accounts.
 * Schedule to run Everyday at 9Pm CST
 * Created Date: 07th Dec 2016
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 
 ============================================================================*/
public class BatchAccEDQDependencyExistUpdate implements Database.Batchable<sObject>
{
    //start method of batch class
    public Database.QueryLocator start(Database.BatchableContext BC){                    
        return Database.getQueryLocator([select id,Name,AccountId from Asset where Product2.Business_Line__c='Experian Data Quality' AND Status__c IN ('Live','Live - Terminated','Scheduled')]);      
    }
    //execute method of batch class
    public void execute(Database.BatchableContext BC, List<Asset> lstAssets){    
        list<string> lstAccIds = new list<string>();
        for(Asset objAss:lstAssets){ //get the Account id's with Assets 
            lstAccIds.add(objAss.AccountId);
        } 
        //get the Partner Accounts which are related to Asset Accounts  
        for(Partner objP:[select AccountFromId,AccountToId from Partner where AccountFromId=:lstAccIds]){  
            lstAccIds.add(objP.AccountToId);
        }
        //Query the Account with edq dependency is false and update to true 
        list<Account> lstAcc = [select id, Name, EDQ_Dependency_Exists__c from Account where id=:lstAccIds AND EDQ_Dependency_Exists__c =false];
        for(Account acc:lstAcc){
            acc.EDQ_Dependency_Exists__c = true;
        }
        Database.update(lstAcc);        
    }  
    
    
    public void finish(Database.BatchableContext BC){
        BatchHelper bh = new BatchHelper();
       bh.checkBatch(bc.getJobId(), 'BatchAccEDQDependencyExistUpdate', true);
    }
    
}