/**=====================================================================
 * Experian
 * Name: AttachmentTriggerHandler
 * Description: Handler for Attachment Trigger
 * Created Date: Oct 26th, 2015
 * Created By: Paul Kissick 
 * 
 * Date Modified       Modified By                Description of the update
 * Oct  28th, 2015     Paul Kissick               Case 01169810 Added counting of attachments on Confidential Information records.
 * Mar  15th, 2016     Paul Kissick               Case 01903418: Fix for null exceptions
 * June 10th, 2016     Richard Joseph             Commented out Deal object attahcment realated changes. 
 * Sep  28th, 2016     Manoj Gopu                 Case 02148690: Updating Seal OCR File Exists field to true on Confidential Information record when attachment is created by SealAPIUSer
 * Jan  31st, 2017     Manoj Gopu                 ITSM: W-006704: Added 2 new methods for Transaction Queue record Insertion and deletion.
 * Feb  13th, 2017     Richard Joseph             ITSM: Added validation for File Type.   
*  Feb  13th, 2017     Manoj Gopu                 ITSM: W-006704: Updated the 2 methods for transaction Queue Insertion/Deletion to fire when the recordtype is Service Central.
 =====================================================================*/

public with sharing class AttachmentTriggerHandler {

//RJ - Declared an custom exception
public class FileExtException extends Exception {}


  public static void beforeInsert(List<Attachment> newList) {
    handleDealAttachments(newList);
    handleValidationAttachmentType(newList); 
  }
  
  public static void afterInsert(Map<Id,Attachment> newMap) {
    updateConfidentialInformationCount(newMap,true);
    insertTransactionQueue(newMap.Values());
  }
  
  public static void afterDelete(Map<Id,Attachment> oldMap) {
    updateConfidentialInformationCount(oldMap,false);
    deleteTransactionQueue(oldMap.Values());
  }
  
  public static void afterUndelete(Map<Id,Attachment> newMap) {
    updateConfidentialInformationCount(newMap,true);
  }
  //RJ Added 
  public static void beforeInsertIsDataAdmin(List<Attachment> newList) {
    if  (isITSMSkipUser__c.getInstance().isITSMSkipUser__c == false)
    handleValidationAttachmentType(newList); 
  }
  
  public static void afterInsertIsDataAdmin(Map<Id,Attachment> newMap) {
    if  (isITSMSkipUser__c.getInstance().isITSMSkipUser__c == false)
    insertTransactionQueue(newMap.Values());
  }
  
  public static void afterDeleteIsDataAdmin(Map<Id,Attachment> oldMap) {
    if  (isITSMSkipUser__c.getInstance().isITSMSkipUser__c == false)
    deleteTransactionQueue(oldMap.Values());
  }
  
  public static void afterUndeleteIsDataAdmin(Map<Id,Attachment> newMap) {

  }
  //RJ changes ends 
  public static void insertTransactionQueue(list<Attachment> newAttach){
      try{
          list<Transaction_Queue__c> lstTrans = new list<Transaction_Queue__c>();
           set<Id> setParentIds = new set<Id>();
           for(Attachment att:newAttach){
                string parentId = att.ParentId;
                if(parentId.startswith('500')){
                    setParentIds.add(att.ParentId);
                }          
           }
           if(setParentIds.isEmpty())
                return ;
           
           set<Id> setCaseIds = new set<Id>();
           for(Case objCase:[select id from Case where id IN: setParentIds AND RecordType.Name =:Constants.RECORDTYPE_Service_Central]){
                setCaseIds.add(objCase.Id);
           }
           for(Attachment att:newAttach){
           system.debug('newAttach@@'+att.Parent.Type);
           string parentId = att.ParentId;
               if(parentId.startswith('500') && setCaseIds.contains(parentId)){
                      Transaction_Queue__c obj = new Transaction_Queue__c();
                      obj.Action_Type__c = 'Insert';
                      obj.Transaction_Object_Name__c = 'CaseAttachment';
                      obj.Transaction_Status__c = 'New';
                      obj.SF_Record_ID__c = att.Id;                
                      obj.Target_Application__c = 'ServiceNow';
                      obj.Operation_Type__c = 'CaseAttachmentCreation';
                      obj.Transaction_Type__c = 'Outbound';
                      lstTrans.add(obj);
                 }
           }
           if(!lstTrans.isEmpty()){                          
              Database.insert(lstTrans);
          }          
      }
      catch (DMLException ex) {
      system.debug('[CaseTriggerHandler: syncCaseStatustoAgileStories] Exception: ' + ex.getMessage());
      ApexLogHandler.createLogAndSave('CaseTriggerHandler', 'syncCaseStatustoAgileStories', ex.getStackTraceString(), ex);
    }

  }
  public static void deleteTransactionQueue(list<Attachment> oldAttach){
      try{
          list<Transaction_Queue__c> lstTrans = new list<Transaction_Queue__c>();
           set<Id> setParentIds = new set<Id>();
           for(Attachment att:oldAttach){
                string parentId = att.ParentId;
                if(parentId.startswith('500')){
                    setParentIds.add(att.ParentId);
                }          
           }
           if(setParentIds.isEmpty())
                return ;
           
           set<Id> setCaseIds = new set<Id>();
           for(Case objCase:[select id from Case where id IN: setParentIds AND RecordType.Name =:Constants.RECORDTYPE_Service_Central]){
                setCaseIds.add(objCase.Id);
           }
           for(Attachment att:oldAttach){
           
               string parentId = att.ParentId;
               if(parentId.startswith('500') && setCaseIds.contains(parentId)){
                      Transaction_Queue__c obj = new Transaction_Queue__c();
                      obj.Action_Type__c = 'Delete';
                      obj.Transaction_Object_Name__c = 'CaseAttachment';
                      obj.Transaction_Status__c = 'New';
                      obj.SF_Record_ID__c = att.Id;                
                      obj.Target_Application__c = 'ServiceNow';
                      obj.Operation_Type__c = 'CaseAttachmentDelete';                    
                      obj.Transaction_Type__c = 'Outbound';
                      lstTrans.add(obj);
                 }
           }
           if(!lstTrans.isEmpty()){                          
              Database.insert(lstTrans);
          }          
      }
      catch (DMLException ex) {
      system.debug('[CaseTriggerHandler: syncCaseStatustoAgileStories] Exception: ' + ex.getMessage());
      ApexLogHandler.createLogAndSave('CaseTriggerHandler', 'syncCaseStatustoAgileStories', ex.getStackTraceString(), ex);
    }

  }
  //RJ ITSM Validation on content type.
  public static void handleValidationAttachmentType (List<Attachment> newList) {
  Set<string> inValidSet= new Set<string>((System.Label.ITSM_Community_Invalid_File_Type).split(';.'));
  //system.debug('Richard test 102 inValidSet ' + inValidSet );
  Set<id> caseIdSet = new Set<id>();
  
  for (Attachment record : newList) {    
       if (record.ParentId != null && record.ParentId.getSObjectType() == case.SObjectType ){
           caseIdSet.add(record.ParentId);
       }
  }
  
      Map<id , Case> caseMap = new Map<id , Case> ([Select Id ,RecordType.DeveloperName,status from case where Id in :caseIdSet]);
  
  for (Attachment record : newList) {
         // system.debug('Richard test 104 RecordType' + caseMap.get(record.ParentId).RecordType.DeveloperName );
      if (record.ParentId != null && record.ParentId.getSObjectType() == case.SObjectType && caseMap.size()>0 && caseMap.get(record.ParentId) != null && caseMap.get(record.ParentId).RecordType.DeveloperName == 'Service_Central' ) {
                 //system.debug('Richard test 106 File Name' + inValidSet.contains(record.name.split('\\.(?=[^\\.]+$)')[1]));
            If (record.name.split('\\.(?=[^\\.]+$)').size()>1 && inValidSet.contains(record.name.toLowerCase().split('\\.(?=[^\\.]+$)')[1])){
                throw new FileExtException(System.Label.ITSM_File_EXT_Validation_Message); 
            }
      }
      }
  
  
  }
  
  //RJ ITSM Ends
  public static void handleDealAttachments(List<Attachment> newList) {
    Set<id> dealIds = new Set<Id>();
    for (Attachment record : newList) {
      // Case 01903418: Adding check for ParentId not null
      if (record.ParentId != null && record.ParentId.getSObjectType() == Deal__c.SObjectType  ) {
        //dealIds.add(record.ParentId); //RJ Commented it out.        
      }
    }
    if (dealIds.size() == 0) {
      // Leave this method if no attachments are deal related...
      return;
    }
    Map<Id,Deal__c> dealRecs = new Map<Id,Deal__c>([
      SELECT Id, Encryption_Key__c 
      FROM Deal__c 
      WHERE Id IN :dealIds
    ]);
    // For each deal record, if it doesn't have a key yet, generate one and save it
    for(Deal__c dr : dealRecs.values()) {
      if (dr.Encryption_Key__c == null) {
        dr.Encryption_Key__c = EncodingUtil.base64Encode(Crypto.generateAesKey(256));
        
      }
      dr.IsSkipValidation__c = true;
    }
    
    update dealRecs.values();
    
    // For each attachment, if it's deal related, encode using the key from the parent
    for (Attachment record : newList) {
      if (record.ParentId.getSObjectType() == Deal__c.SObjectType) {
        Blob encBody = record.Body;
        record.Body = Crypto.encryptWithManagedIV('AES256', EncodingUtil.base64Decode(dealRecs.get(record.ParentId).Encryption_Key__c), encBody);
        record.Name = record.Name + '.enc'; // Append .enc to the name
      }
    }
  }
  
  public static void updateConfidentialInformationCount(Map<Id,Attachment> newMap, Boolean incrementCount) {
    string strSealAPIUserId = Global_Settings__c.getValues('Global').Seal_API_User__c;//Get the Seal API UserId from Custom setting
    
    Map<Id,Integer> confInfoIdToAttCountMap = new Map<Id,Integer>();
    for(Attachment att : newMap.values()) {
      if (att.ParentId != null && att.ParentId.getSobjectType() == Confidential_Information__c.sObjectType) {
        if (!confInfoIdToAttCountMap.containsKey(att.ParentId)) {
          confInfoIdToAttCountMap.put(att.ParentId,0);
        }
        confInfoIdToAttCountMap.put(att.ParentId,confInfoIdToAttCountMap.get(att.ParentId)+1);
      }
    }
    if (confInfoIdToAttCountMap.size() > 0) {
      List<Confidential_Information__c> confInfoToUpdate = [
        SELECT Id, Attachment_Count__c 
        FROM Confidential_Information__c 
        WHERE Id IN :confInfoIdToAttCountMap.keySet()
      ];
      for(Confidential_Information__c ci : confInfoToUpdate) {
        if (ci.Attachment_Count__c == null) {
          ci.Attachment_Count__c = 0;
        }
        if (incrementCount) {
          // Increment the count
          ci.Attachment_Count__c += confInfoIdToAttCountMap.get(ci.Id);
        }
        else {
          // Decrement the count
          ci.Attachment_Count__c -= confInfoIdToAttCountMap.get(ci.Id);
        }
        if (ci.Attachment_Count__c < 0) {
          ci.Attachment_Count__c = 0;
        }
        //Checking whether the logged in user is seal API USer or not. if yes, set the "seal OCR File Exists" to true 
        if (String.isNotBlank(strSealAPIUserId) && Userinfo.getUserId().contains(strSealAPIUserId)) {
            ci.Seal_OCR_File_Exists__c = true;
        }       
      }
      update confInfoToUpdate;
    }
  }
  

}