global class ScheduleAddPreviousChatterUserStat implements Schedulable
{
  /*
  * Author:     Diego Olarte (Experian)
  * Description:  The following class is for scheduling the 'AddPreviousChatterUserStat.cls' class to run at specific intervals.
  */  
  
  global void execute(SchedulableContext sc)
  {
    AddPreviousChatterUserStat batchToProcess = new AddPreviousChatterUserStat();
    database.executebatch(batchToProcess);
  }
}