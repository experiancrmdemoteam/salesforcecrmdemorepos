/**=====================================================================
 * Experian
 * Name: BatchHelper
 * Description: Designed to handle the error notification from a batch process
 * Created Date: 9 Nov 2015
 * Created By: Paul Kissick
 *
 * Date Modified         Modified By                  Description of the update
 * Mar 14, 2016          Paul Kissick                 Case 01890211: Moved common method to this class 
 * Sep 19th, 2016        Paul Kissick                 W-005698: Adding method to generate a GUID
 =====================================================================*/
 
public class BatchHelper {
  
  public Messaging.SingleEmailMessage mail;
  public Boolean batchHasErrors;
  public String emailBody;
  
  public BatchHelper() {
    emailBody = '';
    batchHasErrors = false;
  }
  
  //===========================================================================
  // Check the batch process and see if any errors are there already
  //===========================================================================
  public void checkBatch(Id batchJobId, String className, Boolean sendNow) {
    
    // take this id and send an alert if there is a failure.
    AsyncApexJob job = [
      SELECT Id, Status, NumberOfErrors, 
        JobItemsProcessed, ExtendedStatus,
        TotalJobItems, CreatedById
      FROM AsyncApexJob 
      WHERE Id = :batchJobId
    ];
    
    Integer numOfError = job.NumberOfErrors;
    
    if (numOfError > 0) {
      batchHasErrors = true;
    }
    
    emailBody = String.format(
      'The {0} batch job processed {1} out of {2} batches, but generated {3} error{4}.\n\n',
      new List<String>{
        className,
        String.valueOf(job.JobItemsProcessed),
        String.valueOf(job.TotalJobItems),
        String.valueOf(job.NumberOfErrors),
        ((numOfError==1) ? '': 's')
      }
    );
    
    if (String.isNotBlank(job.ExtendedStatus)) {
      emailBody += 'The last error was: '+job.ExtendedStatus+'\n\n';
    }
    
    system.debug(emailBody);
    
    mail = new Messaging.SingleEmailMessage();
    mail.setSaveAsActivity(false);
    mail.setToAddresses(FailureNotificationUtility.retrieveRecipients(className));
    mail.setSubject('Force.com '+className+' Error Handler');
    mail.setBccSender(false);
    mail.setUseSignature(false);
    
    if (sendNow) {
      sendEmail();
    }
  }
  
  //===========================================================================
  // Sends the email from the batch, but only if no errors are listed.
  //===========================================================================
  public void sendEmail () {
    
    emailBody += '\n\n****************************';
    emailBody += '\nSent from '+UserInfo.getOrganizationName() + ' (ID: '+UserInfo.getOrganizationId()+')\n';
    
    String emailHtmlBody = emailBody.replace('\n','<br/>');
    mail.setHtmlBody(emailHtmlBody);
    mail.setPlainTextBody(emailBody);
    if (!Test.isRunningTest() && batchHasErrors) {
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
  }
  
  //===========================================================================
  // Retrieve the timestamp, or create an empty one if not there already
  //===========================================================================
  public static Datetime getBatchClassTimestamp (String name) {
    if (Batch_Class_Timestamp__c.getAll().containsKey(name)) {
      return Batch_Class_Timestamp__c.getValues(name).Time__c;
    }
    else {
      insert new Batch_Class_Timestamp__c(Name = name);
      return null;
    }
  }
  
  //===========================================================================
  // Update the timestamp entry
  //===========================================================================
  public static void setBatchClassTimestamp (String name, Datetime val) {
    if (Batch_Class_Timestamp__c.getAll().containsKey(name)) {
      // return Batch_Class_Timestamp__c.getValues(name).Time__c;
      Batch_Class_Timestamp__c b = Batch_Class_Timestamp__c.getValues(name);
      b.Time__c = val;
      update b;
    }
    else {
      insert new Batch_Class_Timestamp__c(Name = name, Time__c = val);
    }
  }
  
  //===========================================================================
  // Parse the errors out into a list from the db errors
  //===========================================================================
  public static List<String> parseErrors(List<Database.Error> dbErrors) {
    List<String> errorsFound = new List<String>();
    for(Database.Error err : dbErrors) {
      errorsFound.add('Code: '+err.getStatusCode()+' - Message:'+err.getMessage()+' - Fields:'+String.join(err.getFields(),','));
    }
    return errorsFound;
  }
  
  private static String kHexChars = '0123456789abcdef';
  
  //===========================================================================
  // Generate a guid in the form: a03c534e-4f51-4212-af16-8b2594e1bb6b
  //===========================================================================
  public static String newGuid() {
    String returnValue = '';
    Integer nextByte = 0;
    for (Integer i = 0; i < 16; i++) {
      if (i == 4 || i == 6 || i == 8 || i == 10) {
        returnValue += '-';
      }
      nextByte = (Math.round(Math.random() * 255)-128) & 255;
      if (i == 6) {
        nextByte = nextByte & 15;
        nextByte = nextByte | (4 << 4);
      }
      if (i == 8) {
        nextByte = nextByte & 63;
        nextByte = nextByte | 128;
      }
      returnValue += getCharAtIndex(kHexChars, nextByte >> 4);
      returnValue += getCharAtIndex(kHexChars, nextByte & 15);
    }
    return returnValue;
  }

  //===========================================================================
  // Used above
  //===========================================================================
  public static String getCharAtIndex(String str, Integer index) {
    if (str == null) return null;
    if (str.length() <= 0) return str;
    if (index == str.length()) return null;
    return str.substring(index, index+1);
  }
  
}