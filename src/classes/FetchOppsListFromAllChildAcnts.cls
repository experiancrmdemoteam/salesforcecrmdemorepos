/**=====================================================================
 * Experian, Inc
 * Name:   FetchOppsListFromAllChildAcnts
 * Created Date: Jul 11, 2016
 * Created By:  Tyaga Pati(Experian)
 * Description: Function will return the account list from all Account and their children.               
 *                    
 * Date Modified          Modified By           Description of the update
 * 27th July 2016(QA)     Cristian Torres       CRM2:W-005383: Added a new query and support for filters
 * Sep 6th, 2016          Paul Kissick          CRM2:W-005383: Cleaned up formatting and removed unnecessary code.
 * Oct 21st, 2016         Manoj Gopu            CRM2:W-005616: Added new method exportToExcel for export
  =====================================================================*/  
public with sharing class FetchOppsListFromAllChildAcnts {

  public ApexPages.standardController stdController {get; set;}
  public Opportunity opp1 {get; set; } //used for the type selection
  
  private Boolean searchBtnClicked;
  private Id AccountId; //This one holds the Id of the account under consideration
    
  public Integer noOfRecords {get;set;}
  public Integer size {get;set;}
  public String queryOppString;
  public list<Opportunity> lstExportData{get;set;}
  
  private List<Id> allAccountIdList;
    
  //Sorting Requirement
  private String sortFullExp; 
  private String sortDirection = 'ASC';
  private String sortExp = 'name'; 
   
  public String sortExpression {
    get {
      return sortExp;
    }
    set {
      //if the column is clicked on then switch between Ascending and Descending modes
      if (value == sortExp) {
        sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
      }
      else {
        sortDirection = 'ASC';
      }
      sortExp = value;
    }
  }

  public String getSortDirection() {
    //if not column is selected 
    if (String.isBlank(sortExpression)) {
      return 'ASC';
    }
    else {
      return sortDirection;
    }
  }

  public void setSortDirection(String value) {
    sortDirection = value;
  }
  
  //=====================================================================
  //  Constructor taking standard controller as a parameter and fetch the Account
  //=====================================================================
  public FetchOppsListFromAllChildAcnts(ApexPages.standardController controller) {
    stdController = controller;
    AccountId = controller.getId();     
    opp1 = new Opportunity();     
    opp1.Type = '';
    opp1.Mash_Approval_Date__c = null; 
    opp1.Contract_End_Date__c = null;
    searchBtnClicked = false;
  }
  
  public PageReference backToAccount() {
    return stdController.view();
  } 

  //=====================================================================
  //Method to generate
  //===================================================================== 
  public void getopps4llChildAcnt(){
    //This list will the Accounts that are children of a particular level
    allAccountIdList = AccountUtility.fetchAllChildAccounts(AccountId); //This list will hold the final list of Accounts to be queried to get the Opps
    system.debug(allAccountIdList);
 
    List<String> fieldList = new List<String>{
      'Amount', 
      'Account.Name', 
      'Name', 
      'Type', 
      'StageName', 
      'Amount_Corp__c', 
      'CloseDate', 
      'Current_FY_revenue_impact__c',
      'Owner.Name',
      'Owner_s_Business_Unit__c',
      'Owner_s_Sales_team__c',
      'Owner_s_Sales_Sub_Team__c'
    };
    
    queryOppString = 'SELECT '+String.join(fieldList, ',') + ' FROM Opportunity WHERE AccountId IN :allAccountIdList ';
     
    if (searchBtnClicked == true) {
      if (String.isNotBlank(opp1.Type)) {
        String typeStr = opp1.Type;
        queryOppString += ' AND Type = \'' + String.escapeSingleQuotes(typeStr) + '\' ';
      }
      // We appear to be using another opp field to store a temp date variable
      if (opp1.Mash_Approval_Date__c != null) {
        DateTime beginningDate = opp1.Mash_Approval_Date__c;
        queryOppString += ' AND CloseDate >=  ' + beginningDate.format('yyyy-MM-dd') + ' ';
      }

      if (opp1.Contract_End_Date__c != null) {
        DateTime endingDate = opp1.Contract_End_Date__c;
        queryOppString += ' AND CloseDate <=  ' + endingDate.format('yyyy-MM-dd') + ' ';
      }
      if (String.isNotBlank(opp1.StageName)) {
        String stageStr = opp1.StageName;
        queryOppString += ' AND StageName = \''  + String.escapeSingleQuotes(stageStr) + '\' ';
      }
    }
    sortFullExp = sortExpression  + ' ' + sortDirection;
    if (sortFullExp.length() > 0) {
      queryOppString += ' ORDER BY ' + sortFullExp + ' LIMIT 1000 ';
    }
    system.debug('Query: ' + queryOppString);
  }
  
  // Added new methos to export -Added by Manoj 
  public PageReference exportToExcel(){   
    lstExportData = new list<Opportunity>();
    lstExportData = Database.Query(queryOppString);
    PageReference pf=new PageReference('/apex/FetchOppsListFromAllChildAcntsExport');
    return pf;
  }

  public ApexPages.StandardSetController setCon {
    get {
      if (setCon == null) {
        size = 50;
        getopps4llChildAcnt();
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryOppString));
        setCon.setPageSize(size);
        noOfRecords = setCon.getResultSize();
      }
      return setCon;
    }
    set;
  }
    
  public List<Opportunity> getOpps() {
    List<Opportunity> oppList = new List<Opportunity>();
    for (Opportunity o : (List<Opportunity>)setCon.getRecords()) {
      oppList.add(o);
    }
    return oppList;
  }
    
  public pageReference refresh() {
    setCon = null;
    getOpps();
    setCon.setPageNumber(1);
    return null;
  }
    
  public PageReference queryChildOpps() {
    searchBtnClicked = true;
    refresh();
    return null;
  }
    
}