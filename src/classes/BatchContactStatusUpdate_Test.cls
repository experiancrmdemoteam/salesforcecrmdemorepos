/*=============================================================================
 * Experian
 * Name: BatchContactStatusUpdate_Test
 * Description: 
 * Created Date: 22 Aug 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

@isTest
private class BatchContactStatusUpdate_Test {

  static testMethod void testBatchActives() {
    
    Datetime startTime = Datetime.now();
    
    Test.startTest();
    
    Database.executeBatch(new BatchContactStatusUpdate());
    
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM Contact WHERE Status__c = :Constants.CONTACT_STATUS_INACTIVE AND Status_Last_Updated_Date__c >= :startTime], '1 contact should have been updated!');
    
  }
  
  static testMethod void testBatchInactivesTasks() {
    
    Datetime startTime = Datetime.now();
    
    Test.startTest();
    
    Database.executeBatch(new BatchContactStatusUpdate(new BatchContactStatusUpdate.RunProcess[]{BatchContactStatusUpdate.RunProcess.InactivesRecentTasks}));
    
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM Contact WHERE Status__c = :Constants.STATUS_ACTIVE AND Status_Last_Updated_Date__c >= :startTime], '1 contact should have been updated!');
    
  }
  
  static testMethod void testBatchInactivesOpps() {
    
    Datetime startTime = Datetime.now();
    
    Test.startTest();
    
    Database.executeBatch(new BatchContactStatusUpdate(new BatchContactStatusUpdate.RunProcess[]{BatchContactStatusUpdate.RunProcess.InactivesRecentOpenOpps}));
    
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM Contact WHERE Status__c = :Constants.STATUS_ACTIVE AND Status_Last_Updated_Date__c >= :startTime], '1 contact should have been updated!');
    
  }
  
  static testMethod void testBatchInactivesCamps() {
    
    Datetime startTime = Datetime.now();
    
    Test.startTest();
    
    Database.executeBatch(new BatchContactStatusUpdate(new BatchContactStatusUpdate.RunProcess[]{BatchContactStatusUpdate.RunProcess.InactivesRecentCampaigns}));
    
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM Contact WHERE Status__c = :Constants.STATUS_ACTIVE AND Status_Last_Updated_Date__c >= :startTime], '1 contact should have been updated!');
    
  }
    
  static testMethod void testSchedule() {
    
    Test.startTest();
    
    system.schedule('Contact - Update Status Test 123', '0 0 * * * ?', new BatchContactStatusUpdate());
    
    Test.stopTest();
    
  }
  
  @testSetup
  private static void setupData() {
    // Initialise data for test here.
    
    Account a1 = Test_Utils.insertAccount();
    Contact c1 = Test_Utils.createContact(a1.Id);
    c1.Status__c = Constants.STATUS_ACTIVE;
    
    Contact c2 = Test_Utils.createContact(a1.Id);
    c2.Status__c = Constants.STATUS_ACTIVE;
    c2.Status_Last_Updated_Date__c = Datetime.now().addYears(-2);
    
    Contact c3 = Test_Utils.createContact(a1.Id);
    c3.Status__c = Constants.CONTACT_STATUS_INACTIVE;
    c3.Status_Last_Updated_Date__c = Datetime.now().addDays(-2);
    
    Contact c4 = Test_Utils.createContact(a1.Id);
    c4.Status__c = Constants.CONTACT_STATUS_INACTIVE;
    c4.Status_Last_Updated_Date__c = Datetime.now().addDays(-2);
    
    Contact c5 = Test_Utils.createContact(a1.Id);
    c5.Status__c = Constants.CONTACT_STATUS_INACTIVE;
    c5.Status_Last_Updated_Date__c = Datetime.now().addDays(-2);
    
    insert new Contact[]{c1,c2,c3,c4,c5};
    Test.setCreatedDate(c2.Id, DateTime.newInstance(2012,12,12));
    
    Task t1 = Test_Utils.createTask(c1.Id, null);
    t1.Subject = 'New Task';
    t1.ActivityDate = Date.today();
    t1.Status = 'Completed';
    Task t2 = Test_Utils.createTask(c3.Id, null);
    t2.Subject = 'New Task';
    t2.Status = 'Completed';
    t2.ActivityDate = Date.today();
    insert new Task[]{t1,t2};
    
    Opportunity o1 = Test_Utils.createOpportunity(a1.Id);
    insert new Opportunity[]{o1};
    
    OpportunityContactRole ocr1 = Test_Utils.insertOpportunityCR(true, c4.Id, o1.Id);
    
    Campaign ca1 = Test_Utils.createCampaign();
    ca1.Status = 'Active';
    update ca1;
    CampaignMember cam1 = new CampaignMember(ContactId = c5.Id, CampaignId = ca1.Id, Status = 'Sent');
    insert cam1;
    
  }
  
}