/**=====================================================================
 * Experian
 * Name: QuoteTriggerHandler_Test 
 * Description: To test Handler class for QuoteTrigger (Case 01059650)
 * Created Date: 18th Jan 2016
 * Created By: James Wills
 * 
 * Date Modified    Modified By            Description of the update
 * Apr 7th, 2016    Paul Kissick           Case 01932085: Fixing Test User Email Domain
 * Jul 28th, 2016   Diego Olarte           CRM2:W-005496: Updated to test method primaryQuoteApproved
  =====================================================================*/

@isTest 
private class QuoteTriggerHandler_Test {
  
  //TEST METHOD 1
  //Test that the addition and then deletion of Quote__c sObjects to an Opportunity results in the correct calculation of Opportunity.Quote_Count__c  
  static testMethod void testInsertAndDeleteQuotes() {

    Opportunity newOpportunity = createOpportunity('1');
    insert newOpportunity;
     
    Profile p = [select id from profile where name = :Constants.PROFILE_SYS_ADMIN];       
    User user1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    user1.isActive = True;
    insert user1;
    //end of create data for tests section
 
    Integer quoteCount1 = 0;
     
    Test.startTest();

    //TEST NUMBER 2 - afterDelete #1     
    List<Quote__c> newQuoteList = new List<Quote__c>(); 
     
    Integer quotesToAddToOpportunity = 10;
    
    for (Integer quoteCounter = 0; quoteCounter < quotesToAddToOpportunity; quoteCounter++) {
      Quote__c newQuote = insertQuote(newOpportunity); 
      newQuoteList.add(newQuote);
      quoteCount1++;
    }
    insert newQuoteList;
     
    //Manually call QuoteTriggerHandler.afterInsert() as it is not enabled to run during tests
    QuoteTriggerHandler.afterInsert(newQuoteList);
          
    checkQuoteTotals(newOpportunity, quoteCount1, 'QuoteTriggerHandler_Test: TEST 1a - Testing the addition of ' + quotesToAddToOpportunity + ' to Opportunity.');
     
    List<Quote__c> quoteListToDelete = new List<Quote__c>();
    integer quoteCounter2;
    integer quotesToDelete = 5;
    for (quoteCounter2 = 0; quoteCounter2 < quotesToDelete; quoteCounter2++) {
      quoteListToDelete.add(newQuoteList[quoteCounter2]);
      quoteCount1--;
    }
    delete quoteListToDelete;

    //Manually call QuoteTriggerHandler.afterDelete() as it is not enabled to run during tests     
    QuoteTriggerHandler.afterDelete(newQuoteList);   
     
    Test.stopTest();
    checkQuoteTotals(newOpportunity, quoteCount1, 'QuoteTriggerHandler_Test: TEST 1b - Testing the deletion of ' + quotesToDelete + ' to Opportunity.');

  }
    
  //TEST METHOD 2
  static testMethod void testAdditionOf_Multiple_Quotes_To_Multiple_Opportunities() {

    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];       
    User user1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    user1.isActive = True;
     
     
    List<Opportunity> oppList = new List<Opportunity>();
    integer opportunityCounter;
    String opportunityNameIdentifier;
    for (opportunityCounter = 0; opportunityCounter < 10; opportunityCounter++) {
      opportunityNameIdentifier = String.valueOf(opportunityCounter);
      Opportunity newOpportunity = createOpportunity(opportunityNameIdentifier);
      oppList.add(newOpportunity);
    }
    insert oppList;
    //end of create data for tests section
     
    // List<QuoteCounter> opportunityQuoteCountList = new List<QuoteCounter>();
        
    Test.startTest();
    
    Map<Id,Integer> oppToQuoteCount = new Map<Id,Integer>();

    for (Opportunity opp : oppList) {
      List<Quote__c> newQuoteList = new List<Quote__c>(); 

      for (Integer quoteCounter = 0; quoteCounter < 10; quoteCounter++) {                    
        Quote__c newQuote = insertQuote(opp); 
        newQuoteList.add(newQuote);
      }
      oppToQuoteCount.put(opp.Id, 10);
      insert newQuoteList;
      
      //Manually call QuoteTriggerHandler.afterInsert() as it is not enabled to run during tests     
      QuoteTriggerHandler.afterInsert(newQuoteList);     
       
    }
     
    Test.stopTest();     
     
    Integer opportunityCounter2 = 0;
    String opportunityNumber;
     
    for (Id oppId : oppToQuoteCount.keySet()) {
      opportunityNumber = String.valueOf(opportunityCounter2);  
      checkQuoteTotals(oppList[opportunityCounter2], oppToQuoteCount.get(oppId), 'QuoteTriggerHandler_Test: Count of Quotes for Opportunity Number ' + opportunityNumber);
      opportunityCounter2++;
    }
   
   List<Quote__c> qListToUpdate1 = new List<Quote__c>();
    
    for (Quote__c qList : [select id, Status__c, Opportunity__c FROM Quote__c WHERE Opportunity__c IN : oppList LIMIT 1]){
        qList.Status__c = System.Label.CPQ_Completed_Status;
        qListToUpdate1.add(qList);  
    }
     update qListToUpdate1;
    
    List<Quote__c> qListToUpdate2 = new List<Quote__c>();
    
    for (Quote__c qList : [select id, Status__c, Opportunity__c FROM Quote__c WHERE Opportunity__c IN : oppList LIMIT 1]){
        qList.Status__c = System.Label.CPQ_Approved_Status;
        qListToUpdate2.add(qList);  
    }
     update qListToUpdate2;
   
  }

  @testSetup
  private static void createDataForTest() {
     
     Global_Settings__c custSettings = new Global_Settings__c(name= Constants.GLOBAL_SETTING, Account_Team_Member_Default_Role__c = 'Sales Rep');
     insert custSettings;
     
     Account newAccount = new Account(Name = 'TestAccount001', BillingCountry = 'Sweden');
     insert newAccount;
     
     Contact newContact = new Contact(FirstName = 'Bob', LastName = 'Smith', accountId=newAccount.Id);     
     insert newContact;   
     
  }
  
  private static Opportunity createOpportunity(String opportunityNameIdentifier) {
    Date closeDate = Date.today().addYears(1);
    
    Opportunity newOpportunity = new Opportunity(
      Name = 'Test Opportunity1' + opportunityNameIdentifier,
      StageName = 'Identify',
      CloseDate = closeDate      
    );     
    return newOpportunity;
  }
  
  private static Quote__c insertQuote(Opportunity opp) {    
    Quote__c newQuote = new Quote__c(Opportunity__c = opp.id);
    return newQuote;
  }
  
  private static void checkQuoteTotals(Opportunity opportunityToTest, Integer quoteCount1, String testNumber) {
    List<Opportunity> updatedOpportunities = [SELECT Name, Quote_Count__c FROM Opportunity WHERE ID = :opportunityToTest.id LIMIT 1];
    if (updatedOpportunities.size() > 0) {
      for (Opportunity updatedOpportunity : updatedOpportunities) {
        system.assertEquals(quoteCount1, updatedOpportunity.Quote_Count__c,  'TEST NUMBER' + testNumber + ' Contact:\'' + updatedOpportunity.Name + '\': updatedOpportuntiy.Quote_Count__c not equal to expected value of '  + quoteCount1  + '. Value is: ' + updatedOpportunity.Quote_Count__c);
      }
    }  
  }
  
  
}