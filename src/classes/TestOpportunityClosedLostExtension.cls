/**=====================================================================
 * Appirio, Inc
 * Name: TestOpportunityClosedLostExtension
 * Description: This test class is for testing the 'OpportunityClosedLostExtension.cls' class
         Story:    S-153835
         Task:     T-205590
 * Created Date: 
 * Created By: Pankaj Mehra (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Jan 30th, 2014               Jinesh Goyal(Appirio)        T-232760: Homogenize the comments
 * Mar 05th, 2014               Arpita Bose(Appirio)         T-243282: Added Constants in place of String
 * Mar 14th, 2014               Arpita Bose(Appirio)         T-253906: Fixed error
 * Apr 24th, 2014               Arpita Bose                  Updated testmethod to increase the code coverage
 * Nov 17th, 2015               Paul Kissick                 Case 01122944 - Fix for triggers not being fired properly.
 * Apr 1st, 2016                Sarah Barber                 Case 01661353 - Added a new field for win back date unknown
 * Apr 26th, 2016				Sarah Barber				 Case 01951938 - Added Contract_End_Date__c to display on the page
 =====================================================================*/

@isTest
private class TestOpportunityClosedLostExtension {

  static testMethod void testOpportunityUpdate() {
     // Insert custom setting
    Global_Settings__c setting = Test_Utils.insertGlobalSettings();
    Test_Utils.insertIsDataAdmin(true);
    
    Account account = Test_Utils.insertAccount();
    Opportunity opportunity = Test_Utils.createOpportunity(account.Id);
    opportunity.Primary_Reason_W_L__c = Constants.PRIMARY_REASON_WLC_COMPETITION;
    insert opportunity;
      
    Account competitorAccount1 = Test_Utils.createAccount();
    competitorAccount1.Is_Competitor__c = true;
    insert competitorAccount1;
    Competitor__c competitor1 = Test_Utils.createCompetitor(opportunity.Id);
    competitor1.Account__c = competitorAccount1.Id;
    insert competitor1;
    Account competitorAccount2 = Test_Utils.createAccount();
    competitorAccount2.Is_Competitor__c = true;
    insert competitorAccount2;
    Competitor__c competitor2 = Test_Utils.createCompetitor(opportunity.Id);
    competitor2.Account__c = competitorAccount2.Id;
    insert competitor2;
    List<SelectOption> options = new List<SelectOption>();
    Test.startTest();
    
    ApexPages.currentPage().getParameters().put('type', 'lost'); 
    
    ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(opportunity);
    OpportunityClosedLostExtension extension = new OpportunityClosedLostExtension(controller);
      
    // Create Competitor Select Option List
    List<SelectOption> competitorSelectList = extension.getCompetitors();
    system.assert(competitorSelectList.size() == 3);
      
    Opportunity opp = [select Name,StageName,Primary_Reason_W_L__c,Type,isClosed, isWon, Other_Closed_Reason__c, Lost_To__c, Contract_End_Date__c, Win_Back_Date__c,Win_Back_Date_Unknown__c, Primary_Winning_Competitor__c from Opportunity where id=:opportunity.Id];
    system.debug('#####opp#####' +opp);
      
    List<SelectOption> PrimaryReasonsSelectList = extension.getPrimaryReasons();
    // Set Selected Primary
    extension.selectedPrimaryReason = 'Competition';
    // Update Selected Primary
    extension.primaryReasonChanged();
    // Set Selected Comprtitor
    extension.selectedCompetitor = competitor1.Id;
    extension.opp.Primary_Winning_Competitor__c = competitor1.Id;
      
    // Set the Opportunity to closed and set status of selected Competitor
    extension.saveRecord();

    Test.stopTest();

    //Opportunity updatedOppty = [SELECT StageName FROM Opportunity WHERE Id =: opportunity.Id];
    //System.assert(updatedOppty.StageName == Constants.OPPTY_CLOSED_LOST);
    
    //Competitor__c updatedCompetitor = [SELECT Lost_To__c FROM Competitor__c WHERE Id =: competitor1.Id];
    //System.assert(updatedCompetitor.Lost_To__c == true);
  }
  
  
  static testMethod void testErrorsFromPage() {
    Global_Settings__c setting = Test_Utils.insertGlobalSettings();
    Test_Utils.insertIsDataAdmin(true);
    
    Account account = Test_Utils.insertAccount();
    
    Account competitor1 = Test_Utils.createAccount();
    competitor1.Is_Competitor__c = true;
    insert competitor1;
    
    // Testing Errors :
    Opportunity opportunitywithoutPrimaryReason = Test_Utils.createOpportunity(account.Id);
    opportunitywithoutPrimaryReason.StageName = setting.Opp_Closed_Lost_Stagename__c;
    opportunitywithoutPrimaryReason.Primary_Reason_W_L__c = Constants.PRIMARY_REASON_WLC_TERMS;
    insert opportunitywithoutPrimaryReason;
    ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(opportunitywithoutPrimaryReason);
    OpportunityClosedLostExtension extension = new OpportunityClosedLostExtension(controller);
    
    // Controller will generate page message as opportuntiy is closed    
    system.assertEquals(true, checkMessages(ApexPages.getMessages(),Label.OCL_Message_Closed_Opportunity));
    

    extension.createComp();
    system.assertEquals(0, [SELECT COUNT() FROM Competitor__c WHERE Opportunity__c = :opportunitywithoutPrimaryReason.Id]);
    
    // Set the incorrect competitor    
    extension.compRec.Account__c = opportunitywithoutPrimaryReason.Id;
    extension.createComp();
    system.assertEquals(0, [SELECT COUNT() FROM Competitor__c WHERE Opportunity__c = :opportunitywithoutPrimaryReason.Id]);
    
    // Set the correct competitor    
    extension.compRec.Account__c = competitor1.Id;
    extension.createComp();
    system.assertEquals(1, [SELECT COUNT() FROM Competitor__c WHERE Opportunity__c = :opportunitywithoutPrimaryReason.Id]);
    
    
    extension.opp.Primary_Reason_W_L__c = '';
    // Save record will through a Error as Opportunity PrimaryReason is blank
    extension.saveRecord();
    system.assertEquals(true, checkMessages(ApexPages.getMessages(),Label.OCL_Message_Primary_Reason_required));
    // Save record will throw a Error as Opportunity PrimaryReason is Other and Other_Closed_Reason is null
    extension.opp.Primary_Reason_W_L__c = 'Other';
    extension.opp.Win_Back_Date__c = null;
    extension.opp.Win_Back_Date_Unknown__c = false;
    extension.saveRecord();
    system.assertEquals(true, checkMessages(ApexPages.getMessages(),Label.OCL_Win_Back_Date_Info_Required));
    
    extension.opp.Win_Back_Date__c = Date.today().addDays(50);
    extension.opp.Other_Closed_Reason__c = '';
    extension.saveRecord();
    system.assertEquals(true, checkMessages(ApexPages.getMessages(),Label.OCL_Message_Other_Closed_Reason_required));
     
    // Set Opportunity PrimaryReason 
    extension.opp.Primary_Reason_W_L__c = Constants.PRIMARY_REASON_WLC_COMPETITION;
    // Save record will through no competitor is selected
    extension.saveRecord();
    
    
  }
  
  static testMethod void testNoDecisionOpportunity() {
     // Insert custom setting
    Global_Settings__c setting = Test_Utils.insertGlobalSettings();
    Test_Utils.insertIsDataAdmin(true);
    
    Account account = Test_Utils.insertAccount();
    Opportunity opportunity = Test_Utils.createOpportunity(account.Id);
    insert opportunity;
    
    Test.startTest();
    
    ApexPages.currentPage().getParameters().put('type', 'nodecision'); 
    OpportunityClosedLostExtension extension = new OpportunityClosedLostExtension(new ApexPages.Standardcontroller(opportunity));
      
    // Create Competitor Select Option List
    List<SelectOption> competitorSelectList = extension.getCompetitors();
    system.assert(competitorSelectList.size() == 1); // set to none
      
    List<SelectOption> PrimaryReasonsSelectList = extension.getPrimaryReasons();
    // Set Selected Primary
    extension.selectedPrimaryReason = Constants.OPPTY_PRIMARY_CLOSED_REASON_CUSTOMER_UNDECIDED;
    // Update Selected Primary
    extension.primaryReasonChanged();
      
    // Set the Opportunity to closed and set status of selected Competitor
    extension.saveRecord();
    
    Test.stopTest();

    system.assertEquals(true, [SELECT IsClosed FROM Opportunity WHERE Id = :opportunity.Id].IsClosed);
    
  }
  
  
  private static Boolean checkMessages(List<ApexPages.Message> msgList, String text) {
    Set<String> messages = new Set<String>();
    for(ApexPages.Message msg : msgList) {
      messages.add(msg.getSummary());
    }
    if (messages.size() == 0) return false;
    return messages.contains(text);
  }
  
}