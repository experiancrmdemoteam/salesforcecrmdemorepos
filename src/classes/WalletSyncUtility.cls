/**=====================================================================
 * Experian
 * Name: WalletSyncUtility
 * Description: Used to control the current state of the wallet sync processing based on the integration state.
 * 
 * Created Date: 11th August, 2015
 * Created By: Paul Kissick
 *
 * Date Modified       Modified By                  Description of the update
 * Oct 1st, 2015       Paul Kissick                 Adding Wallet_Sync_Processing_Last_Run__c
 * Nov 27th, 2015      Paul Kissick                 Case 01266075: Replacing Global_Setting__c with new Batch_Class_Timestamp__c
 =====================================================================*/
global class WalletSyncUtility {
  
  global static Datetime getWalletSyncProcessingLastRun() {
    checkForMissingSettings();
    return Batch_Class_Timestamp__c.getValues('WalletSyncProcessingLastRun').Time__c;
  }
  
  global static void checkForMissingSettings() {
    List<Batch_Class_Timestamp__c> missingSettings = new List<Batch_Class_Timestamp__c>();
    if (!Batch_Class_Timestamp__c.getAll().containsKey('WalletSyncProcessingLastRun')) {
      missingSettings.add(new Batch_Class_Timestamp__c(Name = 'WalletSyncProcessingLastRun', Time__c = null));
    }
    if (!Batch_Class_Timestamp__c.getAll().containsKey('WalletSyncProcessingStart')) {
      missingSettings.add(new Batch_Class_Timestamp__c(Name = 'WalletSyncProcessingStart', Time__c = null));
    }
    if (!Batch_Class_Timestamp__c.getAll().containsKey('WalletSyncProcessingComplete')) {
      missingSettings.add(new Batch_Class_Timestamp__c(Name = 'WalletSyncProcessingComplete', Time__c = null));
    }
    if(missingSettings.size() > 0) {
      insert missingSettings;
    }
  }

  global static void processingStarted() {
    // update the custom setting
    checkForMissingSettings();

    Batch_Class_Timestamp__c lastRun = Batch_Class_Timestamp__c.getValues('WalletSyncProcessingLastRun');
    
    Batch_Class_Timestamp__c processStart = Batch_Class_Timestamp__c.getValues('WalletSyncProcessingStart');
    
    lastRun.Time__c = processStart.Time__c; 
    
    if (lastRun.Time__c == null) {
      lastRun.Time__c = DateTime.newInstance(2015,1,1,0,0,0); // set to midnight in case this is empty.
    }
    processStart.Time__c = system.now();
    update lastRun;
    update processStart;
  }
  
  global static void processingCompleted() {
    checkForMissingSettings();
    // update the custom setting 
    Batch_Class_Timestamp__c processComplete = Batch_Class_Timestamp__c.getValues('WalletSyncProcessingComplete');
    processComplete.Time__c = system.now();
    update processComplete;
  }
  
  global static Boolean isProcessingRunning() {
    checkForMissingSettings();
    Batch_Class_Timestamp__c processStart = Batch_Class_Timestamp__c.getValues('WalletSyncProcessingStart');
    Batch_Class_Timestamp__c processComplete = Batch_Class_Timestamp__c.getValues('WalletSyncProcessingComplete');
    
    if (processComplete.Time__c == null && processStart.Time__c != null) return true;
    if (processComplete.Time__c != null) {
      if (processComplete.Time__c < processStart.Time__c) return true;
    }
    return false;
  }
  
  global static Boolean isUploadingRunning() {
    checkForMissingSettings();
    Global_Settings__c gs = Global_Settings__c.getInstance(Constants.GLOBAL_SETTING);
    if (gs.WalletIntegn_CompletionDate__c== null && gs.WalletIntegn_StartDate__c!= null) return true;
    if (gs.WalletIntegn_CompletionDate__c!= null) {
      if (gs.WalletIntegn_CompletionDate__c< gs.WalletIntegn_StartDate__c) return true;
    }
    return false;
  }
  
  

}