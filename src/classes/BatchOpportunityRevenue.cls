/**=====================================================================
 * Experian
 * Name: BatchOpportunityRevenue (Was SumScheduleRevenueCFYandNFY)
 * Description:  The following batch class is designed to be scheduled to run once every night.
                    This class will get all Opportunity line items related to open opps and fill up the FY revenue
                    NOTE: This is scheduled by BatchOpportunityLIScheduleRevenue
 * Created Date: 19 Jan 2016
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Apr 4, 2016                  Paul Kissick                 Case 01928992: Adding support to run on all opps
 =====================================================================*/
public class BatchOpportunityRevenue implements Database.Batchable<sObject>, Database.Stateful  {
  
  public Boolean runAllOpenOpps = false;  // runtime variable to run for all open opps
  
  public Boolean runAllOpps = false;  // runtime variable to run for all opps
  
  private List<String> updateErrors; // Holds all update errors
  
  public Date closeDateStart; // runtime variable to find all opps between close dates
  public Date closeDateEnd;
  
  @testVisible private Date runDate; // Use to set a specific test date
  
  private Boolean customRun = false; // identifies this as a custom run batch (not timed)
  
  private Datetime startTime;  // Holds when the batch started to find all updated schedules after this time
  
  @testVisible private static Map<String,Decimal> revTotalsMapClean = new Map<String,Decimal>{
    'CFY_SR_April__c' => 0, 
    'CFY_SR_May__c' => 0, 
    'CFY_SR_June__c' => 0, 
    'CFY_SR_July__c' => 0, 
    'CFY_SR_August__c' => 0, 
    'CFY_SR_September__c' => 0, 
    'CFY_SR_October__c' => 0, 
    'CFY_SR_November__c' => 0, 
    'CFY_SR_December__c' => 0, 
    'CFY_SR_January__c' => 0, 
    'CFY_SR_February__c' => 0, 
    'CFY_SR_March__c' => 0, 
    'NFY_SR_April__c' => 0, 
    'NFY_SR_May__c' => 0, 
    'NFY_SR_June__c' => 0, 
    'NFY_SR_July__c' => 0, 
    'NFY_SR_August__c' => 0, 
    'NFY_SR_September__c' => 0, 
    'NFY_SR_October__c' => 0, 
    'NFY_SR_November__c' => 0, 
    'NFY_SR_December__c' => 0, 
    'NFY_SR_January__c' => 0, 
    'NFY_SR_February__c' => 0, 
    'NFY_SR_March__c' => 0,
    'First_12_Months_Revenue_Impact__c' => 0
  };
  
  public Database.QueryLocator start (Database.BatchableContext bc) {
    updateErrors = new List<String>();
    
    startTime = system.now();
    
    Datetime lastRunTime = BatchHelper.getBatchClassTimestamp('BatchOpportunityRevenue');
    if (lastRunTime == null) {
      lastRunTime = Datetime.now().addDays(-7);
    }
    
    if (runAllOpenOpps) {
      customRun = true; // Used to identify this as a custom run (not timed)
      return Database.getQueryLocator([
        SELECT Id
        FROM Opportunity
        WHERE IsClosed = false
      ]);
    }
    if (runAllOpps) {
      customRun = true; // Used to identify this as a custom run (not timed)
      return Database.getQueryLocator([
        SELECT Id
        FROM Opportunity
      ]);
    }
    if (closeDateStart != null && closeDateEnd != null) {
      customRun = true; // Used to identify this as a custom run (not timed)
      return Database.getQueryLocator([
        SELECT Id
        FROM Opportunity
        WHERE CloseDate >= :closeDateStart 
        AND CloseDate <= :closeDateEnd
      ]);
    }
    return Database.getQueryLocator([
      SELECT Id
      FROM Opportunity
      WHERE Id IN (
        SELECT OpportunityID FROM OpportunityLineItem WHERE SystemModstamp >= :lastRunTime
      )
    ]);
  }
  
  public void execute (Database.BatchableContext BC, list<Opportunity> scope){
    
    List<Opportunity> oppToUpdate = new List<Opportunity>();
    
    for (Opportunity opp : [SELECT Id, CFY_SR_April__c, CFY_SR_May__c, CFY_SR_June__c,
                                   CFY_SR_July__c, CFY_SR_August__c, CFY_SR_September__c,
                                   CFY_SR_October__c, CFY_SR_November__c, CFY_SR_December__c,
                                   CFY_SR_January__c, CFY_SR_February__c, CFY_SR_March__c,
                                   NFY_SR_April__c, NFY_SR_May__c, NFY_SR_June__c,
                                   NFY_SR_July__c, NFY_SR_August__c, NFY_SR_September__c,
                                   NFY_SR_October__c, NFY_SR_November__c, NFY_SR_December__c,
                                   NFY_SR_January__c, NFY_SR_February__c, NFY_SR_March__c,
                                   First_12_Months_Revenue_Impact__c,
                              (SELECT Id, CFY_SR_April__c, CFY_SR_May__c, CFY_SR_June__c,
                                      CFY_SR_July__c, CFY_SR_August__c, CFY_SR_September__c,
                                      CFY_SR_October__c, CFY_SR_November__c, CFY_SR_December__c,
                                      CFY_SR_January__c, CFY_SR_February__c, CFY_SR_March__c,
                                      NFY_SR_April__c, NFY_SR_May__c, NFY_SR_June__c,
                                      NFY_SR_July__c, NFY_SR_August__c, NFY_SR_September__c,
                                      NFY_SR_October__c, NFY_SR_November__c, NFY_SR_December__c,
                                      NFY_SR_January__c, NFY_SR_February__c, NFY_SR_March__c,
                                      First_12_Months_Revenue_Impact__c
                               FROM OpportunityLineItems)
                             FROM Opportunity
                             WHERE Id IN :scope]) {
      Map<String,Decimal> totalRevMap = new Map<String,Decimal>(revTotalsMapClean);
      
      if (opp.OpportunityLineItems != null && opp.OpportunityLineItems.size() > 0) {
        for (OpportunityLineItem oli : opp.OpportunityLineItems) {
          for(String fieldName : totalRevMap.keySet()) {
            if ((Decimal)oli.get(fieldName) != null) {
              totalRevMap.put(fieldName,totalRevMap.get(fieldName)+(Decimal)oli.get(fieldName));
            }
          }
        }
      }
      // Case 01806431: Moved this to outside the if statement above, to ensure the fields are updated, even to 0.
      for(String fieldName : totalRevMap.keySet()) {
        opp.put(fieldName,totalRevMap.get(fieldName));
      }
        
      oppToUpdate.add(opp);
    }
    
    List<Database.SaveResult> updateResList = Database.update(oppToUpdate,false);
    for(Database.SaveResult sr : updateResList) {
      if(!sr.isSuccess()) {
        for(Database.Error err : sr.getErrors()) {
          updateErrors.add(err.getMessage());
        }
      }
    }
    
  }
  
  public void finish (Database.BatchableContext BC){
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchOpportunityRevenue', false);
    
    if (!customRun) {
      BatchHelper.setBatchClassTimestamp('BatchOpportunityRevenue', startTime);
    }
    
    String emailBody = '';
    if (updateErrors != null && updateErrors.size() > 0) {
      bh.batchHasErrors = true;
      emailBody += String.join(updateErrors,'\n');
      bh.emailBody += emailBody;
    }
    
    bh.sendEmail();
  }

}