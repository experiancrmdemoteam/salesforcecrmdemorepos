/*=============================================================================
 * Experian
 * Name: MembershipOnboardingToolExt_Test
 * Description: 
 * Created Date: 25 Jul 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

@isTest
private class MembershipOnboardingToolExt_Test {
  
  @isTest static void testNewMembership() {
    
    Account tstAcc = Test_Utils.insertAccount();
    Membership__c memb = Test_Utils.insertMembership(true, tstAcc.Id);
    
    memb = [
      SELECT Selected_Client_Industry__c, Selected_Product_to_be_Sold__c, Selected_Permissible_Purpose__c,
        Selected_Third_Party_Involvement__c, Onboarding_Documents_JSON__c, Onboarding_Documents_Complete__c,
        Onboarding_Documents_Required__c, Onboarding_Bypass__c
      FROM Membership__c
      WHERE Id = :memb.Id
    ];
    
    MembershipOnboardingToolExt obt = new MembershipOnboardingToolExt(new ApexPages.StandardController(memb));
    obt.loadPage();
    
    system.assertEquals(true, [SELECT Onboarding_Documents_Required__c FROM Membership__c WHERE Id =:memb.Id].Onboarding_Documents_Required__c);
    
    
    
  }

  static testMethod void testDocs() {
    
    MembershipOnboardingToolMgmt obt = new MembershipOnboardingToolMgmt();
    
    List<Membership_Onboarding_Document__c> testDocs = obt.getAllDocuments();
    
    Integer origDocsSize = testDocs.size();
    system.assertNotEquals(0, testDocs.size());
    
    system.assertNotEquals(4, obt.getAllLookupOptions().size());
    
    obt.newDoc = new Membership_Onboarding_Document__c();
    obt.addNewDocument();
    
    obt.newDoc.Full_Detail__c = 'This is a supporting document'; 
    obt.newDoc.Type_of_Document__c = 'Supporting Document';
    obt.newDoc.Lookup_Type__c = 'Client Industry';
    obt.newDoc.Lookup_Value__c = obt.getProductToBeSold()[3].getValue();
    obt.addNewDocument();
    
    testDocs = obt.getAllDocuments();
    Integer newDocsSize = testDocs.size();
    system.assertNotEquals(newDocsSize, origDocsSize, 'There should be 1 more doc');
    
    Id tmpId = testDocs.get(newDocsSize-1).Id;
    
    obt.docId = tmpId;
    obt.editDocument();
    
    system.assertEquals(obt.newDoc.Id, tmpId);
    
    obt.cloneDocument();
    system.assertEquals(obt.newDoc.Id, null);
    
    obt.docId = tmpId;
    obt.deleteDocument();
    
    testDocs = obt.getAllDocuments();
    newDocsSize = testDocs.size();
    system.assertEquals(newDocsSize, origDocsSize, 'Docs should be back to original size');

  }
  
  static testMethod void testOpts() {
    
    MembershipOnboardingToolMgmt obt = new MembershipOnboardingToolMgmt();
    
    List<MembershipOnboardingToolMgmt.optionWrapper> testOpts = obt.getAllOptions();
    
    Integer origObtSize = testOpts.size();
    
    system.assertNotEquals(0, testOpts.size());
    
    obt.addNewOptions();
    
    system.assertEquals(origObtSize, obt.getAllOptions().size());
    
    obt.selectedIndustries.add(obt.getClientIndustry()[1].getValue());
    obt.selectedProducts.add(obt.getProductToBeSold()[2].getValue());
    obt.selectedPurposes.add(obt.getPermissiblePurpose()[3].getValue());
    obt.selectedThirdParties.add(obt.getThirdParty()[1].getValue());
    
    obt.addNewOptions();
    
    // A new option should have been added now.
    system.assertEquals(origObtSize+1, obt.getAllOptions().size());
    
    // Adding again should through an exception regarding duplicate...
    obt.addNewOptions();
    
    // There should be no new options...
    system.assertEquals(origObtSize+1, obt.getAllOptions().size());
    
    testOpts = obt.getAllOptions();
    
    obt.optionId = testOpts[testOpts.size()-1].getAnyId(); // get an id
    obt.editOptions();
    // Apply 4 third party options
    obt.selectedPurposes.add(obt.getPermissiblePurpose()[5].getValue());
    obt.selectedPurposes.add(obt.getPermissiblePurpose()[6].getValue());
    obt.selectedPurposes.add(obt.getPermissiblePurpose()[7].getValue());
    
    obt.addNewOptions();
    
    testOpts = obt.getAllOptions();
    // system.assertNotEquals(origObtSize+1, testOpts.size());
    
    obt.optionId = testOpts[testOpts.size()-1].getAnyId(); // get an id
    obt.deleteOption();
    
    system.assertEquals(testOpts.size()-1, obt.getAllOptions().size());
    
    
  }
  
  @testSetup
  private static void setupData() {
    // Initialise data for test here.
    
    
    List<Membership_Onboarding_Allowed_Option__c> allOpts = new List<Membership_Onboarding_Allowed_Option__c>();
    List<Membership_Onboarding_Document__c> allDocs = new List<Membership_Onboarding_Document__c>();
    
    List<SelectOption> allInds = MembershipOnboardingToolUtils.getClientIndustry();
    List<SelectOption> allPurps = MembershipOnboardingToolUtils.getPermissiblePurpose(new Set<String>(), false);
    List<SelectOption> allProds = MembershipOnboardingToolUtils.getProductToBeSold(new Set<String>(), false);
    List<SelectOption> allThirds = MembershipOnboardingToolUtils.getThirdParty(new Set<String>(), false);
    
    for (Integer a = 1; a <= 4; a++) {
      
      Membership_Onboarding_Allowed_Option__c newOpt = new Membership_Onboarding_Allowed_Option__c(
        Client_Industry__c = allInds[a].getValue(),
        Product_to_be_Sold__c = allProds[a].getValue(),
        Permissible_Purpose__c = allPurps[a].getValue(),
        Third_Party_Involvement__c = allThirds[1].getValue()
      );
      allOpts.add(newOpt);
    }
    
    insert allOpts;
    
    for (Integer a = 1; a <= 4; a++) {
      Membership_Onboarding_Document__c newDoc = new Membership_Onboarding_Document__c(
        Full_Detail__c = 'Here is the detail', 
        Type_of_Document__c = 'Client Document',
        Lookup_Type__c = 'Client Industry', 
        Lookup_Value__c = allInds[a].getValue()
      );
      allDocs.add(newDoc);
    }
    
    insert allDocs;
    
  }
  
}