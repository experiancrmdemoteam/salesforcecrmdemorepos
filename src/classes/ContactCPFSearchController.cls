/**=====================================================================
 * Appirio, Inc
 * Name: ContactCPFSearchController
 * Description: 
 * Created Date: 
 * Created By: Noopur (Appirio)
 * 
 * Date Modified      Modified By                  Description of the update
 * Aug 25th, 2015     Arpita Bose                  Updated to populate Account & removed reference of Serasa Profiles
 * Sep 28th, 2015     Arpita Bose                  I-182126: Updated to fully cleared and not display create button
 * Sep 30th, 2015     Arpita Bose                  I-182686: Updated
 * Oct 26th, 2015     Paul Kissick                 Case 01209498: Changes for Mainframe Integration
 * Mar 21st, 2017     Ryan (Weijie) Hu             W-007090: Record recordType id, if it is consumer contact, put special url parameters
 *                                                 before redirection
 * May 30th, 2017     Charlie Park                 I-1105: Add Mother's maiden name to pagereference.
 * June 1st, 2017     Ryan (Weijie) Hu             I1124: Experian Serasa Customer Care profiles users shoule not allow to create new contacts from blank
*  =====================================================================*/
public class ContactCPFSearchController {
  public Id contactId {get;set;}
  public String accountId {get;set;}
  public String accName {get;set;}
  public static Contact con {get; set;}
  public static List<Contact> contacts;
  public Boolean isSearchresult;
  public Boolean hasAccess {get;set;}
  public Boolean isEdit {get;set;} 
  public Boolean isMainframe {get;set;}
  public Boolean isConsumerContact {get;set;}
  public Boolean isSerasaCustomerCareUser {get;set;}
  
  public SerasaMainframeWSRequest.webserviceResponse serviceResults {get;set;}
  
  // get 2nd block display 
  public Boolean shouldDisplay {
    get {
      System.Debug(isSearchresult);
      return isSearchresult;
      }
  }  
  
  // CNPJNum property 
  public string cpfNum { 
    get {
      if (cpfNum == null)
        cpfNum = '';
        return cpfNum;
      }
    set;
  }
  
  // SearchResults Property
  public List<Contact> searchResults {
    get {
      if (searchResults == null)
        searchResults = new List<Contact>();
        return searchResults;
      } 
    set;
  }
  
  // Constructor
  public ContactCPFSearchController(ApexPages.StandardController controller){
    hasAccess = false;
    isMainframe = false;
    con = (Contact) controller.getRecord();
    isSearchresult = false;
    isConsumerContact = false;
    isSerasaCustomerCareUser = false;
    if (ApexPages.currentPage().getParameters().containsKey('accId')) {
      accountId = ApexPages.currentPage().getParameters().get('accId');
    }
    else {
      accountId = null;
    }
    serviceResults = new SerasaMainframeWSRequest.webserviceResponse();
    accName = ApexPages.currentPage().getParameters().get('accName');
    isEdit = false;
    serviceResults = new SerasaMainframeWSRequest.webserviceResponse();
  }
  
  // Method to search accounts on the basis of CNPJ Number   
  public void searchContacts() {
    
    if (!cpfNum.isNumeric() || cpfNum.length() != 11) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Invalid_CPF_Number));
      return;
    }
    if (!SerasaUtilities.validateCPFNumber(cpfNum)) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Invalid_CPF_Checksum_Error_Message));
      return;
    }
    
    // Now we can search, first in SF...
    isMainframe = false;
    searchResults.clear();
    contacts = new List<Contact>();

    // I1124
    Set<String> validSerasaCustomerCareProfiles = new Set<String>();
    Serasa_User_Profiles__c serasaCustomerCareProfiles = Serasa_User_Profiles__c.getall().get(Constants.SERASA_CUSTOMER_CARE_PROFILES);
    if(serasaCustomerCareProfiles != null && serasaCustomerCareProfiles.Profiles__c != null){
      for(String profileName : serasaCustomerCareProfiles.Profiles__c.split(',')) {

        // Adding the profile name and the 'Experian Serasa ' prefix back to the profile name to the set
        validSerasaCustomerCareProfiles.add('Experian Serasa ' + profileName.trim());
      }
    }

    User usrRec = [SELECT Profile.Name FROM User WHERE Id= :UserInfo.getUserId()];

    if (validSerasaCustomerCareProfiles.contains(usrRec.Profile.Name)) {
      isSerasaCustomerCareUser = true;
      String recordTypeId;
      if (ApexPages.currentPage().getParameters().containsKey('RecordType')) {
        recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');

        RecordType consumer_contact_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Consumer_Contact'];
        if (consumer_contact_rt.Id == Id.valueOf(recordTypeId)) {
          isConsumerContact = true;
        }
      }

      if (recordTypeId == null || recordTypeId == '') {
        RecordType consumer_contact_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'LATAM_Serasa'];
        recordTypeId = consumer_contact_rt.Id;
      }

      contacts = [SELECT Id, LastName, FirstName, CPF__c, Owner.Name, AccountId, Account.Name,
                  (SELECT Id, Name, Contact__c, Address__c, Address_Type__c, Primary_Billing__c, Primary_Shipping__c, 
                      Contact_Address1__c, Contact_CPF_Number__c, Address_Unique_Id__c FROM Contact_Addresses__r) 
                  FROM Contact 
                  WHERE CPF__c = : cpfNum AND RecordTypeId =: recordTypeId];
    }
    else {
      isSerasaCustomerCareUser = false;
      contacts = [SELECT Id, LastName, FirstName, CPF__c, Owner.Name, AccountId, Account.Name,
                  (SELECT Id, Name, Contact__c, Address__c, Address_Type__c, Primary_Billing__c, Primary_Shipping__c, 
                      Contact_Address1__c, Contact_CPF_Number__c, Address_Unique_Id__c FROM Contact_Addresses__r) 
                  FROM Contact 
                  WHERE CPF__c = : cpfNum];
    }

    //Populating hasAccess map to check further access.
	  for (UserRecordAccess userAccess  : [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :accountId]) {
	    system.debug(userAccess.HasEditAccess);
	    if (userAccess.HasEditAccess == true) {
	      hasAccess = true; 
	    }
	  }
    if (contacts.size() > 0) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.New_Contact_Detail_Message));
      isSearchresult = true;
      isEdit = true; 
	    searchResults = contacts;
    }
    else {
      isSearchresult = true;
      isEdit = true; 
      serviceResults = SerasaMainframeWSRequest.CallCPFCheckWS(cpfNum);
      
      if (serviceResults.offline) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.New_Contact_Create_Message_Offline));
      }
      else if (serviceResults.success) {
        isMainframe = true;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.New_Contact_Create_Message));
      }
      else {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.Contact_CPF_Search_Message+': '+serviceResults.errorMsg));
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.Contact_CPF_Search_Not_on_Mainframe));
      }
    }

  }
  
  //
  // handle the action of the commandButton
  public PageReference processButtonClick() {
    String url;

    String recordTypeId;
    if (ApexPages.currentPage().getParameters().containsKey('RecordType')) {
      recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
    }

    RecordType consumer_contact_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Consumer_Contact'];

    if (accountId != null && recordTypeId != null && (consumer_contact_rt.Id == Id.valueOf(recordTypeId))) {
      url = '/apex/CSS_AddOrCreateNewContactAddress?accId='+accountId+'&RecordType='+recordTypeId+'&cpfNum='+cpfNum;
      isConsumerContact = true;
    }
    else if (accountId != null && accName != null) {
      url = '/apex/CSS_AddOrCreateNewContactAddress?accId='+accountId+'&cpfNum='+cpfNum; 
    }
    else {
      url = '/apex/CSS_AddOrCreateNewContactAddress?cpfNum='+cpfNum;
    }
    PageReference pageRef = new PageReference(url);
    pageRef.setRedirect(true);
    return pageRef;
  }
  
  public PageReference createFromServiceClick() {
    // String url;
    PageReference pageRef = Page.CSS_AddOrCreateNewContactAddress;
    
    SerasaMainframeWSRequest.cpfResult res = serviceResults.cpfResults[0];
    
    pageRef.getParameters().put('cpfNum',res.CPFNumber);
    pageRef.getParameters().put('conFName',res.ContactName.substringBefore(' '));
    pageRef.getParameters().put('conLName',res.ContactName.substringAfter(' '));
    pageRef.getParameters().put('dateOfBirth',JSON.serialize(res.getDateofBirthDate()));
    pageRef.getParameters().put('mothersMaidenName', res.MothersMaidenName);
    
    String recordTypeId;
    if (ApexPages.currentPage().getParameters().containsKey('RecordType')) {
      recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
    }

    RecordType consumer_contact_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Consumer_Contact'];

    if (recordTypeId != null && (consumer_contact_rt.Id == Id.valueOf(recordTypeId))) {
      pageRef.getParameters().put('RecordType', recordTypeId);
    }

    if (accountId != null && accName != null) {
      pageRef.getParameters().put('accId',accountId);
      //url = '/apex/CSS_AddOrCreateNewContactAddress?accId='+accountId+'&cpfNum='+cpfNum; 
    }
    else {
      //url = '/apex/CSS_AddOrCreateNewContactAddress?cpfNum='+cpfNum;
    }
    // PageReference pageRef = new PageReference(url);
    pageRef.setRedirect(true);
    return pageRef;
  }
     
  // Method to clear search results
  public PageReference resetResults() {
     isEdit = false;
     isSearchresult = false;
     isMainframe = false;
     searchResults.clear();
     serviceResults = new SerasaMainframeWSRequest.webserviceResponse();
     PageReference pg = new PageReference('/apex/ContactCPFSearch');
     pg.setRedirect(false);
     return pg;
   }   
    
}