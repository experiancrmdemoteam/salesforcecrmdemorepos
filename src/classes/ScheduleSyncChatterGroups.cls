/**=====================================================================
 * Experian
 * Name: BatchSyncChatterGroups 
 * Description: Scheduler class to assign chatter groups to user based on Oracle Region value
 * Created Date: 17th, July 2017
 * Created By: Mauricio Murillo
 *
 * Date Modified       Modified By          Description of the update
 =====================================================================*/
global class ScheduleSyncChatterGroups implements Schedulable {

    global void execute(SchedulableContext sc) {
    
        System.scheduleBatch(
            new BatchSyncChatterGroups(),
            'BatchSyncChatterGroups'+String.valueOf(Datetime.now().getTime()),
            0,
            ScopeSizeUtility.getScopeSizeForClass('BatchSyncChatterGroups')
          );
    
    }
}