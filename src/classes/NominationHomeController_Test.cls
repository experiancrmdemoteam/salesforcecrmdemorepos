/**=====================================================================
 * Experian
 * Name: NominationHomeController_Test
 * Description: Test class to cover  NominationHomeController
 * Created Date: 
 * Created By: 
 *
 * Date Modified      Modified By                Description of the update

 =====================================================================*/
@isTest
private class NominationHomeController_Test {
    @isTest
    private static void testNominationHistory() {
        Test.startTest();
        
            NominationHomeController objHomeContr = new NominationHomeController();
            
            objHomeContr.initialisePage();
            objHomeContr.navToNewRewardPage();
            objHomeContr.navToManagerDashboard();
            objHomeContr.navToRecognitionCoordinatorDashboard();
            objHomeContr.getRecentNominations();
            
            string strQuery = 'select id,Name from Nomination__c';
            string strQuery2 = 'select id from WorkBadge';
            NominationHomeController.SectionPaging objS = new NominationHomeController.SectionPaging(strQuery);
            
            List<Nomination__c> lstNom = objS.getNomRecords();
                                    
            List<Integer> lstPg = objS.getPageList();
            objS.gotoPage();
            objS.getCurrentPage();
            objS.getHasNext();
            objS.getHasPrevious();
            objS.getNext();
            objS.getPrevious();
            
            NominationHomeController.SectionPaging objSWB = new NominationHomeController.SectionPaging(strQuery2,2);
            List<WorkBadge> lstBadge = objSWB.getBadgeRecords();
            
        Test.stopTest();
    }
    
    @testSetup
  private static void setupData() {
    
    NominationTestHelper_Test.createTestUsers();
    
    NominationTestHelper_Test.createRecognitionBadges();
    
    system.runAs(NominationTestHelper_Test.testUsers.get(NominationTestHelper_Test.dataAdminEmail)) {
      
      NominationTestHelper_Test.createNominations(
        NominationHelper.NomConstants.get('PendingApproval'), 
        NominationHelper.NomConstants.get('Level2SpotAward'), 
        1,
        false
      );
      
      NominationTestHelper_Test.createNominations(
        NominationHelper.NomConstants.get('PendingApproval'), 
        NominationHelper.NomConstants.get('Level3Individual'), 
        1,
        false
      );
      
      NominationTestHelper_Test.createNominations(
        NominationHelper.NomConstants.get('Submitted'), 
        NominationHelper.NomConstants.get('Level3Team'), 
        4,
        true
      );
      
      // Fix the team nomination master to pending approval.
      Nomination__c fixTeamNom = [SELECT Id, Status__c FROM Nomination__c WHERE Type__c = :NominationHelper.NomConstants.get('Level3Team') AND Master_Nomination__c = null AND Status__c = :NominationHelper.NomConstants.get('Submitted')];
      fixTeamNom.Status__c = NominationHelper.NomConstants.get('PendingApproval');
      update fixTeamNom;
      
    }
    
  }
}