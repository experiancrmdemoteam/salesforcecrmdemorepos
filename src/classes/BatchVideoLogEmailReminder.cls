/**=============================================================================
* Experian plc
* Name: BatchVideoLogEmailReminder - used in a scheduler [SchedulerVideoLogEmailReminder]
* Description: Case #02069865 Custom object for Training management console
* Created Date: March 24th, 2017
* Created By: Sanket Vaidya
* 
* Date Modified        Modified By                  Description of the update
* 
=============================================================================*/
public class BatchVideoLogEmailReminder implements Database.Batchable<sObject>
{
    public Database.QueryLocator start(Database.BatchableContext bc)
    {
          return Database.getQueryLocator([SELECT Id, Name, Video_Review_Date__c FROM Video_Log__c WHERE Video_Review_Date__c = TODAY]);        
    }
    
    public void execute(Database.BatchableContext bc, List<Video_Log__c> videoLogList)
    {
        String teamEmail = Custom_Fields_Ids__c.getInstance().BatchTrainingReminder_TrainingTeamEmail__c;
        System.Debug('Team Email : ' + teamEmail);

        SendEmail(videoLogList, teamEmail);
    }
    
    public void finish(Database.BatchableContext bc)
    {          
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();        
        mail.setToAddresses(new String[] {UserInfo.getUserEmail()});                
        mail.setSubject('Batch Process Completed');
        mail.setPlainTextBody('Batch Process : \"BatchVideoLogEmailReminder\" has completed');        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public void SendEmail(List<Video_Log__c> videoLogList, string teamEmail)
    {
        if(String.isEmpty(teamEmail))
        {         
         	System.debug('No team email address found.');
            return;
        }
        
        // First, reserve email capacity for the current Apex transaction to ensure
        // that we won't exceed our daily email limits when sending email after
        // the current transaction is committed.
        Messaging.reserveSingleEmailCapacity(2);
       
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setToAddresses(new String[]{teamEmail});
       
        mail.setSubject('This is a Video Log reminder.');        
        mail.setBccSender(false);        
        mail.setUseSignature(true);

        String emailBodyText = '';

        Integer counter = 1;
        emailBodyText = '<table><tr>' 
                                + '<th>-</th>'
                                + '<th>Video Log Name</th>'                                
                               + '</tr>';
        for(Video_Log__c vl : videoLogList)
        {
            emailBodyText += '<tr>' +
                                '<td><strong>' + counter++ + '.</strong></td>' + 
                                '<td>' + 
                                    '<a href=\"' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + vl.Id + '\"> ' + vl.Name + '</a>' +
                                '</td>' + 
                            '</tr>';
        }

        emailBodyText += '</table>';

        // Specify the text content of the email.
        mail.setHTMLBody('Reminder for Video Log records. <br/><br/>' +
                          '<strong>Their \"Review Date\" is today.<br/>Please review the video and update the date.</strong><br/>' +
                          '<br/><br/>' + 
                                emailBodyText );


        // Send the email you have created.
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}