/******************************************************************************
 * Name: expcommCaseExt.cls
 * Created Date: 2/16/2017
 * Created By: Hay Win
 * Description : Controller for expcomm_caseCreate for new Exp Community
 *               to create Platform support or new user cases
 * Change Log- 
 ****************************************************************************/
 
public class expcommCaseExt{

    public Case newCase {get;set;}
    private Final Case origCase {get;set;}
    public ID newUserRT {get;set;}
    public ID platformRecord {get;set;}
    private Case tempCase{get;set;}
    public String selectedType {get;set;}
    public boolean isPlatform {get;set;}
    
    public List<SelectOption> getCaseTypes() {
        List<SelectOption> caseOptions = new List<SelectOption>();
        caseOptions.add(new SelectOption('Platform Support','Platform Support'));
        caseOptions.add(new SelectOption('New User','New User'));
        
        return caseOptions;
    }
    
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public expcommCaseExt(ApexPages.StandardController stdController) {
        isPlatform = true;
        User curUser = [Select id, Phone from User where id =: UserInfo.getUserId()];
        String caseID = ApexPages.currentPage().getParameters().get('id');
        String reason = ApexPages.currentPage().getParameters().get('reason');
        
        newUserRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('New User').getRecordTypeId();
        platformRecord = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Platform Support').getRecordTypeId();
        //System.debug('path1 ' + Site.getPathPrefix());
        if (caseID != null) {
            this.origCase = (Case)stdController.getRecord();
            List<Case> newCaseList = [Select Id, CaseNumber, User_Requestor__c, Origin, Requestor_Email__c, Requestor_Work_Phone__c, Requester_BU__c, Priority, Reason, Status, Subject, Description, Business_Impact__c, User_Impact__c from Case where id =: this.origCase.ID];
            this.newCase= newCaseList[0].clone(false);
            System.debug('this.origCase.ID '+ this.origCase.ID);
            newCase.status ='Open';
            
        } else {
            
             
            this.newCase = (Case)stdController.getRecord();
            newCase.type = 'GCSS Support';  
            newCase.status ='Open';
            newCase.RecordTypeID = platformRecord;
            
            if(newCase.RecordTypeID == platformRecord) {
                isPlatform = true;
            } else {
                isPlatform = false;
            }
        }
        
        if (reason == 'knowledge') {
            newCase.Reason = 'Knowledge';
            newCase.Secondary_Case_Reason__c = 'Other';
        }
        
        newCase.User_Requestor__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
        newCase.Requestor_Email__c = UserInfo.getUserEmail();
        newCase.Priority = 'Medium';
        newCase.Requestor_Work_Phone__c = curUser.Phone;
            newCase.Origin = 'Community';
        System.debug('new CAse ' +  newCase);
    }
    
    public pagereference Save() {    
    System.debug('new Case ' + newCase);
        insert newCase;
        PageReference pg = new PageReference(Site.getPathPrefix()+'/apex/expcomm_viewCase?id=' + newCase.id);
        pg.setredirect(true);
        return pg;
    }
    
    public pagereference Cancel() {
        PageReference returnPage = new PageReference(Site.getPathPrefix()+'/apex/expcomm_newCommunity');
        return returnPage;
    }
    
    public void changeRType() {
       
        /*    
        if (newCase.RecordTypeID == platformRecord) {           
            newCase.RecordTypeID = newUserRT;
        System.debug('new user');
        } else {
            newCase.RecordTypeID = platformRecord;
        System.debug('platform');
        }*/
        
        if (selectedType == 'New User') {           
            newCase.RecordTypeID = newUserRT;
            isPlatform= false;
        } else if(selectedType == 'Platform Support'){
            newCase.RecordTypeID = platformRecord;
            isPlatform =true;
        }
    }
}