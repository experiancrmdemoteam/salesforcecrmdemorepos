/**=====================================================================
 * Experian
 * Name: BatchContactRelationshipBULead
 * Description: The following batch class is designed to be scheduled to run every day.
                    This class will get all Contacts and related Contact Relationships and set the BULead when no BULead has been assigned yet
 * Created Date: 07/22/2015
 * Created By: Diego Olarte (Experian)
 * 
 * Date Modified      Modified By        Description of the update
 * 28/07/2016(QA)     Diego Olarte       CRM2:W-005406: Contact relationship BU Lead
 =====================================================================*/
global class BatchContactRelationshipBULead implements Database.Batchable<sObject> {

  global Database.Querylocator start ( Database.BatchableContext bc ) {
    
    String query = 'Select Id, Primary_User__c, Business_Unit__c, Contact__c, Custom_Record_Creation_ID__c,CreatedDate  FROM Contact_team__c WHERE Contact__c != null AND CreatedDate = LAST_N_DAYS:2';
   
    return Database.getQueryLocator (query);
  }

  global void execute (Database.BatchableContext bc, List<Contact_team__c> contactRelationships) {
    
    //Set variables to allocate data
    
    Set<String> buLeadSet = new Set<String>();
    Set<String> businessUnitSet = new Set<String>();
    Set<Id> nonBULContactIdSet = new Set<Id>();
    Set<Id> contactIdSet = new Set<Id>();
    
    // Gets the BU and related contact for the Contact Relationship that will be later be used to filter existing records
    
     for (Contact_team__c newContactTeam : contactRelationships) {
      
      if (newContactTeam.Business_Unit__c != null) {
        businessUnitSet.add(newContactTeam.Business_Unit__c);
      }
      contactIdSet.add(newContactTeam.Contact__c);            
    }
    
    List<Contact_Team__c> buLeads = [
      SELECT Id, Primary_User__c, Business_Unit__c, Contact__c, Relationship_Owner__c
      FROM Contact_Team__c
      WHERE Business_Unit__c IN :businessUnitSet
      AND Contact__c IN :contactIdSet
      AND Contact__c != null      
      AND Primary_User__c = true
    ];
    
    for (Contact_team__c oldContactTeam : buLeads) {
    
    buLeadSet.add(oldContactTeam.Business_Unit__c);
    nonBULContactIdSet.add(oldContactTeam.Contact__c);
    
    }
    
    system.debug('Value for businessUnitSet:' + businessUnitSet);
    system.debug('Value for contactIdSet:' + contactIdSet);

    //List of Existing BU Leads on Contact
        
    List<Contact_Team__c> ctToUpdate = [
      SELECT Id, Primary_User__c, Business_Unit__c, Contact__c, Relationship_Owner__c
      FROM Contact_Team__c
      WHERE Business_Unit__c IN :businessUnitSet
      AND Business_Unit__c NOT IN :buLeadSet
      AND Contact__c IN :contactIdSet
      AND Contact__c NOT IN :nonBULContactIdSet
      AND Contact__c != null     
      order by Custom_Record_Creation_ID__c desc limit 1
    ];

    if (!ctToUpdate.isEmpty()) {
      for (Contact_Team__c ct : ctToUpdate) {
        ct.Primary_User__c = true;
      }
      try {
        update ctToUpdate;
      }
      catch (DMLException ex) {
        ApexLogHandler.createLogAndSave('ContactTeamTriggerHandler','setUniqueBULead', ex.getStackTraceString(), ex);
      }
    }    
  }  

    //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {

    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchContactRelationshipBULead', true);
            
  }
    
}