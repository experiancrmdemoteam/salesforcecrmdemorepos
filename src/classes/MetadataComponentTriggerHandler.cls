/**=====================================================================
 * Name: MetadataComponentTriggerHandler
 * Trigger Name: MetadataComponentTrigger  
 * Description: TriggerHandler for Metadata Component trigger
 * Created Date: June 25th, 2015
 * Created By: Nur Azlini
 * 
 * Date Modified       Modified By                  Description of the update
 * Jul 23rd, 2015      Paul Kissick                 Fixed trigger failure due to updating records in before update trigger. Removed lines not being used to improve coverage.
 =====================================================================*/
public class MetadataComponentTriggerHandler{

  //===========================================================================
  //Before Insert
  //===========================================================================
	public static void beforeInsert(List<Metadata_Component__c> newList) {
		updtComponentLabelField(newList, null);
	}
	
  //===========================================================================
  //Before Update
  //===========================================================================	
	public static void beforeUpdate(Map<ID, Metadata_Component__c> newMap, Map<ID, Metadata_Component__c> oldMap){
		updtComponentLabelField(newMap.Values(), oldMap);
	}
	
  //===========================================================================
  //Method to Update Component Label to be unique
  // PK: Updated to prevent the 'Update' as it's in a before trigger and isn't needed
  //===========================================================================
	public static void updtComponentLabelField(List<Metadata_Component__c> newList, Map<Id,Metadata_Component__c> oldMap){
			for(Metadata_Component__c md : newList){
                md.Name = md.Metadata_Member__c.mid(0,80); // PK : Name is a max of 80 chars.
	}
			
				
	}
	
}