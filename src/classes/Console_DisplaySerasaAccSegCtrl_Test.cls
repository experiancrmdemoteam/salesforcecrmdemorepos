/**=====================================================================
 * Appirio, Inc
 * Name: Console_DisplaySerasaAccSegCtrl_Test
 * Description: Test class for Console_DisplaySerasaAccSegController
 * Created Date: Dec 10th, 2015
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By                Description of the update
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
 
@isTest
public with sharing class Console_DisplaySerasaAccSegCtrl_Test {
  
  static String testEmail = 'sda1235dafk@experian.com';
  
  static testMethod void controllerTest() {
    

    Test.startTest();
    
    User tstUser = [SELECT Id FROM User WHERE Email = :testEmail];
    
    system.runAs(tstUser) {
      
      Account account1 = [SELECT Id FROM Account WHERE Name = 'TestAccount1234' LIMIT 1];
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(account1);
    
	    Console_DisplaySerasaAccSegController controller = new Console_DisplaySerasaAccSegController(sc);
	
	    System.assertNotEquals(null, controller.fieldSet);
	    System.assertEquals(false, controller.isEdit);
	
	    controller.edit();
	    System.assertEquals(true, controller.isEdit);
	
	    controller.cancel();
	    System.assertEquals(false, controller.isEdit);
	
	    controller.accSeg.Total_Won__c = 15;
	    controller.save();
	    System.assertEquals(false, controller.isEdit);
	    
	    controller.edit();    
	    controller.accSeg.AHS_Health_Status__c = 'Yellow';
	    controller.accSeg.AHS_Include_in_Status_Report__c = true;
	    controller.save();
	    
	    // Enforce validation rules to fail and assert for page messages
	    controller.edit(); 
	    controller.accSeg.AHS_Include_in_Status_Report__c = false;
	    controller.save();
	    system.assert(ApexPages.getMessages().size() > 0);
	    
	    Test.stopTest();
    }
  }

  @testSetup 
  static void createTestData() {
    
    User nUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    nUser.Global_Business_Line__c = 'My GBL';
    nUser.Business_Line__c = 'My BL';
    nUser.Business_Unit__c = 'My BU';
    nUser.Country__c = 'Brazil';
    nUser.Email = testEmail;
    insert nUser;
    
    system.runAs(nUser) {
      Account testAcc1 = Test_Utils.createAccount();
      testAcc1.Name = 'TestAccount1234';
      insert testAcc1;
    }
    
    Account testAcc = [SELECT Id FROM Account WHERE Name = 'TestAccount1234' LIMIT 1];

    Hierarchy__c countryHierarchy = new Hierarchy__c();
    countryHierarchy.Type__c = 'Country';
    countryHierarchy.Value__c = 'Brazil';
    countryHierarchy.Unique_Key__c = 'Brazil';
    insert countryHierarchy;

    Hierarchy__c grandParentHierarchy = new Hierarchy__c();
    grandParentHierarchy.Type__c = 'Global Business Line';
    grandParentHierarchy.Value__c = 'My GBL';
    grandParentHierarchy.Unique_Key__c = 'My GBL';
    insert grandParentHierarchy;

    Account_Segment__c segment = Test_Utils.insertAccountSegment(false, testAcc.Id, grandParentHierarchy.Id, null);
    insert segment;

    Account_Segment__c cSegment = Test_Utils.insertAccountSegment(false, testAcc.Id, countryHierarchy.Id, null);
    insert cSegment;

    Account_Segmentation_Mapping__c mapping = new Account_Segmentation_Mapping__c();
    mapping.Name = 'Serasa';
    mapping.Global_Business_Line__c = 'My GBL';
    mapping.Common_View_Name__c = 'LATAM';
    mapping.Field_Set_API_Name__c = 'LATAM_Serasa';
    insert mapping;
    
  }

}