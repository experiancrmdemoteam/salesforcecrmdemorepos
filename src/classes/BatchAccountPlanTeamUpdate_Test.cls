/**=====================================================================
 * Experian, Inc
 * Name: BatchAccountPlanTeamUpdate_Test
 * Description: 
 *
 * Created Date: Unknown
 * Created By: Tyaga Pati (Experian)
 *
 * Date Modified            Modified By                 Description of the update
 * 19th May 2017            James Wills                 Case #02417328: Updated to reflect changes to BatchAccountPlanTeamUpdate class.
 * 22nd May 2017            James Wills                 Case #02420919: Updated test class.
 ======================================================================*/
@isTest
private class BatchAccountPlanTeamUpdate_Test{

    
    static testmethod void accountPlanTeamInsert_Test() {

      List<Account_Plan_Team__c> apt_List = [SELECT id FROM Account_Plan_Team__c];
      Integer aptListSizeBeforeDelete = apt_List.size();
      delete apt_List[0];
                             
      Test.startTest();
        //Execution of the batch
        BatchAccountPlanTeamUpdate batu = new BatchAccountPlanTeamUpdate();
        batu.execute((Database.BatchableContext) null, [SELECT id, (SELECT UserId, User.isActive, TeamMemberRole, AccountId, isDeleted FROM AccountTeamMembers)
                                                        FROM Account
                                                        WHERE Id IN (SELECT Account__c FROM Account_Plan__c WHERE Account__c <> NULL)
                                                       ]);
      //Stop test
      Test.StopTest();
        
      System.assert([SELECT id FROM Account_Plan_Team__c].size()!= aptListSizeBeforeDelete,'BatchAccountPlanTeamUpdate_Test.accountPlanTeamInsert_Test: Expected Account_Plan_Team__c record not inserted.');           
      
    }
    
    static testmethod void accountPlanTeamUpdate_Test() {
                
       AccountTeamMember atm = [SELECT id, TeamMemberRole FROM AccountTeamMember LIMIT 1];
       
       atm.TeamMemberRole = 'Test';                       
       update atm;
        
       Test.startTest();
         //Execution of the batch
          BatchAccountPlanTeamUpdate batu = new BatchAccountPlanTeamUpdate();
          batu.execute((Database.BatchableContext) null, [SELECT id, (SELECT UserId, User.isActive, TeamMemberRole, AccountId, isDeleted FROM AccountTeamMembers)
                                                          FROM Account
                                                          WHERE Id IN (SELECT Account__c FROM Account_Plan__c WHERE Account__c <> NULL)
                                                         ]);
       //Stop test
       Test.StopTest();
       
       System.assert([SELECT id FROM Account_Plan_Team__c WHERE Account_Team_Role__c='Test'].size()>0,'BatchAccountPlanTeamUpdate_Test.accountPlanTeamUpdate_Test: Expected Account_Plan_Team__c record not updated.');           
 
        
    }
    
    static testmethod void accountPlanTeamDelete_Test() { 
        
      List<Account_Plan_Team__c> apt_List = [SELECT Account_Plan__c, User__c FROM Account_Plan_Team__c];
      Integer aptListSizeBeforeInsert = apt_List.size();
      
      Account_Plan_Team__c apt2 = new Account_Plan_Team__c(Account_Plan__c = apt_List[0].Account_Plan__c, User__c = apt_List[0].User__c);
      insert apt2;
                       
        
      Test.startTest();
        //Execution of the batch
        BatchAccountPlanTeamUpdate batu = new BatchAccountPlanTeamUpdate();
        batu.execute((Database.BatchableContext) null, [SELECT id, (SELECT UserId, User.isActive, TeamMemberRole, AccountId, isDeleted FROM AccountTeamMembers)
                                                        FROM Account
                                                        WHERE Id IN (SELECT Account__c FROM Account_Plan__c WHERE Account__c <> NULL)
                                                        ]);
      //Stop test
      Test.StopTest();
        
      System.assert([SELECT id FROM Account_Plan_Team__c].size()!= aptListSizeBeforeInsert,'BatchAccountPlanTeamUpdate_Test.accountPlanTeamDelete_Test: Expected Account_Plan_Team__c record not deleted.');
        
    }
    
    
    static testmethod void accountPlanTeamInactiveUser_Test() { 
        
      List<Account_Plan_Team__c> apt_List = [SELECT Account_Plan__c, User__c FROM Account_Plan_Team__c];
      Integer aptListSizeBeforeInsert = apt_List.size();
      
      User u1 = [SELECT isActive FROM User WHERE email='test1@experian.com' LIMIT 1];
      
      system.runAs(u1){
        List<AccountTeamMember> atm_List = [SELECT id FROM AccountTeamMember];
        delete atm_List;
      }
      
      u1.isActive = false;
      update u1;
        
      Test.startTest();
        //Execution of the batch
        BatchAccountPlanTeamUpdate batu = new BatchAccountPlanTeamUpdate();
        batu.execute((Database.BatchableContext) null, [SELECT id, (SELECT UserId, User.isActive, TeamMemberRole, AccountId, isDeleted FROM AccountTeamMembers)
                                                        FROM Account
                                                        WHERE Id IN (SELECT Account__c FROM Account_Plan__c WHERE Account__c <> NULL)
                                                        ]);
      //Stop test
      Test.StopTest();
        
      //System.assert([SELECT id FROM Account_Plan_Team__c].size()!= aptListSizeBeforeInsert,'BatchAccountPlanTeamUpdate_Test.accountPlanTeamDelete_Test: Expected Account_Plan_Team__c record not deleted.');
        
    }
    
    
    @testSetup
    static void createTestData(){
      Account acc1 = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
        
      List<String> AccountList = New List<String>();
      AccountList.add(acc1.Id);
      String AcntId = AccountList[0];

      Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];       
      User testUser = Test_Utils.createUser(p, 'test1@experian.com', 'Account Plan Team Test User');
      insert testUser;
        
      //Create an Account plan
      Account_Plan__c acntPlanRec = AccountPlanHelperClass.CreateAccountPlan(AccountList,acc1.id);
      AccountPlanHelperClass.updateAccountPlanTeamMembers(acc1.id, acntPlanRec);
      
      //add an account Team member
      AccountTeamMember atm = Test_Utils.insertAccountTeamMember(false, acc1.Id, testUser.Id, Constants.TEAM_ROLE_ACCOUNT_MANAGER );
      List<AccountTeamMember> atm_List = New List<AccountTeamMember>();
      atm_List.add(atm);
      insert atm_List;
    }
    
}