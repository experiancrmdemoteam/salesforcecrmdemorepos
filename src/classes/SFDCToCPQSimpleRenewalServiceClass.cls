/*******************
Created BY : Richard Joseph.
Created Date: Oct 13th 2014.
Desc: Service takes Opty Id and creates an simple renewal quote

Change log - 
March 3rd 2016 -RJ:  Case 01886363 to Skip Product description node if its empty
March 21st 2016- RJ: Case 01910259 to skip special characters in Opty Name.

*************/
global  class SFDCToCPQSimpleRenewalServiceClass {
  
  Public static HttpResponse responseCPQ;
  
  //Asyc call invoked with Opty id - Changed this as it will be called from VF 
  //@future (callout=true)
  webservice static string callCPQSimpleRenewalService(string OptyId, string userName){
    
    Boolean isError = false;
    string retrunURL = null;
    string optyCurrencyIsoCode = null;
    Transient set<string> orginalOptyLnItemIdSet = new set<string>();
    Transient list <OpportunityLineItem > optyLst = new list<OpportunityLineItem >([SELECT OpportunityId,Original_Asset_ID__c,Original_Opportunity_Line_Item_Id__c, Id,Opportunity.Name,Opportunity.CurrencyIsoCode   FROM OpportunityLineItem where OpportunityId = :OptyId ]);
    Set<id> assetIdSet = new set<id>();
    String optyName= null;
    for(OpportunityLineItem  optyLnItem:optyLst)
    {
      if(optyLnItem.Original_Asset_ID__c != null)
        assetIdSet.add(optyLnItem.Original_Asset_ID__c);
      
      if(optyLnItem.Opportunity.Name != null && optyName == null)
        optyName =   optyLnItem.Opportunity.Name; 
      
      if ( optyLnItem.Opportunity.CurrencyIsoCode != null && optyCurrencyIsoCode ==null)  
        optyCurrencyIsoCode = optyLnItem.Opportunity.CurrencyIsoCode; 
      if (optyLnItem.Original_Opportunity_Line_Item_Id__c != null)
        orginalOptyLnItemIdSet.add(optyLnItem.Original_Opportunity_Line_Item_Id__c);
      
      //optyCurrencyIsoCode =null;
      
    }
    
    If(assetIdSet.size() == 0 || (optyLst.size()>assetIdSet.size())){
      
      Map<Id,asset> orgAssetLst = new Map<id,asset>([Select id from Asset where Order_Line__c in (select id from Order_line_item__c where Opportunity_Line_Item_Id__c  in :orginalOptyLnItemIdSet)]);
      assetIdSet.addAll(orgAssetLst.keySet());   
    }
    
    
    If (assetIdSet.size() >0)
    {
      
      Transient List<Asset> assetLst = new list<Asset>();
      
      String objName = 'Asset';
      
      Transient Map<String,Schema.SObjectType> globalDescMap = Schema.getGlobalDescribe(); 
      Schema.SObjectType sobjType = globalDescMap.get(objName ); 
      Schema.DescribeSObjectResult describeResult = sobjType.getDescribe(); 
      Transient Map<String,Schema.SObjectField> fieldsMap = describeResult.fields.getMap(); 
      
      String queryFields= null;
      For (Schema.SObjectField fieldName:fieldsMap.Values() ){
        If (queryFields == null)
          queryFields=String.valueof(fieldName);
        else  
          queryFields=queryFields+','+fieldName ;  
      }
      
      String queryString = 'Select ' +queryFields + ' From Asset where Id In :assetIdSet';
      system.debug('Query String: ' + queryString);
      assetLst   = Database.query(queryString );
      
      If (assetLst.size() >0)   
        retrunURL = callCPQSimpleRenewalMethod(assetLst,OptyId,optyName,userName, optyCurrencyIsoCode  );
      
      system.debug('Return URL: ' + retrunURL);
    }
    
    return retrunURL ;
  }
  //List <Asset> assetList, Opportunity opty)
  Public static string callCPQSimpleRenewalMethod(list<asset> assetLst ,String optyId, String optyName, String username, String optyCurrencyIsoCode){
    
    String resultString = null;
    
    CPQ_Settings__c CPQSetting = CPQ_Settings__c.getInstance('CPQAlt');
    
    Transient String renewalQuoteDetails= '<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webcominc.com/\">';
    
    renewalQuoteDetails+= '<soapenv:Header/>';
    renewalQuoteDetails+= '<soapenv:Body>';
    renewalQuoteDetails+= '<web:NewQuote>';
    renewalQuoteDetails+= '<web:username>'+CPQSetting.CPQ_API_UserName__c+'</web:username>';
    renewalQuoteDetails+= '<web:password>'+CPQSetting.CPQ_API_Access_Word__c+'</web:password>';
    renewalQuoteDetails+= '<web:owner>'+username+'</web:owner>';
    //renewalQuoteDetails+= '<web:compositeCartId>'+quoteRecord.Name+'</web:compositeCartId>';
    renewalQuoteDetails+='<web:xml>';
    renewalQuoteDetails+='<Cart PREVENT_EMPTY_QUOTE="1">';
    renewalQuoteDetails+='<MarketCode>'+(optyCurrencyIsoCode != null?optyCurrencyIsoCode:'USD' )+'</MarketCode>';
    renewalQuoteDetails+='<CartComment/>';
    //Richard - Changed the value from ApplyMappings="1" to ApplyMappings="0" according to the discussion with callidus
    renewalQuoteDetails+='<Crm ApplyMappings="1">'; //EC:: ApplyMappings = 1 very important to create quote object in SF
    renewalQuoteDetails+='<OpportunityId>'+optyId+'</OpportunityId>';
    renewalQuoteDetails+='<OpportunityName><![CDATA['+(optyName != null ?optyName:'')+']]></OpportunityName>'; //RJ changed for Case 01910259 
    renewalQuoteDetails+='</Crm>';
    
    /* renewalQuoteDetails+= '<web:domainName>'+label.CPQ_Domain+'</web:domainName>';
renewalQuoteDetails+= '<web:sessionId>'+UserInfo.getSessionId().escapeXml()+'</web:sessionId>';
renewalQuoteDetails+= '<web:sfAPIUrl>'+(URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/28.0/' + UserInfo.getOrganizationId()).escapeXml()+'</web:sfAPIUrl>';*/
    
    
    renewalQuoteDetails+='<Items>';
    For(asset assetRec:assetlst)
    {
      
      
      renewalQuoteDetails+='<Item>';
      renewalQuoteDetails+='<Quantity><Value>'+assetRec.Quantity+'</Value></Quantity>';
      renewalQuoteDetails+='<CatalogueCode>Simple Renewal</CatalogueCode>';
      renewalQuoteDetails+='<Attributes>';
      renewalQuoteDetails+='<Attribute>';
      renewalQuoteDetails+='<Name>Original Asset ID</Name>';
      renewalQuoteDetails+='<Value>'+assetRec.Id+'</Value>';
      renewalQuoteDetails+='</Attribute>';
      renewalQuoteDetails+='<Attribute>';
      renewalQuoteDetails+='<Name>Application</Name>';
      renewalQuoteDetails+='<Value><![CDATA['+(assetRec.Application__c != null  ?assetRec.Application__c :'') +']]></Value>';
      renewalQuoteDetails+='</Attribute>';
      renewalQuoteDetails+='<Attribute>';
      renewalQuoteDetails+='<Name>ProductName</Name>';
      renewalQuoteDetails+='<Value><![CDATA['+(assetRec.Name != null ?assetRec.Name :'')+']]></Value>';
      renewalQuoteDetails+='</Attribute>';
      renewalQuoteDetails+='<Attribute>';
      renewalQuoteDetails+='<Name>Status</Name>';
      renewalQuoteDetails+='<Value>' + assetRec.Status__c + '</Value>';
      renewalQuoteDetails+='</Attribute>';
      renewalQuoteDetails+='<Attribute>';
      renewalQuoteDetails+='<Name>Product</Name>';
      renewalQuoteDetails+='<Value><![CDATA['+(assetRec.CRM_Product_Name__c != null ? assetRec.CRM_Product_Name__c :'')+']]></Value>';
      renewalQuoteDetails+='</Attribute>';
      renewalQuoteDetails+='<Attribute>';
      renewalQuoteDetails+='<Name>Callidus Item Number</Name>';
      renewalQuoteDetails+='<Value>'+assetRec.Callidus_Item_Number__c+'</Value>';
      renewalQuoteDetails+='</Attribute>';
      renewalQuoteDetails+='<Attribute>';
      renewalQuoteDetails+='<Name>Callidus Quote Number</Name>';
      renewalQuoteDetails+='<Value>'+assetRec.Callidus_Quote_Number__c+'</Value>';
      renewalQuoteDetails+='</Attribute>';
      renewalQuoteDetails+='<Attribute>';
      renewalQuoteDetails+='<Name>ClickExpiry</Name>';
      renewalQuoteDetails+='<Value>' + assetRec.Click_Expiry__c + '</Value>';
      renewalQuoteDetails+='</Attribute>';
      renewalQuoteDetails+='<Attribute>';
      renewalQuoteDetails+='<Name>Data Usage</Name>';
      renewalQuoteDetails+='<Value><![CDATA[' + assetRec.Data_Usage__c + ']]></Value>';
      renewalQuoteDetails+='</Attribute>';
      renewalQuoteDetails+='<Attribute>';
      renewalQuoteDetails+='<Name>Data Mapping</Name>';
      renewalQuoteDetails+='<Value><![CDATA[' + assetRec.Data_Usage__c + ']]></Value>';
      renewalQuoteDetails+='</Attribute>';
      //RJ : Case 01886363 to Skip Product description node if its empty
      if (assetRec.Description != null){
        renewalQuoteDetails+='<Attribute>';
        renewalQuoteDetails+='<Name>ProductDescription</Name>';
        renewalQuoteDetails+='<Value><![CDATA['+(assetRec.Description  != null ? assetRec.Description : '')+']]></Value>';
        renewalQuoteDetails+='</Attribute>';
      }
      renewalQuoteDetails+='<Attribute><Name>Exclude from RPI</Name><Value>' + assetRec.Exclude_from_RPI__c + '</Value> </Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Hardware</Name><Value><![CDATA[' + assetRec.Hardware__c + ']]></Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Implementation</Name><Value><![CDATA[' + assetRec.Implementation__c + ']]></Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Product Mapping</Name><Value><![CDATA[' + assetRec.Implementation__c + ']]></Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Manuals Required</Name><Value>' + assetRec.Manuals_Required__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Operating System</Name><Value><![CDATA[' + assetRec.Operating_System__c + ']]></Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Original Renewal List Price</Name><Value>' + assetRec.Original_Renewal_List_Price__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Original Renewal Price</Name><Value>' + assetRec.Original_Renewal_Price__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Parent Item Number</Name><Value><![CDATA[' + assetRec.Parent_Item_Number__c + ']]></Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Part Number</Name><Value><![CDATA[' + assetRec.Part_Number__c + ']]></Value></Attribute>';
      //Removed Install Data  as it will be derived of CPQ 
      //String InstallDate = string.valueOfGmt(assetRec.InstallDate);
      //If (InstallDate  != null)
      //renewalQuoteDetails+='<Attribute><Name>Install Date</Name> <Value>'+(InstallDate  != null?InstallDate   :null)+'</Value> </Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Product Reference</Name><Value><![CDATA[' + (String.isEmpty(assetRec.Product_Reference__c) ? '' : assetRec.Product_Reference__c) + ']]></Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>ItemQuantity</Name><Value>' + assetRec.Quantity+ '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>QuantityType</Name><Value><![CDATA[' + assetRec.Quantity_Type__c + ']]></Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>RPI</Name><Value>' + assetRec.RPI__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>RPI Amount</Name><Value>' + assetRec.RPI_Amount__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>RPI Batch Number</Name><Value>' + assetRec.RPI_Batch_Number__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Renewal Count</Name><Value>' + assetRec.Renewal_Count__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Renewal Date</Name><Value>' + assetRec.Renewal_Date__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Renewal Discount Percentage</Name><Value>' + assetRec.Renewal_Discount__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Renewal Discount Amount</Name><Value>' + assetRec.Renewal_Discount_Amount__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Renewal EDQ Margin</Name><Value>' + assetRec.Renewal_EDQ_Margin__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute><Name>Renewal Extended Amount</Name><Value>' + assetRec.Renewal_Extended_Amount__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute> <Name>Renewal List Price</Name><Value>' + assetRec.Renewal_List_Price__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute> <Name>Renewal Partner Percentage</Name><Value>' + assetRec.Renewal_Partner__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute> <Name>MRC Partner Percentage</Name><Value>' + assetRec.Renewal_Partner__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute> <Name>Renewal Partner Amount</Name><Value>' + assetRec.Renewal_Partner_Amount__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute> <Name>Renewal Sale Price</Name><Value>' + assetRec.Renewal_Sale_Price__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute> <Name>Renewal Total Royalty</Name><Value>' + assetRec.Renewal_Total_Royalty__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute> <Name>Renewals Exclude</Name><Value>' + assetRec.Renewals_Exclude__c + '</Value></Attribute>';
      //renewalQuoteDetails+='<Attribute> <Name>Licensing Start Date</Name><Value>'+assetRec.Start_Date__c+'</Value> </Attribute>';// Commented Start Date as it will be derived in CPQ.
      //renewalQuoteDetails+='<Attribute> <Name>Licensing End Date</Name><Value>'+assetRec.UsageEndDate+'</Value> </Attribute>';// Commented End Date as it will be derived in CPQ.
      renewalQuoteDetails+='<Attribute> <Name>UpdateFrequency</Name><Value><![CDATA[' + assetRec.Update_Frequency__c + ']]></Value></Attribute>';
      renewalQuoteDetails+='<Attribute> <Name>Users From</Name><Value>' + assetRec.Users_From__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute> <Name>Users To</Name><Value>' + assetRec.Users_To__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute> <Name>eRenewalException</Name><Value>' + assetRec.eRenewal_Exception__c + '</Value></Attribute>';
      renewalQuoteDetails+='<Attribute> <Name>Currency Code</Name><Value><![CDATA[' + assetRec.CurrencyIsoCode + ']]></Value></Attribute>';
      //Commented the Royalty Part as it will be caculated in CPQ.
      /*renewalQuoteDetails+='<Attribute> <Name>APAC Services Royalty</Name>';
renewalQuoteDetails+='<Rows>';
renewalQuoteDetails+='     <Row>';
renewalQuoteDetails+='     <Columns>';
renewalQuoteDetails+='     <Column>';
renewalQuoteDetails+='    <Name>APAC Services</Name>';
renewalQuoteDetails+='    <Value>AUS Data Royalty</Value>';
renewalQuoteDetails+='    </Column>';   
renewalQuoteDetails+='     </Columns>';
renewalQuoteDetails+='     </Row>';
renewalQuoteDetails+='    </Rows>';
renewalQuoteDetails+='  </Attribute>';*/
      renewalQuoteDetails+='</Attributes>';
      renewalQuoteDetails+='</Item>';
      
      
    }
    
    //Sample Values for testing or trouble shooting in future
    /*renewalQuoteDetails+='<Item>';
renewalQuoteDetails+='<Quantity>1</Quantity>';
renewalQuoteDetails+='<CatalogueCode>Simple Renewal</CatalogueCode>';
renewalQuoteDetails+='<!--<ProductName>Simple Renewal</ProductName> -->';
renewalQuoteDetails+='<Attributes>';
renewalQuoteDetails+='<Attribute>';
renewalQuoteDetails+='<Name>Original Asset ID</Name>';
renewalQuoteDetails+='<Value>02ie0000004r6ti</Value>';
renewalQuoteDetails+='</Attribute>';
renewalQuoteDetails+='<Attribute>';
renewalQuoteDetails+='<Name>Application</Name>';
renewalQuoteDetails+='<Value>the application</Value>';
renewalQuoteDetails+='</Attribute>';
renewalQuoteDetails+='<Attribute>';
renewalQuoteDetails+='<Name>ProductName</Name>';
renewalQuoteDetails+='<Value>ARG Data</Value>';
renewalQuoteDetails+='</Attribute>';
renewalQuoteDetails+='<Attribute>';
renewalQuoteDetails+='<Name>Status</Name>';
renewalQuoteDetails+='<Value>Scheduled</Value>';
renewalQuoteDetails+='</Attribute>';
renewalQuoteDetails+='<Attribute>';
renewalQuoteDetails+='<Name>Product</Name>';
renewalQuoteDetails+='<Value>QAS ARG Address Data - Annual Licence</Value>';
renewalQuoteDetails+='</Attribute>';
renewalQuoteDetails+='<Attribute>';
renewalQuoteDetails+='<Name>Callidus Item Number</Name>';
renewalQuoteDetails+='<Value>4d2876e9-b8f8-451e-8540-fd5ed92050a5</Value>';
renewalQuoteDetails+='</Attribute>';
renewalQuoteDetails+='<Attribute>';
renewalQuoteDetails+='<Name>Callidus Quote Number</Name>';
renewalQuoteDetails+='<Value>09030053</Value>';
renewalQuoteDetails+='</Attribute>';
renewalQuoteDetails+='<Attribute>';
renewalQuoteDetails+='<Name>ClickExpiry</Name>';
renewalQuoteDetails+='<Value>False</Value>';
renewalQuoteDetails+='</Attribute>';
renewalQuoteDetails+='<Attribute>';
renewalQuoteDetails+='<Name>Data Usage</Name>';
renewalQuoteDetails+='<Value>Web</Value>';
renewalQuoteDetails+='</Attribute>';
renewalQuoteDetails+='<Attribute>';
renewalQuoteDetails+='<Name>Data Mapping</Name>';
renewalQuoteDetails+='<Value>Web</Value>';
renewalQuoteDetails+='</Attribute>';
renewalQuoteDetails+='<Attribute>';
renewalQuoteDetails+='<Name>ProductDescription</Name>';
renewalQuoteDetails+='<Value>QAS ARG Address Data - Annual Licence</Value>';
renewalQuoteDetails+='</Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Exclude from RPI</Name> <Value>False</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Hardware</Name> <Value>False</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Implementation</Name> <Value>N/A</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Product Mapping</Name> <Value>N/A</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Manuals Required</Name> <Value>False</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Operating System</Name> <Value>fasle</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Original Renewal List Price</Name> <Value>9865.06</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Original Renewal Price</Name> <Value>986505.47</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Parent Item Number</Name> <Value>null</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Part Number</Name> <Value>1-1W39OM</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Install Date</Name> <Value>7/10/2016</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Product Reference</Name> <Value>1-2M7R35</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>ItemQuantity</Name> <Value>100</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>QuantityType</Name> <Value>null</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>RPI</Name> <Value>Null</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>RPI Amount</Name> <Value>null</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name> RPI Batch Number</Name> <Value>null</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Renewal Count</Name> <Value>1</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Renewal Date</Name> <Value>4/18/2017</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Renewal Discount Percentage</Name> <Value>0</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Renewal Discount Amount</Name> <Value>0</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Renewal EDQ Margin</Name> <Value>0</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Renewal Extended Amount</Name> <Value>986505.47</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Renewal List Price</Name> <Value>9865.06</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Renewal Partner Percentage</Name> <Value>0</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>MRC Partner Percentage</Name> <Value>0</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Renewal Partner Amount</Name> <Value>0</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Renewal Sale Price</Name> <Value>1500</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Renewal Total Royalty</Name> <Value>-986505.47</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Renewals Exclude</Name> <Value>Null</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Licensing Start Date</Name> <Value>4/19/2016</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Licensing End Date</Name> <Value>4/17/2017</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>UpdateFrequency</Name> <Value>Quarterly</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Users From</Name> <Value>0</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Users To</Name> <Value>100</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>eRenewalException</Name> <Value>False</Value> </Attribute>';
renewalQuoteDetails+='<Attribute> <Name>Currency Code</Name> <Value>USD</Value> </Attribute>';
renewalQuoteDetails+='</Attributes>';
renewalQuoteDetails+='</Item>'; */
    renewalQuoteDetails+='</Items>';
    renewalQuoteDetails+='</Cart>';
    renewalQuoteDetails+='</web:xml>';
    renewalQuoteDetails+='</web:NewQuote>';
    renewalQuoteDetails+='</soapenv:Body>';
    renewalQuoteDetails+='</soapenv:Envelope>';
    
    Http h = new http();
    HttpRequest req = new HttpRequest();
    req.setEndpoint(CPQSetting.CPQ_API_Endpoint__c);
    req.setMethod('POST');
    req.setHeader('Content-Type', 'text/xml;charset=utf-8'); 
    req.setHeader('SOAPAction', '\"http://webcominc.com/NewQuote"');
    req.setHeader('Host', CPQSetting.CPQ_API_Host__c);
    req.setTimeout(60000); //this is not async, the max is 60k
    req.setBody(renewalQuoteDetails );
    
    system.debug('HTTP Request Body: '+ req.getBody());
    
    try{
      if(!Test.isRunningtest())
        responseCPQ = h.send(req);
      
      system.debug('HTTP Response: '+ responseCPQ);
      
      If(responseCPQ != null && responseCPQ.getStatusCode() == 200)
      {  
        system.debug('HTTP Response Body: ' + responseCPQ.getBody());
        
        Dom.Document xmlDoc = responseCPQ.getBodyDocument();
        XPath xp = new XPath(xmlDoc);
        Dom.XmlNode[] res = xp.find('/soap:Envelope/soap:Body/NewQuoteResponse/NewQuoteResult/Result');
        String resultStatus = xp.getText(res[0], 'Status');
        System.debug('RESULT: ' + resultStatus);
        
        if(resultStatus == 'OK'){
          resultString = xp.getText(res[0], 'QuotationNumber');
          if(String.isEmpty(resultString)){
            throw new SimpleRenewalWebCallException('No Quote Number was returned from simple renewal call');
          }
        } else {
          throw new SimpleRenewalWebCallException(xp.getText(res[0], 'Error/Description'));
        }
      }   
    }
    catch(System.CalloutException e) {
      System.debug('Callout error: '+ e);
      If (responseCPQ != null){
        System.debug(responseCPQ.toString());
        resultString = 'Service Response : '+responseCPQ.toString();
      }else
        resultString = 'Error calling the Service';
    }
    catch (SimpleRenewalWebCallException e){
      System.debug('Exception error: ' + e.getMessage());
      resultString = 'Quote Not Created: ' + e.getMessage();
    }
    catch (Exception e)
    {
      System.debug('Exception error: ' + e.getMessage());
      resultString = 'Exception Deatils: '+ e.getLineNumber() + ' Stack:' +e.getStackTraceString();
    }
    
    return resultString;
  } 
  class SimpleRenewalWebCallException extends Exception {}
}