/**=============================================================================
 * Experian plc
 * Name: UATReminderForAgileAcceleratorBatch - used in a scheduler [UATReminderForAgileAcceleratorScheduler]
 * Description: Case #02296348 UAT - Repository, Distribution and Tracking
 * Created Date: Feb 28th, 2017
 * Created By: Sanket Vaidya
 * 
 * Date Modified        Modified By                  Description of the update
 * 
 =============================================================================*/

public class BatchUATReminderForAgileAccelerator implements Database.Batchable<sObject>
{    
	public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Test_Case_Name__c, 
                                                    Name, 
                                                    Short_Description__c,  
                                                    Work__r.agf__Due_Date__c, 
                                                    Project__r.Name,
                                                    Project__r.UAT_Reminder__c,                                                    
                                                    UAT_Owner__r.Email, 
                                                    (Select Tester__r.Email from Test_Case_Teams__r) 
                                                    FROM Test_Case__c ]);        
    }
    
    public void execute(Database.BatchableContext bc, List<Test_Case__c> testCaseList)
    {        
        for(Test_Case__C tc : testCaseList)
        {            
            if(tc.Project__r.UAT_Reminder__c != null &&
                    tc.Work__r.agf__Due_Date__c != null)
            {
                Integer diff = Date.today().daysBetween(date.valueof(tc.Work__r.agf__Due_Date__c));

                System.Debug('Test Case Name : ' + tc.Name + 
                                '; Difference : ' + diff + 
                                '; Reminder Value : ' + tc.Project__r.UAT_Reminder__c);

                if(diff <= tc.Project__r.UAT_Reminder__c)
                {
                    Set<String> toAddresses = new Set<String>();
                    String emailOfUATOwnerOfTestCase = '';
                    
                    System.Debug('Test Case Teams : ' + tc.Test_Case_Teams__r);
                    
                    for(Test_Case_Team__c teamC : tc.Test_Case_Teams__r)
                    {
                        System.Debug('Tester\'s email : ' + teamC.Tester__r.Email);
                        if(String.isNotEmpty(teamC.Tester__r.Email))
                        {
                            toAddresses.add(teamC.Tester__r.Email);
                        }
                    }

                    emailOfUATOwnerOfTestCase = tc.UAT_Owner__r.Email;
                    
                    UATEmailReminderForAgileAccelerator.SendEmail(tc, new List<String>(toAddresses), emailOfUATOwnerOfTestCase);
                }
            }
        }    
    }
    
    public void finish(Database.BatchableContext bc)
    {          
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();        
        mail.setToAddresses(new String[] {UserInfo.getUserEmail()});                
        mail.setSubject('Batch Process Completed');
        mail.setPlainTextBody('Batch Process : \"UATReminderForAgileAcceleratorBatch\" has completed');        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}