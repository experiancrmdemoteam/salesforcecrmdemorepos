/**=====================================================================
 * Appirio, Inc
 * Name: URLRedirectController
 * Description: This class contains logic to build url depending on where the user is
 *              coming from and where s/he plans on going - T-249349
 * Created Date: March 2014
 * Created By: Nathalie LE GUAY (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 * Apr 14, 2014                 Nathalie Le Guay             Adding EncodingUtil.urlEncode(paramName, 'UTF-8'); in buildOpp/OrderUrl()
 * Apr 25, 2014                 Nathalie Le Guay             Adding 'retURL' to urls
 * Jul 28, 2014                 James Weatherall             Case #2323 Added ability to create Follow-on Opportunity
 * Aug 04, 2014                 James Weatherall             Case #2323 Added query for Primary Contact Id and pass Opp Id and Name
 *                                                           for Previous Opp relationship
 * Aug 27, 2014                 Tyaga Pati                   Case #6948 Added code to copy Lead Source from Contact to new Opportunity(paramLeadSourceName)
 * Sep 04, 2014                 Naresh Kr Ojha (Appirio)     T-317502: Remove CF from line 108 as per task.
 * Oct 20, 2014                 James Weatherall             Case #8566 Added code to default the Reporting Sales Team on a new Opportunity
 * Jan 16th, 2015               James Weatherall             Case #21168 Added code to prevent CPQ Users from creating Transactional
 * Feb 26th, 2015               James Weatherall             Case #562501 Added Lead Source check in getParameterValues() for Cross Business Unit Leads
 *                                                           Case #540588 Added COPS user check in BuildOrderUrl to allow Transactional Orders to be created
 * Apr 07th, 2015               Nur Azlini                   Case #12481 Added new Method to check is user part of the Account Team.
 * July 10th, 2015              James Weatherall             Case #648853 Modified buildPreSalesSupportURL to check user region
 * Aug 27th, 2015               Paul Kissick                 S-210749/T-430758: Adding checks for 'Inactive' users.
 * Nov 24th, 2015               Diego Olarte                 Case #01235685: Add campaign id to the new opp created.
 * Dec 9th,  2015               Sadar Yacob                  Case #01235685 : Primary Campaign Source was not getting set on Opty when created from a Task.
 * Jan 5th, 2016                Paul Kissick                 Case #00926399: Adding check for UK in presales redirect
 * Apr 19th, 2016               Diego Olarte                 Case #01846129: Added Account to be prepopulated in New Presales Support request
 * Apr 20th, 2016               Diego Olarte                 Case #01822826: Added PO required for Opps if selected in Accounts
 * Apr 27th, 2016               Esteban Castro               Case #00008840: Added Currency Rate Region to Opportunity
 * Jun 2nd, 2016                Paul Kissick                 Case #01984048: Adding trim to Opp Name, and replacing length check
 * Jul 18th, 2016               Manoj Gopu                   CRM2:W-005436  Added code to update the contact role in contact when creating new opp from contact.
 * Jul 21th, 2016               Diego Olarte                 Case #01975917: Set PO required for Opps to none if not selected in Accounts
 * Aug 15th, 2016               Manoj Gopu                   CRM2:W-005421: Added code to redirect to Opp Creation page when CPQ user is True for Transactional Order
 * Aug 19th, 2016               Tyaga Pati                   CRM2:W-005599: code to Check Flag on orginating Activity When Opp is created from Marketing Activity.
 * Aug 26th, 2016               Tyaga Pati                   CRM2:W-005402: Removed Reference to Inactive flag. Rather now check with be on status field.
 * Aug 31st, 2016               Tyaga Pati                   CRM2:W-005402: Re-Activated Active flag check on contact before opp creation
 * Aug 31st, 2016               Tyaga Pati                   CRM2:W-005402: Replaced Inactive__C with Status__c in other SOQL Queries
 * Sep 2nd, 2016                Tyaga Pati                   CRM2:W-005402: Adding function call to not let Order Creation for non-CPQ users creating transactional Order when Con is not with Company
 * Sep 9th, 2016                Manoj Gopu                   CRM2:W-005421: Added Restriction for CSDA and EDQ users in order not to create Transactional Sale Opp
 * Sep 12th, 2016               Paul Kissick                 CRM2:W-005421: Improved code to more scalability for Trans orders
 * Oct 05th, 2016               Manoj Gopu                   CRM2:W-005952: Added code to populate the Opp fields when new Opp is created from marketing Activity.
 * Oct 14th, 2016               Paul Kissick                 Cleaned up some code.
 * Dec  14th 2016               Manoj Gopu                   Case:02108066: Added code to update the existing Task Status to completed when creating New Opp or New order.
 * Jan 26th, 2017               Sanket Vaidya                Case #02239844: Added condition to use constant BUSINESS_UNIT_UKI_GTM;
 * Jun 22nd, 2017               Diego Olarte                 Case #02136423: Added field Opportunity_Primary_Partner_at_Close_Stg__c and Opportunity_Account_Name_at_Close_Stage__c for historical tracking
 =====================================================================*/
public without sharing class URLRedirectController {

  private String recordId;
  private String recordName = ''; // Case #2323
  private String objectName;
  Private String action;
  private String userSalesTeam; // Case #21168 - Added by JW 01/16/2015
  private SObject sourceRecord;
  private Contact contact;
  private Task task;
  private Event event;
  private Opportunity opportunity; // Case #2323 - Added by JW 07/28/2014
  private ELQA_Marketing_Activity__c mktgActivity;
  private User user; // Case #21168 - Added by JW 01/16/2015
  private String paramCampaignSourceId = '';
  private String paramCampaignSourceName = '';
  private String paramOppOrderName;
  private String paramOppId;
  private String paramOppName;
  private String paramAccountId;
  private String paramAccountName;
  private String paramContactId = '';
  private String paramTaskId = '';
  private String paramContactName;
  private String paramReportingSalesTeam = ''; // Case #8566 - Added by JW 10/20/2014
  private String paramLeadSourceName = '';  //[TP] Changes Added Case #6948 09/02/2014
  private Boolean paramAccOppPOReq = false; // [DO] Case #01822826 04/20/2016

  private string paramSource ='';//Added by Manoj
  private string paramCampaign = '';
  private string paramCampaignName = '';
  private string paramCompetitor = '';
  private string paramCompetitorName = '';
  private string paramPrimaryCapability ='';
  private string paramProductGroup ='';
  private string paramLeadReferrerName ='';
  private string paramPrimaryPartneratClose ='';//Added by DO
  private string paramAccatClose ='';//Added by DO

  public String errorMessage {get;set;}
  private Custom_Fields_Ids__c fieldIds;
  private Record_Type_Ids__c recordTypeIds;
  public Id recAccountId; //07042015 Case #12481 - Added by [AZ]
  private Set<String> oppOrderAllowedFromObjectsSet = new Set<String>{
    Constants.SOBJECT_EVENT,
    Constants.SOBJECT_TASK,
    Constants.SOBJECT_MKTG_ACTIVITY,
    Constants.SOBJECT_CONTACT,
    Constants.SOBJECT_OPPORTUNITY
  }; // Case #2323 - Added by JW 07/28/2014

  public URLRedirectController() {
  }

  //========================================================================================
  // This method checks which action to perform, loads the custom settings and will either
  // return the error message if any, or the built url otherwise
  //========================================================================================
  public PageReference getURL() {
    recordId = ApexPages.currentPage().getParameters().get('Id');
    recordName = ApexPages.currentPage().getParameters().get('Name');
    action = ApexPages.currentPage().getParameters().get('action');
    String decision = ApexPages.currentPage().getParameters().get('Decision');

    if (String.isEmpty(recordId) || String.isEmpty(action)) {
      return new PageReference('/'+recordId);
    }

    List<Contact> checkContact = [
      SELECT Id, Decision_Maker__c
      FROM Contact
      WHERE Id = :recordId
    ];
    if (checkContact != null && checkContact.size() > 0) {
      for (Contact c : checkContact) {
        c.Decision_Maker__c = (decision == 'yes');  // Will be true if decision is set 'yes' otherwise false
      }
      update checkContact;
    }

    objectName = DescribeUtility.getSObjectName(recordId);
    system.debug('\n[URLRedirectController: constructor] : objectName is ' + objectName);

    // Start : Case #21168 - Added by JW 01/16/2015
    // Get User to check CPQ User and to set the Sales Team
    user = getCurrentUser(UserInfo.getUserId());
    // End
    //TP: CRM2.0W-005599: Query for the "IsLead" Flag on Initiating Activity and set the Flag to True after Opp
    //is created. This will be used before sending Marketing Activity Back to Marketing team because of no action.

    List<Task> lstTask = [
      SELECT Id, RecordType.Name, IsLead__c, Campaign__r.Name, Competitor__r.Name, Source__c, Campaign__c,
             Competitor__c, Primary_Capability__c, Product_Type__c, Lead_Referrer_s_Name__c
      FROM Task
      WHERE Id = :recordId ];
    if (lstTask != null && lstTask.size() > 0) {
      for (Task t:lstTask){
        if (t.RecordType.Name == Constants.RECORDTYPE_MARKETING_TASK){
          paramSource = t.Source__c;
          paramCampaign = t.Campaign__c;
          paramCampaignName = t.Campaign__r.Name;
          paramCompetitor = t.Competitor__c;
          paramCompetitorName = t.Competitor__r.Name;
          paramPrimaryCapability = t.Primary_Capability__c;
          paramProductGroup = t.Product_Type__c;
          paramLeadReferrerName = t.Lead_Referrer_s_Name__c;
        }
      }
    }
    Boolean fetchSuccess = fetchFieldIdsCustomSettings();
    if (!fetchSuccess) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errorMessage));
      return null;
    }

    if (action.equalsIgnoreCase(Constants.REDIRECT_CREATE_OPPORTUNITY)) {
      String url = buildOpportunityURL();
      if (url == null) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errorMessage));
        return null;
      }



      //TP: CRM2.0W-005599: Cntd
      if (lstTask != null && lstTask.size() > 0) {
        for (Task t : lstTask) {
          if(t.RecordType.Name == Constants.RECORDTYPE_MARKETING_TASK || t.RecordType.Name == Constants.RECORDTYPE_STANDARD_TASK)
            t.IsLead__c = true;
          if(decision == 'yes')
            t.Status = Constants.TASK_STATUS_COMPLETED;
        }
        update lstTask;
      }
      return new PageReference(url);
    }
    else if (action.equalsIgnoreCase(Constants.REDIRECT_CREATE_ORDER)) {
      //Querying the user to check whether the user is CPQ
      String url;
      Set<String> bypassBusinessUnits = new Set<String>{
        'APAC MS Data Quality',
        'UK&I MS Data Quality',
        'NA CS Data Quality',
        'NA CS CIS',
        'NA CS BIS'
      };
      if (user.CPQ_User__c == true && !bypassBusinessUnits.contains(user.Business_Unit__c)) {
        Id transRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.RECORDTYPE_Transactional_Sale).getRecordTypeId();
        url = buildOpportunityURL();
        url = (url != null) ? url + '&RecordType=' + transRecordTypeId : url;
      }
      else {
        url = buildOrderURL();
      }
      if (url == null) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errorMessage));
        return null;
      }
      //TP: CRM2.0W-005599: Cntd
      if (lstTask != null && lstTask.size() > 0) {
        for (Task t : lstTask) {
          if(t.RecordType.Name == Constants.RECORDTYPE_MARKETING_TASK || t.RecordType.Name == Constants.RECORDTYPE_STANDARD_TASK)
            t.IsLead__c = true;
          if(decision == 'yes')
            t.Status = Constants.TASK_STATUS_COMPLETED;
        }
        update lstTask;
      }
      return new PageReference(url);
    }
    else if (action.equalsIgnoreCase(Constants.REDIRECT_PRE_SALES_SUPPORT)) {
      String url = buildPreSalesSupportURL();
      if (url == null) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errorMessage));
        return null;
      }
      return new PageReference(url);
    }
    return new PageReference('/'+recordId);
  }

  //========================================================================================
  // Will build the url to create an Opp by populating the Name, Account, Stage, Campaign
  // Source (if applicable) as well as the Originating_Contact_Id__c field
  //========================================================================================
  private String buildOpportunityURL() {
    if (!oppOrderAllowedFromObjectsSet.contains(objectName)) {
      errorMessage = Label.URL_Redirect_Opp_From_Object_Not_Supported;
      return null;
    }
    if (!validateSourceData()) {
      system.debug('\n[URLRedirect: buildOpportunityURL]: Validate was unsuccessful.');
      return null;
    }
    if (!checkContactIsActive()) {
      errorMessage = Label.New_Opp_Not_Allowed;
      return null;
    }
    // [AZ] Start Case #12481
    if (!userhaveAccountAccess() && objectName.equalsIgnoreCase(Constants.SOBJECT_TASK)) {
      system.debug('User have no access to create Opportunity');
      errorMessage = Label.OPP_DONTHAVEACCESS_MSG;
      return null;
    }
    // [AZ] End Case #12481
    system.debug('\n[URLRedirect: buildOpportunityURL]: Validate was successful.');

    String url = '/' + DescribeUtility.getPrefix(Constants.SOBJECT_OPPORTUNITY) + '/e?' +
                 'opp11='+ Constants.OPPTY_STAGE_3;
    getParameterValues();

    //[TP] Changes Start Case #6948
    if (paramLeadSourceName == null) {
      paramLeadSourceName = '';
    }
    if (String.isNotBlank(paramSource)) {
      paramLeadSourceName = paramSource;
    }
    if (String.isNotBlank(paramCampaign) && String.isNotBlank(paramCampaignName)) {
      paramCampaignSourceId = paramCampaign;
      paramCampaignSourceName = paramCampaignName;
    }
    //[TP] Changes End Case #6948
    if (String.isNotBlank(paramCampaignSourceId) && String.isNotBlank(paramCampaignSourceName)) {
      url += '&opp17_lkid=' + paramCampaignSourceId
          + '&campid=' + paramCampaignSourceId //DO Case #01235685
          + '&opp17=' + EncodingUtil.urlEncode(paramCampaignSourceName, 'UTF-8');
    }
    paramLeadReferrerName = String.isNotBlank(paramLeadReferrerName)? paramLeadReferrerName:'';
    url += '&opp3=' + EncodingUtil.urlEncode(paramOppOrderName + ' - ', 'UTF-8')
        + '&opp6=' + EncodingUtil.urlEncode(paramLeadSourceName, 'UTF-8') // [TP] Changes for Case #6948 09/02/2014
        + '&opp4_lkid='+ paramAccountId
        + '&opp4=' + EncodingUtil.urlEncode(paramAccountName, 'UTF-8')
        + '&conid=' + paramContactId
        + '&' + fieldIds.Opportunity_Originating_Contact_Id__c + '=' + paramContactId
        + '&' + fieldIds.Opportunity_Originating_Task_Id__c + '=' + paramTaskId
        + '&' + fieldIds.Opportunity_Competitor__c + '=' + paramCompetitorName
        + '&' + fieldIds.Opportunity_Competitor__c + '_lkId=' + paramCompetitor
        + '&' + fieldIds.Opportunity_PrimaryCapability__c + '=' + paramPrimaryCapability
        + '&' + fieldIds.Opportunity_ProductGroup__c + '=' + paramProductGroup
        + '&' + fieldIds.Opportunity_LeadReferrerName__c + '=' + paramLeadReferrerName
        //Case 02136423: DO added historical fields
        + '&' + fieldIds.Opportunity_Primary_Partner_at_Close_Stg__c + '=' + paramPrimaryPartneratClose
        + '&' + fieldIds.Opportunity_Account_Name_at_Close_Stage__c + '=' + paramAccatClose
        // Start: Case #8566 - Added by JW 10/20/2014
        + '&'+fieldIds.Opportunity_Reporting_Sales_Team__c + '=' + EncodingUtil.urlEncode(paramReportingSalesTeam, 'UTF-8');
        // :End

    // [DO] Case #01822826 04/20/2016
    if (paramAccOppPOReq) {
      url += '&' + fieldIds.Opportunity_PO_Required__c + '=' + 'Yes';
    }
    /*
    else {
      url += '&' + fieldIds.Opportunity_PO_Required__c + '=' + 'No';
    }
    */
    // :End

    // Start: Case #2323 Added to prevent incorrect Id being passed into Previous Opp field
    if (objectName.equalsIgnoreCase(Constants.SOBJECT_OPPORTUNITY)) {
      url += '&CF'+fieldIds.Opportunity_Previous_Opportunity__c + '_lkid=' + recordId
          + '&CF'+fieldIds.Opportunity_Previous_Opportunity__c + '=' + EncodingUtil.urlEncode(recordName, 'UTF-8');
    }
    // :End
    // Start: [EC] Case 00008840
    if (String.isNotBlank(user.Region__c)) {
      url += '&' + fieldIds.Opportunity_Client_Budget_Region__c + '=' + EncodingUtil.urlEncode(user.Region__c, 'UTF-8');
    }
    // :End
    url += '&retURL=/' + recordId;

    system.debug('\n[URLRedirectController: buildOpportunityURL]: URL is ' + url);
    return url;
  }

  //========================================================================================
  // Will build the url to create an Order by populating the Name, Account, Contact, Campaign
  // Source (if applicable)
  //========================================================================================
  private String buildOrderURL() {
    Set<String> copsRoles = new Set<String>{
      Constants.ROLE_NA_COPS,
      Constants.ROLE_UKI_COPS,
      Constants.ROLE_GLOBAL_ADMIN
    }; // JW - 26/02/2015 - Case #540588

    if (!oppOrderAllowedFromObjectsSet.contains(objectName)) {
      errorMessage = Label.URL_Redirect_Order_From_Object_Not_Supported;
      return null;
    }

    // Start : Case #21168 - Modified by JW 01/16/2015
    if (user.CPQ_User__c == true && !copsRoles.contains(user.UserRole.Name) && IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {
      errorMessage = Label.URL_Redirect_cannot_create_Transactional_Orders;
      return null;
    }
    // End
    if (!validateSourceData()) {
      system.debug('\n[URLRedirect: buildOrderURL]: Validate was unsuccessful.');
      return null;
    }

    //TP: function Call to check if Contact is Active
    if (!checkContactIsActive()) {
      errorMessage = Label.New_Order_Not_Allowed;
      return null;
    }

    //[AZ] Start Case #12481
    if (!userhaveAccountAccess() && objectName.equalsIgnoreCase(Constants.SOBJECT_TASK)) {
      system.debug('User have no access to create order');
      errorMessage = Label.ORDER_DONTHAVEACCESS_MSG;
      return null;
    }
    //[AZ] End Case #12481

    system.debug('\n[URLRedirect: buildOrderURL]: Validate was successful.');

    String url = '/' + DescribeUtility.getPrefix(Constants.SOBJECT_ORDER) + '/e?';
    getParameterValues();

    url += 'Name=' + paramOppOrderName + ' - '
        + '&' + fieldIds.Order_Transactional_Sale__c + '=1'
        + '&CF' + fieldIds.Order_Account__c + '_lkid='+ paramAccountId
        + '&CF' + fieldIds.Order_Account__c + '=' + EncodingUtil.urlEncode(paramAccountName, 'UTF-8')
        + '&CF' + fieldIds.Order_Contact__c + '_lkid=' + paramContactId
        + '&CF' + fieldIds.Order_Contact__c + '=' + EncodingUtil.urlEncode(paramContactName, 'UTF-8')
        // Start: Case #8566 - Added by JW 10/20/2014
        + '&'+fieldIds.Order_Reporting_Sales_Team__c + '=' + EncodingUtil.urlEncode(paramReportingSalesTeam, 'UTF-8')
        + '&retURL=/' + recordId;
    if (String.isNotBlank(paramCampaignSourceId) && String.isNotBlank(paramCampaignSourceName)) {
      url += '&CF' + fieldIds.Order_Campaign__c + '_lkid=' + paramCampaignSourceId
          + '&CF' + fieldIds.Order_Campaign__c + '=' + EncodingUtil.urlEncode(paramCampaignSourceName, 'UTF-8');
    }
    system.debug('\n[URLRedirectController: buildOrderURL]: URL is ' + url);
    return url;
  }

  //========================================================================================
  // Will build the url to create a Pre Sales Support record by populating the
  // Opportunity Name, Contact
  //========================================================================================
  private String buildPreSalesSupportURL() {
    String recordTypeId; // Case #648853 - JW

    if (!validateSourceData()) {
      system.debug('\n[URLRedirect: buildPreSalesSupportURL]: Validate was unsuccessful.');
      return null;
    }
    system.debug('\n[URLRedirect: buildPreSalesSupportURL]: Validate was successful.');

    String url = '/' + DescribeUtility.getPrefix(Constants.SOBJECT_SALES_SUPPORT_REQUEST) + '/e?';
    getParameterValues();


    System.Debug('User Region : ' + user.Region__c);
    System.Debug('User Business_Unit__c : ' + user.Business_Unit__c );

    // START: Case #648853 - JW
    if (user.Region__c == Constants.REGION_APAC) {
      recordTypeId = recordTypeIds.Sales_Support_Request_APAC_Pre_Sales_Sup__c;
    }
    // PK: 00926399
    else if (user.Region__c == Constants.REGION_UKI)
    {
      if(user.Business_Unit__c != null && user.Business_Unit__c.contains(Constants.BUSINESS_UNIT_UKI_GTM))
      {
        recordtypeid =  recordTypeIds.UK_I_GTM_Consultancy_Support__c;
      }
      else
      {
        recordTypeId = recordTypeIds.Sales_Support_Request_Pre_Sales_UK__c;
      }
    }    
    else
    {
      recordTypeId = recordTypeIds.Sales_Support_Request_Pre_Sales_Request__c;
    }
    // END: Case #648853
    url += fieldIds.SalesSupportRequest_Opportunity__c + '_lkid=' + paramOppId
        + '&' + fieldIds.SalesSupportRequest_Opportunity__c + '=' + paramOppName
        + '&' + fieldIds.SalesSupportRequest_Account__c + '_lkid=' + paramAccountId
        + '&' + fieldIds.SalesSupportRequest_Account__c + '=' + paramAccountName
        + '&RecordType=' + recordTypeId
        + '&retURL=/' + recordId;
    system.debug('\n[URLRedirectController: buildPreSalesSupportURL]: URL is ' + url);
    return url;
  }

 /* private Boolean checkContactIsActive() {
    if (contact != null) {
      if (contact.Inactive__c) {
           return false;
      }
    }
    return true;
  }*/

  private Boolean checkContactIsActive() {
    if (contact != null) {
      if (contact.Status__c == Constants.CONTACT_STATUS_LEFT) {
        return false;
      }
    }
    return true;
  }

  //========================================================================================
  // Checks if the data is correct:
  // - Activities need to be attached to a Contact
  // - Marketing Activities need to have both an Account & a Contact
  //========================================================================================
  private Boolean validateSourceData() {
    //record = getObject();
    String whoObject;
    if (objectName.equalsIgnoreCase(Constants.SOBJECT_EVENT)) {
      event = [
        SELECT Id, WhoId, WhatId
        FROM Event
        WHERE Id = :recordId
      ];
      whoObject = DescribeUtility.getSObjectName(event.WhoId);
      if (!whoObject.equalsIgnoreCase(Constants.SOBJECT_CONTACT)) {
        errorMessage = Label.URL_Redirect_Missing_Contact;
        return false;
      }
    }
    else if (objectName.equalsIgnoreCase(Constants.SOBJECT_TASK)) {
      task = [
        SELECT Id, WhoId, WhatId, Type, AccountId, Campaign__c // JW - 26/02/2015 - Case #562501: Added Type field || 07042015- [AZ] Case #12481 - Added AccountId field || 11/24/2015 DO Case # 01235685
        FROM Task
        WHERE Id = :recordId
      ];

      recAccountId = task.AccountId;//07042015- [AZ] Case #12481
      whoObject = DescribeUtility.getSObjectName(task.WhoId);
      if (!whoObject.equalsIgnoreCase(Constants.SOBJECT_CONTACT)) {
        errorMessage = Label.URL_Redirect_Missing_Contact;
        return false;
      }
    }
    else if (objectName.equalsIgnoreCase(Constants.SOBJECT_MKTG_ACTIVITY)) {
      mktgActivity = [
        SELECT Account__c, Contact__c, Contact__r.Name, Account__r.Name, Campaign__c, Campaign__r.Name
        FROM ELQA_Marketing_Activity__c
        WHERE Id = :recordId
      ];
      if (mktgActivity.Account__c == null || mktgActivity.Contact__c == null) {
        errorMessage = Label.URL_Redirect_Missing_Contact_or_Account;
        return false;
      }
    }

    //[TP] Changes Start Case #6948 09/02/2014
    else if (objectName.equalsIgnoreCase(Constants.SOBJECT_CONTACT)) {
      contact = [
        SELECT Id, AccountId, Account.Name, Name, LeadSource, Status__C,
        //Inactive__c,  //CRM2.0W-005402
        Account.Opportunity_PO_Required_By_Default__c
        FROM Contact
        WHERE Id = :recordId
      ];
    } //[TP] End

    // Start: Case #2323 - Added by JW 07/28/2014
    else if (objectName.equalsIgnoreCase(Constants.SOBJECT_OPPORTUNITY)) {
      opportunity = [
        SELECT Id, Name, AccountId, Account.Name, Account_Name_at_Close_Stage__c, Primary_Partner_at_Close_Stage__c, (SELECT Id, ContactId, Contact.Name From OpportunityContactRoles WHERE IsPrimary = true limit 1)
        FROM Opportunity
        WHERE Id = :recordId
      ];
    }
    // End: Case #2323
    return true;
  }

  //========================================================================================
  // Will populate the variable to build the url
  //========================================================================================
  private void getParameterValues() {
    userSalesTeam = user.Sales_Team__c != null ? user.Sales_Team__c : ''; // Added by JW 01/16/2015
    if (objectName.equalsIgnoreCase(Constants.SOBJECT_EVENT)) {
      String whatObject = DescribeUtility.getSObjectName(event.WhatId);

      if (whatObject.equalsIgnoreCase(Constants.SOBJECT_CAMPAIGN)) {
        paramCampaignSourceId = event.WhatId;
        Campaign camp = [
          SELECT Id, Name
          FROM Campaign
          WHERE Id = :event.WhatId
          LIMIT 1
        ];
        if (camp != null) {
          paramCampaignSourceName = camp.Name;
        }
      }
      Contact cont = getContact(event.WhoId);
      paramAccountId = cont.AccountId;
      paramContactId = event.WhoId;
      paramContactName = cont.Name;
      paramReportingSalesTeam = userSalesTeam; // Case #8566 - Added by James Weatherall 10/20/2014
      paramOppOrderName = cont.Account.Name;
    }
    else if (objectName.equalsIgnoreCase(Constants.SOBJECT_TASK)) {
    //fix for Case #01235685
      String whatObject = DescribeUtility.getSObjectName(task.Campaign__c); //task.WhatId);
      if (whatObject.equalsIgnoreCase(Constants.SOBJECT_CAMPAIGN)) {

        //paramCampaignSourceId = task.WhatId; For campaigns WhatID is no longer used
        paramCampaignSourceid = task.Campaign__c;  //Dec 9th 2015

        Campaign camp = [
          SELECT Id, Name
          FROM Campaign
          WHERE Id = :task.Campaign__c
          LIMIT 1
        ]; //task.WhatId
        if (camp != null) {
          paramCampaignSourceName = camp.Name;
        }

      }
      Contact cont = getContact(task.WhoId);
      paramAccountId = cont.AccountId;
      paramContactId = task.WhoId;
      paramContactName = cont.Name;
      paramReportingSalesTeam = userSalesTeam; // JW - 10/20/2014 Case #8566
      paramOppOrderName = cont.Account.Name;
      if (task.Type == Constants.ACTIVITY_TYPE_CROSS_BUSINESS_UNIT) {
        paramLeadSourceName = task.Type; // JW - 26/02/2015 - Case #562501
      }
      paramTaskId = task.ID;
    }
    else if (objectName.equalsIgnoreCase(Constants.SOBJECT_MKTG_ACTIVITY)) {
      paramCampaignSourceId = mktgActivity.Campaign__c;

      if (String.isNotEmpty(paramCampaignSourceId)) {
        paramCampaignSourceName = mktgActivity.Campaign__r.Name;
      }
      paramAccountId = mktgActivity.Account__c;
      paramContactId = mktgActivity.Contact__c;
      paramContactName = mktgActivity.Contact__r.Name;
      paramReportingSalesTeam = userSalesTeam; // Case #8566 - Added by James Weatherall 10/20/2014
      paramOppOrderName = mktgActivity.Account__r.Name;
    }
    else if (objectName.equalsIgnoreCase(Constants.SOBJECT_CONTACT)) {
      paramAccountId = contact.AccountId;
      paramContactId = contact.Id;
      paramContactName = contact.Name;
      paramReportingSalesTeam = userSalesTeam; // Case #8566 - Added by James Weatherall 10/20/2014
      paramOppOrderName = contact.Account.Name;
      paramLeadSourceName = contact.LeadSource; // [TP]  Case #6948 09/02/2014
      paramAccOppPOReq = contact.Account.Opportunity_PO_Required_By_Default__c; // [DO] Case #01822826 04/20/2016
    }
    // Start: Case #2323 - Added by JW 07/28/2014
    else if (objectName.equalsIgnoreCase(Constants.SOBJECT_OPPORTUNITY)) {
      OpportunityContactRole oppContactRole;
      paramAccountId = opportunity.AccountId;
      // Extract Primary Contact
      if (!opportunity.OpportunityContactRoles.isEmpty()) {
        oppContactRole = opportunity.OpportunityContactRoles;
        paramContactId = oppContactRole.ContactId;
      }
      system.debug('paramContactId = ' + paramContactId);
      if (paramContactId != '') {
        paramContactName = oppContactRole.Contact.Name;
      }
      else {
        paramContactName = '';
      }
      paramReportingSalesTeam = userSalesTeam; // Case #8566 - Added by James Weatherall 10/20/2014
      paramOppOrderName = opportunity.Account.Name;
      // 12052014 Case 9748 - JW
      paramOppId = opportunity.Id;
      paramOppName = opportunity.Name;
      paramAccatClose = opportunity.Account_Name_at_Close_Stage__c; //Added by DO
      paramPrimaryPartneratClose = opportunity.Primary_Partner_at_Close_Stage__c; //Added by DO
    }

    paramAccountName = paramOppOrderName;
    // Case 01984048 - Minor fix below.
    //if (paramOppOrderName.length() > 15) {
    paramOppOrderName = paramOppOrderName.mid(0, 15).trim(); // PK 160602: Adding trim, replacing substring with mid
    //}
  }

  //========================================================================================
  // Retrieves the Contact from the database
  //========================================================================================
  private Contact getContact(String contactId) {
    return [
      SELECT Id, AccountId, Account.Name, Name, Status__c
      //Inactive__c //CRM2.0W-005402:
      FROM Contact
      WHERE Id = :contactId
    ];
  }

  // Case #8566 - Added by JW 10/20/2014
  // Modified by JW 01/16/2015
  // Get the current user reporting sales team as the default value for the Opp
  private User getCurrentUser(String userId) {
    return [
      SELECT Id, Region__c, Sales_Team__c, CPQ_User__c, UserRole.Name, Business_Unit__c
      FROM User
      WHERE Id = :userId
    ];
  }

  //========================================================================================
  // Retrieves the custom settings that stores the field Ids (to dynamically build the url,
  // in order not to be org-dependent)
  //========================================================================================
  private Boolean fetchFieldIdsCustomSettings() {
    fieldIds = Custom_Fields_Ids__c.getOrgDefaults();
    recordTypeIds = Record_Type_Ids__c.getOrgDefaults(); // 12052014 Case 9748 - JW
    if (fieldIds == null || String.isEmpty(fieldIds.Order_Account__c)
                         || String.isEmpty(fieldIds.Order_Campaign__c)
                         || String.isEmpty(fieldIds.Order_Contact__c)) {
      errorMessage = Label.URL_Redirect_Custom_Setting_Missing;
      return false;
    }
    return true;
  }

  //========================================================================================
  // Link behind the "Go Back" button in case of error message
  //========================================================================================
  public PageReference goBack() {
    return new PageReference('/' + recordId);
  }

//TYAGA PATI Added URL for Navigation to New Nomination Page
Public PageReference newNomination(){
PageReference pr = new PageReference('/apex/nominationHome');
//pr.getParameters().put('key','value');
Map<string, string> params = pr.getParameters();
//params.getParameters().put('inline','0');
params.remove('inline');
//pr.setRedirect(true); // If you want a redirect. Do not set anything if you want a forward.
return pr;

}

  //========================================================================================
  // Method to validate is user have access to create Opportunity on the Account or not.
  // Case #12481 - Added by AZ 07042015
  //========================================================================================
  public boolean userhaveAccountAccess() {
    List <UserRecordAccess> userAccess = [
      SELECT RecordId
      FROM UserRecordAccess
      WHERE UserId = :user.id
      AND HasEditAccess = true
      AND RecordId = :recAccountId
    ];
    if (!userAccess.isEmpty()) {
      return true;
    }
    return false;
  }

}