/*=============================================================================
 * Experian
 * Name: ChatterGroupHelper_Test
 * Description: Test class for the ChatterGroupHelper class 
 * Created Date: July 21 2017
 * Created By: Mauricio Murillo
 *
 * Date Modified      Modified By           Description of the update
 =============================================================================*/

@isTest(SeeAllData=true)
private class ChatterGroupHelper_Test {

    static testmethod void testAssignChatterGroupsToUser(){
        
        test.startTest();
        
        List<User> usersToUpdate = new List<User>();
        
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        
        User testUser2 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser2;        
        
        testUser2.oracle_region__c = 'NA';
        
        usersToUpdate.add(testUser2);
        update usersToUpdate;
        
        test.stopTest();
        
        //system.assertNotEquals(0, [SELECT COUNT() FROM CollaborationGroupMember where memberId = :testUser2.id]);
    
    }

}