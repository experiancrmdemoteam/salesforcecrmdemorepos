/**====================================================================================
 * Experian
 * Name: TimecardTriggerHandler_Test
 * Description: Tests for trigger on Timecard object
 * Created Date: Jul 13th, 2015
 * Created By: Paul Kissick
 * 
 * Modified Date        Modified By      Description of the update
 * Jan 7th, 2016        Paul Kissick     Case 01268829: Adding test for new methods
==================================================================================== */
@isTest
private class TimecardTriggerHandler_Test {

  static testMethod void testRollupsToProject() {
    User tmpTestUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert tmpTestUser;
    Project__c proj1 = Test_Utils.insertProject(false);
    ProjectTriggerHandler.enableLoeRollup = true;
    proj1.OwnerId = tmpTestUser.Id;
    proj1.Type__c = 'Delivery';
    insert proj1;
    
    ProjectTriggerHandler.calledUpdateLoe = false;
    
    Delivery_Line__c delLine1  = Test_Utils.insertDeliveryLine(false, proj1.Id);
    delLine1.Revenue__c = 1000;
    delLine1.Estimated_LOE_Hours__c = 40;
    delLine1.Service_Type__c = 'Delivery';
    insert delLine1;
    
    ProjectTriggerHandler.calledUpdateLoe = false;
    
    Test.startTest();
    
    Timecard__c tc1 = new Timecard__c(Date__c = Date.today(), Delivery_Line__c = delLine1.Id, Hours__c = 1.0, Project__c = proj1.Id, Resource__c = tmpTestUser.Id, Type__c = 'Delivery');
    insert tc1;
    
    ProjectTriggerHandler.calledUpdateLoe = false;
    
    system.assertEquals(1,[SELECT COUNT() FROM Timecard__c WHERE Project__c = :proj1.Id]);
    
    tc1.Hours__c = 1.5;
    update tc1;
    
    ProjectTriggerHandler.calledUpdateLoe = false;
    system.assertEquals(1,[SELECT COUNT() FROM Timecard__c WHERE Project__c = :proj1.Id]);
    
    delete tc1;
    
    system.assertEquals(0,[SELECT COUNT() FROM Timecard__c WHERE Project__c = :proj1.Id]);
    Test.stopTest();
    
  }
  
  static testMethod void testEditingAfterLocked() {
    
    String customBusinessUnit = 'UK&I Corp Testing';
    
    Timecard_Settings__c tcs = new Timecard_Settings__c(
      Name = customBusinessUnit,
      Locking_End_Date__c = Date.today().addDays(-30)
    );
    
    insert tcs;
    
    User tmpTestUser = Test_Utils.createUser(Constants.PROFILE_EXP_SALES_EXEC);
    tmpTestUser.Business_Unit__c = customBusinessUnit;
    insert tmpTestUser;
    
    system.runAs(tmpTestUser) {
    
	    Project__c proj1 = Test_Utils.insertProject(false);
	    ProjectTriggerHandler.enableLoeRollup = true;
	    proj1.OwnerId = UserInfo.getUserId();
	    proj1.Type__c = 'Internal';
	    insert proj1;
	    
	    ProjectTriggerHandler.calledUpdateLoe = false;
	    
	    Delivery_Line__c delLine1  = Test_Utils.insertDeliveryLine(false, proj1.Id);
	    delLine1.Revenue__c = 1000;
	    delLine1.Estimated_LOE_Hours__c = 40;
	    delLine1.Service_Type__c = 'Internal';
	    insert delLine1;
	    
	    ProjectTriggerHandler.calledUpdateLoe = false;
	    
	    Test.startTest();
	    
	    Timecard__c tc1 = new Timecard__c(
	      Date__c = Date.today(), 
	      Delivery_Line__c = delLine1.Id, 
	      Hours__c = 1.0, 
	      Project__c = proj1.Id, 
	      Resource__c = tmpTestUser.Id, 
	      Type__c = 'Internal'
	    );
	    insert tc1;
	    
	    try {
	      tc1.Date__c = Date.today().addDays(-50);
	      update tc1;
	    }
	    catch (Exception e) {
        ApexPages.Message[] messages = ApexPages.getMessages();
        system.assertNotEquals(0, messages.size());
        system.assertEquals(Label.Timecard_Locked, messages.get(0).getSummary());
      } 
      
      
	    try {
	      
	      Timecard__c tc2 = new Timecard__c(
	        Date__c = Date.today().addDays(-40), 
	        Delivery_Line__c = delLine1.Id, 
	        Hours__c = 1.0, 
	        Project__c = proj1.Id, 
	        Resource__c = tmpTestUser.Id, 
	        Type__c = 'Internal'
	      );
	      insert tc2;
	    }
	    catch (Exception e) {
	      ApexPages.Message[] messages = ApexPages.getMessages();
        system.assertNotEquals(0, messages.size());
        system.assertEquals(Label.Timecard_Locked, messages.get(0).getSummary());
	    }		  
	    
	    Test.stopTest();
	    
	    system.assertEquals(1, [SELECT COUNT() FROM Timecard__c]);
    
    }
    
  }
}