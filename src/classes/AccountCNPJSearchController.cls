/**=====================================================================
 * Appirio, Inc
 * Name: AccountCNPJSearchController
 * Description: T-421509
 * Created Date: Jul 23rd, 2015
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified      Modified By                  Description of the update
 * Jul 28th, 2015     Parul Gupta                  Modified (T-421829)
 * Jul 29th, 2015     Arpita Bose                  Updated for T-421831
 * Aug 03rd, 2015     Arpita Bose                  T-421509: updated as per the discussion
 * Aug 17th, 2015     Arpita Bose                  I-176317 
 * Sep 15th, 2015     Arpita Bose                  I-176674: Updated to show or disable Create button 
 * Sep 28th, 2015     Arpita Bose                  I-182126: Updated to fully cleared and not display create button
 * Oct 29th, 2015     Paul Kissick                 Case 01209492: Changes for Mainframe Integration
 * May 30th, 2017     Ryan (Weijie) Hu             I1106, I1095, I1096, I1060 (Experian Serasa Project) fix applied
*  =====================================================================*/
public class AccountCNPJSearchController {
  public static Account acc {get; set;}
  public static List<Account> accounts;
  public Boolean isSearchresult;
  public static List<AccountTeamMember> lstATM;
  public static Map<Id, Boolean> mapAccIdToShowEdit {get; set;}   
  public Boolean isEdit {get;set;}
  public Boolean isMainframe {get;set;}
  public Boolean allowInactiveFromMainFram {get;set;}
  
  public SerasaMainframeWSRequest.webserviceResponse serviceResults {get;set;}
  
  // get 2nd block display 
  public Boolean shouldDisplay {
    get {
      System.Debug(isSearchresult);
      return isSearchresult;
      }
  }  
  
  // CNPJNum property 
  public string cnpjNum { 
    get {
      if (cnpjNum == null)
        cnpjNum = '';
        return cnpjNum;
      }
    set;
  }
  
  // SearchResults Property
  public List<Account> searchResults {
    get {
      if (searchResults == null)
        searchResults = new List<Account>();
        return searchResults;
      }
    set;
  }
  
  // Constructor
  public AccountCNPJSearchController(ApexPages.StandardController controller){
    acc = (Account) controller.getRecord();
    isSearchresult = false;
    isEdit = false;
    allowInactiveFromMainFram = false;
  }
  
  // Method to search accounts on the basis of CNPJ Number   
  public void searchAccounts() {
    lstATM = new List<AccountTeamMember> ();
    mapAccIdToShowEdit = new Map<Id, Boolean> ();
    searchResults.clear();
    accounts = new List<Account>();    
    accounts = [SELECT Id, Name, CNPJ_Number__c, Owner.Name, Account_Type__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
               //   (SELECT Address__c, Address1__c, Address_City__c, Address_Country__c, State__c, Address_Postal_Code__c FROM Account_Address__r),
                  (Select a.UserId, a.AccountId From AccountTeamMembers a)
                  FROM Account 
                  WHERE CNPJ_Number__c = : cnpjNum];  
                  
    // User usrRec = [Select Id from User where id=:UserInfo.getUserId() LIMIT 1];              
    //lstATM = [Select a.UserId, a.AccountId From AccountTeamMember a WHERE accountId IN : accounts];
      
    for ( Account accnt: accounts ) {
      if( accnt.AccountTeamMembers.size() == 0 ) {
        mapAccIdToShowEdit.put(accnt.Id, false);
      }
      else {
        for (AccountTeamMember atm : accnt.AccountTeamMembers){ 
          system.debug('mapAccIdToShowEdit~~~'+mapAccIdToShowEdit);      
          if (atm.UserId == UserInfo.getUserId()){
            mapAccIdToShowEdit.put(atm.AccountId, true);
            break;
          }
          else {
            mapAccIdToShowEdit.put(atm.AccountId, false);
          }
        }
      }
    }
    // I-180571 :invalid standard message
    if ((!cnpjNum.isNumeric() || cnpjNum.length() != 14)) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Invalid_CNPJ_Number_Format));
      isSearchresult = false;
    }
    
    //if CNPJ_Number__c is valid and Account details does not exist, show error message   
    else if (SerasaUtilities.validateCNPJNumber(cnpjNum)) {           
      if (accounts.isEmpty()) {
        isSearchresult = true;
        isEdit = true;

        // Exception for serasa customer care profile users to create "inactive" account from mainframe
        Serasa_User_Profiles__c serasaCustomerCareProfiles = Serasa_User_Profiles__c.getall().get(Constants.SERASA_CUSTOMER_CARE_PROFILES);
        Set<String> validSeresaProfiles = new Set<String>();
        if(serasaCustomerCareProfiles != null && serasaCustomerCareProfiles.Profiles__c != null){
        for(String profileName : serasaCustomerCareProfiles.Profiles__c.split(',')) {
            // Adding the profile name and the 'Experian Serasa ' prefix back to the profile name to the set
            validSeresaProfiles.add('Experian Serasa ' + profileName.trim());
          }
        }
        
        User usrRec = [SELECT Profile.Name, Country__c,Region__c FROM User WHERE id=:UserInfo.getUserId()];

        if (validSeresaProfiles.contains(usrRec.Profile.Name)) {
          allowInactiveFromMainFram= true;
        }


        // search mainframe here...
        serviceResults = SerasaMainframeWSRequest.CallCNPJCheckWS(cnpjNum);
          
        if (serviceResults.offline) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.New_Account_Detail_Message_Offline));
        }
        else if (serviceResults.success) {
          isMainframe = true;
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.New_Account_Detail_Message));
        }
        else {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.Account_CNPJ_Search_Message+': '+serviceResults.errorMsg));
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.Account_CNPJ_Search_Not_on_Mainframe));
        }
      } 
      else {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.New_Account_Create_Message));
        searchResults = accounts;
        isSearchresult = true;
      } 
    }
    else{
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Invalid_CNPJ_Checksum_Error_Message));
      isSearchresult = false; //I-176674
    }              
  }
  
  //
  // handle the action of the commandButton
  public PageReference processButtonClick() {
    System.debug('cnpjNum==>>'+cnpjNum);
    PageReference pageRef = Page.ASS_AddOrCreateNewAccountAddress;
    // now process the variable by doing something...
    // String url = '/apex/ASS_AddOrCreateNewAccountAddress?cnpjNum='+cnpjNum; 
    // PageReference pageRef = new PageReference(url);
    pageRef.getParameters().put('cnpjNum',cnpjNum);
    pageRef.setRedirect(true);
    return pageRef;
  }
    
  // Method to clear search results
  public PageReference resetResults() {
    isEdit = false;
    isSearchresult = false; // I-182126
    isMainframe = false;
    searchResults.clear();
    serviceResults = new SerasaMainframeWSRequest.webserviceResponse();
    PageReference pg = new PageReference('/apex/AccountCNPJSearchPage');
    pg.setRedirect(false);
    return pg;
  }
   
   
  public PageReference createFromServiceClick() {
    // at this point, we'll know there is an account with some information. 
    SerasaMainframeWSRequest.cnpjResult wsAcc = serviceResults.cnpjResults[0];
    Savepoint sp = Database.setSavepoint();
    try {
      
      RecordType accType = [SELECT Id FROM RecordType WHERE DeveloperName = 'LATAM_Serasa' AND sObjectType = 'Account' LIMIT 1];
      
      Account newAccount = (Account)Account.sObjectType.newSObject(accType.Id,true);
      newAccount.Name = wsAcc.AccountName;
      newAccount.AKA__c = wsAcc.AKA;
      newAccount.CNPJ_Number__c = cnpjNum;
      newAccount.BillingStreet = wsAcc.StreetAddr1 + ' ' + wsAcc.StreetNo;
      newAccount.BillingCity = wsAcc.City;
      newAccount.BillingState = wsAcc.StateCode;
      newAccount.BillingPostalCode = wsAcc.PostalCode;
      newAccount.BillingCountry = 'Brazil'; // CNPJ number is specific to Brazil.
      newAccount.Industry = 'Other'; // Will be handled by Wallet Integration
      newAccount.Sector__c = 'To be Allocated'; // Will be handled by Wallet Integration
      
      
      Address__c newAddr = new Address__c(
        Address_1__c = wsAcc.StreetAddr1 + ' ' + wsAcc.StreetNo,
        Address_2__c = wsAcc.StreetAddr2,
        Address_3__c = wsAcc.StreetAddr3,
        City__c = wsAcc.City,
        Country__c = 'Brazil', // CNPJ number is specific to Brazil.
        Postcode__c = wsAcc.PostalCode,
        State__c = wsAcc.StateCode
      );
      insert newAccount;
      newAddr = AddressUtility.checkDuplicateAddress(newAddr);
      if (String.isEmpty(newAddr.Id)) {
        insert newAddr;
      }
      Account_Address__c newAccAdrRec = new Account_Address__c(Address_Type__c='Registered');
      newAccAdrRec.Account__c = newAccount.Id;
      newAccAdrRec.Address__c = newAddr.Id;
    
      insert newAccAdrRec;
      PageReference pr = new PageReference('/'+newAccount.Id);
      pr.setRedirect(true);
      return pr;
    }
    catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage()));
      Database.rollBack(sp);
    } 
    return null;
  }
}