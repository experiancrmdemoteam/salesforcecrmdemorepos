/*=============================================================================
 * Experian
 * Name: NominationTestHelper_Test
 * Description: Test helper class to create information for nomination tests.
 * Created Date: 17 Nov 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

@isTest
public class NominationTestHelper_Test {
  
  // For use later
  public static String giverEmail = '23oisadkjabfsakjfawken@experian.com';
  public static String managEmail = 'laksndaskldnsald92@experian.com';
  public static String coordEmail = 'ajfbahfbiebbuwee982@experian.com';
  public static String dataAdminEmail = 'sakjfnakfnniuodb2ub2@experian.com';
  public static String projSponEmail = 'kdfbaskjfbdsjifb89ru389u3@experian.com';
  
  //===========================================================================
  // Initiates a random payroll value (instead of hard coding!)
  //===========================================================================
  public static String oraclePayroll {get{
    if (oraclePayroll == null) {
      oraclePayroll = User.Oracle_Payroll_Name__c.getDescribe().getPicklistValues().get(Integer.valueOf(Math.floor(Math.random() * User.Oracle_Payroll_Name__c.getDescribe().getPicklistValues().size()))).getValue(); 
    }
    return oraclePayroll;
  }set;}
  
  static String testUserTitle = 'TEST USER RECOGNITION - 12345';
  
  //===========================================================================
  // Static maps to hold email to user 
  //===========================================================================
  public static Map<String, User> testRecipUsers {get{
    if (testRecipUsers == null) {
      testRecipUsers = new Map<String, User>();
      for (User u : [SELECT Id, Email, Name, Title
                     FROM User
                     WHERE Title = :testUserTitle]) {
        testRecipUsers.put(u.Email, u);
      }
    }
    return testRecipUsers;
  }set;}
  
  public static Map<String, User> testUsers {get{
    if (testUsers == null) {
      testUsers = new Map<String, User>();
      for (User u : [SELECT Id, Email, Name, Title
                     FROM User
                     WHERE Email IN (:giverEmail, :managEmail, :coordEmail, :dataAdminEmail, :projSponEmail)]) {
        testUsers.put(u.Email, u);
      }
    }
    return testUsers;
  }set;}
  
  public static Map<String, WorkBadgeDefinition> testBadges = new Map<String, WorkBadgeDefinition>();

  //===========================================================================
  // Create all the test users we require - creates 10 recipient users (at the moment)
  //===========================================================================  
  public static void createTestUsers() {
    
    IsDataAdmin__c ida = new IsDataAdmin__c(SetupOwnerId = UserInfo.getUserId(), IsDataAdmin__c = true);
    insert ida;
    
    List<User> recipUsers = new List<User>();
    
    // Create 10 users with which we can add nominations for
    for (Integer i = 0; i < 10; i++) {
      User rUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
      rUser.Title = testUserTitle;
	    rUser.Oracle_Payroll_Name__c = oraclePayroll;
	    rUser.Oracle_Cost_Center__c = generateCostCenter();
	    rUser.Oracle_Global_Grade__c = 'EB7-EB8';
	    rUser.Oracle_User_Person_Type__c = 'Employee';
	    recipUsers.add(rUser);
    }
    
    User giverUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    giverUser.Email = giverEmail;
    
    User managUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    managUser.Email = managEmail;
    managUser.Oracle_IsManager__c = true;
    
    User coordUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    coordUser.Email = coordEmail;
    
    User dataAdminUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    dataAdminUser.Email = dataAdminEmail;
    
    User projSponUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    projSponUser.Email = projSponEmail;
    
    insert recipUsers;
    
    insert new List<User>{giverUser, managUser, coordUser, dataAdminUser, projSponUser};
    
    // Assign the manager to all the recipients
    for (User u : recipUsers) {
      u.ManagerId = managUser.Id;
    }
    update recipUsers;
    
    testUsers.put(giverEmail, giverUser);
    testUsers.put(managEmail, managUser);
    testUsers.put(coordEmail, coordUser);
    testUsers.put(dataAdminEmail, dataAdminUser);
    testUsers.put(projSponEmail, projSponUser);
    
    for (User u : recipUsers) {
      testRecipUsers.put(u.Email, u);
    }
    
    // Finally assign permission sets to 2 users, 1 for data admin, 1 for coordinator
    List<PermissionSet> dataAdminPermSetList = [SELECT Id FROM PermissionSet WHERE Name = 'Recognition_Data_Admin' LIMIT 1];
    // Record a failure since there are no permission sets found
    if (dataAdminPermSetList.size() == 0) system.assert(false, 'Permission set matching API Name Recognition_Data_Admin not found!');
    PermissionSet psda = dataAdminPermSetList.get(0);
    List<PermissionSet> recogCoordPermSetList = [SELECT Id FROM PermissionSet WHERE Name = 'Recognition_Coordinator' LIMIT 1];
    if (recogCoordPermSetList.size() == 0) system.assert(false, 'Permission set matching API Name Recognition_Coordinator not found!');
    PermissionSet pscoord = recogCoordPermSetList.get(0);

    // Create some extra permissions for the users to check other features.
    system.runAs(new User(Id = UserInfo.getUserId())) {
      PermissionSetAssignment psa1 = new PermissionSetAssignment(
        PermissionSetId = psda.Id,
        AssigneeId = dataAdminUser.Id
      );
      PermissionSetAssignment psa2 = new PermissionSetAssignment(
        PermissionSetId = pscoord.Id,
        AssigneeId = coordUser.Id
      );
      insert new List<PermissionSetAssignment>{psa1, psa2};
    }
    
    delete ida;
  }
  
  //===========================================================================
  // Helper method to create nomination records, based on a supplied status, type
  // and number of records to create of those.
  //===========================================================================
  public static List<Nomination__c> createNominations(String nomStatus, String nomType, Integer numToCreate, Boolean isTeam) {
    
    // Check that we have users and badges to use, if not then create them.
    if (testUsers.isEmpty()) {
      createTestUsers();
    }
    if (testBadges.isEmpty()) {
      createRecognitionBadges();
    }
    
    List<Nomination__c> nomsToCreate = new List<Nomination__c>();
    
    // To insert nominations, we need to use the recognition data admin user, we created before.
    system.runAs(testUsers.get(dataAdminEmail)) {
      if (isTeam == false) {
	      for (Integer i = 0; i < numToCreate; i++) {
		      nomsToCreate.add(new Nomination__c(
		        Status__c = nomStatus,
		        Requestor__c = testUsers.get(giverEmail).Id,
		        Nominee__c = testRecipUsers.get(new List<String>(testRecipUsers.keySet()).get(Integer.valueOf(Math.floor(Math.random() * testRecipUsers.size())))).Id,
		        Badge__c = testBadges.get(getRandomRecogCategory()).Id,
		        Justification__c = nomStatus + ' ' + nomType + ':' + i,
		        Category__c = getRandomCategory(),
		        Type__c = nomType,
		        Spot_Award_Amount__c = (NominationHelper.NomConstants.get('Level2SpotAward').equals(nomType) ? Nomination__c.Spot_Award_Amount__c.getDescribe().getPicklistValues().get(Integer.valueOf(Math.floor(Math.random() * Nomination__c.Spot_Award_Amount__c.getDescribe().getPicklistValues().size()))).getValue() : null)
		      ));
        }
      }
      if (isTeam == true) {
        Nomination__c mastNom = new Nomination__c(
          Status__c = nomStatus,
          Requestor__c = testUsers.get(giverEmail).Id,
          Type__c = nomType,
          Justification__c = nomStatus + ' ' + nomType,
          Team_Name__c = 'Test Team',
          Badge__c = testBadges.get(getRandomRecogCategory()).Id,
          Category__c = getRandomCategory(),
          Project_Sponsor__c = testUsers.get(projSponEmail).Id
        );
        insert mastNom;
        for (Integer i = 0; i < numToCreate; i++) {
          Nomination__c membNom = mastNom.clone(false, true, false, false);
          membNom.Nominee__c = testRecipUsers.values().get(i).Id;
          membNom.Master_Nomination__c = mastNom.Id;
          nomsToCreate.add(membNom);
        }
      }
      insert nomsToCreate;
    }
    return nomsToCreate;
  }
  
  //===========================================================================
  // Pull out a random category (or whatever it's called) for the nomination
  //===========================================================================
  private static String getRandomCategory() {
    Integer categSize = Nomination__c.Category__c.getDescribe().getPicklistValues().size();
    return Nomination__c.Category__c.getDescribe().getPicklistValues().get(Integer.valueOf(Math.floor(Math.random() * categSize))).getValue();
  }
  
  //===========================================================================
  // Pull out a random Recognition Category for the nomination
  //===========================================================================
  private static String getRandomRecogCategory() {
    Integer categSize = Nomination__c.Recognition_Category__c.getDescribe().getPicklistValues().size();
    return Nomination__c.Recognition_Category__c.getDescribe().getPicklistValues().get(Integer.valueOf(Math.floor(Math.random() * categSize))).getValue();
  }
  
  //===========================================================================
  // Create all the badges we'll need for nominations.
  //===========================================================================
  public static void createRecognitionBadges() {
    
    IsDataAdmin__c ida = new IsDataAdmin__c(SetupOwnerId = UserInfo.getUserId(), IsDataAdmin__c = true);
    insert ida;
    
    Document badgeImage = new Document(
      FolderId = UserInfo.getUserId(),
      Name = 'BadgeImage',
      DeveloperName = 'BadgeImage',
      Body = Blob.valueOf('Test Image')
    );
    insert badgeImage;
    
    List<WorkBadgeDefinition> newBadges = new List<WorkBadgeDefinition>();
    
    for (Schema.PickListEntry recogCat : WorkBadgeDefinition.Recognition_Category__c.getDescribe().getPicklistValues()) {
      // Create 1 badge for each nomination state value
      for (Schema.PickListEntry nomState : WorkBadgeDefinition.Nomination_State__c.getDescribe().getPicklistValues()) {
	      newBadges.add(new WorkBadgeDefinition(
	        Name = recogCat.getValue() + ' - ' + nomState.getValue(),
	        IsActive = true,
	        IsCompanyWide = false,
	        Description = 'Details: '+recogCat.getValue() + ' - ' + nomState.getValue(),
	        IsLimitPerUser = false,
	        ImageUrl = badgeImage.Id,
	        Recognition_Category__c = recogCat.getValue(),
	        Nomination_State__c = nomState.getValue()
	      ));
      }
      // Create a badge with no nomination state set, which is the default shown badge.
      newBadges.add(new WorkBadgeDefinition(
	      Name = recogCat.getValue(),
	      IsActive = true,
	      IsCompanyWide = true,
	      Description = 'Details: '+recogCat.getValue(),
	      IsLimitPerUser = false,
	      ImageUrl = badgeImage.Id,
	      Recognition_Category__c = recogCat.getValue()
	    ));
    }
    
    insert newBadges;
    
    for (WorkBadgeDefinition wb : newBadges) {
      // Keep the badges in a map to use when assigning to nominations.
      if (wb.Nomination_State__c == null && wb.IsCompanyWide == true) {
        testBadges.put(wb.Recognition_Category__c, wb);
      }
    }
    
    delete ida;
    
  }
  
  //===========================================================================
  // Method to create a random cost center string of the form xxxx.xx.xxx.xxxxxx
  //===========================================================================
  private static String generateCostCenter() {
    List<String> firstParts = new List<String>{'1000','2000','3000','4000','5000'};
    List<String> secondParts = new List<String>{'GB','US','CA','FR','DE'};
    List<String> thirdParts = new List<String>{'123','234','345','456','567'};
    List<String> fourthParts = new List<String>{'123456','234567','345678','456789','567890'};
    return firstParts.get(Integer.valueOf(Math.floor(Math.random() * firstParts.size()))) + '.' + 
           secondParts.get(Integer.valueOf(Math.floor(Math.random() * secondParts.size()))) + '.' +
           thirdParts.get(Integer.valueOf(Math.floor(Math.random() * thirdParts.size()))) + '.' +
           fourthParts.get(Integer.valueOf(Math.floor(Math.random() * fourthParts.size())));
  }
   
}