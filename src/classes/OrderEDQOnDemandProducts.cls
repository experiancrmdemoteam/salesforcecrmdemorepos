/**=====================================================================
 * Appirio, Inc
 * Name: OrderEDQOnDemandProducts
 * Description: The following batch class is designed to be scheduled to run every night.
                    This class will get all Order line items and reflect on the order if at least one is an EDQ On Demand Product
 * Created Date: 7/2/2015
 * Created By: Diego Olarte (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 * Dec 3rd, 2015                Paul Kissick                 Reduced query to only what was required. Also added batch error handling
 =====================================================================*/
global class OrderEDQOnDemandProducts implements Database.Batchable<sObject> {

  global Database.Querylocator start ( Database.BatchableContext bc ) {
  
    // String query = 'select id,EDQ_On_Demand_Product__c,Order__r.Id FROM Order_Line_Item__c';
   
    return Database.getQueryLocator ([
      SELECT Id, EDQ_On_Demand_Product__c, Order__r.Id, Order__r.Has_EDQ_On_Demand_Products__c 
      FROM Order_Line_Item__c 
      WHERE EDQ_On_Demand_Product__c = true
      AND Order__r.Has_EDQ_On_Demand_Products__c = false
    ]);

  }

  global void execute (Database.BatchableContext bc, List<Order_Line_Item__c> scope) {
    
    Set<Id> orderIdSet = new Set<Id>();
    
    for(Order_Line_Item__c oli : scope) {
      orderIdSet.add(oli.Order__r.Id);
    }
    
    List<Order__c> ordersToUpdate = new List<Order__c>();
    
    for(Id orderId : orderIdSet) {
      ordersToUpdate.add(new Order__c(Id = orderId, Has_EDQ_On_Demand_Products__c = true));
    }
    
    if (ordersToUpdate!=null && ordersToUpdate.size() > 0) {
      update ordersToUpdate;
    }
  }   

  //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {
        
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'OrderEDQOnDemandProducts', true);
    
  }
    
}