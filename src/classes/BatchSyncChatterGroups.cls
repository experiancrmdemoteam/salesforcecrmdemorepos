/**=====================================================================
 * Experian
 * Name: BatchSyncChatterGroups 
 * Description: Batch to set assign chatter groups to user based on Oracle Region value
 * Created Date: 17th, July 2017
 * Created By: Mauricio Murillo
 *
 * Date Modified       Modified By          Description of the update
 =====================================================================*/
global class BatchSyncChatterGroups implements  Database.Batchable<sObject>, Database.Stateful {

  global List<String> updateErrors;

  global Database.QueryLocator start(Database.BatchableContext BC) {
  
    updateErrors = new List<String>();
    
    return Database.getQueryLocator([
      Select id, country__c, oracle_region__c, Need_to_assign_chatter_groups__c  
      from User where Need_to_assign_chatter_groups__c = true
    ]);
  
  }
  
  global void execute(Database.BatchableContext BC, list<User> users){
  
    System.debug('MAU Execute method.  Number of users: ' + users.size());
  
    //custom metadata type which contains all regions and chatter groups to be added by region
    Chatter_groups_by_region__mdt[] chatterGroups = 
        [SELECT MasterLabel, QualifiedApiName, Chatter_groups__c FROM Chatter_groups_by_region__mdt];

    // Set of distinct group names
    Set<String> chatterGroupNamesAll = new Set<String>();
    // Map to store regions and groups to add.  The value of this map is a String with all the regions 
    // separated by end of line
    Map<String, String> groupsByRegionMap = new Map<String, String>();
    // This map will contain every collaboration group with its ID
    Map<String, Id> collaborationGroupMap = new Map <String, Id>();

    //Iterate through custom metadata type to get all group names and regions
    for (Chatter_groups_by_region__mdt chatterGroup : chatterGroups) {
        groupsByRegionMap.put(chatterGroup.MasterLabel.toUpperCase(), chatterGroup.Chatter_groups__c );
        for (String groupName : chatterGroup.Chatter_groups__c.split('\n')){
           chatterGroupNamesAll.add(groupName.trim().toUpperCase());
        }       
    }
    
    //Iterate through Collaboration Groups, get all the groups which names are in custom metadata type
    for (CollaborationGroup cg : [select id, name, description from CollaborationGroup where name in :chatterGroupNamesAll]){
        collaborationGroupMap.put(cg.name.toUpperCase(), cg.id);    
    }
  
    //List of CollaborationGroupMember to insert
    List<CollaborationGroupMember> cgmList = new List<CollaborationGroupMember>();
    
    for (User userInstance : users) {
        System.debug('MAU User.. oracle region: ' + userInstance.oracle_region__c);
        if (String.isNotEmpty(userInstance.oracle_region__c)){
            
           List<String> groupsToAdd = new List<String>();
           String regionToSearch;
           
           //Since Oracle Region for Serasa Users and non-Serasa users is LATAM, it's needed to
           //diferentiate Serasa users by filtering Country=Brazil
           
           if(userInstance.country__c == 'Brazil' && (userInstance.oracle_region__c == 'LATAM' || userInstance.oracle_region__c == 'Latin America' ) ){
               //LATAM Serasa should be within custom metadata type
               regionToSearch = 'LATAM SERASA';
           }else{
               regionToSearch = userInstance.oracle_region__c.toUpperCase();           
           }
           
           //get list
           if(groupsByRegionMap.containsKey( regionToSearch )){
               groupsToAdd = groupsByRegionMap.get(regionToSearch).split('\n');
           }
           
           if (groupsToAdd.size() >  0){
               //Create a collaboration group member for every group
               for (String chatterGroup : groupsToAdd){
                   CollaborationGroupMember cgm = new CollaborationGroupMember();
                   cgm.CollaborationGroupId = collaborationGroupMap.get(chatterGroup.trim().toUpperCase());
                   cgm.CollaborationRole = 'Standard';
                   cgm.MemberId = userInstance.Id; 
                   cgm.NotificationFrequency = 'N';
                   cgmList.add(cgm);
               } //end for
               System.debug('MAU User.. assigning flag to user: ' + userInstance.Id);
               userInstance.Need_to_assign_chatter_groups__c = false;
               
           } // end if (groupsToAdd.size() >  0)
        } // end if (userInstance.oracle_region__c != '')
    }//  end for (User userInstance : newList)
    
    try {
        //insert collaboration member list;
        if (cgmList.size() >0 ){
            System.debug('MAU users to update: ' + users);
            List<Database.SaveResult> membersCreated = Database.insert(cgmList,false);
            List<Database.SaveResult> usersUpdated = Database.update(users,false);            
            
            for (Database.SaveResult sr : membersCreated) {
               if (!sr.isSuccess()) {
                   for (Database.Error err : sr.getErrors()) {
                      //filter out DUPLICATE_VALUE error, since a user could be a member of a group already
                      if (err.getStatusCode() != StatusCode.DUPLICATE_VALUE ){
                        updateErrors.add(err.getMessage());
                      }
                   }
               }
            }
            
            for (Database.SaveResult sr : usersUpdated ) {
               if (!sr.isSuccess()) {
                   for (Database.Error err : sr.getErrors()){
                      updateErrors.add(err.getMessage());
                   }
               }
            }                        
        }
    } 
    catch(DMLException e) {
      system.debug('BatchSyncChatterGroups: e = ' + e);          
    }      
  
  }  
  
  global void finish(Database.BatchableContext BC){
  
      try {
          BatchHelper bh = new BatchHelper();
          bh.checkBatch(BC.getJobId(), 'BatchSyncChatterGroups ', false);
          
          if (!updateErrors.isEmpty() || Test.isRunningTest()) {
            bh.batchHasErrors = true;
            bh.emailBody += '\nThe following errors were observed when updating records:\n';
            bh.emailBody += String.join(updateErrors,'\n');
          }
      }
      catch (Exception e) {
          system.debug(e.getMessage()); 
      }
      
      //bh.sendEmail();
  
  }  

}