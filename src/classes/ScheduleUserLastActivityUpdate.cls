/**===========================================================================
 * Experian Plc
 * Name: ScheduleUserLastActivityUpdate 
 * Description: Case #5237 - Schedule BatchUserLastActivityUpdate to run daily
 * Created Date: Jul 30th, 2014
 * Created By: James Weatherall
 * 
 * Date Modified     Modified By        Description of the update
 * Sep 7th, 2015     Paul Kissick       Fixed issue when trying to run tests for this class.
 * Apr 4, 2016       Paul Kissick       Case 01927884: Added improvements
 ==============================================================================*/
global class ScheduleUserLastActivityUpdate implements Schedulable {
  
  @testVisible private static List<User> testUsers;
 
  global void execute(SchedulableContext SC) {
    //Integer batchSize = 10; // TODO: Replace this with ScopeSizeUtility from Phase 4
    //String query = '';
    //if (Test.isRunningTest()) {
    //  query += ' LIMIT '+String.valueOf(batchSize);
    //}
    BatchUserLastActivityUpdate b = new BatchUserLastActivityUpdate();
    if (Test.isRunningTest()) {
      b.testUsers = testUsers;
    }
    Database.executeBatch(b,ScopeSizeUtility.getScopeSizeForClass('BatchUserLastActivityUpdate')); 
  }
}