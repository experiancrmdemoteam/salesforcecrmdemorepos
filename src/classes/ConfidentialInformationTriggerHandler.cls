/**=====================================================================
 * Appirio, Inc
 * Name: ConfidentialInformationTriggerHandler
 * Description: T-266931: Synching Confidential Information Share & OpportunityTeamMembers
 *                        Handler Class for ConfidentialInformationTrigger
 * Created Date: Mar 28th, 2014
 * Created By: Naresh Kr Ojha (Appirio)
 *
 * Date Modified          Modified By                  Description of the update
 * Mar 31, 2014           Nathalie Le Guay (Appirio)   rename to ConfidentialInformationTriggerHandler
 *                                                     Add brackets to one line if-statements, set tabs to 2 spaces
 * Apr 01, 2014           Naresh Kr Ojha               Getting access level from OpportunityShares bc SFDC OpportunityTeamMember
 *                                                     is buggy (cannot properly read the access level of the OTM record)
 * Apr 07,2014            Nathalie Le Guay             Removing 'with sharing'
 * Apr 08, 2014           Arpita Bose(Appirio)         T-269372: Added addError()in try-catch block
 * May 01, 2014           Arpita Bose                  Updated catch(Exception) to catch(DMLException)
 * Nov 13th, 2014         Nathalie Le Guay             I-138911: made it without sharing - with sharing caused issues with Sharing records creation (rowCause)
 * Feb 10th, 2015         Arpita Bose                  T-360092: Added method afterDelete(), afterUnDelete() and updateConInfoCountOnContract()
 * Feb 11th, 2015         Arpita Bose                  T-360092: Added method afterUpdate()
 * Feb 24th, 2015         Gaurav Kumar Chadha          T-364943: Replicated Synchronize Confidential Information Share for Account(Added syncAccountTeam method)
 * Feb 26th, 2015         Paul Kissick                 Case #570783 - Adding check for Active users only
 * Mar 12th, 2015         Gaurav Kumar Chadha          Added User.IsActive in the Soql query as test class was failing
 * May 15h,  2015         Terri Jiles                  I-165234: Modified Method createConfidentialInfoShare to include in query: WHERE User.IsActive = True 
 * Oct 17th  2016         Manoj Gopu                   W-006060: Added Confidential Information count in After Update call
 * Oct 25th, 2016         Paul Kissick                 Case 02181863: Adding bypass when adding a competitor. 
 * Mar 9th,  2017         Sanket Vaidya                Case 02329281: Added method createConfidentialInfoShareForProject(). 
 * Jun 11th, 2017         Richard Joseph               For Starts and Reseller Community project added share record createPermissionConfidential and updatePermissionConfidential 
 * Jun 30th, 2017         Mauricio Murillo             Case 02466810: Added before insert method to update ESign Contact
  * Jul 10th, 2017         Richard Joseph                Case 05963390: Added isactive filter createConfidentialInfoShareForProject
  =====================================================================*/
public without sharing /*with sharing*/ class ConfidentialInformationTriggerHandler {

  //============================================================================
  // Before Insert call
  //============================================================================
  public static void beforeInsert (List<Confidential_Information__c> newList) {
    updateESignContact(newList);
  }
  
  //============================================================================
  // After Insert call
  //============================================================================
  public static void afterInsert (Map<ID, Confidential_Information__c> newMap) {
    system.debug('*********newMap-------'+newMap);
    synchOpportunityTeam(newMap, null);
    //T-364943
    syncAccountTeam(newMap, null);
    //T-360092
    updateConInfoCountOnContract(newMap.values(), null);
    updateCICount(newMap, null);

    createConfidentialInfoShareForProject(newMap);


    //RJ Added it for Strat and resellers
    List<String> confidenceIdList = new List<String>();
      for (Confidential_Information__c confidence : newMap.values()) {
          confidenceIdList.add(confidence.Id);
      }
       if (!system.isFuture())
      ConfidentialInformationTriggerHandler.createPermissionConfidential(confidenceIdList);
  




  }

  //============================================================================
  // After Update call
  //============================================================================
  public static void afterUpdate(Map<ID, Confidential_Information__c> newMap,
                                 Map<ID, Confidential_Information__c> oldMap) {
    synchOpportunityTeam(newMap, oldMap);
    system.debug('*********newMap-------'+newMap);
    //T-364943
    syncAccountTeam(newMap, oldMap);
    //T-360092
    updateConInfoCountOnContract(newMap.values(), oldMap);
    updateCICount(newMap, oldMap); // W-006060    
    //RJ Added for Strat and Resellers 
    if (!system.isFuture())
    updatePermissionConfidential(newMap.values());
  


  }

  //============================================================================
  // T-360092: After Delete call
  //============================================================================
  public static void afterDelete(Map<ID, Confidential_Information__c> newMap, Map<ID, Confidential_Information__c> oldMap) {
    updateConInfoCountOnContract(null, oldMap);
    updateCICount(newMap, oldMap);
  }

  //============================================================================
  // T-360092: After UnDelete call
  //============================================================================
  public static void afterUnDelete(List<Confidential_Information__c> newList) {
    updateConInfoCountOnContract(newList, null);
  }

  //============================================================================
  // Synching Account team members
  //============================================================================
  public static void syncAccountTeam(Map<ID, Confidential_Information__c> newMap,
                                       Map<ID, Confidential_Information__c> oldMap) {
    Set<ID> syncAccountIDs = new Set<ID>();
    Set<ID> unSyncConfInfos = new Set<ID>();
    List<Confidential_Information__c> filteredConfInfos = new List<Confidential_Information__c>();

    // Filtering synch enabled records to process
    for (Confidential_Information__c confInfo : newMap.values()) {
      if (confINfo.Account__c!= null && confINfo.Synch_Account_Team_Members__c == true
          && (oldMap != null ? confInfo.Synch_Account_Team_Members__c != oldMap.get(confInfo.ID).Synch_Account_Team_Members__c : true)) {
        // Account ID adding to set to synch Team
        syncAccountIDs.add(confInfo.Account__c);
        filteredConfInfos.add(confInfo);
      }
      // Remove shares if Synch is false
      if (oldMap != null && confInfo.Synch_Account_Team_Members__c == false
                         && oldMap.get(confInfo.ID).Synch_Account_Team_Members__c == true) {
        unSyncConfInfos.add(confInfo.ID);
      }
    }

    // Create confidential information shares
    if (syncAccountIDs.size() > 0 && filteredConfInfos.size() > 0) {
      createConfidentialInfoShareAcc(syncAccountIDs, filteredConfInfos);
    }
    // Remove confidential information shares
    if (unSyncConfInfos.size() > 0) {
      removeConfidentialInfoShareAcc(unSyncConfInfos);
    }
  }

  //============================================================================
  // Remove confidential information shares
  //============================================================================
  public static void removeConfidentialInfoShareAcc(Set<ID> unSyncConfInfos) {
    List<Confidential_Information__Share> confInfoShares = new List<Confidential_Information__Share>();
    try {
      for (Confidential_Information__Share share : [SELECT Id
                                                    FROM Confidential_Information__Share
                                                    WHERE ParentId IN : unSyncConfInfos
                                                    AND RowCause =: Constants.ROWCAUSE_ACCOUNT_TEAM]) {
        confInfoShares.add(share);
      }
      // delete confidential information shares
      if (confInfoShares.size() > 0) {
        delete confInfoShares;
      }
    } 
    catch (DMLException ex) {
      system.debug('\n[ConfidentialsInformationTriggerHandler: removeConfidentialInfoShareAcc]: ['+ex.getMessage()+']]');
      apexLogHandler.createLogAndSave('ConfidentialsInformationTriggerHandler','removeConfidentialInfoShareAcc', ex.getStackTraceString(), ex);
      for (Integer i = 0; i < ex.getNumDml(); i++) {
        confInfoShares.get(0).addError(ex.getDMLMessage(i));
      }
    }
  }

  //============================================================================
  // Create Confidential Information shares
  //============================================================================
  public static void createConfidentialInfoShareAcc(Set<ID> syncAccountIDs, List<Confidential_Information__c> filteredConfInfos) {

    Map<String, Account> accID_accMap = new Map<String, Account>();
    Map<String, AccountShare> accIdUid_AccShareMap = new Map<String, AccountShare>();

    try {
      // Getting Account share record
      // May 28th, 2015 - TTK - I-164234 added  WHERE User.IsActive = True to query
      for (Account acc : [SELECT Id, (Select AccountId, UserOrGroupId, AccountAccessLevel, RowCause From Shares),
                                       (SELECT Id, AccountId, UserId, User.Name, AccountAccessLevel
                                        FROM AccountTeamMembers WHERE User.IsActive = True)
                                FROM Account
                                WHERE Id IN: syncAccountIDs]) {
        accID_accMap.put(acc.ID, acc);
        for (accountShare accs : acc.Shares) {
          if (!accIdUid_AccShareMap.containsKey(acc.ID + '~~' + accs.UserOrGroupId)) {
            accIdUid_AccShareMap.put(acc.ID + '~~' + accs.UserOrGroupId, accs);
          }
        }
      }

      // Creating the Confidential_Information__Share according to the AccountShare
      List<Confidential_Information__Share> confInfoShares = new List<Confidential_Information__Share>();
      Confidential_Information__Share newConfInfoShare;
      for (Confidential_Information__c confInfo : filteredConfInfos) {
        for (AccountTeamMember teamMember : accID_accMap.get(confInfo.Account__c).AccountTeamMembers) {
          String accessLevel = Constants.ACCESS_LEVEL_READ; //Default to be set.

          if (accIdUid_AccShareMap.containsKey(teamMember.AccountID + '~~' + teamMember.UserId)) {
            accessLevel = accIdUid_AccShareMap.get(teamMember.AccountID + '~~' + teamMember.UserId).AccountAccessLevel;
          }
          // All access level is not on the ConfInfoShare picklist so it will have edit.
          if (accessLevel == Constants.ACCESS_LEVEL_ALL) {
            accessLevel = Constants.ACCESS_LEVEL_EDIT;
          }

          system.debug('[ConfidentialInformationTriggerHandler:CreateShares][~~~~teamMember.UserId~~~]:'+teamMember.UserId+'--[~~~accessLevel~~~]:'+accessLevel);

          newConfInfoShare = new Confidential_Information__Share();

          newConfInfoShare.AccessLevel    = accessLevel;
          newConfInfoShare.ParentId       = confInfo.Id;
          newConfInfoShare.RowCause       = Constants.ROWCAUSE_ACCOUNT_TEAM;
          newConfInfoShare.UserOrGroupId  = teamMember.UserId;
          confInfoShares.add(newConfInfoShare);
          system.debug('\n[ConfidentialInformationTriggerHandler:CreateShares]Creating new Share: '+ newConfInfoShare);
        }
      }
      // Insert confidentials information share records
      if (!confInfoShares.isEmpty()) {
        insert confInfoShares;
      }
    } 
    catch (DMLException ex) {
      system.debug('\n[ConfidentialsInformationTriggerHandler: createConfidentialInfoShareAcc]: ['+ex.getMessage()+']]');
      apexLogHandler.createLogAndSave('ConfidentialsInformationTriggerHandler','createConfidentialInfoShareAcc', ex.getStackTraceString(), ex);
      for (Integer i=0; i < ex.getNumDml(); i++) {
        filteredConfInfos.get(0).addError(ex.getDMLMessage(i));
      }
    }
  }

  //============================================================================
  // Synching opportunity team members
  //============================================================================
  public static void synchOpportunityTeam(Map<ID, Confidential_Information__c> newMap,
                                           Map<ID, Confidential_Information__c> oldMap) {
    Set<ID> syncOpptyIDs = new Set<ID>();
    Set<ID> unSyncConfInfos = new Set<ID>();
    List<Confidential_Information__c> filteredConfInfos = new List<Confidential_Information__c>();

    // Filtering synch enabled records to process
    for (Confidential_Information__c confInfo : newMap.values()) {
      if (confINfo.Opportunity__c != null && confINfo.Synch_Opportunity_Team_Members__c == true
          && (oldMap != null ? confInfo.Synch_Opportunity_Team_Members__c != oldMap.get(confInfo.ID).Synch_Opportunity_Team_Members__c : true)) {
        // Oppty ID adding to set to synch Team
        syncOpptyIDs.add(confInfo.Opportunity__c);
        filteredConfInfos.add(confInfo);
      }
      // Remove shares if Synch is false
      if (oldMap != null && confINfo.Synch_Opportunity_Team_Members__c == false
                         && oldMap.get(confInfo.ID).Synch_Opportunity_Team_Members__c == true) {
        unSyncConfInfos.add(confInfo.ID);
      }
    }

    // Create confidential information shares
    if (syncOpptyIDs.size() > 0 && filteredConfInfos.size() > 0) {
      createConfidentialInfoShare(syncOpptyIDs, filteredConfInfos);
    }
    // Remove confidential information shares
    if (unSyncConfInfos.size() > 0) {
      removeConfidentialInfoShare(unSyncConfInfos);
    }
  }

  //============================================================================
  // Remove confidential information shares
  //============================================================================
  public static void removeConfidentialInfoShare(Set<ID> unSyncConfInfos) {
    List<Confidential_Information__Share> confInfoShares = new List<Confidential_Information__Share>();
    try {
      for (Confidential_Information__Share share : [SELECT Id
                                                    FROM Confidential_Information__Share
                                                    WHERE ParentId IN :unSyncConfInfos
                                                    AND RowCause = :Constants.ROWCAUSE_OPPTY_TEAM]) {
        confInfoShares.add(share);
      }
      // delete confidentials information shares
      if (confInfoShares.size() > 0) {
        delete confInfoShares;
      }
    } 
    catch (DMLException ex) {
      system.debug('\n[ConfidentialsInformationTriggerHandler: removeConfidentialInfoShare]: ['+ex.getMessage()+']]');
      apexLogHandler.createLogAndSave('ConfidentialsInformationTriggerHandler','removeConfidentialInfoShare', ex.getStackTraceString(), ex);
      for (Integer i=0; i < ex.getNumDml(); i++) {
        confInfoShares.get(0).addError(ex.getDMLMessage(i));
      }
    }
  }

  //============================================================================
  // Create Confidentials Information shares
  //============================================================================
  public static void createConfidentialInfoShare(Set<ID> syncOpptyIDs, List<Confidential_Information__c> filteredConfInfos) {

    Map<String, Opportunity> opptyID_opptyMap = new Map<String, Opportunity>();
    Map<String, OpportunityShare> oppIdUid_oppShareMap = new Map<String, OpportunityShare>();

    // String confInfoOpptyRT_Id = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CONFIDENTIAL_INFO, Constants.RECORDTYPE_CONF_INFO_OPPTY);

    try {
      // Getting opportunity share record
      for (Opportunity oppty : [SELECT Id, 
                                 (SELECT OpportunityId, UserOrGroupId, OpportunityAccessLevel, RowCause FROM Shares),
                                 (SELECT Id, OpportunityId, UserId, User.IsActive ,  User.Name, OpportunityAccessLevel
                                  FROM OpportunityTeamMembers)
                                FROM Opportunity
                                WHERE Id IN :syncOpptyIDs]) {
        opptyID_opptyMap.put(oppty.ID, oppty);
        for (OpportunityShare os : oppty.Shares) {
          if (!oppIdUid_oppShareMap.containsKey(oppty.ID + '~~' + os.UserOrGroupId)) {
            oppIdUid_oppShareMap.put(oppty.ID + '~~' + os.UserOrGroupId, os);
          }
        }
      }

      // Creating the Confidential_Information__Share according to the OpportunityShare
      List<Confidential_Information__Share> confInfoShares = new List<Confidential_Information__Share>();
      Confidential_Information__Share newConfInfoShare;
      for (Confidential_Information__c confInfo : filteredConfInfos) {
        for (OpportunityTeamMember teamMember : opptyID_opptyMap.get(confInfo.Opportunity__c).OpportunityTeamMembers) {
          String accessLevel = Constants.ACCESS_LEVEL_READ; //Default to be set.
          // PK: Case #570783 - Adding check for Active users only
          if (!teamMember.User.IsActive) {
            system.debug('[ConfidentialInformationTriggerHandler:CreateShares][~~~~teamMember.UserId~~~]:'+teamMember.UserId+'-- IS NOT ACTIVE');
            continue;
          }
          if (oppIdUid_oppShareMap.containsKey(teamMember.OpportunityID + '~~' + teamMember.UserId)) {
            accessLevel = oppIdUid_oppShareMap.get(teamMember.OpportunityID + '~~' + teamMember.UserId).OpportunityAccessLevel;
          }
          // All access level is not on the ConfInfoShare picklist so it will have edit.
          if (accessLevel == Constants.ACCESS_LEVEL_ALL) {
            accessLevel = Constants.ACCESS_LEVEL_EDIT;
          }

          system.debug('[ConfidentialInformationTriggerHandler:CreateShares][~~~~teamMember.UserId~~~]:'+teamMember.UserId+'--[~~~accessLevel~~~]:'+accessLevel);

          newConfInfoShare = new Confidential_Information__Share();

          newConfInfoShare.AccessLevel    = accessLevel;
          newConfInfoShare.ParentId       = confInfo.Id;
          newConfInfoShare.RowCause       = Constants.ROWCAUSE_OPPTY_TEAM;
          newConfInfoShare.UserOrGroupId  = teamMember.UserId;
          confInfoShares.add(newConfInfoShare);
          system.debug('\n[ConfidentialInformationTriggerHandler:CreateShares]Creating new Share: '+ newConfInfoShare);
        }
      }
      // Insert confidentials information share records
      if (!confInfoShares.isEmpty()) {
        insert confInfoShares;
      }
    } 
    catch (DMLException ex) {
      system.debug('\n[ConfidentialsInformationTriggerHandler: createConfidentialInfoShare]: ['+ex.getMessage()+']]');
      apexLogHandler.createLogAndSave('ConfidentialsInformationTriggerHandler','createConfidentialInfoShare', ex.getStackTraceString(), ex);
      for (Integer i=0; i < ex.getNumDml(); i++) {
        filteredConfInfos.get(0).addError(ex.getDMLMessage(i));
      }
    }
  }

  //===========================================================================
  // T-360092 : Method to count Confidential_Information records on Contract__c
  //===========================================================================
  public static void updateConInfoCountOnContract(List<Confidential_Information__c> newList, Map<ID, Confidential_Information__c> oldMap) {
    Set<Id> contractIds = new Set<Id>();
    Map<Id, Contract__c> contractMap = new Map<Id, Contract__c>();
    List<Confidential_Information__c> lstConInfoRec = new List<Confidential_Information__c>();
    List<Confidential_Information__c> conInfoList;
    Boolean isInsert = Trigger.isInsert;
    Boolean isUpdate = Trigger.isUpdate;
    Boolean isDelete = Trigger.isDelete;
    Boolean isUnDelete = Trigger.isUnDelete;

    // If we are inserting, updating, or undeleting, use the new ID values
    //gather all the Contract__c Ids from Confidential_Information__c and store in Set
    if (Trigger.isInsert || Trigger.isUpdate || Trigger.isUndelete) {
      for (Confidential_Information__c conInfo : newList) {
        if (conInfo.Contract__c != null || 
            (oldMap != null && conInfo.Contract__c != oldMap.get(conInfo.ID).Contract__c)) {
          contractIds.add(conInfo.Contract__c);
        }
      }
    }
    
    // If we are updating, Contract__c might change, so include that as well as deletes
    if (Trigger.isUpdate || Trigger.isDelete) {
      for (Confidential_Information__c conInfo : oldMap.values()) {
        if (conInfo.Contract__c != null) {
          contractIds.add(conInfo.Contract__c);
        }
      }
    }
    if (contractIds.isEmpty()) {
      return;
    }
    
    for (Id cntrctId : contractIds) {
      contractMap.put(cntrctId, new Contract__c(Id = cntrctId, Attachment_Count__c = 0));
    }

    //Query all Confidential_Information__c for all contractIds and update Attachment_Count__c
    for (Confidential_Information__c ci : [SELECT Id, Contract__c
                                           FROM Confidential_Information__c
                                           WHERE Contract__c IN :contractIds]) {
      contractMap.get(ci.Contract__c).Attachment_Count__c++;
    }

    //update count on Contract__c object
    try {
      if (!contractMap.isEmpty()) {
        update contractMap.values();
      }
    } 
    catch (DMLException e) {
      system.debug('[ConfidentialInformationTriggerHandler: updateConInfoCountOnContract] Exception: ' + e.getMessage());
      ApexLogHandler.createLogAndSave('ConfidentialInformationTriggerHandler','updateConInfoCountOnContract', e.getStackTraceString(), e);
      for (Integer i=0; i < e.getNumDml(); i++) {
        newList.get(0).addError(e.getDMLMessage(i));
      }
    }
  }

  // SHOULD BE MERGED WITH updateConInfoCountOnContract()
  //============================================================================
  // Update Opportunity field 
  //============================================================================
  public static void updateCICount(Map<ID, Confidential_Information__c> newMap, Map<ID, Confidential_Information__c> oldMap) {
    
    Set<ID> confInfoOpptyIDs = new Set<ID>();
    List<Opportunity> opptList = new List<Opportunity>();
    
    // If newMap is null then use oldMap
    if (newMap == null) {
      newMap = oldMap;    
    }
    
    for (Confidential_Information__c confInfo : newMap.values()) {
      if (confInfo.Opportunity__c != null) {
        confInfoOpptyIDs.add(confInfo.Opportunity__c);
      }
    }

    System.debug('confInfoOpptyIDs = ' + confInfoOpptyIDs);
    if (confInfoOpptyIDs.size() > 0) {

      for (Opportunity oppsWithCI : [SELECT Id, Confidential_Information_Count__c, 
                                      (SELECT Id FROM Confidential_Informations__r WHERE IsDeleted = false) 
                                     FROM Opportunity 
                                     WHERE Id IN :confInfoOpptyIDs]) {
        oppsWithCI.Confidential_Information_Count__c = oppsWithCI.Confidential_Informations__r.size();
        opptList.add(oppsWithCI);
      }
    }
    
    if (!opptList.isEmpty()) {
      OpportunityTriggerHandler.hasCheckedExitCriteria = true; // Case 02181863 - Bypass exit criteria checks
      update OpptList;
    }
  }

  //When CI is created from Project, add Project-Resources in CI_Share
  public static void createConfidentialInfoShareForProject(Map<ID, Confidential_Information__c> newMap)
  {    
    Map<Id, Id> map_CIId_To_PrjId = new Map<Id, Id>();



    for(Confidential_Information__c ci : [SELECT ID, PROJECT__C 
                                            FROM Confidential_Information__C 
                                            WHERE ID IN :newMap.keySet()])
    {
        map_CIId_To_PrjId.put(ci.Id, ci.Project__c);



    }

    Map<Id, Project__c> projMap = new Map<Id, Project__c>([SELECT Name, (SELECT Name, Resource__c, Resource__r.Name FROM Project_Resources__R where Resource__r.Isactive=true)
                                                              FROM Project__c
                                                              WHERE ID IN :map_CIId_To_PrjId.values()]);

    List<Confidential_Information__Share> listCIShare = new List<Confidential_Information__Share>();

    for(Id ciID : newMap.keySet())
    {
        Confidential_Information__c ci = newMap.get(ciID);        
        Id projectId = map_CIID_to_PrjId.get(ciId);       
        Project__c prj = projMap.get(projectId);

        
        if(prj != null)
        {           
            if(prj.project_Resources__r.size() > 0)
            {            
               for(Project_Resource__c prjRes : prj.project_Resources__r)
               {                  
                   if(ci.OwnerID != prjRes.Resource__c)
                   {

                       Confidential_Information__Share ciShare =




                              new Confidential_Information__Share(
                                  ParentId = ci.Id,
                                  UserOrGroupId = prjRes.Resource__c,
                                  AccessLevel = Constants.ACCESS_LEVEL_EDIT,
                                  RowCause = Constants.MANUAL
                                );

                        listCIShare.add(ciShare);                   
                 }
               }




            }
        }








    }


    if(listCIShare.size() > 0)
    {
      UPSERT listCIShare;
    }    
  }
  

    /*
     * INSERT Manual Sharing for confidential information 
     * for User with same acc as that confidential Information
     * This class will be called from confidential_information__c trigger after Insert
     * Author : Hay Mun Win
     *
     * @param      confidentialID      List of confidential information ID to be processed, usually from trigger_new
     * @return     None
     * @since      1.0
     */ 
     @future
    public static void createPermissionConfidential(List<String> confidentialID){
        
        Map<String, List<Confidential_Information__c>> accMapConfReseller = new Map<String, List<Confidential_Information__c>>();   //<AccId, List<Confidential_Information__c>
        Map<String, List<Confidential_Information__c>> accMapConfStrats = new Map<String, List<Confidential_Information__c>>(); //<AccId, List<Confidential_Information__c>
        Map<String, List<String>> confMapUser = new Map<String, List<String>>();    //<AccId, List<User>
        List<String> accList = new List<String>();  //List of account
        List<Confidential_Information__Share> infoShareList = new List<Confidential_Information__Share>();
        
        /*
         * Loop throught Confidential_Information__c to Map <AccId, List<Confidential_Information__c>
         * Add accountId to list
         */
        for (Confidential_Information__c confidence : [Select Id, Account__c, RecordType.Name, Document_Type__c from Confidential_Information__c where Id IN: confidentialID AND Account__c != null AND RecordType.Name = 'Community Document']) {
            
                if (confidence.Document_Type__c == 'BIS Community Document' || confidence.Document_Type__c == 'CIS Community Document') {
                    
                    if (!accMapConfReseller.containsKey(confidence.Account__c)) {
                        accMapConfReseller.put(confidence.Account__c, new List<Confidential_Information__c>());
                    }
                        
                    accMapConfReseller.get(confidence.Account__c).add(confidence);
                    accList.add(confidence.Account__c);
                    
                } else if (confidence.Account__c != null && confidence.Document_Type__c == 'Strat Clients Document') {
                    
                    if (!accMapConfStrats.containsKey(confidence.Account__c)) {
                        accMapConfStrats.put(confidence.Account__c, new List<Confidential_Information__c>());
                    }
                        
                    accMapConfStrats.get(confidence.Account__c).add(confidence);
                    accList.add(confidence.Account__c); 
                }
            
        }
        
        //perform the rest if there's value in accMapConf
        if (accList.size() > 0 ) {  
        
            // User AccId List to query User
            List<User> userList = [Select id, ContactId, Contact.AccountId, Profile.Name from User where isActive = true AND IsPortalEnabled = true AND ContactId != null AND Contact.AccountId IN: accList];
            
            // only continue is there's user
            if (userList.size() > 0 ) {             
                System.debug('user list > 0');
                // Loop throught userList to Map <AccId, List<User>              
                for(User tempUser : userList) {                 
                    System.debug('tempUser.Profile.Name ' + tempUser.Profile.Name);
                    if (tempUser.Profile.Name == 'Xperian Global Community Resellers' ) {
                        System.debug('checkpoint 1');
                        if (accMapConfReseller.get(tempUser.Contact.AccountId) != null) {
                            //you'll get a list of confidential_information__c
                            //use that to link with user and put that to the map
                            for (Confidential_Information__c conInfo : accMapConfReseller.get(tempUser.Contact.AccountId)) {
                                
                                if (!confMapUser.containsKey(conInfo.Id)) {
                                    confMapUser.put(conInfo.Id, new List<String>());
                                }
                                
                                confMapUser.get(conInfo.Id).add(tempUser.Id);
                            }
                                    
                    
                        }
                        
                    } else if (tempUser.Profile.Name == 'Xperian Global Community Strategic') {
                        System.debug('checkpoint 2');
                        if (accMapConfStrats.get(tempUser.Contact.AccountId) != null) {
                            
                            for (Confidential_Information__c conInfo : accMapConfStrats.get(tempUser.Contact.AccountId)) {
                                
                                if (!confMapUser.containsKey(conInfo.Id)) {
                                    confMapUser.put(conInfo.Id, new List<String>());
                                }
                                
                                confMapUser.get(conInfo.Id).add(tempUser.Id);
                            }
                                    
                    
                        }
                        
                        
                        
                    }   //ends else 
                        System.debug('confMapUser ' + confMapUser);
                        
                                        
                }   //Ends for  
                System.debug('end for');
                
            }
            
            
            for (String confidential : confMapUser.keySet()) {
            System.debug('confidential ' + confidential);   
                for(String indivUser : confMapUser.get(confidential)) {   

System.debug('indivUser' + indivUser);              
                    
                    Confidential_Information__Share infoShare = new Confidential_Information__Share();




                    infoShare.AccessLevel = 'Read';
                    infoShare.ParentId = confidential;
                    infoShare.UserOrGroupId =  indivUser;
                    infoShare.RowCause =  'Manual';
                    infoShareList.add(infoShare);
                    
                }
                
            }
            
            if (infoShareList.size() > 0) {
                System.debug('infoShareList > 0' + infoShareList);  
                insert infoShareList;
            }
            
        }
        
    }












    
    /*
     * Update Manual Sharing for confidential information 
     * for User with same acc as that confidential Information
     * This class will be called from confidential_information__c trigger after Update
     * Author : Hay Mun Win
     *
     * @param      trigger_new      new List of confidential_information__c inserted
     * @return     None
     * @since      1.0
     */ 
    public static void updatePermissionConfidential (List<Confidential_Information__c> trigger_new) {
        








        Map<String, String> profileTypeMap = new Map<String, String>{'BIS Community Document' => 'Xperian Global Community Resellers', 'CIS Community Document' => 'Xperian Global Community Resellers', 'Strat Clients Document' => 'Xperian Global Community Strategic'};
        List<Confidential_Information__Share> deleteList = new List<Confidential_Information__Share>();     
        List<String> finalIdList = new List<String>();  
        
        //query all the sharing rules responding to the list
                
        for (Confidential_Information__Share sharing : [Select Id, RowCause, UserOrGroupId, UserOrGroup.Profile.Name, ParentId, Parent.Document_Type__c from Confidential_Information__Share where ParentId IN: trigger_new AND RowCause = 'Manual']) {









            
            if (profileTypeMap.get(sharing.Parent.Document_Type__c) != sharing.UserOrGroup.Profile.Name) {
                deleteList.add(sharing);
                finalIdList.add(sharing.ParentId);
            }
        }
        
        if (deleteList.size() > 0) {
            delete deleteList;
        }   
        
        createPermissionConfidential(finalIdList);
        
        
    }
    

  /*****************************************************************************/
  /* Mauricio Murillo
  /* Case 02466810: Created method to update ESign Contact for all Confidential 
  /*                Information records.  ESign Field will be populated for records
  /*                associated to one opportunity, and with one contact role of 
  /*                type Commercial.
  /******************************************************************************/
  public static void updateESignContact(List<Confidential_Information__c> newList){
  
      List<Confidential_Information__c> ciRecordsToUpdate = new List<Confidential_Information__c>();
      List<Id> opportunityIds = new List<id>();
      Map<Id, Id> oppContactMap = new Map<Id, Id>();
    
      //get opportunities so we can get contact roles later
      for (Confidential_Information__c ci: newList){
          if(ci.Opportunity__c != null){
              ciRecordsToUpdate.add(ci);
              opportunityIds.add(ci.Opportunity__c);
          }
      }
      
      //get list of contact roles for all opportunities
      List<OpportunityContactRole> contactList = [select contactId, opportunityId
                                                  from OpportunityContactRole where opportunityId IN :opportunityIds and role = 'Commercial' ];
                                                  
      //go through contact list to get the first one of type Commercial                                            
      for (OpportunityContactRole ocr : contactList){      
          if(!oppContactMap.containsKey(ocr.opportunityId)){
              oppContactMap.put(ocr.opportunityId,ocr.contactId);
          }
      }                                            
      
      //set E-Sign Contact to all Confidential Information records which have an opportunity associated
      for (Confidential_Information__c ci: ciRecordsToUpdate ){
          if(oppContactMap.containsKey(ci.Opportunity__c)){
              ci.E_Sign_Contact__c = oppContactMap.get(ci.Opportunity__c);
          }
      }
  }    
    


  
}