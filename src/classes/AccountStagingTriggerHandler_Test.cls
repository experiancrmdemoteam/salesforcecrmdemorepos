/**=====================================================================
 * Experian
 * Name: AccountStagingTriggerHandler_Test
 * Description: Test Class for AccountStagingTriggerHandler
 *
 * Created Date: Dec. 15th 2016
 * Created By: James Wills for DRM Project
 * 
=====================================================================*/
@isTest
private class AccountStagingTriggerHandler_Test{
  
  
  private static testMethod void test_DQ_Processing_1(){
    List<Account_Staging__c> accStagingList = new List<Account_Staging__c>();
    Account_Staging__c accStaging1 = new Account_Staging__c(
      Action_Code__c = 2      
    );
    insert accStaging1;
        
    Test.startTest();
      accStaging1.Account_ID__c = [SELECT id, Name FROM Account WHERE Name = 'Account Matching Test' LIMIT 1].id;
      update accStaging1;      
      System.assert([SELECT id, Account_Name__c FROM Account_Staging__c WHERE id = :accStaging1.id].Account_Name__c=='Account Matching Test',
                    'The Account Staging record has not been updated with the values from the matched Account.');
      
      accStaging1.Account_ID__c = null;
      update accStaging1;
      System.assert([SELECT id, Account_Name__c FROM Account_Staging__c WHERE id = :accStaging1.id].Account_Name__c== null, 'The Account Staging records have not been cleared down.');
      
      accStaging1.Account_ID__c = [SELECT id, Name FROM Account WHERE Name  = 'Account Matching Test' LIMIT 1].id;
      accStaging1.Action_Code__c = 3;
      update accStaging1;
      
      System.assert([SELECT id FROM Transaction_Queue__c WHERE Transaction_Object_Name__c = :accStaging1.getsObjectType().getDescribe().getName()
                     AND Operation_Type__c = :Label.Action_Code_Changed_From_2_to_3]!=null, 
                     'A Transaction Queue record has not been created for the Account Staging Action Code update from 2 to 3.');
      
      accStaging1.Action_Code__c = 4;
      update accStaging1;
      
      System.assert([SELECT id FROM Transaction_Queue__c WHERE Transaction_Object_Name__c = :accStaging1.getsObjectType().getDescribe().getName()
                     AND Operation_Type__c = :Label.Action_Code_Changed_From_3_to_4]!=null, 
                     'A Transaction Queue record has not been created for the Account Staging Action Code update from 3 to 4.');
      
    Test.stopTest();
  
  }
  

  @testSetup
  private static void createData(){
    Account testAccount1 = Test_Utils.insertAccount();
    testAccount1.Name = 'Account Matching Test';
    update testAccount1; 
    
    Account testAccount2 = Test_Utils.insertAccount();
    testAccount2.Ultimate_Parent_Account__c = testAccount1.id;
    update testAccount2;
  
  }

}