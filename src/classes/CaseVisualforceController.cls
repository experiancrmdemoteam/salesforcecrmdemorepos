/*=============================================================================
 * Experian
 * Name: CaseVisualforceController
 * Description: Controller used for the VF page CISPriceChangeform to process as a form the CIS Price Changes
 * Created Date: 11/29/2016
 * Created By: Diego Olarte
 *
 * Date Modified      Modified By           Description of the update
 * 

 =============================================================================*/

public with sharing class CaseVisualforceController {
  
  ApexPages.StandardController stdCon;
  ApexPages.StandardsetController setCon;
  sObject currentRecord;
  id accID;
  
  // the associated Case assignment rules
  public Boolean UseAssignmentRules{get;set;}
  
  // the associated Case options
  public Case objCase{get;set;}
  
  // the associated Price Change Details
  public List<Price_Change_Detail__c> priceChanges;
  
  // the associated Account SubCodes Details
  public List<Account_SubCode__c> accountSubCodes;
  
  // the chosen Price Change Detail id - used when deleting a Price Change
  public Id chosenPriceChangeDetailId {get; set;}
  
  // the chosen Account SubCode Detail id - used when deleting a Price Change
  public Id chosenAccountSubCodesDetailId {get; set;}
  
  public CaseVisualforceController(ApexPages.StandardController con) {
  
    stdCon = con;    
    currentRecord = (sObject)stdCon.getRecord();
   
    objCase = [select id, AccountId,Account.Id,ContactId, Origin, Sub_Origin__c, Support_Team__c, RecordType.Name, Experian_Id__c, Owner_Change_Time_Stamp__c, Owner.Name, Status, Priority, Parent.CaseNumber, Account.Name,
                      Account_ID_18_chars__c,Opportunity__c, Contact.Name, Membership_Number__c, Account_Sales_Support_Type__c, Reason, Type, Secondary_Case_Reason__c, Subject, Description,
                      Service_Area__c, Account_Executive__c, Sales_Manager__c, Does_Pricing_apply_to_the_entire_company__c, Only_Apply_Prices_to_Certain_Subcode_s__c, Effective_Date__c,
                      Company_ID__c, Bill_Code__c, Bill_Code_Pricing__c, User_Requestor__c, Requestor_Work_Phone__c, Initial_Response_Date__c, Requestor_Email__c                                            
                 FROM Case WHERE id=:currentRecord.Id];
                 
    accID = objCase.AccountId;
  }
  
  public CaseVisualforceController(ApexPages.StandardsetController setCon) {
    }
  public Case getCase()
    {
     return (Case) stdCon.getRecord();
    }
      
   private boolean updatePriceChanges()
    {
        boolean result=true;
        if (null!=priceChanges)
           {
           List<Price_Change_Detail__c> updPCD=new List<Price_Change_Detail__c>();
              try
              {
               update priceChanges;
              }
              catch (Exception e)
              {
                 String msg=e.getMessage();
                 integer pos;
                  
                 // if its field validation, this will be added to the messages by default
                 if (-1==(pos=msg.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')))
                 {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
                 }
                  
                 result=false;
              }
           }
            
           return result;
    }  
  
  private boolean updateAccountSubCodes()
    {
        boolean result=true;
        if (null!=accountSubCodes)
           {
           List<Account_SubCode__c> updAccSC=new List<Account_SubCode__c>();
              try
              {
               update accountSubCodes;
              }
              catch (Exception e)
              {
                 String msg=e.getMessage();
                 integer pos;
                  
                 // if its field validation, this will be added to the messages by default
                 if (-1==(pos=msg.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')))
                 {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
                 }
                  
                 result=false;
              }
           }
            
           return result;
    }
  
  public PageReference saveAndExit()
    {
     boolean result=true;
     result=updatePriceChanges();
     result=updateAccountSubCodes();
      
     if (result)
     {
        // call standard controller save
        return stdCon.save();
     }
     else
     {
      return null;
     }
    }
     
    public PageReference save()
    {
    
    //Fetching the assignment rules on case
    AssignmentRule AR = new AssignmentRule();
    AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
        
    //Creating the DMLOptions for "Assign using active assignment rules" checkbox
    Database.DMLOptions dmlOpts = new Database.DMLOptions();
    dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
    
     Boolean result=true;
     PageReference pf = Page.CISPriceChangeform;
     
     if(UseAssignmentRules == true)
     {
         objCase.setOptions(dmlOpts);
         update objCase;
     }
     else
     {
         update objCase;
     }
     
     if (null!=getCase().id)
     {
      result=updatePriceChanges();
      result=updateAccountSubCodes();
     }
     else
     {      
      pf.setRedirect(true);
     }
      
     if (result)
     {
        // call standard controller save, but don't capture the return value which will redirect to view page
        stdCon.save();
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Changes saved'));
     }
       
    PageReference pf1 = new PageReference('/'+objcase.id);
    return pf1;
    
    }
  
  public void newPriceChange()
    {
       if (updatePriceChanges())
       {
          Price_Change_Detail__c PCD=new Price_Change_Detail__c(New_Price__c =0, Case__c=getCase().id);
          insert PCD;
         
          // null the price change detail list so that it is rebuilt
          priceChanges=null;
       }
    }
  
  public void deletePriceChange()
    {
       if (updatePriceChanges())
       {
          if (null!=chosenPriceChangeDetailId)
          {
             Price_Change_Detail__c PCD=new Price_Change_Detail__c(Id=chosenPriceChangeDetailId);
              delete PCD;
        
           // null the price change detail list so that it is rebuilt
              priceChanges=null;
              chosenPriceChangeDetailId=null;
          }
       }
    }
  
  public List<Price_Change_Detail__c> getPCD()
    {
       if ( (null!=getCase().id) && (priceChanges == null) )
       {
           priceChanges=[SELECT Id, Name, Case__c, Case__r.AccountId, Product_Lookup__c, Hits_No_Hits__c, New_Price__c,  
                        Product__c, CurrencyIsoCode
                         FROM Price_Change_Detail__c 
                         WHERE Case__c = : getCase().ID
                         ORDER BY CreatedDate];
       }
                           
       return priceChanges;
    }
  
  public void newAccountSubCodes()
    {
       if (updateAccountSubCodes())
       {
          
          Account_SubCode__c AccSC=new Account_SubCode__c(Case_Account__c=accID, Case__c=getCase().id);
          insert AccSC;
         
          // null the account SubCode list so that it is rebuilt
          accountSubCodes=null;
       }
    }
  
  public void deleteAccountSubCodes()
    {
       if (updateAccountSubCodes())
       {
          if (null!=chosenAccountSubCodesDetailId)
          {
             Account_SubCode__c AccSC=new Account_SubCode__c(Id=chosenAccountSubCodesDetailId);
              delete AccSC;
        
           // null the account SubCode list so that it is rebuilt
              accountSubCodes=null;
              chosenAccountSubCodesDetailId=null;
          }
       }
    }
  
  public List<Account_SubCode__c> getAccSC()
    {
       if ( (null!=getCase().id) && (accountSubCodes == null) )
       {
           accountSubCodes=[SELECT Id, Name, Case__c, Case__r.AccountId, Case_Account__c, Sub_Code__c, Sub_Code_Lookup__c, Subscriber_Code__c
                         FROM Account_SubCode__c 
                         WHERE Case__c = : getCase().ID
                         ORDER BY CreatedDate];
       }
                           
       return accountSubCodes;
    }
  
}