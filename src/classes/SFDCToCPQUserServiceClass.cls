/**=====================================================================
 * Experian
 * Name: SFDCToCPQUserServiceClass
 * Description: Service class takes inparameter as User record and calls CPQ User Administration for User Creation or Update.
 * Created Date: Aug 22nd 2014
 * Created By: Richard Joseph
 * 
 * Date Modified            Modified By                 Description of the update
 * 29th Sep 2014            Richard Joseph              Added User Region to drive CPQ Company value
 * 2nd April 2015           Richard Joseph              Added CSDA fields Bill plan user and CSDA Quote User
 * 10th April 2015          Richard Joseph              Added validation to skip null/empty CPQ User Type.
 * 26th May 2015            Paul Kissick                Fixed code formatting
 * 1st June 2015            Richard Joseph              Case # 00944115 Added logic to include 'Decision Analytics' BU also to Map it to CS company value.
 * 20th Jan 2016            Paul Kissick                Case #01815212: Adding checks for truncation on the Address Field
 * 17 June 2016             Esteban Castro              Case #02030810: Base CPQ Integration User Company on CPQ_User_Type__c
 * 09 Feb 2017              Esteban Castro              Project "CPQ Implementation - DA/BIS (UK&I)" New User Type Mapping
 * 04 Apr 2017              Esteban Castro              Case: 02297812 - Please add custom fields to CPQ to enable better management of the approver listing
 * 29 May 2017              Esteban Castro              Case: 02332539 Update CPQ Web Service Calls with CDATA tags
 =====================================================================*/

public class SFDCToCPQUserServiceClass {
  
  public static HttpResponse responseCPQ;
  
  @future(callout=true)
  public static void callCPQUserAdminSerivceAsync(Id userId){ 
    //RJ - 4/2/15 Added Global_Business_Line__c to the query.
    User userRecord = [
      SELECT Username, Title, State, PostalCode, Phone, Name, MobilePhone, LastName, IsActive, FirstName, 
             Email, Country__c, CompanyName, Country, City, CPQ_User__c, CPQ_User_Type__c, Street, Alias,
             Region__c, Global_Business_Line__c, CSDA_Create_Quote_User__c, CSDA_Bill_Plan_User__c,
             Managers_Name__c, Sales_Sub_Team__c
      FROM User
      WHERE Id = :userId
    ];  
    if(userRecord != null) {
      callCPQUserAdminSerivce(userRecord);
    }
  }
  
  public static void callCPQUserAdminSerivce(User userRecord ){
    
    CPQ_Settings__c CPQSetting = CPQ_Settings__c.getInstance('CPQ');
    system.debug('===========+=====CPQ' + CPQSetting);
    //[RJ]- Added company value to override the default Experian with region spec company values.
    String companyValue = CPQSetting.Company_Code__c;
    //RJ- Added CSDA Credit Services company logic.
    //RJ - Added logic to include 'Decision Analytics' BU also to Map it to CS company value in CPQ Case#00944115
    //if (userRecord.Global_Business_Line__c == 'Credit Services' || userRecord.Global_Business_Line__c == 'Decision Analytics') {
    //EC - Changed logic to check for CPQ User Type to assign companies instead of Global Business Line Case #02030810
    if (userRecord.CPQ_User_Type__c != Null                      // BIS AND CSDA belong to CS
        && (userRecord.CPQ_User_Type__c.contains('CSDA')
    	|| userRecord.CPQ_User_Type__c.contains('BIS'))) {
      if (userRecord.Region__c == 'UK&I') {
        companyValue = 'CS UK&I';
      }
      else if (userRecord.Region__c.contains('America')) {
        companyValue = 'CS NA';
      }
    }
    else if (userRecord.CPQ_User_Type__c != Null                 // defaulting Automotive company for Automotive
        && userRecord.CPQ_User_Type__c.contains('Automotive')) {
        companyValue = 'Automotive';
    }
    else if (userRecord.CPQ_User_Type__c != Null                 // the user types that start with UK&I are for GTM UK&I
        && (userRecord.CPQ_User_Type__c.startsWith('UK&I'))) {
        companyValue = 'GTM UK&I';
    }
    else {                                                       // EDQ will be default if no coincidences above
      if (userRecord.Region__c == 'UK&I') {
        companyValue = 'EDQ UK&I';
      }
      else if (userRecord.Region__c.contains('America')) {
        companyValue = 'EDQ NA';
      }
      else if (userRecord.Region__c == 'APAC') {
        companyValue = 'EDQ APAC';
      }
      else if (userRecord.Region__c == 'EMEA') {
        companyValue = 'EDQ EMEA';
      }
    }

    String userProperties = '<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webcominc.com/\">';
    userProperties += '<soapenv:Header/>';
    userProperties += '<soapenv:Body>';
    userProperties += '<UserAdministration xmlns="http://webcominc.com/">';
    userProperties += '<userNameAtDomain>'+CPQSetting.CPQ_API_UserName__c+'</userNameAtDomain>';
    userProperties += '<password>'+CPQSetting.CPQ_API_Access_Word__c+'</password>';
    userProperties += '<action>ADDORUPDATE</action>';
    userProperties += '<xDoc>';

    userProperties += '<USERPROPERTIES xmlns=\"\">'; 
    userProperties +='<USERNAME>'+userRecord.Username+'</USERNAME>';
    userProperties +='<PASSWORD>Password'+(Math.round(Math.random()*1000)) +'</PASSWORD>';
    userProperties +='<TITLE><![CDATA['+ (userRecord.Title ==null ? '':userRecord.Title )+']]></TITLE>';
    userProperties +='<FIRSTNAME><![CDATA['+userRecord.FirstName+']]></FIRSTNAME>';
    userProperties +='<LASTNAME><![CDATA['+userRecord.LastName+']]></LASTNAME>';
    if ((userRecord.CPQ_User_Type__c ) != null) {
      userProperties +='<TYPE><![CDATA['+userRecord.CPQ_User_Type__c+']]></TYPE>';
    }
    userProperties +='<EMAILADDRESS><![CDATA['+userRecord.email+']]></EMAILADDRESS>';
    // Case 01815212
    String addr1 = (String.isNotBlank(userRecord.Street) ? userRecord.Street : '');
    if (addr1.length() > 50) {
      addr1 = addr1.mid(0,50);
    }
    userProperties +='<ADDRESS1><![CDATA['+addr1+']]></ADDRESS1>';
    userProperties +='<ADDRESS2 />'; 
    //[RJ]- Logic to Activate or Inactivate user
    if(!userRecord.IsActive || !userRecord.CPQ_User__c) {
      userProperties +='<ACTIVE>0</ACTIVE>'; 
    }
    else {
      userProperties +='<ACTIVE>1</ACTIVE>';
    }     
    //[RJ]- Ends
    userProperties +='<CITY><![CDATA['+(userRecord.city ==null ? '':userRecord.city )+']]></CITY>';
    userProperties +='<STATE><![CDATA['+(userRecord.state==null ? '' :userRecord.state)+']]></STATE>';
    userProperties +='<ZIPCODE><![CDATA['+(userRecord.PostalCode == null ? '' :userRecord.PostalCode )+']]></ZIPCODE>';
    userProperties +='<COUNTRY><![CDATA['+(userRecord.country__c == null ? '':userRecord.country__c )+']]></COUNTRY>';
    userProperties +='<PHONENUMBER><![CDATA['+(userRecord.phone == null ? '' :userRecord.phone)+']]></PHONENUMBER>';
    userProperties +='<FAXNUMBER />';
    //[RJ] changed to acpt company code value according to User Region 
    userProperties +='<COMPANYCODE><![CDATA['+companyValue+']]></COMPANYCODE>';
    userProperties +='<MUSTCHANGEPASSWORD>'+'0'+'</MUSTCHANGEPASSWORD>';
    userProperties +='<PASSWORDLOCKED>'+'0'+'</PASSWORDLOCKED>';
    userProperties +='<ORDERINGPARENT></ORDERINGPARENT>';
    userProperties +='<MANGAGINGPARENT></MANGAGINGPARENT>';
    userProperties +='<APPROVINGPARENT></APPROVINGPARENT>';
    userProperties +='<CrmUserId>'+userRecord.id+'</CrmUserId>';
    userProperties +='<CrmName><![CDATA['+userRecord.Name+']]></CrmName>';
    userProperties +='<CrmUserName><![CDATA['+userRecord.Username+']]></CrmUserName>';
    userProperties +='<CrmPassword>'+'</CrmPassword>';
    userProperties += '<CUSTOMFIELDS>';
    //EC change for Case: 02297812
    userProperties +='<CUSTOMFIELD><NAME>Manager</NAME><VALUE><![CDATA['+(userRecord.Managers_Name__c == null ? '' : userRecord.Managers_Name__c)+']]></VALUE></CUSTOMFIELD>';
    userProperties +='<CUSTOMFIELD><NAME>Region</NAME><VALUE><![CDATA['+(userRecord.Region__c == null ? '' : userRecord.Region__c)+']]></VALUE></CUSTOMFIELD>';
    userProperties +='<CUSTOMFIELD><NAME>Sales_SubTeamRegion</NAME><VALUE><![CDATA['+(userRecord.Sales_Sub_Team__c == null ? '' : userRecord.Sales_Sub_Team__c)+']]></VALUE></CUSTOMFIELD>';
    //RJ change for CSDA fields. CSDA_Create_Quote_User__c
    userProperties +='<CUSTOMFIELD><NAME>Quote_User</NAME>'+'<VALUE>'+((userRecord.CSDA_Create_Quote_User__c)? 'Y' : 'N')+'</VALUE>'+'</CUSTOMFIELD>';
    userProperties +='<CUSTOMFIELD>'+'<NAME>Bill_Plan_User</NAME>'+'<VALUE>'+((userRecord.CSDA_Bill_Plan_User__c)? 'Y' : 'N')+'</VALUE>'+'</CUSTOMFIELD>';
    userProperties +='</CUSTOMFIELDS>';

    //userProperties +='<DefaultMarket>'+'USD'+'</DefaultMarket>';
    userProperties +='</USERPROPERTIES>';
    userProperties +='</xDoc>';
    userProperties += '</UserAdministration>';
    userProperties +='</soapenv:Body>';
    userProperties += '</soapenv:Envelope>';
    
    string elementValue = 'Not Found';
    Http h = new http();
    HttpRequest req = new HttpRequest();
    req.setEndpoint(CPQSetting.CPQ_API_Endpoint__c);
    req.setMethod('POST');
    req.setHeader('Content-Type', 'text/xml;charset=utf-8'); 
    req.setHeader('SOAPAction', '\"http://webcominc.com/UserAdministration\"');
    req.setHeader('Host', 'test.webcomcpq.com');
    req.setTimeout(12000);
    req.setBody(userProperties);
    system.debug('Req:  '+ req.getBody());
    try {
      if(!Test.isRunningtest()) {
        responseCPQ = h.send(req);
      }
      system.debug('Body was:  '+ responseCPQ);    
      if(responseCPQ != null && responseCPQ.getStatusCode() == 200) {    
        system.debug('Body was:  '+ responseCPQ);
        system.debug('String was:' + responseCPQ.getBody());
        XmlStreamReader reader = responseCPQ.getXmlStreamReader();
        reader.setNamespaceAware(true);
        while(reader.hasNext()) {
          if (reader.getEventType() == XmlTag.START_ELEMENT && 'Result' == reader.getLocalName()) {
            reader.next();
            elementValue = reader.getText();
            system.debug('The element value was:  ' + elementValue);
            if ('OK' != elementValue) {
              // error found...
              
            }
          }
          reader.next();
        }
      }   
    }
    catch(System.CalloutException e) {
      system.debug('Callout error: '+ e);
      system.debug(responseCPQ.toString());
    }
    catch (Exception e) {
      system.debug('Exception error: '+ e);
    }
  } 
}