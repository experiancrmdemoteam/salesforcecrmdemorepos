/**=====================================================================
 * Appirio, Inc
 * Name:         CheckExistingAccount_Test
 * Description:  Test Class for CheckExistingAccount
 * Created Date: July 03rd, 2015
 * Created By:   Naresh Kr Ojha
 *
 * Date             Modified By                 Description
 * Aug 24th, 2015   Naresh Kr Ojha              Updated test class to cover various scenarios.
 * Aug 25th, 2015   Naresh Kr Ojha              Updated test class to cover No Address scenario
 * Dec 3rd, 2015    Paul Kissick                Case 01266075: Removing Global_Settings__c for timings
 =====================================================================*/
@isTest
private class CheckExistingAccount_Test {
  // static members to be used in main test method
  private static CheckExistingAccount.WSAccount acc;
  private static CheckExistingAccount.WSContact cont;
  private static CheckExistingAccount.WSAddress add;

  private static CheckExistingAccount.WSResponse responseObj;
  //=====================================================================
  //Scenarios covered
  //1. New Account, New Contact, New Address created. Also creates Account Address and Contact Address
  //2. Search Account By Name
  //3. Search Account By Id
  //4. Account already created passed in request should return values
  //=====================================================================
  static testMethod void MatchingCreatingAccountsContactsWSTest() { 
    CheckExistingAccount.WSResponse response;
    createTestData();  
    Test.startTest();
      response = CheckExistingAccount.MatchingCreatingAccountsContactsWS(acc, cont, add);
    Test.stopTest();
    
    //Account created
    System.assert(response.RES_Account.SFDCAccountID != null);
    System.assertEquals(response.RES_Account.Action, 'Created');
    
    //Contact Created
    System.assert(response.RES_Contact.SFDCContactID != null);
    System.assertEquals(response.RES_Contact.Action, 'Created');

    //Address, Account Address and Contact Address created
    System.assert(response.RES_Address.SFDCAddressID != null);
    System.assertEquals(response.RES_Address.ActionAddress, 'Created');

   // System.assert(response.RES_Address.SFDCAccountAddressID != null);
   // System.assertEquals(response.RES_Address.ActionAccountAddress, 'Created');

    System.assert(response.RES_Address.SFDCContactAddressID != null);
    System.assertEquals(response.RES_Address.ActionContactAddress, 'Created');
    
    //Search by name
    acc.Name = response.RES_Account.Name;
    //Calling webservice
    response = CheckExistingAccount.MatchingCreatingAccountsContactsWS(acc, cont, add);
    //Serach by id
    acc.SFDCAccountID = response.RES_Account.SFDCAccountID;

    //Calling with existing IDs created earlier
    response = CheckExistingAccount.MatchingCreatingAccountsContactsWS(acc, cont, add);
    System.assertEquals(response.RES_Account.Action, 'Matched');
    System.assertEquals(response.RES_Contact.Action, 'Matched');
    System.assertEQuals(response.RES_Address.ActionAddress, 'Matched');
  }
  //=====================================================================
  //Scenario: No address provided it should fetch defaulted address (registered)
  //=====================================================================
  static testMethod void MatchingCreatingAccountsContactsWSTest2() { 
    CheckExistingAccount.WSResponse response;
    
    Account testAccount = Test_Utils.insertAccount();
    Address__c newAddress = Test_Utils.insertAddress(true);
    Test_Utils.insertAccountAddress(true, newAddress.ID, testAccount.ID);

    createTestData();  
    acc.SFDCAccountID = testAccount.ID;
    add = null;
    Test.startTest();
      response = CheckExistingAccount.MatchingCreatingAccountsContactsWS(acc, cont, add);
    Test.stopTest();
    
    System.assert(response.RES_Address.SFDCAddressID != null);
    System.assertEquals(response.RES_Address.ActionAddress, Constants.ACC_WS_ACTION_DEFAULT_ADDRESS);
  }
  //=====================================================================
  //Method to create test data.
  //=====================================================================
  static void createTestData() {

    Global_Settings__c settings = Global_Settings__c.getInstance(Constants.GLOBAL_SETTING);
    BatchHelper.setBatchClassTimestamp('AssignmentTeamMemberJobLastRun', system.now().addSeconds(-20));
    if( settings != null ) {
      settings.Batch_Failures_Email__c = 'test@test.com.test'; 
      update settings;
    }
    else {
      settings = new Global_Settings__c(name=Constants.GLOBAL_SETTING,Account_Team_Member_Default_Role__c= Constants.TEAM_ROLE_ACCOUNT_MANAGER);
      settings.Batch_Failures_Email__c = 'test@test.com.test'; 
      insert settings;
    }

    acc = new CheckExistingAccount.WSAccount();
    acc.Industry = 'Agriculture';
    acc.Name = 'New Test Account';
    acc.Region = 'Asia';
    acc.SaaSAccountID = 'TESTSAASID';
    acc.Sector = 'Test Sector';
    acc.CompanyRegNumber = 'TestRegNumber001';
    
    cont = new CheckExistingAccount.WSContact();
    cont.Email = 'testingWs@test.com'; 
    cont.FirstName = 'Test FirstName';
    cont.LastName = 'Test LastName';
    cont.Phone = '+019999999999';

    add = new CheckExistingAccount.WSAddress();
    add.Address1 = 'Address1Test';
    add.Address2 = 'Address2Test';
    add.City = 'TestCity';
    add.State = 'TestState';
    add.Country = 'USA';
    add.PostalCode = '12345';
  }
}