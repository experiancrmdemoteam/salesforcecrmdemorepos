/**=====================================================================
 * Appirio, Inc
 * Name: SMX_EMS_NominationProcessor_Test
 * Description: Test class for SMX_EMS_NominationProcessor 
 * Created Date: 7th Sep, 2015
 * Created By: Parul Gupta (Appirio)
 *
 * Date Modified            Modified By                  Description of the update
 * Sep 10 2015              Naresh Ojha                  Updated test method to run in sys admin user mode
 =====================================================================*/
@isTest
private class SMX_EMS_NominationProcessor_Test {
	
	public static void setTestResponseValues(Integer testCaseNum){
      if(testCaseNum == 0){
        SMX_EMS_NominationProcessor.testHttpStatusCode = 200;
        SMX_EMS_NominationProcessor.testHttpResponseXML = '<webserviceresponse><status><code>0</code><message>Success</message></status><list><ContactInfo><contactRecordId></contactRecordId><personId></personId><surveyURL>https://nexeosolutions.staging.satmetrix.com</surveyURL></ContactInfo></list></webserviceresponse>';
      }else if(testCaseNum  == 1){
        SMX_EMS_NominationProcessor.testHttpStatusCode = 200;
        SMX_EMS_NominationProcessor.testHttpResponseXML = '<webserviceresponse><status><code>0</code><message>No Send Rule is applied for the provider</message></status><list><ContactInfo><contactRecordId></contactRecordId><personId></personId></ContactInfo></list></webserviceresponse>';        
      }else if(testCaseNum  == 2){
        SMX_EMS_NominationProcessor.testHttpStatusCode = 200;
        SMX_EMS_NominationProcessor.testHttpResponseXML = '<webserviceresponse><status><code>0</code><message>provider</message></status><list><ContactInfo><contactRecordId></contactRecordId><personId></personId></ContactInfo></list></webserviceresponse>';        
      }else if(testCaseNum  == 3){
        SMX_EMS_NominationProcessor.testHttpStatusCode = 200;
        SMX_EMS_NominationProcessor.testHttpResponseXML = '<webserviceresponse><code>-1</code><description></description><row value=""></row></webserviceresponse>';        
      }else if(testCaseNum  == 4){
        SMX_EMS_NominationProcessor.testHttpStatusCode = 404;
        SMX_EMS_NominationProcessor.testHttpResponseXML = '<webserviceresponse><code>-1</code><description></description><row value=""></row></webserviceresponse>';        
      }else if(testCaseNum  == 4){
        SMX_EMS_NominationProcessor.testHttpStatusCode = 404;
        SMX_EMS_NominationProcessor.testHttpResponseXML = '<webserviceresponse><code>-1</code><description></description><row value=""></row></webserviceresponse>';        
      }      
    }

		// Test method
    static testMethod void testProcessNomination() {
      User testUser = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
      System.runAs(testUser) {	
        List<Feedback__c> lstFeedback = prepareTestData();
    	for(Integer i = 0; i<6; i++) {
      	  SMX_EMS_NominationProcessor_Test.setTestResponseValues(i);
      	  SMX_EMS_NominationProcessor.processNomination(lstFeedback[i].Name);
        }
      }
    }
    
    // Prepare test data
    static List<Feedback__c> prepareTestData()
      {
      	Global_Settings__c custSettings = Global_Settings__c.getValues('Global');
        if (custSettings == null) {
          custSettings = new Global_Settings__c(name= Constants.GLOBAL_SETTING ,Account_Team_Member_Default_Role__c= Constants.TEAM_ROLE_ACCOUNT_MANAGER);
          insert custSettings;
        }
        
        Account a = Test_Utils.createAccount();
        a.NAME='SMX Test Account';
        insert a; 
        
        Contact c = Test_Utils.createContact(a.Id); 
        c.FirstName='SMX TestFName1';
        c.LastName='SMX TestLName1';
        c.Email='this.is.a.smx.test@acmedemo.com';
        c.Phone='9999999';
        insert c;    
        c.HasOptedOutOfEmail = false;                    
        update c;
        
        Contact c1 = Test_Utils.createContact(null); 
        c1.FirstName='SMX TestFName1';
        c1.LastName='SMX TestLName1';
        c1.Phone='9999999';
        insert c1;
        
        Case cse = Test_Utils.insertCase(true, a.id); 
        
        List <Feedback__c> feedbackList = new List<Feedback__c>();
        Feedback__c feedback = new Feedback__c();
        feedback.Name = 'TESTCRM12345';
        feedback.Contact__c = c.Id; //ContactName
        feedback.DataCollectionId__c = 'SPARTASYSTEMS_126684';
        feedback.DataCollectionName__c = 'Test Survey';
        feedback.Status__c = 'Nominated';                       
        feedback.PrimaryScore__c = 9;
        feedback.PrimaryComment__c = 'Test comment';
        feedback.Status__c = 'Test Status';
        feedback.StatusDescription__c = 'Test Description';
        feedback.SurveyDetailsURL__c = '';
        feedbackList.add(feedback);  
        
        feedback = new Feedback__c();
        feedback.Name = 'TESTCRM12346';
        feedback.Contact__c = c.Id; //ContactName
        feedback.DataCollectionId__c = 'EXPERIAN_107228';
        feedback.Case__c = cse.id;
        feedback.DataCollectionName__c = 'Test Survey';
        feedback.Status__c = 'Nominated';                       
        feedback.PrimaryScore__c = 9;
        feedback.PrimaryComment__c = 'Test comment';
        feedback.Status__c = 'Test Status';
        feedback.StatusDescription__c = 'Test Description';
        feedback.SurveyDetailsURL__c = '';
        feedbackList.add(feedback);
        
        feedback = new Feedback__c();
        feedback.Name = 'TESTCRM12347';
        feedback.Contact__c = c.Id; //ContactName
        feedback.DataCollectionId__c = 'EXPERIAN_107228';
        feedback.Case__c = cse.id;
        feedback.DataCollectionName__c = 'Test Survey';
        feedback.Status__c = 'Nominated';                       
        feedback.PrimaryScore__c = 9;
        feedback.PrimaryComment__c = 'Test comment';
        feedback.Status__c = 'Test Status';
        feedback.StatusDescription__c = 'Test Description';
        feedback.SurveyDetailsURL__c = '';
        feedbackList.add(feedback);
        
        feedback = new Feedback__c();
        feedback.Name = 'TESTCRM12348';
        feedback.Contact__c = c.Id; //ContactName
        feedback.DataCollectionId__c = 'EXPERIAN_107228';
        feedback.Case__c = cse.id;
        feedback.DataCollectionName__c = 'Test Survey';
        feedback.Status__c = 'Nominated';                       
        feedback.PrimaryScore__c = 9;
        feedback.PrimaryComment__c = 'Test comment';
        feedback.Status__c = 'Test Status';
        feedback.StatusDescription__c = 'Test Description';
        feedback.SurveyDetailsURL__c = '';
        feedbackList.add(feedback);
        
        feedback = new Feedback__c();
        feedback.Name = 'TESTCRM12349';
        feedback.Contact__c = c.Id; //ContactName
        feedback.DataCollectionId__c = 'EXPERIAN_107228';
        feedback.DataCollectionName__c = 'Test Survey';
        feedback.Status__c = 'Nominated';                       
        feedback.PrimaryScore__c = 9;
        feedback.PrimaryComment__c = 'Test comment';
        feedback.Status__c = 'Test Status';
        feedback.StatusDescription__c = 'Test Description';
        feedback.SurveyDetailsURL__c = '';
        feedbackList.add(feedback);
        
        feedback = new Feedback__c();
        feedback.Name = 'TESTCRM12343';
        feedback.Contact__c = c1.Id; //ContactName
        feedback.DataCollectionId__c = 'EXPERIAN_107228';
        feedback.Case__c = cse.id;
        feedback.DataCollectionName__c = 'Test Survey';
        feedback.Status__c = 'Nominated';                       
        feedback.PrimaryScore__c = 9;
        feedback.PrimaryComment__c = 'Test comment';
        feedback.Status__c = 'Test Status';
        feedback.StatusDescription__c = 'Test Description';
        feedback.SurveyDetailsURL__c = '';
        feedbackList.add(feedback);
        
        insert feedbackList;    
        
        return feedbackList;
      }  
      
}