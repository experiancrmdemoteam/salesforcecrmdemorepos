@isTest
public with sharing class ExperianDataQuality_Contract_TriggerTest {
	public static testmethod void InsertContractTest() {
		InsertAccountTest();
		Account account = [SELECT Id FROM account limit 1];
  
		Contract contract = new Contract(AccountId = account.Id);
		insert contract;        
    }
    
    public static testmethod void InsertAccountTest() {
		Account account = new Account(Name='John Smith',BillingStreet='445 Hamilton Ave', ShippingStreet='445 Hailton');
        insert account;        
    }
}