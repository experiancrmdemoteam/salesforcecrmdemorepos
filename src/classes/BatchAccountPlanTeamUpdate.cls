/**=====================================================================
 * Experian, Inc
 * Name: BatchAccountPlanTeamUpdate
 * Description: The following batch class is designed to be scheduled to run, in the class
 *                  'ScheduleAccountPlanTeamUpdate.cls'
 *                  1. Selects all Accounts and AccountTeamMember records relating to Accounts with Account Plans.
 *                  2. Build a Map of Account_Plan_Team__c records Account Plan for all Accounts identifed by 1.
 *                  3. Find all Account_Plan_Team__c records of inactive users.
 *                  4. Find which Account_Plan_Team__c records i)   Missing
 *                                                             ii)  Updated 
 *                                                             iii) User Duplicates by Account Plan.
 *                  5. Perform necessary inserts/updates/deletes for Account_Plan_Team__c records (Account_Plan_Team__Share deletes turned off and on again, or get error).
 *                  6. Account_Plan_Team__Share deletes turned off and on again for deletes of user duplicates of Account_Plan_Team__c records of active users, or get error
 *                  7. Delete Account_Plan_Team__c records of inactive users, turning off Account_Plan_Team__Share deletes for user duplicates and back on again for the last record delete of a user.
 *                  8. Set last run date for job (although not used by this class any more).
 * Created Date: May 15th 2017 (new version)
 * Created By: James Wills
 * 
 * Date Modified                Modified By                  Description of the update
 * May 15th, 2017               James Wills                  Case 02170385: Fixed Account Plan Team Duplication issue.
 * May 19th, 2017               James Wills                  Case 02417328: Changed Batch scope to include all records for data fix.
 * May 19th, 2017               James Wills                  Case 02170385: Changed class back to process daily Account_Plan_Team__c data changes following data update.
 * May 22nd, 2017               James Wills                  Case 02420919: Added code to remove Account_Plan_Team__c records for inactive users without Account Team Member records for Account.
 *                                                                          Batch class now needs to run on ALL AccountPlanTeam records.
 =====================================================================*/
global class BatchAccountPlanTeamUpdate implements Database.Batchable<sObject>, Database.Stateful {

  private DateTime currentRunTime;
  //private DateTime lastRunTime;

  global Database.Querylocator start ( Database.Batchablecontext bc ) {
    
    currentRunTime = System.now();
    //lastRunTime    = BatchHelper.getBatchClassTimestamp('BatchAccountPlanTeamUpdate LastRun');//Case 02170385 - Changed name  
    
    //Return the AccountTeamMembers updated since the last run of the batch for Accounts that have Account Plans
    return Database.getQueryLocator([SELECT id, (SELECT UserId, User.isActive, TeamMemberRole, AccountId, isDeleted FROM AccountTeamMembers)
                                    FROM Account
                                    WHERE Id IN (SELECT Account__c FROM Account_Plan__c WHERE Account__c <> NULL)
                                    ]);
  }
  

  global void execute (Database.BatchableContext bc, List<Account> scope) {

    List<ID> affectedAccounts = new List<ID>();
    
    for(Account acc : scope){
      affectedAccounts.add(acc.Id);
    }
    
    //1. Find the Account Plan Team member records created since the last run of the batch (this is done when users manually add AccountTeamMember records)
    List<Account_Plan__c> apList = [SELECT id, Name, Account__c, 
                                   (SELECT Name, User__c, Account_Plan__c, Account_Team_Role__c FROM Account_Plan_Teams__r)
                                    FROM Account_Plan__c WHERE Account__c IN :affectedAccounts ORDER BY Account__c];
                                    
  
    //Data structure to contain details of the Account Team Members of all relevant Account Teams of all relevant Accounts
    //This is used to find missing Account Plan Team records (updates and deletes could be found from the List above)
    //NOTE - cannot perform SOQL SELECT into this data structure at this time (Summer '17, 17/05/2017)
    Map<ID, Map<ID, List<Account_Plan_Team__c>>> changedAPTs_Map = new Map<ID, Map<ID, List<Account_Plan_Team__c>>>();


    //Construct the Account Plan Team data structure
    for(Account_Plan__c ap : apList){    
      //There is an entry for the Account so add to this one.
      Map<ID, List<Account_Plan_Team__c>> apt_Map = new Map<ID, List<Account_Plan_Team__c>>();
      if(changedAPTs_Map.containsKey(ap.Account__c)){
        apt_Map = changedAPTs_Map.get(ap.Account__c);
      }
      List<Account_Plan_Team__c> aptList = new List<Account_Plan_Team__c>();
      for(Account_Plan_Team__c apt : ap.Account_Plan_Teams__r){
        aptList.add(apt);
      }
      apt_Map.put(ap.id, aptList);
      changedAPTs_Map.put(ap.Account__c, apt_Map);        
    }
    
    
    List<Account_Plan_Team__c> accountPlanTeamNewMembers      = new List<Account_Plan_Team__c>();
    List<Account_Plan_Team__c> accountPlanTeamMembersToUpdate = new List<Account_Plan_Team__c>();
    List<Account_Plan_Team__c> aptDuplicatesToDelete_List     = new List<Account_Plan_Team__c>();
    List<Account_Plan_Team__c> aptsToDelete_List              = new List<Account_Plan_Team__c>();
    Map<ID, List<Account_Plan_Team__c>> aptsToDelete_Map      = new Map<ID, List<Account_Plan_Team__c>>();
    
    for(Account acc: scope){
      //Find the Account Teams for the Account of the AccountTeamMember being looked at in this iteration
      Map<ID, List<Account_Plan_Team__c>> accountTeamsForAccount_Map = changedAPTs_Map.get(acc.Id);      
      List<AccountTeamMember> atmsForAccount_List = acc.AccountTeamMembers;
      Set<ID> usersForAccount_Set = new Set<ID>();
      for(AccountTeamMember atm : atmsForAccount_List){
        usersForAccount_Set.add(atm.UserId);
      }
      
      //Case 02420919: Find the Account_Plan_Team__c records for inactive users without Account Team Member records for Account.
      for(ID apID : accountTeamsForAccount_Map.keySet()){
        for(Account_Plan_Team__c apt : accountTeamsForAccount_Map.get(apID)){
          if(usersForAccount_Set.contains(apt.User__c)){
          } else {
            //The Account Plan Team record has no equivlent Account Team Member record so mark for deletion - this is for APT records of inactive users
            List<Account_Plan_Team__c>aptList = new List<Account_Plan_Team__c>();
            if(aptsToDelete_Map.containsKey(apt.User__c)){
              aptList = aptsToDelete_Map.get(apt.User__c);
              aptList.add(apt);                
            } else {
              aptList.add(apt);
            }
            aptsToDelete_Map.put(apt.User__c,aptList);
          }
        }
      }
      
      //Find any Account_Plan_Team__c inserts & updates required and duplicate records within Account Plans
      for(AccountTeamMember atm: atmsForAccount_List){      
        //Check an individual Account Plan for each loop iteration      
        for(ID apID : accountTeamsForAccount_Map.keySet()){
          Boolean accountPlanTeamMemberFound = false;
          for(Account_Plan_Team__c apt : accountTeamsForAccount_Map.get(apID)){
            if(atm.UserId==apt.User__c){
              if(accountPlanTeamMemberFound==false){
                accountPlanTeamMemberFound = true;
                if(atm.TeamMemberRole != apt.Account_Team_Role__c){
                  apt.Account_Team_Role__c = atm.TeamMemberRole;
                  accountPlanTeamMembersToUpdate.add(apt);
                }
                //Duplicate Account Plan Team record found - delete it
              } else if(accountPlanTeamMemberFound==true){
                aptDuplicatesToDelete_List.add(apt);   
              }
            }            
          }

          //3. We have not found an equivalent APT, so create one.
          if(accountPlanTeamMemberFound==false && atm.User.isActive){
            Account_Plan_Team__c newAPT = new Account_Plan_Team__c(
                                               Account_Plan__c      = apID,
                                               Account_Team_Role__c = atm.TeamMemberRole,     
                                               User__c              = atm.UserId
                                               );
            system.debug('DEBUG: Creating new Account Plan Team member Account_Plan__c ' + newAPT.Account_Plan__c + ' User__c: ' + newAPT.User__c);
            accountPlanTeamNewMembers.add(newAPT);   
          }
        }
      }      
    }  
    
    if(!aptsToDelete_Map.isEmpty()){
      List<Account_Plan_Team__c> aptList = new List<Account_Plan_Team__c>();
      for(ID userId : aptsToDelete_Map.keySet()){        
        for(Account_Plan_Team__c apt : aptsToDelete_Map.get(userId)){
          //Put first entry of list into list for deleting entries and their Account_Plan_Team__Share records
          if(apt==aptsToDelete_Map.get(userId)[0]){
            aptsToDelete_List.add(apt);
          //Put all other entries into duplicate deletion list (don't delete Account_Plan_Team__Share records).  These are duplicates across Account Plans.
          } else {
            aptDuplicatesToDelete_List.add(apt);
          }
        }
      }
    }
    
    
    system.debug('DEBUG: Inserts ' + accountPlanTeamNewMembers.size());
    system.debug('DEBUG: Updates ' + accountPlanTeamMembersToUpdate.size());
    system.debug('DEBUG: Deletes ' + aptsToDelete_List.size()); 
    system.debug('DEBUG: Duplicate Deletes ' + aptDuplicatesToDelete_List.size()); 
    
    if(!accountPlanTeamNewMembers.isEmpty()){
      try{
        insert accountPlanTeamNewMembers;
      }catch(DMLException e){
        System.debug('\n ****Insert Of Account_Plan_Team__c Failed. ' + e.getLineNumber() + ' Stack:' + e.getStackTraceString() );
      }
    }

    if(!accountPlanTeamMembersToUpdate.isEmpty()){
      try{
        update accountPlanTeamMembersToUpdate;
      }catch(DMLException e){
        System.debug('\n ****Update Of Account_Plan_Team__c Failed. ' + e.getLineNumber() + ' Stack:' + e.getStackTraceString() );
      }
    }


    if(!aptDuplicatesToDelete_List.isEmpty()){
      try{
        //Need to turn off deletion of related Account_Plan_Team__Share records via AccountPlanTeamTriggerHandler for duplicate Account_Plan_Team__c records or there will be an error.
        Account_Plan_TeamTriggerHandler.turnOffAccountShareDeletes = true;
        delete aptDuplicatesToDelete_List;
        Account_Plan_TeamTriggerHandler.turnOffAccountShareDeletes = false;
      }catch(DMLException e){
        System.debug('\n ****Deletion Of Account_Plan_Team__c Duplicates Failed. ' + e.getLineNumber() + ' Stack:' + e.getStackTraceString() );
      } finally {
        Account_Plan_TeamTriggerHandler.turnOffAccountShareDeletes = false;
      }
    }   
    
    if(!aptsToDelete_List.isEmpty()){
      try{
        //This will delete Account Plan Team records and their Account_Plan_Team__Share records via AccountPlanTeamTriggerHandler.
        delete aptsToDelete_List;
      }catch(DMLException e){
        System.debug('\n ****Deletion Of Account_Plan_Team__c Failed. ' + e.getLineNumber() + ' Stack:' + e.getStackTraceString() );
      }
    }
    
  }

  global void finish (Database.BatchableContext bc) {    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchAccountPlanTeamUpdate', true);    
    BatchHelper.setBatchClassTimestamp('BatchAccountPlanTeamUpdate LastRun', currentRunTime);//Case 02170385 - Changed name     
  }
  
}