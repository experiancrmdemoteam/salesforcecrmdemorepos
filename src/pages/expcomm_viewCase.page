<apex:page standardcontroller="Case" extensions="expcomm_ViewCaseExt" showHeader="false" standardStylesheets="true" sidebar="false" docType="html-5.0">
<!-- 
Page -        expcomm_viewCase
Author -      Hay Mun Win
Description - Case view page with id passed from URL for employee community-->

<html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" lang="en">
<head>
  <meta charset="utf-8" />
  <title>Employee Community</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"/> 


  <apex:stylesheet value="{!URLFOR($Resource.SLDS214, 'assets/styles/salesforce-lightning-design-system.min.css')}" />
   <apex:stylesheet value="{!URLFOR($Resource.EmployeeCommunity_MainRes_2017, 'css/community_mainstyles.css')}" />
  
        
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="{!URLFOR($Resource.EmployeeCommunity_MainRes_2017, 'script/community_script.js')}"></script>
  <script src="{!URLFOR($Resource.EmployeeCommunity_MainRes_2017, 'script/svg4everybody.js')}"></script>  
  <script>svg4everybody();</script>

  
        
</head>
<body class="slds-m-top--none">

  <!-- REQUIRED SLDS WRAPPER -->
  <div class="experianSLDS">

    <!-- Wrapper -->
<div class="wrapper slds-size--1-of-1">
    <!--Template-->
    <apex:composition template="expcomm_newTemplate">

    </apex:composition>
    <!--Template-->
    
    
    <!-- Content Wrapper -->
    <div id="top" class="right-wrapper  slds-small-size--2-of-3 slds-medium-size--3-of-5 slds-large-size--5-of-6 slds-float--right is-shrink">
    
        
        <div class="content-block">
            <!--Case Create-->
            <div class="expcase_wrapper slds-size--1-of-1 slds-p-around--large">
            <a class="expcomm_caseList" style="display:block;" href="{!$Site.Prefix}/apex/expcomm_caseList">Return to case list</a><br/>
            <div class="expcase_title slds-page-header__title"> Case Number: {!newCase.CaseNumber}</div>
                <apex:form id="expcase_form">
                <div class="expcase_col slds-size--1-of-11 slds-small-size--1-of-1 slds-medium-size--5-of-12 slds-large-size--5-of-12 slds-m-vertical--medium">             
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">Requestor</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.User_Requestor__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">Requestor Email</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.Requestor_Email__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">Requestor Work Phone</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.Requestor_Work_Phone__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">Requester BU</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.Requester_BU__c}</div>
                    </div>
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">Implementation Status</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.Implementation_Status__c}</div>
                    </div>
                </div>
                
                <div class="expcase_col slds-size--1-of-11 slds-small-size--1-of-1 slds-medium-size--5-of-12 slds-large-size--5-of-12 slds-m-vertical--medium" style="vertical-align:top;">
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">Priority</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small">{!newCase.Priority}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">Case Reason</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small">{!newCase.Reason}</div>
                    </div>
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">Secondary Case Reason</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small">{!newCase.Secondary_Case_Reason__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">Status</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small">{!newCase.Status}</div>
                    </div>
                </div>
                
                <div class="expcase_row">
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">Subject</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.Subject}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">Description</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.Description}</div>
                    </div>
                    <apex:outputPanel id="expcase_impact" rendered="{!isPlatform}">
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--1-of-1 slds-small-size--1-of-1 slds-medium-size--2-of-6 slds-large-size--2-of-5">Business Impact</div>
                        <div class="expcase_data slds-size--1-of-1 slds-small-size--1-of-1 slds-medium-size--8-of-12 slds-large-size--7-of-12">{!newCase.Business_Impact__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--1-of-1 slds-small-size--1-of-1 slds-medium-size--2-of-6 slds-large-size--2-of-5">User Impact</div>
                        <div class="expcase_data slds-size--1-of-1 slds-small-size--1-of-1 slds-medium-size--8-of-12 slds-large-size--7-of-12">{!newCase.User_Impact__c}</div>
                    </div>
                    </apex:outputPanel>
                    <!--<apex:commandButton value="Submit" action="{!Save}"/>
                    <apex:commandButton value="Cancel" action="{!Cancel}"/>-->
                </div>
                
                <apex:outputPanel id="expcase_newUser" rendered="{!NOT(isPlatform)}">
                
                <div class="expcase_col slds-size--1-of-11 slds-small-size--1-of-1 slds-medium-size--5-of-12 slds-large-size--5-of-12 slds-m-vertical--medium"> 
                   
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">New User Start Date</div>
                        <div class="expcase_data slds-size--7-of-12">
                        <apex:outputText value="{0,date,MM/dd/yyyy}">
                           <apex:param value="{!newCase.Date_Due_for_Completion__c}" />
                       </apex:outputText>
                        </div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">License Type</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.License_Type__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">User First Name</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.User_First_Name__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">User Last Name</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.User_Last_Name__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">Organization/BU</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.Organization_BU__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">User Region</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.Region__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">User Country</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.Country__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">Reports Into (in CRM hierarchy)</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.Reports_Into_in_CRM_hierarchy__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">User's Manager's Email</div>
                        <div class="expcase_data slds-size--7-of-12"><a href="mailto:{!newCase.User_s_Manager_s_Email__c}">{!newCase.User_s_Manager_s_Email__c}</a></div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">Employee Number</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.Employee_Number__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5">CPQ access required?</div>
                        <div class="expcase_data slds-size--7-of-12">{!newCase.CPQ_access_required__c}</div>
                    </div>
                    
                </div>
                
                <div class="expcase_col slds-size--1-of-11 slds-small-size--1-of-1 slds-medium-size--5-of-12 slds-large-size--5-of-12 slds-m-vertical--medium">
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">User Job Title</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small">{!newCase.User_Job_Title__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">Function</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small">{!newCase.Function__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">User Email Address</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small"><a href="mailto:{!newCase.User_Email_Address__c}">{!newCase.User_Email_Address__c}</a></div>
                    </div>
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">User Work Phone</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small">{!newCase.User_Work_Phone__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">User Mobile Phone</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small">{!newCase.User_Mobile_Phone__c}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">Case Currency</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small">{!newCase.CurrencyIsoCode}</div>
                    </div>
                    
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">User Timezone</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small">{!newCase.User_Timezone__c}</div>
                    </div>
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">User Language</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small">{!newCase.User_Language__c}</div>
                    </div>
                    <div class="expcase_inRow slds-m-vertical--medium">
                        <div class="expcase_label slds-size--2-of-5 slds-p-left--small">Similar User</div>
                        <div class="expcase_data slds-size--7-of-12 slds-p-left--small"><a href="{!$Site.Prefix}/apex/expcomm_Profile?userID={!newCase.Similar_User__c}">{!newCase.Similar_User__r.Name}</a></div>
                    </div>
                </div>
                
                
                
                </apex:outputPanel>
                
                
                    <div class="expcase_btn_container slds-p-vertical--xx-large">
                        <apex:commandLink action="{!CloneCase}" value="Clone" id="expcase_save" styleClass="expcomm_shadowbtn expcase_save slds-p-horizontal--large slds-p-vertical--medium slds-m-around--large"/>
                        <apex:commandLink action="{!CloseCase}" value="Close Case" id="expcase_cancel" rendered="{!If (CONTAINS(newCase.status, 'Closed'), false, true)}" styleClass="expcomm_shadowbtn expcase_cancel slds-p-horizontal--large slds-p-vertical--medium slds-m-around--large"/>
                    </div>
                    
                </apex:form>
                
                <div class="expcase_upload slds-clearfix  slds-m-vertical--medium">
                    <div class="expcase_subtitle slds-m-vertical--medium">Upload Files - Non Confidential</div>
                    <c:DragnDropFileUploader parentId="{!newCase.Id}" multiple="true" retUrl="{!$Site.Prefix}/apex/expcomm_viewCase?id={!newCase.Id}"/>
                </div>
                
                <div class="expcase_attach  slds-p-around--medium  slds-m-vertical--medium">
                    <div class="expcase_subtitle slds-m-vertical--medium">Attachments</div>
                    <ul class="slds-has-dividers--bottom-space">
                    <apex:repeat value="{!attachmentList}" var="attach">
                      <li class="slds-item slds-m-around--none">
                        <div class="slds-tile slds-media">
                          <div class="slds-media__figure">
                            <svg class="slds-icon" aria-hidden="true">
                              
                              <use xlink:href="{!URLFOR($Resource.SLDS214,'assets/icons/doctype-sprite/svg/symbols.svg#attachment')}"></use>
                            </svg>
                          </div>
                          <div class="slds-media__body">
                            <h3 class="slds-truncate" title="{!attach.Name}"><a href="/{!attach.id}">{!attach.Name}</a></h3>
                            <div class="slds-tile__detail slds-text-body--small">
                              <ul class="slds-list--horizontal slds-has-dividers--right">
                                <li class="slds-item slds-m-around--none"><apex:outputField value="{!attach.LastModifiedDate}"/></li>
                                
                                <li class="slds-item slds-m-around--none"><script>document.write(convertSize({!attach.BodyLength}))</script></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </li>
                     
                      
                    </apex:repeat>
                    </ul>

                </div>
                <div class="expcase_chatter slds-m-vertical--large slds-p-vertical--large">
                    <chatter:feed entityId="{!newCase.Id}"/>
                </div>
            </div>
            
                <!-- Footer -->
            
            <!--Footer - Template-->
            <apex:composition template="Community_template_footer"></apex:composition>
        </div>
    </div>
</div>


    </div>      <!--Experian Scope-->





</body>
</html>
</apex:page>