<!--
/**=====================================================================
 * Appirio, Inc;
 * Name: AccountPlanPenetration.page
 * Description: 
 * Created Date: Nov 12th, 2014
 * Created By: Appirio
 * 
 * Date Modified      Modified By                  Description of the update
 * Dec 30th, 2014     Naresh Kr Ojha               T-343511: Refactoring CSS/Labels
 * Apr 26th, 2016     James Wills                  Removed sections made redundant by Account Planning
 *                                                 Added fields from the Account_Plan__c Object.
 *                                                 This page is now called from the AccountPlanTabPanel VF Page and is known as the Other Resources tab.
 * Oct. 25th, 2016    James Wills                  Case 01983126: Added Custom Labels
*  =====================================================================*/
-->
<!--apex:page controller="AccountPlanPenetration" tabStyle="Account_Plan__c"-->

<apex:page standardController="Account_Plan__c" extensions="AccountPlanControllerExtension" tabStyle="Account_Plan__c">

<!--link rel="stylesheet" href="{!URLFOR($Resource.AccountPlanningCSS, 'css/default-style.css')}" type="text/css"/-->

<style>
  .mainTable {margin:10px;}
  .t-align-center {text-align:center;}
  .CBTableCls {background: #f2f3f3; border-collapse:collapse; 
               border:1px solid #e0e3e5; font-size: .9em;
               font-weight: bold; padding: 5px 2px 4px 5px; width:100% !important;}
   .tr-height {height:30px;}
   .td-cls {padding-top:5px; padding-left:5px; border:1px solid #e0e3e5; width:10% !important;}
   .td-cls05 {padding-top:5px; border:1px solid #e0e3e5; width:4.5% !important;}
   .td-cls06 {padding-left:5px; border:1px solid #e0e3e5; width:12% !important;}
   .td-cls07 {padding:5px; border:1px solid #e0e3e5; width:14%; }

</style>
<apex:form id="frm">

    <apex:pageMessages id="msg"/>
    <apex:pageBlock id="OtherResourcesPageBlock">
      <apex:pageBlockButtons id="detailButtons">
        <!--apex:commandButton action="{!save}" value="Save" reRender="AccountDashboardPageBlock"/-->
        <apex:commandButton action="{!saveAccountPlanOtherTab}" value="{!$Label.BTN_SAVE}" reRender="frm,currentBlock"/>
        <!--apex:commandButton action="{!Save}" value="{!$Label.ACCOUNTPLANNING_ACCPENETRATION_SAVE}" rerender="currentBlock"/-->
        <apex:commandButton action="{!cancel}" value="{!$Label.Cancel}"/>
      </apex:pageBlockButtons>
 
      <apex:pageBlock id="AccountDashboardPageBlock" title="Other Resources">
        <apex:pageBlockSection title="Account Dashboard" columns="2" collapsible="true">
          <apex:inputField value="{!Account_Plan__c.Risks__c}" />
          <apex:inputField value="{!Account_Plan__c.Expiration__c}" />
          <apex:inputField value="{!Account_Plan__c.Reference__c}" />
          <apex:inputField value="{!Account_Plan__c.Contracts__c}" />
          <apex:inputField value="{!Account_Plan__c.Executive__c}" />
          <apex:inputField value="{!Account_Plan__c.Payment_Issues__c}" />
          <apex:inputField value="{!Account_Plan__c.One_Experian__c}" />
          <apex:inputField value="{!Account_Plan__c.Stability__c}" />
          <apex:inputField value="{!Account_Plan__c.Client_Plan__c}" />
          <apex:inputField value="{!Account_Plan__c.Solutions__c}" />
          <apex:inputField value="{!Account_Plan__c.Face_to_Face__c}" />
          <apex:inputField value="{!Account_Plan__c.Value__c}" />
          <apex:inputField value="{!Account_Plan__c.Contact_Plan__c}" />
          <apex:inputField value="{!Account_Plan__c.NPS__c}" />
          <apex:inputField value="{!Account_Plan__c.Health_Status__c}" />
          <apex:inputField value="{!Account_Plan__c.Internal_Account_Team__c}" />
          <apex:inputField value="{!Account_Plan__c.Revenue__c}" />
          <apex:inputField value="{!Account_Plan__c.Industry_Expertise__c}" />
          <apex:inputField value="{!Account_Plan__c.Opportunities__c}" />
          <apex:inputField value="{!Account_Plan__c.Major_Wins__c}" />        
        </apex:pageBlockSection>          
        
        <apex:pageBlockSection title="CAPEX/OPEX" columns="2" collapsible="true">
          <apex:inputField value="{!Account_Plan__c.Experian_Capex_Total__c}"/>
          <apex:outputField value="{!Account_Plan__c.Annual_CAPEX_in_Experian_Domain__c}"/>
          <apex:inputField value="{!Account_Plan__c.Experian_CAPEX_share__c}"/>
          <apex:pageBlockSection > <apex:pageBlockSectionItem > </apex:pageBlockSectionItem> <apex:pageBlockSectionItem /> </apex:pageBlockSection>
          <apex:inputField value="{!Account_Plan__c.Experian_OPEX_Total__c}"/>
          <apex:outputField value="{!Account_Plan__c.Annual_OPEX_in_Experian_Domain__c}"/>
          <apex:inputField value="{!Account_Plan__c.Experian_OPEX_Share__c}"/>
        </apex:pageBlockSection>

    </apex:pageBlock> 
    
    <apex:pageBlock title="{!$Label.ACCOUNTPLANNING_ACCPENETRATION_CURRBUSINESS}" id="currentBlock">
    <!--apex:pageBlockSection title="{!$Label.ACCOUNTPLANNING_ACCPENETRATION_CURRBUSINESS}" columns="1" collapsible="true"-->
      <table width="100%" class="mainTable"><tr><td class="t-align-center;">
        <span>Account plan currency:&nbsp;<b>{!currencySymbolFromIso}</b></span>
        </td></tr></table>
        <table class="CBTableCls" id="CBTable">
          <tr class="tr-height">
            <td class="td-cls"><b>{!$Label.ACCOUNTPLANNING_ACCPENETRATION_CAPABILITY}</b></td>
              <apex:repeat value="{!listAPPTemp}" var="keyHeader">
                <td colspan="2" class="td-cls">
                  <b><apex:inputField value="{!keyHeader.FirstName}"/></b>
                </td>
              </apex:repeat>
            <td class="td-cls"><b>{!$Label.ACCOUNTPLANNING_ACCPENETRATION_TOTALREVENUE}</b></td>
          </tr>
          <apex:repeat value="{!mapApp}" var="key">
            <tr style="height:30px">
              <td class="td-cls">
                <apex:outputText value="{!key}" />
               </td>
          <apex:repeat value="{!mapApp[key]}" var="item">         
            <apex:variable var="colIndex" value="{!0}"/>
            <!---->
            <td class="td-cls">

            <apex:inputField value="{!item.accountPlanPenet.Annual_Revenue__c}" style="width:92%" styleClass="accRev{!colIndex}" onchange="updateTotal({!colIndex});"/>

<!--             <input type="text" style="width:100% !important;" value="{!item.accountPlanPenet.Annual_Revenue__c}" />  -->
           </td>

           <td class="td-cls05">
             <apex:inputField value="{!item.accountPlanPenet.Penetration__c}" style="width:98%"/>
           </td>
           <apex:variable var="colIndex" value="{!colIndex + 1}"/>
         </apex:repeat>
         <td class="td-cls06">
           <br />
<!--             {!currencySymbolFromIso}  -->
             <apex:outputText value="{!mapCapToTotal[key]}" />
         </td>
     </tr>
     </apex:repeat>
     <tr style="height:30px">
       <td align="right" class="td-cls07">{!$Label.ACCOUNTPLANNING_ACCPENETRATION_TOTALS}</td>
        <apex:variable var="colIndex" value="{!0}"/>
       <apex:repeat value="{!listAccBuTotal}" var="item">
       <td class="td-cls" colspan="2">
<!--          {!currencySymbolFromIso}           -->
       <apex:outputText value="{!item}"  />  <!--    styleClass="accRevTotal{!colIndex}"    I-136589 -->
       </td>
        <apex:variable var="colIndex" value="{!colIndex + 1}"/>
       </apex:repeat>
     </tr>
      </table>
    <!--/apex:pageBlockSection--> 

    </apex:pageBlock>
    
    </apex:pageBlock>
    
  </apex:form>
<!--Commented out script as causes issues with Account Plan tabs-->  
<!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"--><!--/script-->

<script>
 function updateTotal(index){
    var total = 0;
    $( ".accRev" + index).each(function(){
      total += $(this).val() != null ? parseInt($(this).val().replace(/,/g,'')) : 0;
    });
    $(".accRevTotal" + index).html((total + '').split( /(?=(?:\d{3})+(?:\.|$))/g ).join( "," ));
 }

</script>

</apex:page>