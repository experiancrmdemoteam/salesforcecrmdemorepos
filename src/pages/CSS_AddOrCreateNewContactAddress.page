<!--
/**=====================================================================
 * Appirio, Inc
 * Name: CSS_AddOrCreateNewContactAddress
 * Description: 
 * Created Date:
 * Created By: 
 * 
 * Date Modified      Modified By                  Description of the update
 * 10 April'2014      Jinesh Goyal(Appirio)        I-109889
 * Apr 24, 2014       Nathalie Le Guay (Appirio)   Update to prevent Cross-site Scripting (XSS)
 * May 19th, 2014     Nathalie Le Guay             Added page title
 * Jan 7th, 2014      Noopur Sundriyal             T-286014: Rerendering error message panel only if error (not entire page)
 * Feb 19, 2014       Noopur                       T-363891: added the logic to show the "Missing_Address__c" field
 * Jul 10th, 2015     Arpita Bose                  T-418184: Added code for deduplication on Contact
 * Jul 16th, 2015     Noopur                       T-418184: Made changes to the attributes while adding the component and changed page version as well.
 * Sep 4th, 2015      Paul Kissick                 S-291639: Improvements to the Duplication Management
 * Sep 24th, 2015     Paul Kissick                 I-181909: Fix for Buttons not rerendering properly at the top/bottom of the block
 * Oct 1st, 2015      Paul Kissick                 T-437699: Adding page action to call checkForAccount.
 * Oct 11th, 2016     Yordan Terziev (Apt Sys)     Updated to use the new ASS_QAS_Address_Lookup_v5 VF component which utilizes EDQ V5 solution.
 * Mar 14th, 2017     Ryan (Weijie) Hu             W-007090: Add support for consumer contact record type (hide account selection field, and auto populate at backend)
 * May 39th, 2017     Charlie Park                 I1105: Make name read only.
 * June 1st, 2017     Ryan (Weijie) Hu             I1124 - Consumer contact can ignore address
  =====================================================================*/
-->
<!-- 
    @author sfdcdev11981
    VF page to create new Contact with fields configured using field set.
 -->
<apex:page standardcontroller="Contact" extensions="AddOrCreateNewContactAddressController" tabstyle="Contact" title="{!$Label.CSS_Title}" action="{!checkForAccount}">
  <apex:stylesheet value="{!$Resource.custom_stylesheet}" />
  <apex:pageMessages />
  <apex:sectionHeader title="{!$ObjectType.Contact.Label}" subtitle="{!IF(action==$Label.CSS_Operation_AddContact,$Label.CSS_Button_Add_Contact,IF(action==$Label.CSS_Operation_AddAddress,$Label.ASS_Button_Add_Address,IF(action==$Label.CSS_Operation_NewContactAddress,$Label.CSS_Button_New_Contact,'')))}" />
  <apex:actionStatus id="status" stopText="">
    <apex:facet name="start">
      <div>
        <div class="popupBackground" ></div>
        <div class="popupPanel">
          <table border="0" width="100%" height="100%">
            <tr>
              <td align="center" style="font-family: Calibri; font-size: 13px; font-weight: normal;"><b>{!$Label.ASS_Wait_searching}</b></td>
            </tr>
            <tr>
              <td align="center"><img src="/img/loading.gif"/></td>
            </tr>
          </table>
        </div>
      </div>
    </apex:facet>
  </apex:actionStatus>
  <apex:form id="showDupeBlock">
    <apex:pageBlock title="{!$Label.ASS_Title_Duplicate_Records}" rendered="{!hasDuplicateResult}">
      <apex:pageBlockTable value="{!duplicateContacts}" var="cont">
        <apex:column headerValue="{!$ObjectType.Contact.fields.Name.label}" >
          <apex:outputLink value="/{!cont.Id}" target="_blank">{!cont.Name}</apex:outputLink>
        </apex:column>
        <apex:column headerValue="{!$Label.ASS_Name}">
          <apex:outputLink value="/{!cont.AccountId}" target="_blank">{!cont.Account.Name}</apex:outputLink>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Contact.fields.LastModifiedDate.label}" value="{!cont.LastModifiedDate}" />
        <apex:column headerValue="{!$ObjectType.Contact.fields.Email.label}" value="{!cont.Email}" />
        <apex:column headerValue="{!$Label.ASS_Street}" value="{!cont.MailingStreet}" />
        <apex:column headerValue="{!$Label.ASS_City}" value="{!cont.MailingCity}" />
        <apex:column headerValue="{!$Label.ASS_State}" value="{!cont.MailingState}" />
        <apex:column headerValue="{!$Label.ASS_Country}" value="{!cont.MailingCountry}" />
        <apex:column headerValue="{!$Label.ASS_ZipCode}" value="{!cont.MailingPostalCode}" />
        <apex:column headerValue="{!$ObjectType.Contact.fields.CPF__c.label}" value="{!cont.CPF__c}" />
      </apex:pageBlockTable> 
    </apex:pageBlock>
  </apex:form>
  <apex:form id="pageForm">  
    <apex:pageMessages id="errorsMsg"/>   
    <!-- Add New Contact page block section -->
    <apex:pageBlock title="{!$ObjectType.Contact.Label}" mode="{!if(enableEditMode,'edit','')}" id="accPageBlock">
      <!-- I-181909: Adding 2 button blocks (top/bottom) for both to rerender. If set to 'both' only the top rerenders -->
      <apex:pageBlockButtons location="top">
        <apex:outputPanel id="pageBlockButtonsPanelTop">
          <apex:commandButton action="{!performSave}" value="{!$Label.OCL_Button_Save}" styleClass="btn" status="status" rerender="errorsMsg,showDupeBlock,pageBlockButtonsPanelTop,pageBlockButtonsPanelBottom"  rendered="{!NOT(saveButtonOverride)}"/>
          <apex:commandButton action="{!performSaveAnyway}" value="{!$Label.BTN_SAVE_Ignore_Alert}" styleClass="btn" status="status" rerender="errorsMsg,showDupeBlock,pageBlockButtonsPanelTop,pageBlockButtonsPanelBottom"  rendered="{!saveButtonOverride}"/>
          <apex:commandButton action="{!cancel}" value="{!$Label.OCL_Button_Cancel}" styleClass="btn" status="status" immediate="true" />
        </apex:outputPanel>
      </apex:pageBlockButtons>
      <apex:pageBlockButtons location="bottom" >
        <apex:outputPanel id="pageBlockButtonsPanelBottom">
        <apex:commandButton action="{!performSave}" value="{!$Label.OCL_Button_Save}" styleClass="btn" status="status" rerender="errorsMsg,showDupeBlock,pageBlockButtonsPanelTop,pageBlockButtonsPanelBottom"  rendered="{!NOT(saveButtonOverride)}"/>
        <apex:commandButton action="{!performSaveAnyway}" value="{!$Label.BTN_SAVE_Ignore_Alert}" styleClass="btn" status="status" rerender="errorsMsg,showDupeBlock,pageBlockButtonsPanelTop,pageBlockButtonsPanelBottom"  rendered="{!saveButtonOverride}"/>
        <apex:commandButton action="{!cancel}" value="{!$Label.OCL_Button_Cancel}" styleClass="btn" status="status" immediate="true" />
        </apex:outputPanel>
      </apex:pageBlockButtons>
      <apex:pageBlockSection collapsible="false" columns="2" title="{!$Label.CSS_Title_Contact_Information}" >
        <apex:repeat value="{!IF((isConsumerContact == true), $ObjectType.Contact.FieldSets.Serasa_Consumer_Contact_Field_Set, $ObjectType.Contact.FieldSets.ContactInfoSectionFieldSet)}" var="f">
          <apex:inputField value="{!contact[f]}" rendered="{!AND(IF(enableEditMode,IF(f =='Missing_Address__c',IF(bypassQAS,true,false),true),false), OR(isSerasaUser == false, AND(f != 'FirstName', f != 'LastName', f != 'BirthDate')))}" required="{!OR(f.required, f.dbrequired)}" styleClass="inputForm" />
          <apex:outputField value="{!contact[f]}" rendered="{!OR(!enableEditMode, AND(isSerasaUser == true, OR(f == 'FirstName', f == 'LastName', f == 'BirthDate')))}"/>
        </apex:repeat>
      </apex:pageBlockSection>
      <!--commented by JG on 13 March'2014 for T-251967 
      <c:ASS_QAS_Address_Lookup addressRec="{!address}" contactAddressRec="{!contactAddress}" enableManualSelection="{!enableManualAddressSelection}" isAddressPopupOnload="{!isAddressPopupOnload}" buttonId="{!$Component.pageForm:accPageBlock:pButtons.save}"/>-->
      <!--the address component-->
      <apex:outputPanel id="addressLookUpComponent">
        <c:ASS_QAS_Address_Lookup_v5 addressRec="{!address}" 
          contactAddressRec="{!contactAddress}" 
          enableManualSelection="{!enableManualAddressSelection}" 
          isAddressPopupOnload="{!isAddressPopupOnload}"
          accountId="{!accountId}" 
          bypassQAS="{!bypassQAS}"
          isConsumerContact="{!isConsumerContact}"
          ignoreAddress="{!ignoreAddressForConsumerContact}"
        />
        <!--  buttonId="{!$Component.pageForm:accPageBlock:pButtons.save}"  -->
      </apex:outputPanel>
      <!--action function to update the address Id (for the address selected) coming from the address component on the page-->
      <apex:actionFunction name="updateAddressId" action="{!updateAddress}" status="status" immediate="true" rerender="">
        <apex:param name="firstParam" assignTo="{!addressIdFrmComponent}" value="" />
      </apex:actionFunction>

      <!--added by JG on 10 April'2014 for I-109889-->
      <!--action function to update the account Id (for the account selected) for the account selected on the page-->
      <apex:actionFunction name="updateAccountId" action="{!accountUpdateId}" rerender="addressLookUpComponent" status="status" immediate="true"> 
        <apex:param name="firstParam" assignTo="{!accountName}" value="" />
      </apex:actionFunction>
      <!-- <apex:actionFunction action="{!performSave}" name="saveAF" rerender="errorsMsg"/> -->
    </apex:pageBlock>
  </apex:form>
  <script type="text/javascript">
  //added by JG on 10 April'2014 for I-109889-->
  window.onload = function () {
    if('{!$CurrentPage.parameters.accId}' == '') {
      var labelsInForm = document.getElementsByTagName('Label');
      var idAccountPageElement;
      for(count = 0; count < labelsInForm.length; ++count) {
        if (labelsInForm[count].innerHTML.toLowerCase().indexOf('account name') != -1) {
          idAccountPageElement = labelsInForm[count].getAttribute("for");
        }
      }
      document.getElementById(idAccountPageElement).onchange=function(){
        updateAccountId (this.value.trim());
      }
    }
  };
  </script>
</apex:page>