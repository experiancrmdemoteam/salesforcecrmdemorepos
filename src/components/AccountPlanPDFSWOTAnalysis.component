<!--
/**=====================================================================
 * Name: AccountPlanPDFSWOTAnalysis
 * Description: 
 * Created Date: Jan. 25th, 2016
 * Created By: James Wills
 * 
 * Date Modified         Modified By            Description of the update
 * Jan. 25th, 2017       James Wills            Created.
 * =====================================================================*/
-->
<apex:component controller="AccountPlanPDFSWOTAnalysisController">
   
   <!-- SWOT Analysis on PDF -->
   <table class="table-bordered" style="width:94%;border-spacing: 0 !important;margin-top:10px;">
      <tr class="header"><td colspan="3" style="padding-left:0px !important;"> <div class="title">{!$Label.ACCOUNTPLANNING_PDF_Experian_SWOT_Analysis}</div></td></tr>
         <tr>
            <td width="50%" style="font-size: 13px; font-weight: bold;text-align: left;padding-left : 10px;vertical-align:top;">
                  <!-- Strengths starts here -->
               <apex:outputPanel styleClass="swot oHidden" layout="block">
                  <apex:outputPanel styleClass="pull-left box greenTheme" style="border:0px;"
                     layout="block">
                     <h4>{!$Label.ACCOUNTPLANNING_PDF_Strengths}</h4>
                     <!-- UL in here -->
                     <ul class="oHidden" id="strengthsId" style="height:auto;padding-bottom:0px;">
                        <apex:repeat value="{!strengthAccPlanSWOTs}" var="accPlanSwot"                        
                           rendered="{!strengthAccPlanSWOTS.size>0}">
                           
                           <apex:variable var="chk" value="" rendered="{!!ISNULL(accPlanSwot.Id)}">
                           <li class="pull-left window" style="height:175px;margin:3px;margin-bottom:5px;">
                              <apex:outputPanel styleClass="description" layout="block" style="height:48%">
                                 <strong>{!$Label.ACCOUNTPLANNING_PDF_Description}: </strong>
                                 <apex:outputText value="{!accPlanSwot.Description__c}"
                                    id="strengthDesc" />
                              </apex:outputPanel>
                              <apex:outputPanel layout="block" style="height: 22%;font-weight: normal;overflow: hidden;">
                                 <strong> {!$Label.ACCOUNTPLANNING_SWOT_IMPACT}: </strong>
                                 <apex:outputPanel styleClass="value"> 
                                    <apex:outputText value="{!accPlanSwot.Impact__c}"
                                       id="strengthImpact" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                              <apex:outputPanel styleClass="importance" layout="block" style="height:12%">
                                 <strong> {!$Label.ACCOUNTPLANNING_PDF_Importance}: </strong>
                                 <apex:outputPanel styleClass="value"> 
                                    <apex:outputText value="{!accPlanSwot.Importance__c}/10"
                                       id="strengthImportance" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                           </li>
                           </apex:variable>
                        </apex:repeat>
                     </ul>
                  </apex:outputPanel>
                  <!-- Strengths end here -->
               </apex:outputPanel>
            </td>
            <td width="50%" style="font-size: 13px; font-weight: bold;text-align: left;vertical-align:top;">
                  <!-- Weaknesses starts here -->
                <apex:outputPanel styleClass="swot oHidden" layout="block">
                  <apex:outputPanel styleClass="pull-left box redTheme" style="border:0px;"
                     layout="block">
                     <h4>{!$Label.ACCOUNTPLANNING_PDF_Weaknesses}</h4>
                     <!-- UL in here -->
                     <ul class="oHidden" id="strengthsId" style="height:auto;padding-bottom:0px;">
                        <apex:repeat value="{!weaknessAccPlanSWOTs}" var="accPlanSwot">
                           <!--rendered="{!weaknessAccPlanSWOTs.size>0}"-->
                           <apex:variable var="chk" value="" rendered="{!!ISNULL(accPlanSwot.Id)}">
                           <li class="pull-left window" style="height:175px;margin:3px;margin-bottom:5px;">
                              <apex:outputPanel styleClass="description" layout="block" style="height:48%">
                                 <strong>{!$Label.ACCOUNTPLANNING_PDF_Description}: </strong>
                                 <apex:outputText value="{!accPlanSwot.Description__c}"
                                    id="strengthDesc" />
                              </apex:outputPanel>
                              <apex:outputPanel layout="block" style="height: 22%;font-weight: normal;overflow: hidden;">
                                 <strong> {!$Label.ACCOUNTPLANNING_SWOT_IMPACT}: </strong>
                                 <apex:outputPanel styleClass="value"> 
                                    <apex:outputText value="{!accPlanSwot.Impact__c}"
                                       id="strengthImpact" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                              <apex:outputPanel styleClass="importance" layout="block" style="height:12%">
                                 <strong> {!$Label.ACCOUNTPLANNING_PDF_Importance}: </strong>
                                 <apex:outputPanel styleClass="value">
                                    <apex:outputText value="{!accPlanSwot.Importance__c}/10"
                                       id="strengthImportance" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                           </li>
                           </apex:variable>
                        </apex:repeat>
                     </ul>
                  </apex:outputPanel>
                  <!-- Weaknesses ends here -->
               </apex:outputPanel>
            </td>
         </tr>
      </table>   
      <!-- End of content block -->
     
     <div class="page-break" /> 
         
         
     <table class="table-bordered" style="width:94%;border-spacing: 0 !important;margin-top:10px;">
      <tr class="header"><td colspan="3" style="padding-left:0px !important;"> <div class="title">{!$Label.ACCOUNTPLANNING_PDF_Experian_SWOT_Analysis}</div></td></tr>
         <tr>
            <td width="50%" style="font-size: 13px; font-weight: bold;text-align: left;padding-left : 10px;vertical-align:top;">
                  <!-- Strengths starts here -->
               <apex:outputPanel styleClass="swot oHidden" layout="block">
                  <apex:outputPanel styleClass="pull-left box greenTheme" style="border:0px;"
                     layout="block">
                     <h4>{!$Label.ACCOUNTPLANNING_PDF_Opportunities}</h4>
                     <!-- UL in here -->
                     <ul class="oHidden" id="strengthsId" style="height:auto;padding-bottom:0px;">
                        <apex:repeat value="{!opportunityAccPlanSWOTs}" var="accPlanSwot"
                           rendered="{!opportunityAccPlanSWOTs.size>0}">
                           <apex:variable var="chk" value="" rendered="{!!ISNULL(accPlanSwot.Id)}">
                           <li class="pull-left window" style="height:175px;margin:3px;margin-bottom:5px;">
                              <apex:outputPanel styleClass="description" layout="block" style="height:48%">
                                 <strong>{!$Label.ACCOUNTPLANNING_PDF_Description}: </strong>
                                 <apex:outputText value="{!accPlanSwot.Description__c}"
                                    id="strengthDesc" />
                              </apex:outputPanel>
                              <apex:outputPanel layout="block" style="height: 22%;font-weight: normal;overflow: hidden;">
                                 <strong> {!$Label.ACCOUNTPLANNING_SWOT_IMPACT}: </strong>
                                 <apex:outputPanel styleClass="value"> 
                                    <apex:outputText value="{!accPlanSwot.Impact__c}"
                                       id="strengthImpact" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                              <apex:outputPanel styleClass="importance" layout="block" style="height:12%">
                                 <strong> {!$Label.ACCOUNTPLANNING_PDF_Importance}: </strong>
                                 <apex:outputPanel styleClass="value"> 
                                    <apex:outputText value="{!accPlanSwot.Importance__c}/10"
                                       id="strengthImportance" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                           </li>
                           </apex:variable>
                        </apex:repeat>
                     </ul>
                  </apex:outputPanel>
                  <!-- Strengths end here -->
               </apex:outputPanel>
            </td>
            <td width="50%" style="font-size: 13px; font-weight: bold;text-align: left;vertical-align:top;">
                  <!-- Weaknesses starts here -->
                <apex:outputPanel styleClass="swot oHidden" layout="block">
                  <apex:outputPanel styleClass="pull-left box redTheme" style="border:0px;"
                     layout="block">
                     <h4>{!$Label.ACCOUNTPLANNING_PDF_Threats}</h4>
                     <!-- UL in here -->
                     <ul class="oHidden" id="strengthsId" style="height:auto;padding-bottom:0px;">
                        <apex:repeat value="{!threatAccPlanSWOTs}" var="accPlanSwot"
                           rendered="{!threatAccPlanSWOTs.size>0}">
                           <apex:variable var="chk" value="" rendered="{!!ISNULL(accPlanSwot.Id)}">
                           <li class="pull-left window" style="height:175px;margin:3px;margin-bottom:5px;">
                              <apex:outputPanel styleClass="description" layout="block" style="height:48%">
                                 <strong>{!$Label.ACCOUNTPLANNING_PDF_Description}: </strong>
                                 <apex:outputText value="{!accPlanSwot.Description__c}"
                                    id="strengthDesc" />
                              </apex:outputPanel>
                              <apex:outputPanel layout="block" style="height: 22%;font-weight: normal;overflow: hidden;">
                                 <strong> {!$Label.ACCOUNTPLANNING_SWOT_IMPACT}: </strong>
                                 <apex:outputPanel styleClass="value"> 
                                    <apex:outputText value="{!accPlanSwot.Impact__c}"
                                       id="strengthImpact" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                              <apex:outputPanel styleClass="importance" layout="block" style="height:12%">
                                 <strong> {!$Label.ACCOUNTPLANNING_PDF_Importance}: </strong>
                                 <apex:outputPanel styleClass="value">
                                    <apex:outputText value="{!accPlanSwot.Importance__c}/10"
                                       id="strengthImportance" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                           </li>
                           </apex:variable>
                        </apex:repeat>
                     </ul>
                  </apex:outputPanel>
                  <!-- Weaknesses ends here -->
               </apex:outputPanel>
            </td>
         </tr>
      </table>  
                  
      <!-- End of content block -->
      <div class="page-break" /> 
     
     
       <!-- Client SWOT Analysis on PDF -->
        <table class="table-bordered" style="width:94%;border-spacing: 0 !important;margin-top:10px;">
      <tr class="header"><td colspan="3" style="padding-left:0px !important;"> <div class="title">{!$Label.ACCOUNTPLANNING_PDF_Client_SWOT_Analysis}</div></td></tr>
         <tr>
            <td width="50%" style="font-size: 13px; font-weight: bold;text-align: left;padding-left : 10px;vertical-align:top;">
                  <!-- Strengths starts here -->
               <apex:outputPanel styleClass="swot oHidden" layout="block">
                  <apex:outputPanel styleClass="pull-left box greenTheme" style="border:0px;"
                     layout="block">
                     <h4>{!$Label.ACCOUNTPLANNING_PDF_Strengths}</h4>
                     <!-- UL in here -->
                     <ul class="oHidden" id="strengthsId" style="height:auto;padding-bottom:0px;">
                        <apex:repeat value="{!ClientstrengthAccPlanSWOTs}" var="accPlanSwot"
                           rendered="{!ClientstrengthAccPlanSWOTs.size>0}">
                           <apex:variable var="chk" value="" rendered="{!!ISNULL(accPlanSwot.Id)}">
                           <li class="pull-left window" style="height:175px;margin:3px;margin-bottom:5px;">
                              <apex:outputPanel styleClass="description" layout="block" style="height:48%">
                                 <strong>{!$Label.ACCOUNTPLANNING_PDF_Description}: </strong>
                                 <apex:outputText value="{!accPlanSwot.Description__c}"
                                    id="strengthDesc" />
                              </apex:outputPanel>
                              <apex:outputPanel layout="block" style="height: 22%;font-weight: normal;overflow: hidden;">
                                 <strong> {!$Label.ACCOUNTPLANNING_SWOT_IMPACT}: </strong>
                                 <apex:outputPanel styleClass="value"> 
                                    <apex:outputText value="{!accPlanSwot.Impact__c}"
                                       id="strengthImpact" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                              <apex:outputPanel styleClass="importance" layout="block" style="height:12%">
                                 <strong> {!$Label.ACCOUNTPLANNING_PDF_Importance}: </strong>
                                 <apex:outputPanel styleClass="value"> 
                                    <apex:outputText value="{!accPlanSwot.Importance__c}/10"
                                       id="strengthImportance" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                           </li>
                           </apex:variable>
                        </apex:repeat>
                     </ul>
                  </apex:outputPanel>
                  <!-- Strengths end here -->
               </apex:outputPanel>
            </td>
            <td width="50%" style="font-size: 13px; font-weight: bold;text-align: left;vertical-align:top;">
                  <!-- Weaknesses starts here -->
                <apex:outputPanel styleClass="swot oHidden" layout="block">
                  <apex:outputPanel styleClass="pull-left box redTheme" style="border:0px;"
                     layout="block">
                     <h4>{!$Label.ACCOUNTPLANNING_PDF_Weaknesses}</h4>
                     <!-- UL in here -->
                     <ul class="oHidden" id="strengthsId" style="height:auto;padding-bottom:0px;">
                        <apex:repeat value="{!ClientweaknessAccPlanSWOTs}" var="accPlanSwot"
                           rendered="{!ClientweaknessAccPlanSWOTs.size>0}">
                           <apex:variable var="chk" value="" rendered="{!!ISNULL(accPlanSwot.Id)}">
                           <li class="pull-left window" style="height:175px;margin:3px;margin-bottom:5px;">
                              <apex:outputPanel styleClass="description" layout="block" style="height:48%">
                                 <strong>{!$Label.ACCOUNTPLANNING_PDF_Description}: </strong>
                                 <apex:outputText value="{!accPlanSwot.Description__c}"
                                    id="strengthDesc" />
                              </apex:outputPanel>
                              <apex:outputPanel layout="block" style="height: 22%;font-weight: normal;overflow: hidden;">
                                 <strong> {!$Label.ACCOUNTPLANNING_SWOT_IMPACT}: </strong>
                                 <apex:outputPanel styleClass="value"> 
                                    <apex:outputText value="{!accPlanSwot.Impact__c}"
                                       id="strengthImpact" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                              <apex:outputPanel styleClass="importance" layout="block" style="height:12%">
                                 <strong> {!$Label.ACCOUNTPLANNING_PDF_Importance}: </strong>
                                 <apex:outputPanel styleClass="value">
                                    <apex:outputText value="{!accPlanSwot.Importance__c}/10"
                                       id="strengthImportance" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                           </li>
                           </apex:variable>
                        </apex:repeat>
                     </ul>
                  </apex:outputPanel>
                  <!-- Weaknesses ends here -->
               </apex:outputPanel>
            </td>
         </tr>
      </table>   
      <!-- End of content block -->
     
     <div class="page-break" /> 
         
         
     <table class="table-bordered" style="width:94%;border-spacing: 0 !important;margin-top:10px;">
      <tr class="header"><td colspan="3" style="padding-left:0px !important;"> <div class="title">{!$Label.ACCOUNTPLANNING_PDF_Client_SWOT_Analysis}</div></td></tr>
         <tr>
            <td width="50%" style="font-size: 13px; font-weight: bold;text-align: left;padding-left : 10px;vertical-align:top;">
                  <!-- Strengths starts here -->
               <apex:outputPanel styleClass="swot oHidden" layout="block">
                  <apex:outputPanel styleClass="pull-left box greenTheme" style="border:0px;"
                     layout="block">
                     <h4>{!$Label.ACCOUNTPLANNING_PDF_Opportunities}</h4>
                     <!-- UL in here -->
                     <ul class="oHidden" id="strengthsId" style="height:auto;padding-bottom:0px;">
                        <apex:repeat value="{!ClientopportunityAccPlanSWOTs}" var="accPlanSwot"
                           rendered="{!ClientopportunityAccPlanSWOTs.size>0}">
                           <apex:variable var="chk" value="" rendered="{!!ISNULL(accPlanSwot.Id)}">
                           <li class="pull-left window" style="height:175px;margin:3px;margin-bottom:5px;">
                              <apex:outputPanel styleClass="description" layout="block" style="height:48%">
                                 <strong>{!$Label.ACCOUNTPLANNING_PDF_Description}: </strong>
                                 <apex:outputText value="{!accPlanSwot.Description__c}"
                                    id="strengthDesc" />
                              </apex:outputPanel>
                              <apex:outputPanel layout="block" style="height: 22%;font-weight: normal;overflow: hidden;">
                                 <strong> {!$Label.ACCOUNTPLANNING_SWOT_IMPACT}: </strong>
                                 <apex:outputPanel styleClass="value"> 
                                    <apex:outputText value="{!accPlanSwot.Impact__c}"
                                       id="strengthImpact" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                              <apex:outputPanel styleClass="importance" layout="block" style="height:12%">
                                 <strong> {!$Label.ACCOUNTPLANNING_PDF_Importance}: </strong>
                                 <apex:outputPanel styleClass="value"> 
                                    <apex:outputText value="{!accPlanSwot.Importance__c}/10"
                                       id="strengthImportance" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                           </li>
                           </apex:variable>
                        </apex:repeat>
                     </ul>
                  </apex:outputPanel>
                  <!-- Strengths end here -->
               </apex:outputPanel>
            </td>
            <td width="50%" style="font-size: 13px; font-weight: bold;text-align: left;vertical-align:top;">
                  <!-- Weaknesses starts here -->
                <apex:outputPanel styleClass="swot oHidden" layout="block">
                  <apex:outputPanel styleClass="pull-left box redTheme" style="border:0px;"
                     layout="block">
                     <h4>{!$Label.ACCOUNTPLANNING_PDF_Threats}</h4>
                     <!-- UL in here -->
                     <ul class="oHidden" id="strengthsId" style="height:auto;padding-bottom:0px;">
                        <apex:repeat value="{!ClientthreatAccPlanSWOTs}" var="accPlanSwot"
                           rendered="{!ClientthreatAccPlanSWOTs.size>0}">
                           <apex:variable var="chk" value="" rendered="{!!ISNULL(accPlanSwot.Id)}">
                           <li class="pull-left window" style="height:175px;margin:3px;margin-bottom:5px;">
                              <apex:outputPanel styleClass="description" layout="block" style="height:48%">
                                 <strong>{!$Label.ACCOUNTPLANNING_PDF_Description}: </strong>
                                 <apex:outputText value="{!accPlanSwot.Description__c}"
                                    id="strengthDesc" />
                              </apex:outputPanel>
                              <apex:outputPanel layout="block" style="height: 22%;font-weight: normal;overflow: hidden;">
                                 <strong> {!$Label.ACCOUNTPLANNING_SWOT_IMPACT}: </strong>
                                 <apex:outputPanel styleClass="value"> 
                                    <apex:outputText value="{!accPlanSwot.Impact__c}"
                                       id="strengthImpact" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                              <apex:outputPanel styleClass="importance" layout="block" style="height:12%">
                                 <strong> {!$Label.ACCOUNTPLANNING_PDF_Importance}: </strong>
                                 <apex:outputPanel styleClass="value">
                                    <apex:outputText value="{!accPlanSwot.Importance__c}/10"
                                       id="strengthImportance" />
                                 </apex:outputPanel>
                              </apex:outputPanel>
                           </li>
                           </apex:variable>
                        </apex:repeat>
                     </ul>
                  </apex:outputPanel>
                  <!-- Weaknesses ends here -->
               </apex:outputPanel>
            </td>
         </tr>
      </table>
   
      <br/><br/>

      <div class="page-break" />

</apex:component>